package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="InvoiceCardType")
public class InvoiceCardType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PKey")
	private Long PKey;
	
	@Column(name="InvoiceId")
	private String InvoiceId;
	
	@Column(name="DeviceId")
	private String DeviceId;
	
	@Column(name="Type")
	private String Type;
	
	@Column(name="[Percent]") 
	private BigDecimal Percent;
	
	@Column(name="IsUPoint")
	private Short IsUPoint;
	
	@Column(name="PaymentAmount")
	private BigDecimal PaymentAmount;
	
	@Column(name="PaymentBonus")
	private BigDecimal PaymentBonus;
	
	@Column(name="BalanceBonus")
	private BigDecimal BalanceBonus;
	
	@Column(name="PaymentUPoint")
	private BigDecimal PaymentUPoint;
	
	@Column(name="BalanceUPoint")
	private BigDecimal BalanceUPoint;

	public Long getPkey() {
		return PKey;
	}

	public void setPkey(Long PKey) {
		this.PKey = PKey;
	}

	public String getInvoiceId() {
		return InvoiceId;
	}

	public void setInvoiceId(String InvoiceId) {
		this.InvoiceId = InvoiceId;
	}

	public String getDeviceId() {
		return DeviceId;
	}

	public void setDeviceId(String DeviceId) {
		this.DeviceId = DeviceId;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public BigDecimal getPercent() {
		return Percent;
	}

	public void setPercent(BigDecimal percent) {
		Percent = percent;
	}

	public Short getIsUPoint() {
		return IsUPoint;
	}

	public void setIsUPoint(Short isUPoint) {
		IsUPoint = isUPoint;
	}

	public BigDecimal getPaymentAmount() {
		return PaymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		PaymentAmount = paymentAmount;
	}

	public BigDecimal getPaymentBonus() {
		return PaymentBonus;
	}

	public void setPaymentBonus(BigDecimal paymentBonus) {
		PaymentBonus = paymentBonus;
	}

	public BigDecimal getBalanceBonus() {
		return BalanceBonus;
	}

	public void setBalanceBonus(BigDecimal balanceBonus) {
		BalanceBonus = balanceBonus;
	}

	public BigDecimal getPaymentUPoint() {
		return PaymentUPoint;
	}

	public void setPaymentUPoint(BigDecimal paymentUPoint) {
		PaymentUPoint = paymentUPoint;
	}

	public BigDecimal getBalanceUPoint() {
		return BalanceUPoint;
	}

	public void setBalanceUPoint(BigDecimal balanceUPoint) {
		BalanceUPoint = balanceUPoint;
	}
	
	
}
