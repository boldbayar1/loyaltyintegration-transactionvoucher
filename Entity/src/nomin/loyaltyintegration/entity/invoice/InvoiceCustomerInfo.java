package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InvoiceCustomerInfo")
public class InvoiceCustomerInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Pkey")
	private Long pkey;

	@Column(name = "InvoiceId")
	private String invoiceId;
	
	@Column(name = "DeviceId")
	private String deviceId;
	
	@Column(name = "ReqeustId")
	private String reqeustId;
	
	@Column(name = "Type")
	private String type;
	
	@Column(name = "CardNumber")
	private String cardNumber;
	
	@Column(name = "CardHolderId")
	private String cardHolderId;
	
	@Column(name = "CardUAId")
	private String cardUAId;
	
	@Column(name = "BillDate")
	private String billDate;
	
	@Column(name = "Email")
	private String email;

	@Column(name = "PhoneNumber")
	private String phoneNumber;
	
	@Column(name = "EbarimtBillType")
	private String ebarimtBillType;
	
	@Column(name = "CustomerNo")
	private String customerNo;
	
	@Column(name = "CreatedDate")
	private String createdDate;

	public Long getPkey() {
		return pkey;
	}

	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getReqeustId() {
		return reqeustId;
	}

	public void setReqeustId(String reqeustId) {
		this.reqeustId = reqeustId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getCardUAId() {
		return cardUAId;
	}

	public void setCardUAId(String cardUAId) {
		this.cardUAId = cardUAId;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEbarimtBillType() {
		return ebarimtBillType;
	}

	public void setEbarimtBillType(String ebarimtBillType) {
		this.ebarimtBillType = ebarimtBillType;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}
