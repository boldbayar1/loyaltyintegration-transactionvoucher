package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="InvoiceRequest")
public class InvoiceRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PKey")
	private Long pkey;
	
	@Column(name="InvoiceId")
	private String invoiceId;
	
	@Column(name="DeviceId")
	private String deviceId;
	
	@Column(name="RequestId")
	private String requestId;
	
	@Column(name="BillNo")
	private Integer billNo;
	
	@Column(name="CardType")
	private String cardType;
	
	@Column(name="CardNumber")
	private String cardNumber;
	
	@Column(name="CardHolderId")
	private Long cardHolderId;
	
	@Column(name="CardUAId")
	private String cardUAId;
	
	@Column(name="PinType")
	private String pinType;
	
	@Temporal(TemporalType.DATE)
	@Column(name="BillDate")
	private Date billDate;
	
	@Column(name="TotalAmount")
	private BigDecimal totalAmount;
	
	@Column(name="Voucher")
	private BigDecimal voucher;
	
	@Column(name="CreatedKey")
	private int createdKey;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="itemBonusBalance")
	private BigDecimal itemBonusBalance;
	
	@Column(name="itemBonusX2Info")
	private String itemBonusX2Info;

	public Long getPkey() {
		return pkey;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public String getRequestId() {
		return requestId;
	}

	public Integer getBillNo() {
		return billNo;
	}

	public String getCardType() {
		return cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public Long getCardHolderId() {
		return cardHolderId;
	}

	public String getCardUAId() {
		return cardUAId;
	}

	public String getPinType() {
		return pinType;
	}

	public Date getBillDate() {
		return billDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public BigDecimal getVoucher() {
		return voucher;
	}

	public int getCreatedKey() {
		return createdKey;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public BigDecimal getItemBonusBalance() {
		return itemBonusBalance;
	}

	public String getItemBonusX2Info() {
		return itemBonusX2Info;
	}

	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setCardHolderId(Long cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public void setCardUAId(String cardUAId) {
		this.cardUAId = cardUAId;
	}

	public void setPinType(String pinType) {
		this.pinType = pinType;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setVoucher(BigDecimal voucher) {
		this.voucher = voucher;
	}

	public void setCreatedKey(int createdKey) {
		this.createdKey = createdKey;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setItemBonusBalance(BigDecimal itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}

	public void setItemBonusX2Info(String itemBonusX2Info) {
		this.itemBonusX2Info = itemBonusX2Info;
	}
}
