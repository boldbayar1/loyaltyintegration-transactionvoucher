package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InvoiceEbarimt")
public class InvoiceEbarimt implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "STATUS")
	private Long status;

	@Column(name = "Data")
	private String data;

	@Column(name = "ResultValue")
	private String resultValue;

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getResultValue() {
		return resultValue;
	}

	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}
}
