package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="InvoiceRequestDtl")
public class InvoiceRequestDtl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PKey")
	private Long pkey;
	
	@Column(name="InvoiceId")
	private String invoiceId;
	
	@Column(name="DeviceId")
	private String deviceId;
	
	@Column(name="BillDate")
	private Date billDate;
	
	@Column(name="ItemKey")
	private Long itemKey;
	
	@Column(name="ItemId")
	private String itemId;
	
	@Column(name="Qty")
	private BigDecimal qty; 
	
	@Column(name="IsWhole")
	private int isWhole;
	
	@Column(name="BasePrice")
	private BigDecimal basePrice;
	
	@Column(name="Price")
	private BigDecimal price;
	
	@Column(name="Amount")
	private BigDecimal amount;
	
	@Column(name="CreatedKey")
	private int createdKey;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	public Long getPKey() { return pkey; }
	public void setPKey(Long pkey) { this.pkey = pkey; }
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }
	
	public String getDeviceId() { return deviceId; }
	public void setDeviceId(String deviceId) { this.deviceId = deviceId; }
	
	public Date getBillDate() { return billDate; }
	public void setBillDate(Date billDate) { this.billDate = billDate; }
	
	public Long getItemKey() { return itemKey; }
	public void setItemKey(Long itemKey) { this.itemKey = itemKey; }
	
	public String getItemId() { return itemId; }
	public void setItemId(String itemId) { this.itemId = itemId; }
	
	public BigDecimal getQty() { return qty; }
	public void setQty(BigDecimal qty) { this.qty = qty; }
	
	public int getIsWhole() { return isWhole; }
	public void setIsWhole(int isWhole) { this.isWhole = isWhole; }
	
	public BigDecimal getBasePrice() { return basePrice; }
	public void setBasePrice(BigDecimal basePrice) { this.basePrice = basePrice; }
	
	public BigDecimal getPrice() { return price; }
	public void setPrice(BigDecimal price) { this.price = price; }
	
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	public int getCreatedKey() { return createdKey; }
	public void setCreatedKey(int createdKey) { this.createdKey = createdKey; }
	
	public Date getCreatedDate() { return createdDate; }
	public void setCreatedDate(Date createdDate) { this.createdDate = createdDate; }
}
