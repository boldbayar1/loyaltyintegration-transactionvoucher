package nomin.loyaltyintegration.entity.invoice;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "InvoiceBill")
public class InvoiceBill implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BillKey")
	private Long billKey;

	@Column(name = "InvoiceId")
	private String invoiceId;

	@Column(name = "DeviceId")
	private String deviceId;
	
	@Column(name = "CreatedDate")
	private String createdDate;
	
	@Column(name = "Status")
	private String status;
	
	@Column(name = "LocationKey")
	private Long locationKey;
	
	@Column(name = "CardType")
	private String cardType;
	
	@Transient
	private String configStatus;

	public InvoiceBill() {
		super();
	}

	public InvoiceBill(Long billKey, String invoiceId, String deviceId, String createdDate, String configStatus, String status) {
		super();
		this.billKey = billKey;
		this.invoiceId = invoiceId;
		this.deviceId = deviceId;
		this.createdDate = createdDate;
		this.configStatus = configStatus;
		this.status = status;
	}

	public Long getBillKey() {
		return billKey;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
}
