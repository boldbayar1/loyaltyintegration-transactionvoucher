package nomin.loyaltyintegration.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GeneralConfigs")
public class GeneralConfigs implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PKey")
	private Long pKey;

	@Column(name = "Application")
	private String application;

	@Column(name = "Type")
	private String type;

	@Column(name = "Name")
	private String name;

	@Column(name = "Url")
	private String url;
	
	@Column(name = "JsonValue")
	private String jsonValue;
	
	@Column(name = "Result")
	private String result;
	
	@Column(name = "ImageUrl")
	private String imageUrl;

	@Column(name = "Token")
	private String token;
	
	@Column(name = "terminal")
	private String terminal;
	
	@Column(name = "Key")
	private String key;
	
	@Column(name = "Auth")
	private String auth;
	
	@Column(name = "BasicAuthU")
	private String basicAuthU;
	
	@Column(name = "BasicAuthP")
	private String basicAuthP;

	public GeneralConfigs() {
		super();
	}

	public GeneralConfigs(Long pKey, String application, String type, String name, String url, String jsonValue, String result, String imageUrl, String token, String terminal, String key, String auth, String basicAuthU, String basicAuthP) {
		super();
		this.pKey = pKey;
		this.application = application;
		this.type = type;
		this.name = name;
		this.url = url;
		this.jsonValue = jsonValue;
		this.result = result;
		this.imageUrl = imageUrl;
		this.token = token;
		this.terminal = terminal;
		this.key = key;
		this.auth = auth;
		this.basicAuthU = basicAuthU;
		this.basicAuthP = basicAuthP;
	}

	public Long getpKey() {
		return pKey;
	}

	public String getApplication() {
		return application;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public String getJsonValue() {
		return jsonValue;
	}

	public String getResult() {
		return result;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public String getToken() {
		return token;
	}

	public String getTerminal() {
		return terminal;
	}

	public String getKey() {
		return key;
	}

	public String getAuth() {
		return auth;
	}

	public String getBasicAuthU() {
		return basicAuthU;
	}

	public String getBasicAuthP() {
		return basicAuthP;
	}

	public void setpKey(Long pKey) {
		this.pKey = pKey;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setJsonValue(String jsonValue) {
		this.jsonValue = jsonValue;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public void setBasicAuthU(String basicAuthU) {
		this.basicAuthU = basicAuthU;
	}

	public void setBasicAuthP(String basicAuthP) {
		this.basicAuthP = basicAuthP;
	}
}