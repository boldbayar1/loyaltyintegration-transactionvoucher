package nomin.loyaltyintegration.entity.finger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.core.entity.businessentity.rest.BaseResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FingerResponse extends BaseResponse {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("data")
	private String data;
	
	public String getData() {
		
		return data;
	}
	
	public void setData(String data) {
		this.data = data; 
	}
}
