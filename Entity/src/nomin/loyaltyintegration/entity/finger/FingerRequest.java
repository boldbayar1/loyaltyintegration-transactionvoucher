package nomin.loyaltyintegration.entity.finger;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FingerRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("fingerData")
	private String fingerData;
	
	@JsonProperty("storedData")
	private String storedData;
	
	
	public FingerRequest() {
		super();
	}

	public FingerRequest(String fingerData, String storedData) {
		super();
		this.fingerData = fingerData;
		this.storedData = storedData;
	}

	public String getFingerData() {
		return fingerData;
	}

	public void setFingerData(String fingerData) {
		this.fingerData = fingerData;
	}

	public String getStoredData() {
		return storedData;
	}

	public void setStoredData(String storedData) {
		this.storedData = storedData;
	}


}
