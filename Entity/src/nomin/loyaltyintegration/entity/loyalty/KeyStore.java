package nomin.loyaltyintegration.entity.loyalty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="KeyStore")
public class KeyStore implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="OrganizationKey")
	private Long organizationKey;
	
	@Column(name="[Key]")
	private String key;
	
	@Column(name="DeviceId")
	private String deviceId;
	
	public Long getOrganizationKey() { return organizationKey; }
	public void setOrganizationKey(Long organizationKey) { this.organizationKey = organizationKey; }

	public String getKey() { return key; }
	public void setKey(String key) { this.key = key; }
	
	public String getDeviceId() { return deviceId; }
	public void setDeviceId(String deviceId) { this.deviceId = deviceId; }
}
