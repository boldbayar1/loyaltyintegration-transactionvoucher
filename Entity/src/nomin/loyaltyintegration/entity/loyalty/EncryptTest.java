package nomin.loyaltyintegration.entity.loyalty;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EncryptTest implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("txt")
	private String txt;
	
	@JsonProperty("key")
	private String key;
	
	public String getTxt() { return txt; }
	public void setTxt(String txt) { this.txt = txt; }
	
	public String getKey() { return key; }
	public void setKey(String key) { this.key = key; }
}
