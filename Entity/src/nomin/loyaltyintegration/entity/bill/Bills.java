package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.entity.payment.VoucherPaymentResponse;

@Entity
@Table(name = "BILLS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bills implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_POS")
	private Long idPos;
	
	@Column(name = "ID_LOCATION")
	private Long idLocation;
	
	@JsonProperty("billKey")
	@Column(name = "ID_BILL")
	private Long idBill;
	
	@JsonProperty("billNo")
	@Column(name = "INVREF")
	private Integer invref;
	
	@Column(name = "ID_CASHIER")
	private Long idCashier;
	
	@Column(name = "ID_CARD")
	private Long idCard;
	
	@Column(name = "CARD_PERCENT")
	private BigDecimal cardPercent;
	
	@Column(name = "IS_POSTED")
	private char isPosted;
	
	@Column(name = "PAYMENT_RECEIVABLE")
	private BigDecimal paymentReceivable;
	
	@Column(name = "PAYMENT_BONUS")
	private BigDecimal paymentBonus;
	
	@JsonProperty("voucherAmountL2")
	@Column(name = "PAYMENT_VOUCHER")
	private BigDecimal paymentVoucher;
	
	@Column(name = "PAYMENT_CASH")
	private BigDecimal paymentCash;
	
	@Column(name = "PAYMENT_CASHGET")
	private BigDecimal paymentCashGet;
	
	@Column(name = "ID_CUSTOMER")
	private Long idCustomer;
	
	@Column(name = "PAYMENT_ITEMBONUS")
	private BigDecimal paymentItemBonus;
	
	@Column(name = "BILL_DATE")
	private Date billDate;
	
	@Column(name = "CARDBONUSADD_AMOUNT")
	private BigDecimal cardBonusAddAmount;
	
	@Column(name = "ITEMBONUSADD_AMOUNT")
	private BigDecimal itemBonusAddAmount;
	
	@JsonProperty("totalAmount")
	@Column(name = "PAYMENT_AMOUNT")
	private BigDecimal paymentAmount;
	
	@Column(name = "CASHIERCODE")
	private String cashierCode;
	
	@Column(name = "PAYMENT_BANKCARD")
	private BigDecimal paymentBankCard;
	
	@Column(name = "PAYMENT_CITYTAX")
	private BigDecimal paymentCityTax;
	
	@JsonProperty("vatBillNo")
	@Column(name = "VAT_BILLNO")
	private String vatBillNo;
	
	@JsonProperty("customerNo")
	@Column(name = "VAT_CUSTOMER_TTD")
	private String vatCustomerTTD;
	
	@Column(name = "BILL_CREATEDTIME")
	private Date billCreatedTime;
	
	@Column(name = "ACCOUNT_INFO")
	private String accountInfo;
	
	@Column(name = "CUSTOMER_PRICING_INFO")
	private String customerPricingInfo;
	
	@Column(name = "SEPAYMENT_INFO")
	private String sepaymentInfo;
	
	@Column(name = "PAYMENT_UPOINT")
	private BigDecimal paymentUpoint;
	
	@Column(name = "UPOINTADD_AMOUNT")
	private BigDecimal upointAddAmount;
	
	@Column(name = "UPOINTADD_MANUFACTURERAMOUNT")
	private BigDecimal upointAddManufactureAmount;
	
	@Column(name = "UPOINTADD_ITEMAMOUNT")
	private BigDecimal upointItemAmount;
	
	@Column(name = "UPOINT_BALANCE")
	private BigDecimal upointBalance;
	
	@Column(name = "ID_UACARD")
	private Long idUACard;
	
	@JsonProperty("sales")
	@Transient
	private List<Sales> sales;
	
	@JsonProperty("voucherPayment")
	@Transient
	List<VoucherPaymentResponse> lstvoucherPayment;
	
	@JsonProperty("ebarimtBillType")
	@Transient
	private Integer ebarimtBillType;
	
	@Transient
	@JsonProperty("vatAmount")
	private BigDecimal vatAmount;

	@Transient
	private String configStatus;

	public Bills() {
		super();
	}

	public Bills(Long idPos, Long idLocation, Long idBill, Integer invref, Long idCashier, Long idCard, BigDecimal cardPercent, char isPosted, BigDecimal paymentReceivable, BigDecimal paymentBonus,
			BigDecimal paymentVoucher, BigDecimal paymentCash, BigDecimal paymentCashGet, Long idCustomer, BigDecimal paymentItemBonus, Date billDate, BigDecimal cardBonusAddAmount,
			BigDecimal itemBonusAddAmount, BigDecimal paymentAmount, String cashierCode, BigDecimal paymentBankCard, BigDecimal paymentCityTax, String vatBillNo, String vatCustomerTTD,
			Date billCreatedTime, String accountInfo, String customerPricingInfo, String sepaymentInfo, BigDecimal paymentUpoint, BigDecimal upointAddAmount, BigDecimal upointAddManufactureAmount,
			BigDecimal upointItemAmount, BigDecimal upointBalance, Long idUACard, List<Sales> sales, Integer ebarimtBillType, String configStatus, List<VoucherPaymentResponse> lstvoucherPayment) {
		super();
		this.idPos = idPos;
		this.idLocation = idLocation;
		this.idBill = idBill;
		this.invref = invref;
		this.idCashier = idCashier;
		this.idCard = idCard;
		this.cardPercent = cardPercent;
		this.isPosted = isPosted;
		this.paymentReceivable = paymentReceivable;
		this.paymentBonus = paymentBonus;
		this.paymentVoucher = paymentVoucher;
		this.paymentCash = paymentCash;
		this.paymentCashGet = paymentCashGet;
		this.idCustomer = idCustomer;
		this.paymentItemBonus = paymentItemBonus;
		this.billDate = billDate;
		this.cardBonusAddAmount = cardBonusAddAmount;
		this.itemBonusAddAmount = itemBonusAddAmount;
		this.paymentAmount = paymentAmount;
		this.cashierCode = cashierCode;
		this.paymentBankCard = paymentBankCard;
		this.paymentCityTax = paymentCityTax;
		this.vatBillNo = vatBillNo;
		this.vatCustomerTTD = vatCustomerTTD;
		this.billCreatedTime = billCreatedTime;
		this.accountInfo = accountInfo;
		this.customerPricingInfo = customerPricingInfo;
		this.sepaymentInfo = sepaymentInfo;
		this.paymentUpoint = paymentUpoint;
		this.upointAddAmount = upointAddAmount;
		this.upointAddManufactureAmount = upointAddManufactureAmount;
		this.upointItemAmount = upointItemAmount;
		this.upointBalance = upointBalance;
		this.idUACard = idUACard;
		this.sales = sales;
		this.ebarimtBillType = ebarimtBillType;
		this.configStatus = configStatus;
		this.lstvoucherPayment = lstvoucherPayment;
	}

	public Long getIdPos() {
		return idPos;
	}

	public void setIdPos(Long idPos) {
		this.idPos = idPos;
	}

	public Long getIdLocation() {
		return idLocation;
	}

	public void setIdLocation(Long idLocation) {
		this.idLocation = idLocation;
	}

	public Long getIdBill() {
		return idBill;
	}

	public void setIdBill(Long idBill) {
		this.idBill = idBill;
	}

	public Integer getInvref() {
		return invref;
	}

	public void setInvref(Integer invref) {
		this.invref = invref;
	}

	public Long getIdCashier() {
		return idCashier;
	}

	public void setIdCashier(Long idCashier) {
		this.idCashier = idCashier;
	}

	public Long getIdCard() {
		return idCard;
	}

	public void setIdCard(Long idCard) {
		this.idCard = idCard;
	}

	public BigDecimal getCardPercent() {
		return cardPercent;
	}

	public void setCardPercent(BigDecimal cardPercent) {
		this.cardPercent = cardPercent;
	}

	public char getIsPosted() {
		return isPosted;
	}

	public void setIsPosted(char isPosted) {
		this.isPosted = isPosted;
	}

	public BigDecimal getPaymentReceivable() {
		return paymentReceivable;
	}

	public void setPaymentReceivable(BigDecimal paymentReceivable) {
		this.paymentReceivable = paymentReceivable;
	}

	public BigDecimal getPaymentBonus() {
		return paymentBonus;
	}

	public void setPaymentBonus(BigDecimal paymentBonus) {
		this.paymentBonus = paymentBonus;
	}

	public BigDecimal getPaymentVoucher() {
		return paymentVoucher;
	}

	public void setPaymentVoucher(BigDecimal paymentVoucher) {
		this.paymentVoucher = paymentVoucher;
	}

	public BigDecimal getPaymentCash() {
		return paymentCash;
	}

	public void setPaymentCash(BigDecimal paymentCash) {
		this.paymentCash = paymentCash;
	}

	public BigDecimal getPaymentCashGet() {
		return paymentCashGet;
	}

	public void setPaymentCashGet(BigDecimal paymentCashGet) {
		this.paymentCashGet = paymentCashGet;
	}

	public Long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Long idCustomer) {
		this.idCustomer = idCustomer;
	}

	public BigDecimal getPaymentItemBonus() {
		return paymentItemBonus;
	}

	public void setPaymentItemBonus(BigDecimal paymentItemBonus) {
		this.paymentItemBonus = paymentItemBonus;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public BigDecimal getCardBonusAddAmount() {
		return cardBonusAddAmount;
	}

	public void setCardBonusAddAmount(BigDecimal cardBonusAddAmount) {
		this.cardBonusAddAmount = cardBonusAddAmount;
	}

	public BigDecimal getItemBonusAddAmount() {
		return itemBonusAddAmount;
	}

	public void setItemBonusAddAmount(BigDecimal itemBonusAddAmount) {
		this.itemBonusAddAmount = itemBonusAddAmount;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public BigDecimal getPaymentBankCard() {
		return paymentBankCard;
	}

	public void setPaymentBankCard(BigDecimal paymentBankCard) {
		this.paymentBankCard = paymentBankCard;
	}

	public BigDecimal getPaymentCityTax() {
		return paymentCityTax;
	}

	public void setPaymentCityTax(BigDecimal paymentCityTax) {
		this.paymentCityTax = paymentCityTax;
	}

	public String getVatBillNo() {
		return vatBillNo;
	}

	public void setVatBillNo(String vatBillNo) {
		this.vatBillNo = vatBillNo;
	}

	public String getVatCustomerTTD() {
		return vatCustomerTTD;
	}

	public void setVatCustomerTTD(String vatCustomerTTD) {
		this.vatCustomerTTD = vatCustomerTTD;
	}

	public Date getBillCreatedTime() {
		return billCreatedTime;
	}

	public void setBillCreatedTime(Date billCreatedTime) {
		this.billCreatedTime = billCreatedTime;
	}

	public String getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}

	public String getCustomerPricingInfo() {
		return customerPricingInfo;
	}

	public void setCustomerPricingInfo(String customerPricingInfo) {
		this.customerPricingInfo = customerPricingInfo;
	}

	public String getSepaymentInfo() {
		return sepaymentInfo;
	}

	public void setSepaymentInfo(String sepaymentInfo) {
		this.sepaymentInfo = sepaymentInfo;
	}

	public BigDecimal getPaymentUpoint() {
		return paymentUpoint;
	}

	public void setPaymentUpoint(BigDecimal paymentUpoint) {
		this.paymentUpoint = paymentUpoint;
	}

	public BigDecimal getUpointAddAmount() {
		return upointAddAmount;
	}

	public void setUpointAddAmount(BigDecimal upointAddAmount) {
		this.upointAddAmount = upointAddAmount;
	}

	public BigDecimal getUpointAddManufactureAmount() {
		return upointAddManufactureAmount;
	}

	public void setUpointAddManufactureAmount(BigDecimal upointAddManufactureAmount) {
		this.upointAddManufactureAmount = upointAddManufactureAmount;
	}

	public BigDecimal getUpointItemAmount() {
		return upointItemAmount;
	}

	public void setUpointItemAmount(BigDecimal upointItemAmount) {
		this.upointItemAmount = upointItemAmount;
	}

	public BigDecimal getUpointBalance() {
		return upointBalance;
	}

	public void setUpointBalance(BigDecimal upointBalance) {
		this.upointBalance = upointBalance;
	}

	public Long getIdUACard() {
		return idUACard;
	}

	public void setIdUACard(Long idUACard) {
		this.idUACard = idUACard;
	}

	public List<Sales> getSales() {
		return sales;
	}

	public void setSales(List<Sales> sales) {
		this.sales = sales;
	}

	public Integer getEbarimtBillType() {
		return ebarimtBillType;
	}

	public void setEbarimtBillType(Integer ebarimtBillType) {
		this.ebarimtBillType = ebarimtBillType;
	}

	public String getConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public List<VoucherPaymentResponse> getLstvoucherPayment() {
		return lstvoucherPayment;
	}

	public void setLstvoucherPayment(List<VoucherPaymentResponse> lstvoucherPayment) {
		this.lstvoucherPayment = lstvoucherPayment;
	}
}
