package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillDetailRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonbProperty("itemId")
	private String itemId;
	
	@JsonbProperty("quantity")
	private BigDecimal quantity;
	
	@JsonbProperty("isWhole")
	private int isWhole;
	
	@JsonProperty("cityTax")
	private int cityTax;
	
	@JsonProperty("hasNuat") // hasNuat -> hasVat 
	private int hasNuat;
	
	@JsonProperty("hasVat") // hasNuat -> hasVat 
	private int hasVat;
	
	@JsonProperty("basePrice")
	private BigDecimal basePrice;
	
	@JsonProperty("price")
	private BigDecimal price;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("amoutNuat") // amoutNuat -> amountVat 
	private BigDecimal amountNuat;
	
	@JsonProperty("amoutVat") // amoutNuat -> amountVat 
	private BigDecimal amountVat;
	
	@JsonProperty("sectionKey") 
	private BigDecimal sectionKey;
	
	@JsonProperty("customerKey") 
	private BigDecimal customerKey;
	
	//Default version of clone() method.
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
	public String getItemId() { return itemId; }
	public void setItemId(String itemId) {this.itemId = itemId;}
	public void setPKey(String itemId) { this.itemId = itemId; }
	
	public BigDecimal getQuantity() { return quantity; }
	public void setQuantity(BigDecimal quantity) { this.quantity = quantity; }
	
	public int getIsWhole() { return isWhole; }
	public void setIsWhole(int isWhole) { this.isWhole = isWhole; }
	
	public int getCityTax() { return cityTax; }
	public void setCityTax(int cityTax) { this.cityTax = cityTax; }
	
	public BigDecimal getBasePrice() { return basePrice; }
	public void setBasePrice(BigDecimal basePrice) { this.basePrice = basePrice; }
	
	public BigDecimal getPrice() { return price; }
	public void setPrice(BigDecimal price) { this.price = price; }
	
	public int getHasNuat() { return hasNuat; }
	public void setHasNuat(int hasNuat) { this.hasNuat = hasNuat; }
	
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	public BigDecimal getAmountNuat() { return amountNuat; }
	public void setAmountNuat(BigDecimal amountNuat) { this.amountNuat = amountNuat; }
	
	public int getHasVat() { return hasVat; }
	public void setHasVat(int hasVat) {	this.hasVat = hasVat; }
	
	public BigDecimal getAmountVat() { return amountVat; }
	public void setAmountVat(BigDecimal amountVat) { this.amountVat = amountVat; }
	
	public BigDecimal getSectionKey() {	return sectionKey; }
	public void setSectionKey(BigDecimal sectionKey) { this.sectionKey = sectionKey; }
	
	public BigDecimal getCustomerKey() { return customerKey; }
	public void setCustomerKey(BigDecimal customerKey) { this.customerKey = customerKey; }
}
