package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnBillResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
			
	@JsonProperty("success")
	private boolean success;

	@JsonProperty("errorCode")
	private Integer errorCode;
	
	@JsonProperty("message")
	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
