package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "SALES")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sales implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_SALE")
	private Long idSale;
	
	@Column(name = "ID_BILL")
	private Long idBill;
	
	@JsonProperty("itemId")
	@Transient
	private String itemId;
	
	@Column(name = "ID_ITEM")
	private Long idItem;
	
	@JsonProperty("sectionKey")
	@Column(name = "ID_SECTION")
	private Long idSection;
	
	@JsonProperty("quantity")
	@Column(name = "QUANTITY")
	private BigDecimal quantity;
	
	@JsonProperty("basePrice")
	@Column(name = "BASE_PRICE")
	private BigDecimal basePrice;
	
	@JsonProperty("price")
	@Column(name = "PRICE")
	private BigDecimal price;
	
	@JsonProperty("amount")
	@Column(name = "AMOUNT")
	private BigDecimal amount;
	
	@Column(name = "BONUS_PERCENT")
	private BigDecimal bonusPercent;
	
	@Column(name = "BONUS_AMOUNT")
	private BigDecimal bonusAmount;
	
	@Column(name = "ITEMBONUS_PERCENT")
	private BigDecimal itemBonusPercent;
	
	@Column(name = "ITEMBONUS_AMOUNT")
	private BigDecimal itemBonusAmount;
	
	@Column(name = "GIFTBONUS_AMOUNT")
	private Long giftBonusAmount;
	
	@Column(name = "DELETED_ID_APPROVER")
	private Long deletedIdApprover;
	
	@Column(name = "DELETED_ID_REASON")
	private Long deletedIdReason;

	@Column(name = "ID_BARCODE")
	private Long idBarcode;
	
	@Column(name = "ID_SELLER")
	private Long idSeller;
	
	@Column(name = "STATUS")
	private char status;
	
	@Column(name = "USECARDBONUS")
	private char useCardBonus;
	
	@Column(name = "BONUSPAYMENT_AMOUNT")
	private BigDecimal bonusPaymentAmount;
	
	@Column(name = "ITEMBONUSPAYMENT_AMOUNT")
	private BigDecimal itemBonusPaymentAmount;
	
	@Column(name = "CITYTAX_AMOUNT")
	private BigDecimal cityTaxAmount;
	
	@JsonProperty("vatBillNo")
	@Column(name = "VAT_BILLNO")
	private String vatBillNo;
	
	@JsonProperty("vatAmount")
	@Column(name = "VAT_AMOUNT")
	private BigDecimal vatAmount;
	
	@Column(name = "MOBILE_CHARGES")
	private String mobileCharges;
	
	@Column(name = "SALE_VOUCHERS")
	private String saleVouchers;
	
	@Column(name = "UPOINTPAYMENT_AMOUNT")
	private BigDecimal upointPaymentAmount;
	
	@Column(name = "UPOINT_AMOUNT")
	private BigDecimal upointAmount;
	
	@Column(name = "UPOINT_MANUFACTURERAMOUNT")
	private BigDecimal upointManufacturerAmount;
	
	@Column(name = "UPOINT_ITEMAMOUNT")
	private BigDecimal upointItemAmount;
	
	@Column(name = "UseBonus")
	private Integer useBonus;
	
	@Column(name = "HasBonus")
	private Integer hasBonus;
	
	@JsonProperty("isWhole")
	@Column(name = "IsWhole")
	private Integer isWhole;
	
	@JsonProperty("hasNuat")
	@Transient
	private Integer hasNuat;
	
	@Transient
	private String configStatus;
	
	@JsonProperty("addBonus")
	@Column(name = "addBonus")
	private String addBonus;
	
	@JsonProperty("addUPoint")
	@Column(name = "addUPoint")
	private String addUPoint;
	
	@JsonProperty("addItemBonusAmount")
	@Column(name = "addItemBonusAmount")
	private String addItemBonusAmount;

	public Long getIdSale() {
		return idSale;
	}

	public void setIdSale(Long idSale) {
		this.idSale = idSale;
	}

	public Long getIdBill() {
		return idBill;
	}

	public void setIdBill(Long idBill) {
		this.idBill = idBill;
	}

	public Long getIdItem() {
		return idItem;
	}

	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}

	public Long getIdSection() {
		return idSection;
	}

	public void setIdSection(Long idSection) {
		this.idSection = idSection;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getBonusPercent() {
		return bonusPercent;
	}

	public void setBonusPercent(BigDecimal bonusPercent) {
		this.bonusPercent = bonusPercent;
	}

	public BigDecimal getBonusAmount() {
		return bonusAmount;
	}

	public void setBonusAmount(BigDecimal bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public BigDecimal getItemBonusPercent() {
		return itemBonusPercent;
	}

	public void setItemBonusPercent(BigDecimal itemBonusPercent) {
		this.itemBonusPercent = itemBonusPercent;
	}

	public BigDecimal getItemBonusAmount() {
		return itemBonusAmount;
	}

	public void setItemBonusAmount(BigDecimal itemBonusAmount) {
		this.itemBonusAmount = itemBonusAmount;
	}

	public Long getGiftBonusAmount() {
		return giftBonusAmount;
	}

	public void setGiftBonusAmount(Long giftBonusAmount) {
		this.giftBonusAmount = giftBonusAmount;
	}

	public Long getDeletedIdApprover() {
		return deletedIdApprover;
	}

	public void setDeletedIdApprover(Long deletedIdApprover) {
		this.deletedIdApprover = deletedIdApprover;
	}

	public Long getDeletedIdReason() {
		return deletedIdReason;
	}

	public void setDeletedIdReason(Long deletedIdReason) {
		this.deletedIdReason = deletedIdReason;
	}

	public Long getIdBarcode() {
		return idBarcode;
	}

	public void setIdBarcode(Long idBarcode) {
		this.idBarcode = idBarcode;
	}

	public Long getIdSeller() {
		return idSeller;
	}

	public void setIdSeller(Long idSeller) {
		this.idSeller = idSeller;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public char getUseCardBonus() {
		return useCardBonus;
	}

	public void setUseCardBonus(char useCardBonus) {
		this.useCardBonus = useCardBonus;
	}

	public BigDecimal getBonusPaymentAmount() {
		return bonusPaymentAmount;
	}

	public void setBonusPaymentAmount(BigDecimal bonusPaymentAmount) {
		this.bonusPaymentAmount = bonusPaymentAmount;
	}

	public BigDecimal getItemBonusPaymentAmount() {
		return itemBonusPaymentAmount;
	}

	public void setItemBonusPaymentAmount(BigDecimal itemBonusPaymentAmount) {
		this.itemBonusPaymentAmount = itemBonusPaymentAmount;
	}

	public BigDecimal getCityTaxAmount() {
		return cityTaxAmount;
	}

	public void setCityTaxAmount(BigDecimal cityTaxAmount) {
		this.cityTaxAmount = cityTaxAmount;
	}

	public String getVatBillNo() {
		return vatBillNo;
	}

	public void setVatBillNo(String vatBillNo) {
		this.vatBillNo = vatBillNo;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public String getMobileCharges() {
		return mobileCharges;
	}

	public void setMobileCharges(String mobileCharges) {
		this.mobileCharges = mobileCharges;
	}

	public String getSaleVouchers() {
		return saleVouchers;
	}

	public void setSaleVouchers(String saleVouchers) {
		this.saleVouchers = saleVouchers;
	}

	public BigDecimal getUpointPaymentAmount() {
		return upointPaymentAmount;
	}

	public void setUpointPaymentAmount(BigDecimal upointPaymentAmount) {
		this.upointPaymentAmount = upointPaymentAmount;
	}

	public BigDecimal getUpointAmount() {
		return upointAmount;
	}

	public void setUpointAmount(BigDecimal upointAmount) {
		this.upointAmount = upointAmount;
	}

	public BigDecimal getUpointManufacturerAmount() {
		return upointManufacturerAmount;
	}

	public void setUpointManufacturerAmount(BigDecimal upointManufacturerAmount) {
		this.upointManufacturerAmount = upointManufacturerAmount;
	}

	public BigDecimal getUpointItemAmount() {
		return upointItemAmount;
	}

	public void setUpointItemAmount(BigDecimal upointItemAmount) {
		this.upointItemAmount = upointItemAmount;
	}

	public Integer getUseBonus() {
		return useBonus;
	}

	public void setUseBonus(Integer useBonus) {
		this.useBonus = useBonus;
	}

	public Integer getHasBonus() {
		return hasBonus;
	}

	public void setHasBonus(Integer hasBonus) {
		this.hasBonus = hasBonus;
	}

	public Integer getIsWhole() {
		return isWhole;
	}

	public void setIsWhole(Integer isWhole) {
		this.isWhole = isWhole;
	}

	public String getConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getHasNuat() {
		return hasNuat;
	}

	public void setHasNuat(Integer hasNuat) {
		this.hasNuat = hasNuat;
	}

	public String getAddBonus() {
		return addBonus;
	}

	public void setAddBonus(String addBonus) {
		this.addBonus = addBonus;
	}

	public String getAddUPoint() {
		return addUPoint;
	}

	public void setAddUPoint(String addUPoint) {
		this.addUPoint = addUPoint;
	}

	public String getAddItemBonusAmount() {
		return addItemBonusAmount;
	}

	public void setAddItemBonusAmount(String addItemBonusAmount) {
		this.addItemBonusAmount = addItemBonusAmount;
	}
}
