package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
//import java.math.String;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class BillResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("cardType")
	private String cardType;
	
	@JsonProperty("totalAmount")
	private String totalAmount;
	
	@JsonProperty("amountWithTax")
	private String amountWithTax;
	
	@JsonProperty("totalCityTax")
	private String totalCityTax;
	
	@JsonProperty("totalNuat")
	private String totalNuat;
	
	@JsonProperty("totalPaymentBonus")
	private String totalPaymentBonus;
	
	@JsonProperty("totalPaymentUPoint")
	private String totalPaymentUPoint;
	
	@JsonProperty("totalPaymentVoucher")
	private String totalPaymentVoucher;
	
	@JsonProperty("totalPaymentAmount")
	private String totalPaymentAmount;
	
	@JsonProperty("totalAddBonus")
	private String totalAddBonus;
	
	@JsonProperty("totalAddUPoint")
	private String totalAddUPoint;
	
	@JsonProperty("totalPaymentItemBonus")
	private String totalPaymentItemBonus;
	
	@JsonProperty("totalEBarimtAmount")
	private String totalEBarimtAmount;
	
	@JsonProperty("billDetail")
	private List<BillDetailResponse> billDetail;

	public String getMessage() { return message; }
	public void setMessage(String message) { this.message = message; }
	
	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalPaymentBonus() {
		return totalPaymentBonus;
	}

	public void setTotalPaymentBonus(String totalPaymentBonus) {
		this.totalPaymentBonus = totalPaymentBonus;
	}

	public String getTotalPaymentUPoint() {
		return totalPaymentUPoint;
	}

	public void setTotalPaymentUPoint(String totalPaymentUPoint) {
		this.totalPaymentUPoint = totalPaymentUPoint;
	}

	public String getTotalPaymentAmount() {
		return totalPaymentAmount;
	}

	public void setTotalPaymentAmount(String totalPaymentAmount) {
		this.totalPaymentAmount = totalPaymentAmount;
	}

	public String getTotalAddBonus() {
		return totalAddBonus;
	}

	public void setTotalAddBonus(String totalAddBonus) {
		this.totalAddBonus = totalAddBonus;
	}

	public String getTotalAddUPoint() {
		return totalAddUPoint;
	}

	public void setTotalAddUPoint(String totalAddUPoint) {
		this.totalAddUPoint = totalAddUPoint;
	}

	public String getTotalEBarimtAmount() {
		return totalEBarimtAmount;
	}

	public void setTotalEBarimtAmount(String totalEBarimtAmount) {
		this.totalEBarimtAmount = totalEBarimtAmount;
	}

	public List<BillDetailResponse> getBillDetails() {
		return billDetail;
	}

	public void setBillDetails(List<BillDetailResponse> billDetail) {
		this.billDetail = billDetail;
	}
	public String getAmountWithTax() {
		return amountWithTax;
	}
	public void setAmountWithTax(String amountWithTax) {
		this.amountWithTax = amountWithTax;
	}
	public String getTotalCityTax() {
		return totalCityTax;
	}
	public void setTotalCityTax(String totalCityTax) {
		this.totalCityTax = totalCityTax;
	}
	public String getTotalNuat() {
		return totalNuat;
	}
	public void setTotalNuat(String totalNuat) {
		this.totalNuat = totalNuat;
	}
	public String getTotalPaymentVoucher() {
		return totalPaymentVoucher;
	}
	public void setTotalPaymentVoucher(String totalPaymentVoucher) {
		this.totalPaymentVoucher = totalPaymentVoucher;
	}
	public String getTotalPaymentItemBonus() {
		return totalPaymentItemBonus;
	}
	public void setTotalPaymentItemBonus(String totalPaymentItemBonus) {
		this.totalPaymentItemBonus = totalPaymentItemBonus;
	}
	public List<BillDetailResponse> getBillDetail() {
		return billDetail;
	}
	public void setBillDetail(List<BillDetailResponse> billDetail) {
		this.billDetail = billDetail;
	}
}
