package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.entity.card.info.CardType;

@Entity
@Cacheable(false)
public class CalcResult implements Serializable {
	
	@Id
	@Column(name = "status")
	@JsonProperty("status")
	private String status;
	
	@Column(name = "message")
	@JsonProperty("message")
	private String message;
	
	@Column(name = "invoiceId")
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@Column(name = "cardTypes")
	@JsonProperty("cardTypes")
	private List<CardType> cardTypes;
	
	@Column(name = "bill")
	@JsonProperty("bill")
	private List<BillResponse> bill;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public List<CardType> getCardTypes() {
		return cardTypes;
	}

	public void setCardTypes(List<CardType> cardTypes) {
		this.cardTypes = cardTypes;
	}

	public List<BillResponse> getBill() {
		return bill;
	}

	public void setBill(List<BillResponse> bill) {
		this.bill = bill;
	}	
}
