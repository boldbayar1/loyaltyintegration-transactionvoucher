package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillRequest implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("billNo")
	private int billNo;

	@JsonProperty("totalAmount")
	private BigDecimal totalAmount;

	@JsonProperty("voucher")
	private BigDecimal voucher;

	@JsonProperty("billDetail")
	private List<BillDetailRequest> billDetail;
	
	public BillRequest() {
		super();
	}

	public BillRequest(int billNo, BigDecimal totalAmount, BigDecimal voucher, List<BillDetailRequest> billDetail) {
		super();
		this.billNo = billNo;
		this.totalAmount = totalAmount;
		this.voucher = voucher;
		this.billDetail = billDetail;
	}

	//Default version of clone() method.
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public int getBillNo() {
		return billNo;
	}

	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVoucher() {
		return voucher;
	}

	public void setVoucher(BigDecimal voucher) {
		this.voucher = voucher;
	}

	public List<BillDetailRequest> getBillDetails() {
		if(billDetail == null)
			billDetail = new ArrayList<>();
		return billDetail;
	}

	public List<BillDetailRequest> getBillDetail() {
		return billDetail;
	}

	public void setBillDetail(List<BillDetailRequest> billDetail) {
		this.billDetail = billDetail;
	}

//	public void setBillDetails(List<BillDetailRequest> sales) {
//		this.billDetail = billDetail;
//	}
	
	
}
