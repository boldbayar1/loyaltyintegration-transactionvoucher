package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillCalcRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("isCanSave")
	private int isCanSave;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("deviceId")
	private String deviceId;
	
	@JsonProperty("cardType")
	private String cardType;
	
	@JsonProperty("cardPercent")
	private BigDecimal cardPercent;
	
	@JsonProperty("hasUa")
	private int hasUa;
	
	@JsonProperty("totalAmount")
	private BigDecimal totalAmount;
	
	@JsonProperty("bonusBalance")
	private BigDecimal bonusBalance;
	
	@JsonProperty("upointBalance")
	private BigDecimal upointBalance;
	
	@JsonProperty("data")
	private List<BillDetailRequest> data;

	public int getIsCanSave() { return isCanSave; }
	public void setIsCanSave(int isCanSave) { this.isCanSave = isCanSave; }
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }
	
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public BigDecimal getCardPercent() {
		return cardPercent;
	}

	public void setCardPercent(BigDecimal cardPercent) {
		this.cardPercent = cardPercent;
	}
	
	public int getHasUa() { return hasUa; }
	public void setHasUa(int hasUa) { this.hasUa = hasUa; }

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getBonusBalance() {
		return bonusBalance;
	}

	public void setBonusBalance(BigDecimal bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public BigDecimal getUpointBalance() {
		return upointBalance;
	}

	public void setUpointBalance(BigDecimal upointBalance) {
		this.upointBalance = upointBalance;
	}

	public List<BillDetailRequest> getData() {
		return data;
	}

	public void setData(List<BillDetailRequest> data) {
		this.data = data;
	}
}
