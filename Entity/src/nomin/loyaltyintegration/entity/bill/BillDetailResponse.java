package nomin.loyaltyintegration.entity.bill;

import java.io.Serializable;
//import java.math.String;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BillDetailResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("billKey")
	private Long billKey;
	
	@JsonbProperty("itemId")
	private String itemId;
	
	@JsonbProperty("name")
	private String name;
	
	@JsonbProperty("measureUnit")
	private String measureUnit;
	
	@JsonbProperty("barCode")
	private String barCode;
	
	@JsonbProperty("quantity")
	private String quantity;
	
	@JsonbProperty("isWhole")
	private int isWhole;
	
	@JsonbProperty("basePrice")
	private String basePrice;
	
	@JsonbProperty("price")
	private String price;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("priceNoNuat")
	private String priceNoNuat;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("nuat")
	private String nuat;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("cityTax")
	private String cityTax;
	
	@JsonbProperty("amount")
	private String amount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("amountWithNuat")
	private String amountWithNuat;
	
	@JsonProperty("paymentBonus")
	private String paymentBonus;
	
	@JsonProperty("paymentUPoint")
	private String paymentUPoint;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("paymentVoucher")
	private String paymentVoucher;
	
	@JsonProperty("paymentAmount")
	private String paymentAmount;
	
	@JsonProperty("balanceBonus")
	private String balanceBonus;
	
	@JsonProperty("addBonus")
	private String addBonus;
	
	@JsonProperty("addUPoint")
	private String addUPoint;
	
	@JsonProperty("eBarimtAmount")
	private String eBarimtAmount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("addItemBonusAmount")
	private String addItemBonusAmount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("paymentItemBonus")
	private String paymentItemBonus;
	
	@JsonProperty("sectionName")
	private String sectionName;

	public Long getBillKey() { return billKey; }
	public void setBillKey(Long billKey) { this.billKey = billKey; }
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public int getIsWhole() {
		return isWhole;
	}

	public void setIsWhole(int isWhole) {
		this.isWhole = isWhole;
	}

	public String getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(String basePrice) {
		this.basePrice = basePrice;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPaymentBonus() {
		return paymentBonus;
	}

	public void setPaymentBonus(String paymentBonus) {
		this.paymentBonus = paymentBonus;
	}

	public String getPaymentUPoint() {
		return paymentUPoint;
	}

	public void setPaymentUPoint(String paymentUPoint) {
		this.paymentUPoint = paymentUPoint;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getBalanceBonus() {
		return balanceBonus;
	}

	public void setBalanceBonus(String balanceBonus) {
		this.balanceBonus = balanceBonus;
	}

	public String getAddBonus() {
		return addBonus;
	}

	public void setAddBonus(String addBonus) {
		this.addBonus = addBonus;
	}

	public String getAddUPoint() {
		return addUPoint;
	}

	public void setAddUPoint(String addUPoint) {
		this.addUPoint = addUPoint;
	}

	public String getEBarimtAmount() {
		return eBarimtAmount;
	}

	public void setEBarimtAmount(String eBarimtAmount) {
		this.eBarimtAmount = eBarimtAmount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMeasureUnit() {
		return measureUnit;
	}
	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String geteBarimtAmount() {
		return eBarimtAmount;
	}
	public void seteBarimtAmount(String eBarimtAmount) {
		this.eBarimtAmount = eBarimtAmount;
	}
	public String getPriceNoNuat() {
		return priceNoNuat;
	}
	public void setPriceNoNuat(String priceNoNuat) {
		this.priceNoNuat = priceNoNuat;
	}
	public String getNuat() {
		return nuat;
	}
	public void setNuat(String nuat) {
		this.nuat = nuat;
	}
	public String getCityTax() {
		return cityTax;
	}
	public void setCityTax(String cityTax) {
		this.cityTax = cityTax;
	}
	public String getAmountWithNuat() {
		return amountWithNuat;
	}
	public void setAmountWithNuat(String amountWithNuat) {
		this.amountWithNuat = amountWithNuat;
	}
	public String getPaymentVoucher() {
		return paymentVoucher;
	}
	public void setPaymentVoucher(String paymentVoucher) {
		this.paymentVoucher = paymentVoucher;
	}
	public String getAddItemBonusAmount() {
		return addItemBonusAmount;
	}
	public void setAddItemBonusAmount(String addItemBonusAmount) {
		this.addItemBonusAmount = addItemBonusAmount;
	}
	public String getPaymentItemBonus() {
		return paymentItemBonus;
	}
	public void setPaymentItemBonus(String paymentItemBonus) {
		this.paymentItemBonus = paymentItemBonus;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
}
