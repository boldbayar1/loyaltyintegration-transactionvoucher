package nomin.loyaltyintegration.entity.voucher;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "InvoiceVouCherPayments")
public class InvoiceVoucherPayments implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Pkey")
	private Long pKey;

	@Column(name = "InvoiceId")
	private String invoiceId;

	@Column(name = "DeviceId")
	private String deviceId;
	
	@Column(name = "ReqeustId")
	private String reqeustId;
	
	@Column(name = "ID_BILL")
	private Long idBill;
	
	@Column(name = "VOUCHERCODE")
	private String voucherCode;
	
	@Column(name = "VOUCHERTYPE")
	private String voucherType;
	
	@Column(name = "VOUCHER_PERCENT")
	private BigDecimal voucherPercent;
	
	@Column(name = "VOUCHER_AMOUNT")
	private BigDecimal voucherAmount;
	
	@Column(name = "USED_AMOUNT")
	private BigDecimal usedAmount;
	
	@Column(name = "ID_ITEM")
	private Long idItem;
	
	@Column(name = "STATUS")
	private String status;

	@Transient
	private String configStatus;

	public InvoiceVoucherPayments() {
		super();
	}

	public InvoiceVoucherPayments(Long pKey, String invoiceId, String deviceId, String reqeustId, Long idBill, String voucherCode, String voucherType, BigDecimal voucherPercent,
			BigDecimal voucherAmount, BigDecimal usedAmount, Long idItem, String status) {
		super();
		this.pKey = pKey;
		this.invoiceId = invoiceId;
		this.deviceId = deviceId;
		this.reqeustId = reqeustId;
		this.idBill = idBill;
		this.voucherCode = voucherCode;
		this.voucherType = voucherType;
		this.voucherPercent = voucherPercent;
		this.voucherAmount = voucherAmount;
		this.usedAmount = usedAmount;
		this.idItem = idItem;
		this.status = status;
	}

	public Long getpKey() {
		return pKey;
	}

	public void setpKey(Long pKey) {
		this.pKey = pKey;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getReqeustId() {
		return reqeustId;
	}

	public void setReqeustId(String reqeustId) {
		this.reqeustId = reqeustId;
	}

	public Long getIdBill() {
		return idBill;
	}

	public void setIdBill(Long idBill) {
		this.idBill = idBill;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public BigDecimal getVoucherPercent() {
		return voucherPercent;
	}

	public void setVoucherPercent(BigDecimal voucherPercent) {
		this.voucherPercent = voucherPercent;
	}

	public BigDecimal getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(BigDecimal usedAmount) {
		this.usedAmount = usedAmount;
	}

	public Long getIdItem() {
		return idItem;
	}

	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}

	public BigDecimal getVoucherAmount() {
		return voucherAmount;
	}

	public void setVoucherAmount(BigDecimal voucherAmount) {
		this.voucherAmount = voucherAmount;
	}
}
