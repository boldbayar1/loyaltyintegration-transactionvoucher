package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String type;
	private BigDecimal cardPercent;
	private BigDecimal paymentBonus;
	private BigDecimal paymentUPoint;
	private BigDecimal paymentAmount;
	private BigDecimal paymentVoucher;
	private BigDecimal balanceBonus;
	private BigDecimal balanceUPoint;
	private BigDecimal balanceMonth;
	private Integer isUPoint;
	private BigDecimal itemBonusBalance;
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	public BigDecimal getCardPercent() {
		return cardPercent;
	}
	public void setCardPercent(BigDecimal cardPercent) {
		this.cardPercent = cardPercent;
	}
	public BigDecimal getPaymentBonus() {
		return paymentBonus;
	}
	public void setPaymentBonus(BigDecimal paymentBonus) {
		this.paymentBonus = paymentBonus;
	}
	public BigDecimal getPaymentUPoint() {
		return paymentUPoint;
	}
	public void setPaymentUPoint(BigDecimal paymentUPoint) {
		this.paymentUPoint = paymentUPoint;
	}
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public BigDecimal getBalanceBonus() {
		return balanceBonus;
	}
	public void setBalanceBonus(BigDecimal balanceBonus) {
		this.balanceBonus = balanceBonus;
	}
	public BigDecimal getBalanceUPoint() {
		return balanceUPoint;
	}
	public void setBalanceUPoint(BigDecimal balanceUPoint) {
		this.balanceUPoint = balanceUPoint;
	}
	public BigDecimal getPaymentVoucher() {
		return paymentVoucher;
	}
	public void setPaymentVoucher(BigDecimal paymentVoucher) {
		this.paymentVoucher = paymentVoucher;
	}
	public BigDecimal getBalanceMonth() {
		return balanceMonth;
	}
	public void setBalanceMonth(BigDecimal balanceMonth) {
		this.balanceMonth = balanceMonth;
	}
	public Integer getIsUPoint() {
		return isUPoint;
	}
	public void setIsUPoint(Integer isUPoint) {
		this.isUPoint = isUPoint;
	}
	public BigDecimal getItemBonusBalance() {
		return itemBonusBalance;
	}
	public void setItemBonusBalance(BigDecimal itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}
}
