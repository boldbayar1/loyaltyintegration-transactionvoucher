package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Cacheable(false)
public class CardInfoProcedureResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CARDHOLDER")
	private String cardHolderId;
	
	@Column(name="ID_UACARD")
	private String uaCardId;
	
	@Column(name="ID_CARDACCOUNT")
	private String cardAccountId;
	
	@Column(name="FIRSTNAME")
	private String firstName;
	
	@Column(name="LASTNAME")
	private String lastName;
	
	@Column(name="PINCODE")
	private String pinCode;
	
	@Column(name="VATCUSTOMERNO")
	private String vatCustomerNo;
	
	@Column(name="VATCUSTOMERNO_TEMP")
	private String vatCustomerNoTemp;
	
	@Column(name="BONUSUSE")
	private String bonusUse;
	
	@Column(name="OPEN_DATE")
	private String openDate;
	
	@Column(name="BONUSCANCEL_DAYCOUNT")
	private Integer bonusCancelDayCount;
	
	@Column(name="IS_DVIP")
	private String isDvip;
	
	@Column(name="DVIPINUSE")
	private String useDvip;
	
	@Column(name="CALCBONUSPERCENT_PURCHASEAMOUNT")
	private Double calcBonusPercentPurchaseAmount;
	
	@Column(name="TIMED_PURCHASEAMOUNT")
	private Double timedPurchaseAmount;
	
	@Column(name="BONUSPERCENT")
	private double bonusPecent;
	
	@Column(name="BONUS_BALANCE")
	private String bonusBalance;
	
	@Column(name="DVIP_BALANCE")
	private double dvipBalance;
	
	@Column(name="DVIP_SAVEDBALANCE")
	private double dvipSavedBalance;
	
	@Column(name="B2_ALL_BALANCE")
	private double coinAllBalance;
	
	@Column(name="B2_MONTH_BALANCE")
	private double coinMonthBalance;
	
	@Column(name="B2_DAY_BALANCE")
	private double coinDayBalance;
	
	@Column(name="ITEMBONUS_BALANCE")
	private double itemBonusBalance;
	
	@Column(name="ADD_ITEMBONUS")
	private String addItemBonus;
	
	@Column(name="USE_ITEMBONUS")
	private String useItemBonus;
	
	@Column(name="ID_ITEMBONUSHEADER")
	private Long itemBonusHeaderId;
	
	@Column(name="IS_BIRTHDAY")
	private String isBirthDay;
	
	@Column(name="DUPLICATED_IDPHONE")
	private String isIdPhoneDuplicated;
	
	@Column(name="REQUESTPHONEUPDATE")
	private String isRequestPhoneUpdate;
	
	@Column(name="UPOINTINUSE")
	private String useUpoint;
	
	@Column(name="FP")
	private String fingerPrintData;
	
	@Column(name = "QR_RESULT")
	private String qrResult;
	
	@Column(name="ITEMBONUSX2INFO")
	private String itemBonusX2Info;
	
	@Column(name="CardNumber")
	private Long cardNumber;
	
	@Column(name="ADDITIONAL_JSON")
	private String additionalJson;
	
	@Column(name="CardType")
	private String cardType;

	@Column(name="VendorBalance")
	private Integer vendorBalance;
	
	@Column(name="EndDate")
	private Date endDate;
	
	@Column(name="VendorPercent")
	private Integer vendorPercent;
	
	@Column(name="IsRefreshPinCode")
	private Long isRefreshPinCode;
	

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public Integer getVendorBalance() {
		return vendorBalance;
	}

	public void setVendorBalance(Integer vendorBalance) {
		this.vendorBalance = vendorBalance;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getVendorPercent() {
		return vendorPercent;
	}

	public void setVendorPercent(Integer vendorPercent) {
		this.vendorPercent = vendorPercent;
	}

	public Long getIsRefreshPinCode() {
		return isRefreshPinCode;
	}

	public void setIsRefreshPinCode(Long isRefreshPinCode) {
		this.isRefreshPinCode = isRefreshPinCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Transient
	private String message;
	
	@Transient
	private Integer status;
	
	public CardInfoProcedureResponse() {
		super();
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getUaCardId() {
		return uaCardId;
	}

	public void setUaCardId(String uaCardId) {
		this.uaCardId = uaCardId;
	}

	public String getCardAccountId() {
		return cardAccountId;
	}

	public void setCardAccountId(String cardAccountId) {
		this.cardAccountId = cardAccountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getVatCustomerNo() {
		return vatCustomerNo;
	}

	public void setVatCustomerNo(String vatCustomerNo) {
		this.vatCustomerNo = vatCustomerNo;
	}

	public String getIsDvip() {
		return isDvip;
	}

	public void setIsDvip(String isDvip) {
		this.isDvip = isDvip;
	}

	public String getUseDvip() {
		return useDvip;
	}

	public void setUseDvip(String useDvip) {
		this.useDvip = useDvip;
	}

	public double getBonusPecent() {
		return bonusPecent;
	}

	public void setBonusPecent(double bonusPecent) {
		this.bonusPecent = bonusPecent;
	}

	public String getBonusBalance() {
		return bonusBalance;
	}

	public void setBonusBalance(String bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public double getDvipBalance() {
		return dvipBalance;
	}

	public void setDvipBalance(double dvipBalance) {
		this.dvipBalance = dvipBalance;
	}

	public double getDvipSavedBalance() {
		return dvipSavedBalance;
	}

	public void setDvipSavedBalance(double dvipSavedBalance) {
		this.dvipSavedBalance = dvipSavedBalance;
	}

	public double getCoinAllBalance() {
		return coinAllBalance;
	}

	public void setCoinAllBalance(double coinAllBalance) {
		this.coinAllBalance = coinAllBalance;
	}

	public double getCoinMonthBalance() {
		return coinMonthBalance;
	}

	public void setCoinMonthBalance(double coinMonthBalance) {
		this.coinMonthBalance = coinMonthBalance;
	}

	public double getCoinDayBalance() {
		return coinDayBalance;
	}

	public void setCoinDayBalance(double coinDayBalance) {
		this.coinDayBalance = coinDayBalance;
	}

	public double getItemBonusBalance() {
		return itemBonusBalance;
	}

	public void setItemBonusBalance(double itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}

	public String getAddItemBonus() {
		return addItemBonus;
	}

	public void setAddItemBonus(String addItemBonus) {
		this.addItemBonus = addItemBonus;
	}

	public String getUseItemBonus() {
		return useItemBonus;
	}

	public void setUseItemBonus(String useItemBonus) {
		this.useItemBonus = useItemBonus;
	}

	public Long getItemBonusHeaderId() {
		return itemBonusHeaderId;
	}

	public void setItemBonusHeaderId(Long itemBonusHeaderId) {
		this.itemBonusHeaderId = itemBonusHeaderId;
	}

	public String getIsBirthDay() {
		return isBirthDay;
	}

	public void setIsBirthDay(String isBirthDay) {
		this.isBirthDay = isBirthDay;
	}

	public String getIsIdPhoneDuplicated() {
		return isIdPhoneDuplicated;
	}

	public void setIsIdPhoneDuplicated(String isIdPhoneDuplicated) {
		this.isIdPhoneDuplicated = isIdPhoneDuplicated;
	}

	public String getIsRequestPhoneUpdate() {
		return isRequestPhoneUpdate;
	}

	public void setIsRequestPhoneUpdate(String isRequestPhoneUpdate) {
		this.isRequestPhoneUpdate = isRequestPhoneUpdate;
	}

	public String getUseUpoint() {
		return useUpoint;
	}

	public void setUseUpoint(String useUpoint) {
		this.useUpoint = useUpoint;
	}

	public String getFingerPrintData() {
		return fingerPrintData;
	}

	public void setFingerPrintData(String fingerPrintData) {
		this.fingerPrintData = fingerPrintData;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getQrResult() {
		return qrResult;
	}

	public void setQrResult(String qrResult) {
		this.qrResult = qrResult;
	}

	public String getMessage() {
		if(message == null)
			message = "No Message";
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getVatCustomerNoTemp() {
		return vatCustomerNoTemp;
	}

	public String getBonusUse() {
		return bonusUse;
	}

	public String getOpenDate() {
		return openDate;
	}

	public Integer getBonusCancelDayCount() {
		return bonusCancelDayCount;
	}

	public Double getCalcBonusPercentPurchaseAmount() {
		return calcBonusPercentPurchaseAmount;
	}

	public Double getTimedPurchaseAmount() {
		return timedPurchaseAmount;
	}

	public String getItemBonusX2Info() {
		return itemBonusX2Info;
	}

	public Long getCardNumber() {
		return cardNumber;
	}

	public void setVatCustomerNoTemp(String vatCustomerNoTemp) {
		this.vatCustomerNoTemp = vatCustomerNoTemp;
	}

	public void setBonusUse(String bonusUse) {
		this.bonusUse = bonusUse;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public void setBonusCancelDayCount(Integer bonusCancelDayCount) {
		this.bonusCancelDayCount = bonusCancelDayCount;
	}

	public void setCalcBonusPercentPurchaseAmount(Double calcBonusPercentPurchaseAmount) {
		this.calcBonusPercentPurchaseAmount = calcBonusPercentPurchaseAmount;
	}

	public void setTimedPurchaseAmount(Double timedPurchaseAmount) {
		this.timedPurchaseAmount = timedPurchaseAmount;
	}

	public void setItemBonusX2Info(String itemBonusX2Info) {
		this.itemBonusX2Info = itemBonusX2Info;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getAdditionalJson() {
		return additionalJson;
	}

	public void setAdditionalJson(String additionalJson) {
		this.additionalJson = additionalJson;
	}
}
