package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("code")
	private String code;
	
	@JsonProperty("unit")
	private Integer unit;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("quantity")
	private Integer quantity;
	
	@JsonProperty("price")
	private BigDecimal price;
	
	@JsonProperty("total_price")
	private BigDecimal total_price;

	public String getCode() {
		return code;
	}

	public Integer getUnit() {
		return unit;
	}

	public String getName() {
		return name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getTotal_price() {
		return total_price;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setTotal_price(BigDecimal total_price) {
		this.total_price = total_price;
	}	
}
