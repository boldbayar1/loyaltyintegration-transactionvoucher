package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CancelTransaction implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("card_number")
	private String card_number;
	
	@JsonProperty("mobile")
	private String mobile;
	
	@JsonProperty("pin_code")
	private String pin_code;
	
	public String getCardNumber() { return card_number; }
	public void setCardNumber(String card_number) { this.card_number = card_number; }
	
	public String getMobile() { return mobile; }
	public void setMobile(String mobile) { this.mobile = mobile; }
	
	public String getPinCode() { return pin_code; }
	public void setPinCode(String pin_code) { this.pin_code = pin_code; }
	
}
