package nomin.loyaltyintegration.entity.card.info.register;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UAConsumerRequest {
	
	@JsonProperty("ua_id")
	private String uaId;

	public String getUaId() { return uaId; }
	public void setUaId(String uaId) { this.uaId = uaId; }

}
