package nomin.loyaltyintegration.entity.card.info.register;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerRequest {
	private Long locationKey;
	private Long posKey;
	private String data;

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
