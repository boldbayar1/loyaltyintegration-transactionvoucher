package nomin.loyaltyintegration.entity.card.info.register;

import nomin.loyaltyintegration.entity.card.info.register.ObjectAsStringSerializer;
import nomin.loyaltyintegration.entity.card.info.register.UAConsumerRequest;

public class UAConsumerRequestSerializer extends ObjectAsStringSerializer<UAConsumerRequest> {
	private static final long serialVersionUID = 1L;
}
