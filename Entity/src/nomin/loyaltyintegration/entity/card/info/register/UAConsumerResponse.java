package nomin.loyaltyintegration.entity.card.info.register;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UAConsumerResponse {
	
	@JsonProperty("ua_id")
	private String uaId;
	
	@JsonProperty("firstname")
	private String firstName;
	
	@JsonProperty("lastname")
	private String lastName;
	
	@JsonProperty("partner_card")
	private String partnerCard;
	
	@JsonProperty("card_number")
	private String cardNumber;
	
	@JsonProperty("reg_no")
	private String regNo;
	
	private int result;
	private String address;
	private String message;
	public String getUaId() {
		return uaId;
	}
	public void setUaId(String uaId) {
		this.uaId = uaId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPartnerCard() {
		return partnerCard;
	}
	public void setPartnerCard(String partnerCard) {
		this.partnerCard = partnerCard;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
