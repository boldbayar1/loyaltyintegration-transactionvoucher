package nomin.loyaltyintegration.entity.card.info.register;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UPointResponse {
	
	private int status;
	private int result;
	private String message;
	private String readEntity;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReadEntity() {
		return readEntity;
	}
	public void setReadEntity(String readEntity) {
		this.readEntity = readEntity;
	}
	
}
