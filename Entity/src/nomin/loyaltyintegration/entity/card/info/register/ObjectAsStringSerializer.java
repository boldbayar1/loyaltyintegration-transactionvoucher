package nomin.loyaltyintegration.entity.card.info.register;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public abstract class ObjectAsStringSerializer<T> extends StdSerializer<T> {

	private static final long serialVersionUID = 1L;

	public ObjectAsStringSerializer() {
        this(null);
    }

    public ObjectAsStringSerializer(Class<T> t) {
        super(t);
    }

    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String serialized = new ObjectMapper().writeValueAsString(value);
        gen.writeString(serialized);
    }
}