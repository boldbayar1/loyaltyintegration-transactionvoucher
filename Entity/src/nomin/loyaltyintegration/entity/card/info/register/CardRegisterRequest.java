package nomin.loyaltyintegration.entity.card.info.register;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardRegisterRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("locationKey")
	private Long locationKey;
	
	@JsonProperty("posKey")
	private Long posKey;
	
	@JsonProperty("cashierKey")
	private Long cashierKey;
	
	@JsonProperty("data")
	private UACardResponse data;
	
	public Long getLocationKey() {
		return locationKey;
	}
	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}
	public Long getPosKey() {
		return posKey;
	}
	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}
	public Long getCashierKey() {
		return cashierKey;
	}
	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}
	public UACardResponse getData() {
		return data;
	}
	public void setData(UACardResponse data) {
		this.data = data;
	}

}
