package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardInfoResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cardHolderId;
	private String cardAccountId;
	private String firstName;
	private String lastName;
	private String vatCustomerNo;
	private String vatCustomerNoTemp;
	private String bonusUse;
	private String openDate;
	private Integer bonusCancelDayCount;
	private String isDvip;
	private String useDvip;
	private Double calcBonusPercentPurchaseAmount;
	private Double timedPurchaseAmount;
	private double bonusPecent;
	private String bonusBalance;
	private double dvipBalance;
	private double dvipSavedBalance;
	private double coinAllBalance;
	private double coinMonthBalance;
	private double coinDayBalance;
	private double itemBonusBalance;
	private String addItemBonus;
	private String useItemBonus;
	private Long itemBonusHeaderId;
	private String isBirthDay;
	private String isIdPhoneDuplicated;
	private String isRequestPhoneUpdate;
	private String useUpoint;
	private String fingerPrintData;
	private String qrResult;
	private String itemBonusX2Info;
	private Long cardNumber;
	private String holderByPhone;
	private String additionalJson;
	
	private String uaId;
	private String uaCardId;
	private String uaCheckInfoStatus;
	private String uaCheckInfoMessage;
	private double uaBalance;
	private String uaPhone;
	private String uaCreatedDate;
	private Integer uaCardStatus;
	private Integer uaResult;
	
	private String cardType2;
	private Integer billNo;
	private String cardType;
	private String pinType;
	private BigDecimal totalAmount;
	private BigDecimal voucher;
	
	private Integer vendorBalance;
	private Date endDate;
	private Integer vendorPercent;
	private Long isRefreshPinCode;
	
	private String message;
	private Integer statusCardInfoResponse;

	public Integer getVendorBalance() {
		return vendorBalance;
	}

	public void setVendorBalance(Integer vendorBalance) {
		this.vendorBalance = vendorBalance;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getVendorPercent() {
		return vendorPercent;
	}

	public void setVendorPercent(Integer vendorPercent) {
		this.vendorPercent = vendorPercent;
	}

	public Long getIsRefreshPinCode() {
		return isRefreshPinCode;
	}

	public void setIsRefreshPinCode(Long isRefreshPinCode) {
		this.isRefreshPinCode = isRefreshPinCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getAddItemBonus() {
		return addItemBonus;
	}
	
	public void setAddItemBonus(String addItemBonus) {
		this.addItemBonus = addItemBonus;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public String getCardAccountId() {
		return cardAccountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getVatCustomerNo() {
		return vatCustomerNo;
	}

	public String getVatCustomerNoTemp() {
		return vatCustomerNoTemp;
	}

	public String getBonusUse() {
		return bonusUse;
	}

	public String getOpenDate() {
		return openDate;
	}

	public Integer getBonusCancelDayCount() {
		return bonusCancelDayCount;
	}

	public String getIsDvip() {
		return isDvip;
	}

	public String getUseDvip() {
		return useDvip;
	}

	public Double getCalcBonusPercentPurchaseAmount() {
		return calcBonusPercentPurchaseAmount;
	}

	public Double getTimedPurchaseAmount() {
		return timedPurchaseAmount;
	}

	public double getBonusPecent() {
		return bonusPecent;
	}

	public String getBonusBalance() {
		return bonusBalance;
	}

	public double getDvipBalance() {
		return dvipBalance;
	}

	public double getDvipSavedBalance() {
		return dvipSavedBalance;
	}

	public double getCoinAllBalance() {
		return coinAllBalance;
	}

	public double getCoinMonthBalance() {
		return coinMonthBalance;
	}

	public double getCoinDayBalance() {
		return coinDayBalance;
	}

	public double getItemBonusBalance() {
		return itemBonusBalance;
	}

	public String getUseItemBonus() {
		return useItemBonus;
	}

	public Long getItemBonusHeaderId() {
		return itemBonusHeaderId;
	}

	public String getIsBirthDay() {
		return isBirthDay;
	}

	public String getIsIdPhoneDuplicated() {
		return isIdPhoneDuplicated;
	}

	public String getIsRequestPhoneUpdate() {
		return isRequestPhoneUpdate;
	}

	public String getUseUpoint() {
		return useUpoint;
	}

	public String getFingerPrintData() {
		return fingerPrintData;
	}

	public String getQrResult() {
		return qrResult;
	}

	public String getItemBonusX2Info() {
		return itemBonusX2Info;
	}

	public Long getCardNumber() {
		return cardNumber;
	}

	public String getHolderByPhone() {
		return holderByPhone;
	}

	public String getUaId() {
		return uaId;
	}

	public String getUaCardId() {
		return uaCardId;
	}

	public String getUaCheckInfoStatus() {
		return uaCheckInfoStatus;
	}

	public String getUaCheckInfoMessage() {
		return uaCheckInfoMessage;
	}

	public double getUaBalance() {
		return uaBalance;
	}

	public String getUaPhone() {
		return uaPhone;
	}

	public String getUaCreatedDate() {
		return uaCreatedDate;
	}

	public Integer getUaCardStatus() {
		return uaCardStatus;
	}

	public Integer getUaResult() {
		return uaResult;
	}

	public Integer getBillNo() {
		return billNo;
	}

	public String getCardType() {
		return cardType;
	}

	public String getPinType() {
		return pinType;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public BigDecimal getVoucher() {
		return voucher;
	}

	public String getMessage() {
		return message;
	}

	public Integer getStatusCardInfoResponse() {
		return statusCardInfoResponse;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public void setCardAccountId(String cardAccountId) {
		this.cardAccountId = cardAccountId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setVatCustomerNo(String vatCustomerNo) {
		this.vatCustomerNo = vatCustomerNo;
	}

	public void setVatCustomerNoTemp(String vatCustomerNoTemp) {
		this.vatCustomerNoTemp = vatCustomerNoTemp;
	}

	public void setBonusUse(String bonusUse) {
		this.bonusUse = bonusUse;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public void setBonusCancelDayCount(Integer bonusCancelDayCount) {
		this.bonusCancelDayCount = bonusCancelDayCount;
	}

	public void setIsDvip(String isDvip) {
		this.isDvip = isDvip;
	}

	public void setUseDvip(String useDvip) {
		this.useDvip = useDvip;
	}

	public void setCalcBonusPercentPurchaseAmount(Double calcBonusPercentPurchaseAmount) {
		this.calcBonusPercentPurchaseAmount = calcBonusPercentPurchaseAmount;
	}

	public void setTimedPurchaseAmount(Double timedPurchaseAmount) {
		this.timedPurchaseAmount = timedPurchaseAmount;
	}

	public void setBonusPecent(double bonusPecent) {
		this.bonusPecent = bonusPecent;
	}

	public void setBonusBalance(String bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public void setDvipBalance(double dvipBalance) {
		this.dvipBalance = dvipBalance;
	}

	public void setDvipSavedBalance(double dvipSavedBalance) {
		this.dvipSavedBalance = dvipSavedBalance;
	}

	public void setCoinAllBalance(double coinAllBalance) {
		this.coinAllBalance = coinAllBalance;
	}

	public void setCoinMonthBalance(double coinMonthBalance) {
		this.coinMonthBalance = coinMonthBalance;
	}

	public void setCoinDayBalance(double coinDayBalance) {
		this.coinDayBalance = coinDayBalance;
	}

	public void setItemBonusBalance(double itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}

	public void setUseItemBonus(String useItemBonus) {
		this.useItemBonus = useItemBonus;
	}

	public void setItemBonusHeaderId(Long itemBonusHeaderId) {
		this.itemBonusHeaderId = itemBonusHeaderId;
	}

	public void setIsBirthDay(String isBirthDay) {
		this.isBirthDay = isBirthDay;
	}

	public void setIsIdPhoneDuplicated(String isIdPhoneDuplicated) {
		this.isIdPhoneDuplicated = isIdPhoneDuplicated;
	}

	public void setIsRequestPhoneUpdate(String isRequestPhoneUpdate) {
		this.isRequestPhoneUpdate = isRequestPhoneUpdate;
	}

	public void setUseUpoint(String useUpoint) {
		this.useUpoint = useUpoint;
	}

	public void setFingerPrintData(String fingerPrintData) {
		this.fingerPrintData = fingerPrintData;
	}

	public void setQrResult(String qrResult) {
		this.qrResult = qrResult;
	}

	public void setItemBonusX2Info(String itemBonusX2Info) {
		this.itemBonusX2Info = itemBonusX2Info;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setHolderByPhone(String holderByPhone) {
		this.holderByPhone = holderByPhone;
	}

	public void setUaId(String uaId) {
		this.uaId = uaId;
	}

	public void setUaCardId(String uaCardId) {
		this.uaCardId = uaCardId;
	}

	public void setUaCheckInfoStatus(String uaCheckInfoStatus) {
		this.uaCheckInfoStatus = uaCheckInfoStatus;
	}

	public void setUaCheckInfoMessage(String uaCheckInfoMessage) {
		this.uaCheckInfoMessage = uaCheckInfoMessage;
	}

	public void setUaBalance(double uaBalance) {
		this.uaBalance = uaBalance;
	}

	public void setUaPhone(String uaPhone) {
		this.uaPhone = uaPhone;
	}

	public void setUaCreatedDate(String uaCreatedDate) {
		this.uaCreatedDate = uaCreatedDate;
	}

	public void setUaCardStatus(Integer uaCardStatus) {
		this.uaCardStatus = uaCardStatus;
	}

	public void setUaResult(Integer uaResult) {
		this.uaResult = uaResult;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public void setPinType(String pinType) {
		this.pinType = pinType;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setVoucher(BigDecimal voucher) {
		this.voucher = voucher;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatusCardInfoResponse(Integer statusCardInfoResponse) {
		this.statusCardInfoResponse = statusCardInfoResponse;
	}

	public String getAdditionalJson() {
		return additionalJson;
	}

	public void setAdditionalJson(String additionalJson) {
		this.additionalJson = additionalJson;
	}

	public String getCardType2() {
		return cardType2;
	}

	public void setCardType2(String cardType2) {
		this.cardType2 = cardType2;
	}
}
