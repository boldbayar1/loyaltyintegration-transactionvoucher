package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("billNo")
	private int billNo;

	@JsonProperty("cardType")
	private String cardType;

	@JsonProperty("cardNumber")
	private String cardNumber;

	@JsonProperty("cardPercent")
	private BigDecimal cardPercent;

	@JsonProperty("cardHolderId")
	private String cardHolderId;

	@JsonProperty("cardUAId")
	private String cardUAId;

	@JsonProperty("pinType")
	private String pinType;

	@JsonProperty("totalAmount")
	private BigDecimal totalAmount;

	@JsonProperty("voucher")
	private BigDecimal voucher;

	@JsonProperty("bonusBalance")
	private String bonusBalance;

	@JsonProperty("dvipBalance")
	private BigDecimal dvipBalance;

	@JsonProperty("useDVip")
	private String useDVip;

	@JsonProperty("coinDayBalance")
	private BigDecimal coinDayBalance;

	@JsonProperty("coinMonthBalance")
	private BigDecimal coinMonthBalance;

	@JsonProperty("upointBalance")
	private BigDecimal upointBalance;

	@JsonProperty("useUPoint")
	private String useUPoint;
	
	@JsonProperty("itemBonusBalance")
	private Double itemBonusBalance;
	
	@JsonProperty("addItemBonus")
	private String addItemBonus;
	
	@JsonProperty("useItemBonus")
	private String useItemBonus;
	
	@JsonProperty("itemBonusX2Info")
	private String itemBonusX2Info;

	public int getBillNo() {
		return billNo;
	}

	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public BigDecimal getCardPercent() {
		return cardPercent;
	}

	public void setCardPercent(BigDecimal cardPercent) {
		this.cardPercent = cardPercent;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getCardUAId() {
		return cardUAId;
	}

	public void setCardUAId(String cardUAId) {
		this.cardUAId = cardUAId;
	}

	public String getPinType() {
		return pinType;
	}

	public void setPinType(String pinType) {
		this.pinType = pinType;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVoucher() {
		return voucher;
	}

	public void setVoucher(BigDecimal voucher) {
		this.voucher = voucher;
	}

	public String getBonusBalance() {
		return bonusBalance;
	}

	public void setBonusBalance(String bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public BigDecimal getDvipBalance() {
		return dvipBalance;
	}

	public void setDvipBalance(BigDecimal dvipBalance) {
		this.dvipBalance = dvipBalance;
	}

	public String getUseDVip() {
		return useDVip;
	}

	public void setUseDVip(String useDVip) {
		this.useDVip = useDVip;
	}

	public BigDecimal getCoinDayBalance() {
		return coinDayBalance;
	}

	public void setCoinDayBalance(BigDecimal coinDayBalance) {
		this.coinDayBalance = coinDayBalance;
	}

	public BigDecimal getCoinMonthBalance() {
		return coinMonthBalance;
	}

	public void setCoinMonthBalance(BigDecimal coinMonthBalance) {
		this.coinMonthBalance = coinMonthBalance;
	}

	public BigDecimal getUpointBalance() {
		return upointBalance;
	}

	public void setUpointBalance(BigDecimal upointBalance) {
		this.upointBalance = upointBalance;
	}

	public String getUseUPoint() {
		return useUPoint;
	}

	public void setUseUPoint(String useUPoint) {
		this.useUPoint = useUPoint;
	}

	public Double getItemBonusBalance() {
		return itemBonusBalance;
	}

	public String getAddItemBonus() {
		return addItemBonus;
	}

	public String getUseItemBonus() {
		return useItemBonus;
	}

	public String getItemBonusX2Info() {
		return itemBonusX2Info;
	}

	public void setItemBonusBalance(Double itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}

	public void setAddItemBonus(String addItemBonus) {
		this.addItemBonus = addItemBonus;
	}

	public void setUseItemBonus(String useItemBonus) {
		this.useItemBonus = useItemBonus;
	}

	public void setItemBonusX2Info(String itemBonusX2Info) {
		this.itemBonusX2Info = itemBonusX2Info;
	}
}
