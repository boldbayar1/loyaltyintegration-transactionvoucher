package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpointTransactionData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("date")
	private String date;
	
	@JsonProperty("manufacturer")
	private List<Manufacturer> manufacturer;
	
	@JsonProperty("items")
	private List<Item> items;
	
	@JsonProperty("bank")
	private List<Bank> bank;
	
	@JsonProperty("card_number")
	private String card_number;
	
	@JsonProperty("bill_number")
	private Long bill_number;
	
	@JsonProperty("spend_amount")
	private int spend_amount;
	
	@JsonProperty("bonus_amount")
	private BigDecimal bonus_amount;
	
	@JsonProperty("bonus_point")
	private int bonus_point;
	
	@JsonProperty("total_amount")
	private BigDecimal total_amount;
	
	@JsonProperty("cash_amount")
	private BigDecimal cash_amount;
	
	@JsonProperty("terminal_id")
	private String terminal_id;
	
//	@JsonProperty("transactionType")
//	private String transactionType;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<Manufacturer> getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(List<Manufacturer> manufacturer) {
		this.manufacturer = manufacturer;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public List<Bank> getBank() {
		return bank;
	}

	public void setBank(List<Bank> bank) {
		this.bank = bank;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public Long getBill_number() {
		return bill_number;
	}

	public void setBill_number(Long bill_number) {
		this.bill_number = bill_number;
	}

	public int getSpend_amount() {
		return spend_amount;
	}

	public void setSpend_amount(int spend_amount) {
		this.spend_amount = spend_amount;
	}

	public BigDecimal getBonus_amount() {
		return bonus_amount;
	}

	public void setBonus_amount(BigDecimal bonus_amount) {
		this.bonus_amount = bonus_amount;
	}

	public int getBonus_point() {
		return bonus_point;
	}

	public void setBonus_point(int bonus_point) {
		this.bonus_point = bonus_point;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	public BigDecimal getCash_amount() {
		return cash_amount;
	}

	public void setCash_amount(BigDecimal cash_amount) {
		this.cash_amount = cash_amount;
	}

	public String getTerminal_id() {
		return terminal_id;
	}

	public void setTerminal_id(String terminal_id) {
		this.terminal_id = terminal_id;
	}
}
