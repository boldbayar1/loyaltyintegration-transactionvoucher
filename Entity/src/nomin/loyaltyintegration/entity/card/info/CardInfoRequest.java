package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardInfoRequest implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("cardType")
	private String cardType;

	@JsonbProperty("cardNumber")
	private String cardNumber;

	@JsonbProperty("pinType")
	private String pinType;

	@JsonbProperty("pinData")
	private String pinData;

	private boolean noCardContinue;

	public CardInfoRequest() {
		super();
	}

	public CardInfoRequest(String cardType, String cardNumber, String pinType, String pinData, boolean noCardContinue) {
		super();
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.pinType = pinType;
		this.pinData = pinData;
		this.noCardContinue = noCardContinue;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getPinType() {
		return pinType;
	}

	public void setPinType(String pinType) {
		this.pinType = pinType;
	}

	public String getPinData() {
		return pinData;
	}

	public void setPinData(String pinData) {
		this.pinData = pinData;
	}

	public boolean isNoCardContinue() {
		return noCardContinue;
	}

	public void setNoCardContinue(boolean noCardContinue) {
		this.noCardContinue = noCardContinue;
	}
}
