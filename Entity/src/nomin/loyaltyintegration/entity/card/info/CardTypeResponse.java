package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardTypeResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("type")
	private String type;
	
	@JsonProperty("cardNumberBonus")
	private String cardNumberBonus;
	
	@JsonProperty("cardNumberUPoint")
	private String cardNumberUpoint;
	
	@JsonProperty("cardPercentBonus")
	private BigDecimal cardPercentBonus;
	
	@JsonProperty("cardPercentUPoint")
	private BigDecimal cardPercentuPoint;
	
	@JsonProperty("balanceBonus")
	private BigDecimal balanceBonus;
	
	@JsonProperty("balanceUPoint")
	private BigDecimal balanceUPoint;
	
	@JsonProperty("balanceMonth")
	private BigDecimal balanceMonth;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCardNumberBonus() {
		return cardNumberBonus;
	}

	public void setCardNumberBonus(String cardNumberBonus) {
		if(cardNumberBonus != null && cardNumberBonus.length() > 5) {
			
			this.cardNumberBonus = cardNumberBonus.substring(0,5) + "***"; 
		}
		else {
			this.cardNumberBonus = "***";
		}
		
	}

	public String getCardNumberUpoint() {
		return cardNumberUpoint;
	}

	public void setCardNumberUpoint(String cardNumberUpoint) {
		if(cardNumberUpoint != null && cardNumberUpoint.length() > 5) {
			
			this.cardNumberUpoint = cardNumberUpoint.substring(0,5) + "***"; 
		}
		else {
			this.cardNumberUpoint = "***";
		}
	}

	public BigDecimal getCardPercentBonus() {
		return cardPercentBonus;
	}

	public void setCardPercentBonus(BigDecimal cardPercentBonus) {
		this.cardPercentBonus = cardPercentBonus;
	}

	public BigDecimal getCardPercentuPoint() {
		return cardPercentuPoint;
	}

	public void setCardPercentuPoint(BigDecimal cardPercentuPoint) {
		this.cardPercentuPoint = cardPercentuPoint;
	}

	public BigDecimal getBalanceBonus() {
		return balanceBonus;
	}

	public void setBalanceBonus(BigDecimal balanceBonus) {
		this.balanceBonus = balanceBonus;
	}

	public BigDecimal getBalanceUPoint() {
		return balanceUPoint;
	}

	public void setBalanceUPoint(BigDecimal balanceUPoint) {
		this.balanceUPoint = balanceUPoint;
	}

	public BigDecimal getBalanceMonth() {
		return balanceMonth;
	}

	public void setBalanceMonth(BigDecimal balanceMonth) {
		this.balanceMonth = balanceMonth;
	}
	
	
	
}
