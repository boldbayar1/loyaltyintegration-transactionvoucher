package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Cacheable(false)
public class QrCardInfoProcedureResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "Id")
	private String id;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "HOLDERID_BYPHONE")
	private String holderByPhone;
	
	@Column(name = "REGISTER")
	private String register;

	@Column(name = "ID_CARDHOLDER")
	private String cardHolderId;

	@Column(name = "ID_UACARD")
	private String uaCardId;

	@Column(name = "ID_CARDACCOUNT")
	private String cardAccountId;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "LASTNAME")
	private String lastName;

	@Column(name = "PINCODE")
	private String pinCode;

	@Column(name = "VATCUSTOMERNO")
	private String vatCustomerNo;

	@Column(name = "BONUSUSE")
	private String bonusUse;

	@Column(name = "OPEN_DATE")
	private String openDate;

	@Column(name = "BONUSCANCEL_DAYCOUNT")
	private Integer bonusCancelDayCount;

	@Column(name = "IS_DVIP")
	private String isDvip;

	@Column(name = "DVIPINUSE")
	private String useDvip;

	@Column(name = "CALCBONUSPERCENT_PURCHASEAMOUNT")
	private Double calcBonusPercentPurchaseAmount;

	@Column(name = "TIMED_PURCHASEAMOUNT")
	private Double timedPurchaseAmount;

	@Column(name = "BONUSPERCENT")
	private Double bonusPecent;

	@Column(name = "BONUS_BALANCE")
	private String bonusBalance;

	@Column(name = "DVIP_BALANCE")
	private Double dvipBalance;

	@Column(name = "DVIP_SAVEDBALANCE")
	private Double dvipSavedBalance;

	@Column(name = "B2_ALL_BALANCE")
	private Double coinAllBalance;

	@Column(name = "B2_MONTH_BALANCE")
	private Double coinMonthBalance;

	@Column(name = "B2_DAY_BALANCE")
	private Double coinDayBalance;

	@Column(name = "ITEMBONUS_BALANCE")
	private Double itemBonusBalance;

	@Column(name = "ADD_ITEMBONUS")
	private String addItemBonus;

	@Column(name = "USE_ITEMBONUS")
	private String useItemBonus;

	@Column(name = "ID_ITEMBONUSHEADER")
	private Long itemBonusHeaderId;

	@Column(name = "IS_BIRTHDAY")
	private String isBirthDay;

	@Column(name = "DUPLICATED_IDPHONE")
	private String isIdPhoneDuplicated;

	@Column(name = "REQUESTPHONEUPDATE")
	private String isRequestPhoneUpdate;

	@Column(name = "UPOINTINUSE")
	private String useUpoint;

	@Column(name = "FP")
	private String fingerPrintData;

	@Column(name = "QR_RESULT")
	private String qrResult;

	@Column(name = "ITEMBONUSX2INFO")
	private String itemBonusX2Info;
	
	@Column(name = "InvoiceId")
	private String invoiceId;

	@Transient
	private String message;

	@Transient
	private Integer status;
	
	@Transient
	private Double uPointBalance;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHolderByPhone() {
		return holderByPhone;
	}

	public void setHolderByPhone(String holderByPhone) {
		this.holderByPhone = holderByPhone;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public String getUaCardId() {
		return uaCardId;
	}

	public void setUaCardId(String uaCardId) {
		this.uaCardId = uaCardId;
	}

	public String getCardAccountId() {
		return cardAccountId;
	}

	public void setCardAccountId(String cardAccountId) {
		this.cardAccountId = cardAccountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getVatCustomerNo() {
		return vatCustomerNo;
	}

	public void setVatCustomerNo(String vatCustomerNo) {
		this.vatCustomerNo = vatCustomerNo;
	}

	public String getBonusUse() {
		return bonusUse;
	}

	public void setBonusUse(String bonusUse) {
		this.bonusUse = bonusUse;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public Integer getBonusCancelDayCount() {
		return bonusCancelDayCount;
	}

	public void setBonusCancelDayCount(Integer bonusCancelDayCount) {
		this.bonusCancelDayCount = bonusCancelDayCount;
	}

	public String getIsDvip() {
		return isDvip;
	}

	public void setIsDvip(String isDvip) {
		this.isDvip = isDvip;
	}

	public String getUseDvip() {
		return useDvip;
	}

	public void setUseDvip(String useDvip) {
		this.useDvip = useDvip;
	}

	public Double getCalcBonusPercentPurchaseAmount() {
		return calcBonusPercentPurchaseAmount;
	}

	public void setCalcBonusPercentPurchaseAmount(Double calcBonusPercentPurchaseAmount) {
		this.calcBonusPercentPurchaseAmount = calcBonusPercentPurchaseAmount;
	}

	public Double getTimedPurchaseAmount() {
		return timedPurchaseAmount;
	}

	public void setTimedPurchaseAmount(Double timedPurchaseAmount) {
		this.timedPurchaseAmount = timedPurchaseAmount;
	}

	public Double getBonusPecent() {
		return bonusPecent;
	}

	public void setBonusPecent(Double bonusPecent) {
		this.bonusPecent = bonusPecent;
	}

	public String getBonusBalance() {
		return bonusBalance;
	}

	public void setBonusBalance(String bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public Double getDvipBalance() {
		return dvipBalance;
	}

	public void setDvipBalance(Double dvipBalance) {
		this.dvipBalance = dvipBalance;
	}

	public Double getDvipSavedBalance() {
		return dvipSavedBalance;
	}

	public void setDvipSavedBalance(Double dvipSavedBalance) {
		this.dvipSavedBalance = dvipSavedBalance;
	}

	public Double getCoinAllBalance() {
		return coinAllBalance;
	}

	public void setCoinAllBalance(Double coinAllBalance) {
		this.coinAllBalance = coinAllBalance;
	}

	public Double getCoinMonthBalance() {
		return coinMonthBalance;
	}

	public void setCoinMonthBalance(Double coinMonthBalance) {
		this.coinMonthBalance = coinMonthBalance;
	}

	public Double getCoinDayBalance() {
		return coinDayBalance;
	}

	public void setCoinDayBalance(Double coinDayBalance) {
		this.coinDayBalance = coinDayBalance;
	}

	public Double getItemBonusBalance() {
		return itemBonusBalance;
	}

	public void setItemBonusBalance(Double itemBonusBalance) {
		this.itemBonusBalance = itemBonusBalance;
	}

	public String getAddItemBonus() {
		return addItemBonus;
	}

	public void setAddItemBonus(String addItemBonus) {
		this.addItemBonus = addItemBonus;
	}

	public String getUseItemBonus() {
		return useItemBonus;
	}

	public void setUseItemBonus(String useItemBonus) {
		this.useItemBonus = useItemBonus;
	}

	public Long getItemBonusHeaderId() {
		return itemBonusHeaderId;
	}

	public void setItemBonusHeaderId(Long itemBonusHeaderId) {
		this.itemBonusHeaderId = itemBonusHeaderId;
	}

	public String getIsBirthDay() {
		return isBirthDay;
	}

	public void setIsBirthDay(String isBirthDay) {
		this.isBirthDay = isBirthDay;
	}

	public String getIsIdPhoneDuplicated() {
		return isIdPhoneDuplicated;
	}

	public void setIsIdPhoneDuplicated(String isIdPhoneDuplicated) {
		this.isIdPhoneDuplicated = isIdPhoneDuplicated;
	}

	public String getIsRequestPhoneUpdate() {
		return isRequestPhoneUpdate;
	}

	public void setIsRequestPhoneUpdate(String isRequestPhoneUpdate) {
		this.isRequestPhoneUpdate = isRequestPhoneUpdate;
	}

	public String getUseUpoint() {
		return useUpoint;
	}

	public void setUseUpoint(String useUpoint) {
		this.useUpoint = useUpoint;
	}

	public String getFingerPrintData() {
		return fingerPrintData;
	}

	public void setFingerPrintData(String fingerPrintData) {
		this.fingerPrintData = fingerPrintData;
	}

	public String getQrResult() {
		return qrResult;
	}

	public void setQrResult(String qrResult) {
		this.qrResult = qrResult;
	}

	public String getItemBonusX2Info() {
		return itemBonusX2Info;
	}

	public void setItemBonusX2Info(String itemBonusX2Info) {
		this.itemBonusX2Info = itemBonusX2Info;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getuPointBalance() {
		return uPointBalance;
	}

	public void setuPointBalance(Double uPointBalance) {
		this.uPointBalance = uPointBalance;
	}
}
