package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

public class UACheckInfoResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * { "ua_id": "5b0660bd6aa1451b33e9f32a", "result": 0, "mobile": "88080854",
	 * "message": "Success.", "balance": 0, "card_status": 0, "created_at":
	 * "2018-05-24T14:50:37.738000", "card_number": "140306001" }
	 */
	private String ua_id;
	private String card_number;
	private String mobile;
	private String balance;
	private String card_status;
	private String created_at;

	private String result;
	private String message;
	
	public String toJson() {
		return "{ \"result\": " + result + ", \"message\": \""+ message +"\", \"balance\": "+balance+", \"card_status\": "+card_status+", }";
	}

	public String getUa_id() {
		return ua_id;
	}

	public void setUa_id(String ua_id) {
		this.ua_id = ua_id;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCard_status() {
		return card_status;
	}

	public void setCard_status(String card_status) {
		this.card_status = card_status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
