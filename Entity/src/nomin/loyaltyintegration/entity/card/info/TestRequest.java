package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

public class TestRequest implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonbProperty("cardType")
	private String cardType;
	
	@JsonbProperty("cardNumber")
	private String cardNumber;
	
	@JsonbProperty("pinType")
	private String pinType;
	
	@JsonbProperty("pinCode")
	private String pinCode;
	
	@JsonbProperty("deviceId")
	private String deviceId;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getPinType() {
		return pinType;
	}

	public void setPinType(String pinType) {
		this.pinType = pinType;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
