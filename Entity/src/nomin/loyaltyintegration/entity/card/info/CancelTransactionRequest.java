package nomin.loyaltyintegration.entity.card.info;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CancelTransactionRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("locationKey")
	private Long locationKey;
	
	@JsonProperty("data")
	private String data;
	
	public Long getLocationKey() { return locationKey; }
	public void setLocationKey(Long locationKey) { this.locationKey = locationKey; }

	public String getData() { return data; }
	public void setData(String data) { this.data = data; }

}
