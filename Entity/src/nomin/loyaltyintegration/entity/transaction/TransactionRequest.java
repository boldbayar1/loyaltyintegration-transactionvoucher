package nomin.loyaltyintegration.entity.transaction;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.entity.login.DeviceInfo;
import nomin.loyaltyintegration.entity.payment.PaymentInfo;

public class TransactionRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("requestId")
	private String requestId;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("bill")
	private EBill bill;
	
	@JsonProperty("payments")
	private List<PaymentInfo> payments;
	
	@JsonProperty("device")
	private DeviceInfo device;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public EBill getBill() {
		return bill;
	}

	public void setBill(EBill bill) {
		this.bill = bill;
	}

	public List<PaymentInfo> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentInfo> payments) {
		this.payments = payments;
	}

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	} 
	
	
	

}
