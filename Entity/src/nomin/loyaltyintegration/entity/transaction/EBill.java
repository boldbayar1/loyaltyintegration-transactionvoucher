package nomin.loyaltyintegration.entity.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EBill implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("billNo")
	private int billNo;
	
	@JsonProperty("totalAmount")
	private BigDecimal totalAmount;
	
	@JsonProperty("vatBillNo")
	private String vatBillNo;
	
	@JsonProperty("vatAmount")
	private BigDecimal varAmount;
	
	@JsonProperty("ebarimtBillType")
	private int ebarimtBillType;
	
	@JsonProperty("customerNo")
	private String customerNo;
	
	@JsonProperty("sales")
	private List<ESale> sales;

	public int getBillNo() {
		return billNo;
	}

	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getVatBillNo() {
		return vatBillNo;
	}

	public void setVatBillNo(String vatBillNo) {
		this.vatBillNo = vatBillNo;
	}

	public BigDecimal getVarAmount() {
		return varAmount;
	}

	public void setVarAmount(BigDecimal varAmount) {
		this.varAmount = varAmount;
	}

	public int getEbarimtBillType() {
		return ebarimtBillType;
	}

	public void setEbarimtBillType(int ebarimtBillType) {
		this.ebarimtBillType = ebarimtBillType;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public List<ESale> getSales() {
		return sales;
	}

	public void setSales(List<ESale> sales) {
		this.sales = sales;
	}
	
	
	
}
