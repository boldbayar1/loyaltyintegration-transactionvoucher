package nomin.loyaltyintegration.entity.transaction;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ESale implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("itemId")
	private String itemId;
	
	@JsonProperty("quantity")
	private BigDecimal quantity;

	@JsonProperty("isWhole")
	private int isWhole;
	
	@JsonProperty("basePrice")
	private BigDecimal basePrice;
	
	@JsonProperty("price")
	private BigDecimal price;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("vatBillNo")
	private String vatBillNo;
	
	@JsonProperty("vatAmount")
	private BigDecimal vatAmount;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public int getIsWhole() {
		return isWhole;
	}

	public void setIsWhole(int isWhole) {
		this.isWhole = isWhole;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getVatBillNo() {
		return vatBillNo;
	}

	public void setVatBillNo(String vatBillNo) {
		this.vatBillNo = vatBillNo;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}
	
	
	
}
