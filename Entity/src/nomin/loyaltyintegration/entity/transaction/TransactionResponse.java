package nomin.loyaltyintegration.entity.transaction;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private int status;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("billKey")
	private String billKey;	

	public int getStatus() { return status; }
	public void setStatus(int status) { this.status = status; }
	
	public String getMessage() { return message; }
	public void setMessage(String message) { this.message = message; }
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }
	
	public String getBillKey() { return billKey; }
	public void setBillKey(String billKey) { this.billKey = billKey; }
	
}
