package nomin.loyaltyintegration.entity.transaction;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnTransactionRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("invoiceId")
	private String invoiceId;

	@JsonProperty("deviceId")
	private String deviceId;

	@JsonProperty("language")
	private String language;

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
