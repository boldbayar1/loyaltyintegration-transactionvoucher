package nomin.loyaltyintegration.entity.sales;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbProperty;

public class Sales implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonbProperty("itemId")
	private String itemId;
	
	@JsonbProperty("quantity")
	private BigDecimal quantity;
	
	@JsonbProperty("isWhole")
	private int isWhole;
	
	@JsonbProperty("basePrice")
	private BigDecimal basePrice;
	
	@JsonbProperty("price")
	private BigDecimal price;
	
	@JsonbProperty("amount")
	private BigDecimal amount;
	
	public String getItemId() { return itemId; }
	public void setPKey(String itemId) { this.itemId = itemId; }
	
	public BigDecimal getQuantity() { return quantity; }
	public void setQuantity(BigDecimal quantity) { this.quantity = quantity; }
	
	public int getIsWhole() { return isWhole; }
	public void setIsWhole(int isWhole) { this.isWhole = isWhole; }
	
	public BigDecimal getBasePrice() { return basePrice; }
	public void setBasePrice(BigDecimal basePrice) { this.basePrice = basePrice; }
	
	public BigDecimal getPrice() { return price; }
	public void setPrice(BigDecimal price) { this.price = price; }
	
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
}
