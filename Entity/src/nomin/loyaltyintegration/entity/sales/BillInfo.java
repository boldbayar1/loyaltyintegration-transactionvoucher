package nomin.loyaltyintegration.entity.sales;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BillInfo  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Double totalAmount;
	private List<SalesInfo> sales;
	
	public Double getTotalAmount() { return totalAmount; }
	public void setTotalAmount(Double totalAmount) { this.totalAmount = totalAmount; }
	
	public List<SalesInfo> getSales(){ return sales; }
	public void setSales(List<SalesInfo> sales) { this.sales = sales; }
	
	
}
