package nomin.loyaltyintegration.entity.sales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("totalAmount")
	private BigDecimal totalAmount;
	
	@JsonProperty("sales")
	private List<Sales> sales;

	public BigDecimal getTotalAmount() { return totalAmount; }
	public void setTotalAmount(BigDecimal totalAmount) { this.totalAmount = totalAmount; }
	
	public List<Sales> getSales(){ return sales; }
	public void setSales(List<Sales> sales) { this.sales = sales; }
	
}
