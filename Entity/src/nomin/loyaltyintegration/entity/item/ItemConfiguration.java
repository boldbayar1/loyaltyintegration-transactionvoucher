package nomin.loyaltyintegration.entity.item;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ItemConfiguration")
public class ItemConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PKey")
	private Long pkey;
	
	@Column(name="ItemKey")
	private Long itemKey;
	
	@Column(name="ItemId")
	private String itemId;
	
	@Column(name="ItemName")
	private String itemName;
	
	@Column(name="ItemShortName")
	private String itemShortName;
	
	@Column(name="BarcodeKey")
	private Long barcodeKey;
	
	@Column(name="IsVat")
	private Short isVat;
	
	@Column(name="UseBonus")
	private Short useBonus;
	
	@Column(name="HasBonus")
	private Short hasBonus;
	
	@Column(name="SectionKey")
	private Long sectionKey;
	
	@Column(name="SectionHasTaxed")
	private Short sectionHasTaxed;
	
	@Column(name="OrganizationKey")
	private Long organizationKey;
	
	@Column(name="LocationKey")
	private Long locationKey;
	
	@Column(name="CreatedKey")
	private Long createdKey;
	
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="ModifiedKey")
	private Long modifiedKey;
	
	@Column(name="ModifiedDate")
	private Date modifiedDate;

	public Long getPkey() {
		return pkey;
	}

	public void setPkey(Long pkey) {
		this.pkey = pkey;
	}

	public Long getItemKey() {
		return itemKey;
	}

	public void setItemKey(Long itemKey) {
		this.itemKey = itemKey;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemShortName() {
		return itemShortName;
	}

	public void setItemShortName(String itemShortName) {
		this.itemShortName = itemShortName;
	}

	public Short getIsVat() {
		return isVat;
	}

	public void setIsVat(Short isVat) {
		this.isVat = isVat;
	}

	public Short getUseBonus() {
		return useBonus;
	}

	public void setUseBonus(Short useBonus) {
		this.useBonus = useBonus;
	}

	public Short getHasBonus() {
		return hasBonus;
	}

	public void setHasBonus(Short hasBonus) {
		this.hasBonus = hasBonus;
	}

	public Long getSectionKey() {
		return sectionKey;
	}

	public void setSectionKey(Long sectionKey) {
		this.sectionKey = sectionKey;
	}

	public Short getSectionHasTaxed() {
		return sectionHasTaxed;
	}

	public void setSectionHasTaxed(Short sectionHasTaxed) {
		this.sectionHasTaxed = sectionHasTaxed;
	}

	public Long getOrganizationKey() {
		return organizationKey;
	}

	public void setOrganizationKey(Long organizationKey) {
		this.organizationKey = organizationKey;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getCreatedKey() {
		return createdKey;
	}

	public void setCreatedKey(Long createdKey) {
		this.createdKey = createdKey;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getModifiedKey() {
		return modifiedKey;
	}

	public void setModifiedKey(Long modifiedKey) {
		this.modifiedKey = modifiedKey;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
	
}
