package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import nomin.loyaltyintegration.entity.login.DeviceInfo;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequestChange implements Serializable{	
	private static final long serialVersionUID = 1L;

	@JsonProperty("invoiceIdChange")
	private String invoiceIdChange;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("cardType")
	private String cardType;
		
	@JsonProperty("device")
	private DeviceInfo device;

	@JsonProperty("requestId")
	private String requestId;
	
	@JsonProperty("payments")
	private List<PaymentInfoNext> payments;
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }

	public String getCardType() { return cardType; }
	public void setCardType(String cardType) { this.cardType = cardType; }
	
	public DeviceInfo getDevice() { 
		if(device ==null)
			device = new DeviceInfo();
		return device; 
	}
	public void setDevice(DeviceInfo device) { this.device = device; }
	
	public String getInvoiceIdChange() { return invoiceIdChange; }
	public void setInvoiceIdChange(String invoiceIdChange) { this.invoiceIdChange = invoiceIdChange; }
	
	public String getRequestId() { return requestId; }
	public void setRequestId(String requestId) { this.requestId = requestId; }

	public List<PaymentInfoNext> getPayments() {
		if(payments == null)
			payments = new ArrayList<>();
		return payments; 
	}
	public void setPayments(List<PaymentInfoNext> payments) { this.payments = payments; }
	
	
}
