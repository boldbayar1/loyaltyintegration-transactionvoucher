package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.user.User;
import nomin.loyaltyintegration.entity.bill.BillRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.login.DeviceInfo;

public class CalcPaymentRequest implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("requestId")
	private String requestId;

	@JsonProperty("qrData")
	private String qrData;

	@JsonProperty("card")
	private CardInfoRequest card;

	@JsonProperty("bill")
	private BillRequest bill;

	@JsonProperty("user")
	private User user;

	@JsonProperty("device")
	private DeviceInfo device;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public CardInfoRequest getCard() {
		if (card == null)
			card = new CardInfoRequest();
		return card;
	}

	public void setCard(CardInfoRequest card) {
		this.card = card;
	}

	public BillRequest getBill() {
		return bill;
	}

	public void setBill(BillRequest bill) {
		this.bill = bill;
	}

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

	public User getUser() {
		if (user == null)
			user = new User();
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}
}
