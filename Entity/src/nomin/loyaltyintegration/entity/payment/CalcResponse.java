package nomin.loyaltyintegration.entity.payment;
import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CalcResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("cardUaId")
	private String cardUaId;
	
	@JsonProperty("billKey")
	private Long billKey;
	
	@JsonProperty("locationKey")
	private Long locationKey;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("paymentAmount")
	private BigDecimal paymentAmount;
	
	@JsonProperty("paymentVoucher")
	private BigDecimal paymentVoucher;
	
	@JsonProperty("paymentUpoint")
	private BigDecimal paymentUpoint;
	
	@JsonProperty("paymentBonus")
	private BigDecimal paymentBonus;
	
	@JsonProperty("addBonus")
	private BigDecimal addBonus;
	
	@JsonProperty("addUpoint")
	private BigDecimal addUpoint;
	
	@JsonProperty("balanceBonus")
	private BigDecimal balanceBonus;
	
	@JsonProperty("balanceUpoint")
	private BigDecimal balanceUpoint;

	public String getCardUaId() { return cardUaId; }
	public void setCardUaId(String cardUaId) { this.cardUaId = cardUaId; }
	
	public Long getBillKey() { return billKey; }
	public void setBillKey(Long billKey) { this.billKey = billKey; }
	
	public Long getLocationKey() { return locationKey; }
	public void setLocationKey(Long locationKey) { this.locationKey = locationKey; }
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public BigDecimal getPaymentVoucher() { return paymentVoucher; }
	public void setPaymentVoucher(BigDecimal paymentVoucher) { this.paymentVoucher = paymentVoucher; } 

	public BigDecimal getPaymentUpoint() {
		return paymentUpoint;
	}

	public void setPaymentUpoint(BigDecimal paymentUpoint) {
		this.paymentUpoint = paymentUpoint;
	}

	public BigDecimal getPaymentBonus() {
		return paymentBonus;
	}

	public void setPaymentBonus(BigDecimal paymentBonus) {
		this.paymentBonus = paymentBonus;
	}

	public BigDecimal getAddBonus() {
		return addBonus;
	}

	public void setAddBonus(BigDecimal addBonus) {
		this.addBonus = addBonus;
	}

	public BigDecimal getAddUpoint() {
		return addUpoint;
	}

	public void setAddUpoint(BigDecimal addUpoint) {
		this.addUpoint = addUpoint;
	}
	
	public BigDecimal getBalanceBonus() { return balanceBonus; }
	public void setBalanceBonus(BigDecimal balanceBonus) { this.balanceBonus = balanceBonus; }
	
	public BigDecimal getBalanceUpoint() { return balanceUpoint; }
	public void setBalanceUpoint(BigDecimal balanceUpoint) { this.balanceUpoint = balanceUpoint; }

}
