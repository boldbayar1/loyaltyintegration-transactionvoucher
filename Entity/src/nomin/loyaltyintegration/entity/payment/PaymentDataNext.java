package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDataNext implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("transactionDate")
	private String transactionDate; 
	
	@JsonProperty("cardNo")
	private Long cardNo;
	
	@JsonProperty("referenceNo")
	private Long referenceNo;
	
	@JsonProperty("traceNo")
	private Long traceNo;
	
	@JsonProperty("batchNo")
	private Long batchNo;
	
	@JsonProperty("idTerminal")
	private String terminalId;
	
	@JsonProperty("invoiceInfo")
	private String invoiceInfo;
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	public String getTransactionDate() { return transactionDate; }
	public void setTransactionDate(String transactionDate) { this.transactionDate = transactionDate; }
	
	public Long getCardNo() { return cardNo; }
	public void setCardNo(Long cardNo) { this.cardNo = cardNo; }
	
	public Long getReferenceNo() { return referenceNo; }
	public void setReferenceNo(Long referenceNo) { this.referenceNo = referenceNo; }
	
	public Long getTraceNo() { return traceNo; }
	public void setTraceNo(Long traceNo) { this.traceNo = traceNo; }
	
	public Long getBatchNo() { return batchNo; }
	public void setBatchNo(Long batchNo) {this.batchNo = batchNo;}
	
	public String getTerminalId() { return terminalId; }
	public void setTerminalId(String terminalId) { this.terminalId = terminalId; }
	
	public String getInvoiceInfo() { return invoiceInfo; }
	public void setInvoiceInfo(String invoiceInfo) { this.invoiceInfo = invoiceInfo; }

}
