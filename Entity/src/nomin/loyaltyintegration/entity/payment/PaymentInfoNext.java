package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentInfoNext implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("data")
	private PaymentDataNext data;

	public PaymentInfoNext() {
		super();
	}

	public PaymentInfoNext(String type, BigDecimal amount, PaymentDataNext data) {
		super();
		this.type = type;
		this.amount = amount;
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public PaymentDataNext getData() {
		return data;
	}

	public void setData(PaymentDataNext data) {
		this.data = data;
	}
	
}
