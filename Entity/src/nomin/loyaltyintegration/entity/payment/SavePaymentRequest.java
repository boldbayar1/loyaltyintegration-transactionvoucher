package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import nomin.loyaltyintegration.entity.bill.BillDetailRequest;

public class SavePaymentRequest implements Serializable{
private static final long serialVersionUID = 1L;
	
	@JsonProperty("ttd")
	private String ttd;
	
	@JsonProperty("transactionDate")
	private String transactionDate;
	
	@JsonProperty("transactionEbarimt")
	private String transactionEbarimt;
	
	@JsonProperty("posName")
	private String posName; 
	
	@JsonProperty("posId")
	private String posId;

	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("count")
	private String count;
	
	@JsonProperty("vat")
	private String vat;
	
	@JsonProperty("amountVat")
	private String amountVat;
	
	@JsonProperty("pay")
	private String pay;
	
	@JsonProperty("bank")
	private String bank;
	
	@JsonProperty("cash")
	private String cash;
	
	@JsonProperty("response")
	private String response;
	
	@JsonProperty("ddtd")
	private String ddtd;
	
	@JsonProperty("lotteryAmount")
	private String lotteryAmount;
	
	@JsonProperty("qrCode")
	private String qrCode;
	
	@JsonProperty("billDetail")
	private List<BillDetailRequest> billDetail;

	public String getTtd() { return ttd; }
	public void setTtd(String ttd) { this.ttd = ttd;}

	public String getTransactionDate() { return transactionDate; }
	public void setTransactionDate(String transactionDate) {this.transactionDate = transactionDate; }

	public String getTransactionEbarimt() {	return transactionEbarimt; }
	public void setTransactionEbarimt(String transactionEbarimt) { this.transactionEbarimt = transactionEbarimt; }

	public String getPosName() { return posName; }
	public void setPosName(String posName) { this.posName = posName; }

	public String getPosId() { return posId; }
	public void setPosId(String posId) { this.posId = posId; }

	public String getAmount() {	return amount; }
	public void setAmount(String amount) { this.amount = amount; }

	public String getCount() { return count; }
	public void setCount(String count) { this.count = count; }

	public String getVat() { return vat; }
	public void setVat(String vat) { this.vat = vat; }

	public String getAmountVat() {return amountVat; }
	public void setAmountVat(String amountVat) { this.amountVat = amountVat; }

	public String getPay() { return pay; }
	public void setPay(String pay) { this.pay = pay; }

	public String getBank() { return bank; }
	public void setBank(String bank) { this.bank = bank; }

	public String getCash() { return cash; }
	public void setCash(String cash) { this.cash = cash; }

	public String getResponse() { return response; }
	public void setResponse(String response) { this.response = response; }

	public String getDdtd() { return ddtd; }
	public void setDdtd(String ddtd) { this.ddtd = ddtd; }

	public String getLotteryAmount() { return lotteryAmount; }
	public void setLotteryAmount(String lotteryAmount) { this.lotteryAmount = lotteryAmount; }

	public String getQrCode() { return qrCode; }
	public void setQrCode(String qrCode) { this.qrCode = qrCode; }

	public List<BillDetailRequest> getBillDetail() { return billDetail; }
	public void setBillDetail(List<BillDetailRequest> billDetail) { this.billDetail = billDetail;}	
	
}
