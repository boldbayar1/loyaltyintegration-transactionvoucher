package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.entity.posapi.PutBill;

public class SaveMobileRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("userKey")
	private String userKey;

	@JsonProperty("group")
	private boolean group;

	@JsonProperty("vat")
	private String vat;

	@JsonProperty("amount")
	private String amount;

	@JsonProperty("billType")
	private String billType;

	@JsonProperty("billIdSuffix")
	private String billIdSuffix;

	@JsonProperty("posNo")
	private String posNo;

	@JsonProperty("bills")
	private List<PutBill> bills;

	public boolean getGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public List<PutBill> getBills() {
		return bills;
	}

	public void setBills(List<PutBill> bills) {
		this.bills = bills;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

}
