package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Voucher implements Serializable{	
	private static final long serialVersionUID = 1L;

	@JsonProperty("voucherCode")
	private String voucherCode;
	
	@JsonProperty("idLocation")
	private String idLocation;
	
	@JsonProperty("xml")
	private String xml;
	
	@JsonProperty("billKey")
	private Long billKey;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("language")
	private String language;

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getIdLocation() {
		return idLocation;
	}

	public void setIdLocation(String idLocation) {
		this.idLocation = idLocation;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public Long getBillKey() {
		return billKey;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
