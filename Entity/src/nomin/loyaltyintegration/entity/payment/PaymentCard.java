package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentCard  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("spendBonus")
	private Double spendBonus; 
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	public Double getSpendBonus() { return spendBonus; }
	public void setSpendBonus(Double spendBonus) { this.spendBonus = spendBonus; }
}