package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import nomin.core.entity.businessentity.rest.BaseResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MobilePaymentResponse extends BaseResponse implements Serializable{
private static final long serialVersionUID = 1L;
	
	@JsonProperty("ttd")
	private String ttd;
	
	@JsonProperty("date")
	private String date1;
	
	@JsonProperty("time")
	private String time;
	
	@JsonProperty("posName")
	private String posName;
	
	@JsonProperty("posId")
	private String posId;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("count")
	private String count;
	
	@JsonProperty("amountVat")
	private String amountVat;
	
	@JsonProperty("pay")
	private String pay;
	
	@JsonProperty("bank")
	private String bank;
	
	@JsonProperty("cash")
	private String cash;
	
	@JsonProperty("response")
	private String response;
	
	// ----- Ebarimt ----- 
	@JsonProperty("ddtd")
	private String ddtd;
	
	@JsonProperty("lottery")
	private String lottery;
		
	@JsonProperty("lotteryAmount")
	private String lotteryAmount;
	
	@JsonProperty("qrCode")
	private String qrCode;
	
	// ----- Bill Item ----- 
	@JsonProperty("BillItem")
	private MobileBillItem MobileBillItem;

	public String getTtd() {
		return ttd;
	}

	public void setTtd(String ttd) {
		this.ttd = ttd;
	}


	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getDdtd() {
		return ddtd;
	}

	public void setDdtd(String ddtd) {
		this.ddtd = ddtd;
	}

	public String getLottery() {
		return lottery;
	}

	public void setLottery(String lottery) {
		this.lottery = lottery;
	}

	public String getLotteryAmount() {
		return lotteryAmount;
	}

	public void setLotteryAmount(String lotteryAmount) {
		this.lotteryAmount = lotteryAmount;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public MobileBillItem getMobileBillItem() {
		return MobileBillItem;
	}

	public void setMobileBillItem(MobileBillItem mobileBillItem) {
		MobileBillItem = mobileBillItem;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getAmountVat() {
		return amountVat;
	}

	public void setAmountVat(String amountVat) {
		this.amountVat = amountVat;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getCash() {
		return cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}		
	
}
