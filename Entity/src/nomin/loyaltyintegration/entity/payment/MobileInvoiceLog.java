package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MobileInvoiceLog")
public class MobileInvoiceLog implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PKey")
	private Long pKey;
	
	@Column(name="InvoiceId")
	private String InvoiceId;
	
	@Column(name="Json")
	private String Json;       
	
	@Column(name="Confirmed")
	private int Confirmed;
	
	@Column(name="Type")  // TULUV. 0 - BUTSAASAN ZAHIALGA 2-BATALSAN ZAHIALGA 1-NOOROG ZAHIALGA 
	private String Type;
	
	@Column(name="InvoiceDate")
	@Temporal(TemporalType.DATE)
	private Date InvoiceDate;
	
	@Column(name="ModifiedKey")
	private Long ModifiedKey;
	
	@Column(name="ModifiedDate")
	@Temporal(TemporalType.DATE)
	private Date ModifiedDate;

	public Long getpKey() {
		return pKey;
	}

	public void setpKey(Long pKey) {
		this.pKey = pKey;
	}

	public String getInvoiceId() {
		return InvoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		InvoiceId = invoiceId;
	}

	public String getJson() {
		return Json;
	}

	public void setJson(String json) {
		Json = json;
	}

	public int getConfirmed() {
		return Confirmed;
	}

	public void setConfirmed(int confirmed) {
		Confirmed = confirmed;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public Date getInvoiceDate() {
		return InvoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		InvoiceDate = invoiceDate;
	}

	public Long getModifiedKey() {
		return ModifiedKey;
	}

	public void setModifiedKey(Long modifiedKey) {
		ModifiedKey = modifiedKey;
	}

	public Date getModifiedDate() {
		return ModifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		ModifiedDate = modifiedDate;
	}

}