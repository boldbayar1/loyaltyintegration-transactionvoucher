package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;


public class PaymentRequestLog implements Serializable{	
	
	private static final long serialVersionUID = 1L;
	
	private PaymentRequest request;
	
	private Long BillKey;
	
	private String paymentRawData;
	
	private String transactionResponse;
	
	private String invoiceId;

	public PaymentRequest getRequest() {
		return request;
	}

	public void setRequest(PaymentRequest request) {
		this.request = request;
	}

	public Long getBillKey() {
		return BillKey;
	}

	public void setBillKey(Long billKey) {
		BillKey = billKey;
	}

	public String getPaymentRawData() {
		return paymentRawData;
	}

	public void setPaymentRawData(String paymentRawData) {
		this.paymentRawData = paymentRawData;
	}

	public String getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(String transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

}
