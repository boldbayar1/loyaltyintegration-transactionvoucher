package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;
import nomin.loyaltyintegration.entity.bill.BillRequest;
import nomin.loyaltyintegration.entity.login.DeviceInfo;

public class CalcPaymentRequestExchange implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("invoiceIdChange")
	private String invoiceIdChange;
	
	@JsonProperty("requestId")
	private String requestId;
	
	@JsonProperty("bill")
	private BillRequest bill;
	
	@JsonProperty("device")
	private DeviceInfo deviceInfo; 
	
	public String getRequestId() { return requestId; }
	public void setRequestId(String requestId) { this.requestId = requestId; }
		
	public BillRequest getBillInfo() { return bill; }
	public void setBillInfo(BillRequest bill) { this.bill = bill; }
	
	public String getInvoiceIdChange() { return invoiceIdChange; }
	public void setInvoiceIdChange(String invoiceIdChange) { this.invoiceIdChange = invoiceIdChange; }
	
	public DeviceInfo getDeviceInfo() { return deviceInfo; }
	public void setDeviceInfo(DeviceInfo deviceInfo) { this.deviceInfo = deviceInfo; }
	
	public BillRequest getBill() { return bill; }
	public void setBill(BillRequest bill) { this.bill = bill; }
	
	
}
