package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CalcPaymentResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private int status;
	
	@JsonProperty("data")
	private String data;
	
	@JsonProperty("invoiceId")
	private String invoiceid;
	
	public String getData() { return data; }
	public void setData(String data) { this.data = data; }
	
	public int getStatus() { return status; }
	public void setStatus(int status) { this.status = status; }
	
	public String getInvoiceid() { return invoiceid; }
	public void setInvoiceid(String invoiceid) { this.invoiceid = invoiceid; }
	
	
}
