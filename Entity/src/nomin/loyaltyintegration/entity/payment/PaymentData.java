package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
//	@JsonSerialize(using = nomin.core.utils.serializer.CustomLocalDateTimeSerializer.class)
	@JsonProperty("transactionDate")
//	@Temporal(TemporalType.TIMESTAMP)
	private String transactionDate; 
	
	@JsonProperty("cardNo")
	private String cardNo;
	
	@JsonProperty("referenceNo")
	private String referenceNo;
	
	@JsonProperty("traceNo")
	private String traceNo;
	
	@JsonProperty("batchNo")
	private String batchNo;
	
	@JsonProperty("idTerminal")
	private String terminalId;
	
	@JsonProperty("invoiceInfo")
	private String invoiceInfo;
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	public String getTransactionDate() { return transactionDate; }
	public void setTransactionDate(String transactionDate) { this.transactionDate = transactionDate; }
	
	public String getCardNo() { return cardNo; }
	public void setCardNo(String cardNo) { this.cardNo = cardNo; }
	
	public String getReferenceNo() { return referenceNo; }
	public void setReferenceNo(String referenceNo) { this.referenceNo = referenceNo; }
	
	public String getTraceNo() { return traceNo; }
	public void setTraceNo(String traceNo) { this.traceNo = traceNo; }
	
	public String getBatchNo() { return batchNo; }
	public void setBatchNo(String batchNo) {this.batchNo = batchNo;}
	
	public String getTerminalId() { return terminalId; }
	public void setTerminalId(String terminalId) { this.terminalId = terminalId; }
	
	public String getInvoiceInfo() { return invoiceInfo; }
	public void setInvoiceInfo(String invoiceInfo) { this.invoiceInfo = invoiceInfo; }

}
