package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private int status;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	public String getMessage() { return message; }
	public void setMessage(String message) { this.message = message; }
	
	public int getStatus() { return status; }
	public void setStatus(int status) { this.status = status; }
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }
	

}
