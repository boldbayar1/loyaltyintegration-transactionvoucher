package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import nomin.loyaltyintegration.entity.login.DeviceInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("invoiceId")
	private String invoiceId;

	@JsonProperty("requestId")
	private String requestId;

	@JsonProperty("cardType")
	private String cardType;

	@JsonProperty("payments")
	private List<PaymentInfo> payments;

	@JsonProperty("voucherPayments")
	private List<Voucher> voucherPayments;

	@JsonProperty("device")
	private DeviceInfo device;

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public List<PaymentInfo> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentInfo> payments) {
		this.payments = payments;
	}

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

	public List<Voucher> getVoucherPayments() {
		if(voucherPayments == null)
			voucherPayments = new ArrayList<>();
		return voucherPayments;
	}

	public void setVoucherPayments(List<Voucher> voucherPayments) {
		this.voucherPayments = voucherPayments;
	}
}
