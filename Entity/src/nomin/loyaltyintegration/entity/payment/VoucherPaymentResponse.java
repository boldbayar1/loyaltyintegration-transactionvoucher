package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Cacheable(false)
public class VoucherPaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "VOUCHERCODE")
	private String voucherCode;

	@Column(name = "VOUCHERTYPE")
	private String voucherType;

	@Column(name = "VOUCHERAMOUNT")
	private String voucherAmount;
	
	@Column(name = "VOUCHERPERCENT")
	private String voucherPercent;

	@Id
	@Column(name = "AVAILABLECOUNT")
	private String availableCount;
	
	@Column(name = "ID_ITEM")
	private String idItem;
	
	@Column(name = "SALEITEMBARCODE")
	private String saleItemBarcode;
	
	@Column(name = "PRICE")
	private String price;
	
	@Column(name = "SUSPENDEDCOUNT")
	private String suspendedCount;
	
	@Column(name = "USEDAMOUNT")
	private String usedAmount;
	
	@Transient
	private String message;
	
	@Transient
	private Integer status;

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getSaleItemBarcode() {
		return saleItemBarcode;
	}

	public void setSaleItemBarcode(String saleItemBarcode) {
		this.saleItemBarcode = saleItemBarcode;
	}

	public String getMessage() {
		if(message == null)
			message = "NO Message";
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		if(status == null)
			status = 0;
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getVoucherAmount() {
		return voucherAmount;
	}

	public void setVoucherAmount(String voucherAmount) {
		this.voucherAmount = voucherAmount;
	}

	public String getVoucherPercent() {
		return voucherPercent;
	}

	public void setVoucherPercent(String voucherPercent) {
		this.voucherPercent = voucherPercent;
	}

	public String getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(String availableCount) {
		this.availableCount = availableCount;
	}

	public String getIdItem() {
		return idItem;
	}

	public void setIdItem(String idItem) {
		this.idItem = idItem;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSuspendedCount() {
		return suspendedCount;
	}

	public void setSuspendedCount(String suspendedCount) {
		this.suspendedCount = suspendedCount;
	}

	public String getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(String usedAmount) {
		this.usedAmount = usedAmount;
	}
}
