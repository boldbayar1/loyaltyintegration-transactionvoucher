package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("data")
	private PaymentData data;
	
	@JsonProperty("invoiceId")
	private String invoiceId;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public PaymentData getData() {
		return data;
	}

	public void setData(PaymentData data) {
		this.data = data;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
}
