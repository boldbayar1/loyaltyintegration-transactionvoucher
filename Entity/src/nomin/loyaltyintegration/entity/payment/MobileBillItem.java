package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MobileBillItem implements Serializable{
private static final long serialVersionUID = 1L;
	
	@JsonProperty("sectionID")
	private String sectionID;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("qty")
	private String qty;
	
	@JsonProperty("price")
	private String price;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("isVat") // 1 - татвартай бараа 0 - татваргүй бараа
	private String isVat;
		
	public String getSectionID() {
		return sectionID;
	}

	public void setSectionID(String sectionID) {
		this.sectionID = sectionID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIsVat() {
		return isVat;
	}

	public void setIsVat(String isVat) {
		this.isVat = isVat;
	}
	
}
