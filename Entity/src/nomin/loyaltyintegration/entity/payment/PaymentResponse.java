package nomin.loyaltyintegration.entity.payment;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.core.entity.businessentity.rest.BaseResponse;
import nomin.loyaltyintegration.entity.bill.BillResponse;
import nomin.loyaltyintegration.entity.card.info.CardTypeResponse;

public class PaymentResponse extends BaseResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("cardType")
	private CardTypeResponse cardType;
	
	@JsonProperty("bill")
	private BillResponse bill;
	
	public String getInvoiceId() { return invoiceId; }
	public void setInvoiceId(String invoiceId) { this.invoiceId = invoiceId; }
	
	public CardTypeResponse getCardType() { return cardType; }
	public void setCardType(CardTypeResponse cardType) { this.cardType = cardType; }
	
	public BillResponse getBill() { return bill; }
	public void setBill(BillResponse bill) { this.bill = bill; }
	
}
