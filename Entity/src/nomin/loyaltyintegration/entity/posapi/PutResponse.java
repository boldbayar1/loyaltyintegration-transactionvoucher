package nomin.loyaltyintegration.entity.posapi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PutResponse {
	@JsonProperty
	private boolean group;
	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private BigDecimal vat;
	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private BigDecimal amount;
	@JsonProperty
	private String billType;
	@JsonProperty
	private String billIdSuffix;
	@JsonProperty
	private String posNo;
	@JsonProperty
	private String taxType;
	@JsonProperty
	private String customerNo;
	@JsonProperty
	private String billId;
	@JsonProperty
	private String lottery;
	@JsonProperty
	private String lotteryWarningMsg;
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = nomin.loyaltyintegration.helper.DateAsStringDeserializer.class)
	private Date date;
	@JsonProperty
	private String macAddress;
	@JsonProperty
	private String internalCode;
	@JsonProperty
	private String qrData;
	@JsonProperty
	private String merchantId;
	@JsonProperty
	private String success;
	@JsonProperty("bills")
	private List<PutGroupResponseBill> lstGroup;
	@JsonProperty
	private String message;
	@JsonProperty
	private Integer errorCode;

	public boolean isGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getLottery() {
		return lottery;
	}

	public void setLottery(String lottery) {
		this.lottery = lottery;
	}

	public String getLotteryWarningMsg() {
		return lotteryWarningMsg;
	}

	public void setLotteryWarningMsg(String lotteryWarningMsg) {
		this.lotteryWarningMsg = lotteryWarningMsg;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getInternalCode() {
		return internalCode;
	}

	public void setInternalCode(String internalCode) {
		this.internalCode = internalCode;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public List<PutGroupResponseBill> getLstGroup() {
		return lstGroup;
	}

	public void setLstGroup(List<PutGroupResponseBill> lstGroup) {
		this.lstGroup = lstGroup;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
}