package nomin.loyaltyintegration.entity.posapi;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import nomin.loyaltyintegration.enums.BillType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PutGroup implements PutRequest {

	@JsonProperty
	private boolean group;
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal vat;
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal amount;
	@JsonProperty
	private BillType billType;
	@JsonProperty
	private String billIdSuffix;
	@JsonProperty
	private String posNo;
	@JsonProperty("bills")
	private List<PutGroupBill> lstGroup;

	public boolean getGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BillType getBillType() {
		return billType;
	}

	public void setBillType(BillType billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix == null ? "" : billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public List<PutGroupBill> getLstGroup() {
		return lstGroup;
	}

	public void setLstGroup(List<PutGroupBill> lstGroup) {
		this.lstGroup = lstGroup;
	}
}
