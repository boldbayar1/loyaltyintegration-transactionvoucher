package nomin.loyaltyintegration.entity.posapi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.enums.BillType;
import nomin.loyaltyintegration.enums.TaxType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PutBill implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("vat")
	private BigDecimal vat;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("cashAmount")
	private BigDecimal cashAmount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("nonCashAmount")
	private BigDecimal nonCashAmount;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("cityTax")
	private BigDecimal cityTax;
	
	@JsonProperty("districtCode")
	private String districtCode;
	
	@JsonProperty("posNo")
	private String posNo;
	
	@JsonProperty("customerNo")
	private String customerNo;
	
	@JsonProperty("billType")
	private BillType billType;
	
	@JsonProperty("billIdSuffix")
	private String billIdSuffix;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty("returnBillId")
	private String returnBillId;
	
	@JsonProperty("taxType")
	private TaxType taxType;
	
	@JsonProperty("stocks")
	private List<Stock> stocks;
	
	@JsonProperty("internalId")
	private String internalId;
	
	@JsonProperty("registerNo")
	private String registerNo;
	
	@JsonProperty("billId")
	private String billId;
	
	@JsonProperty("macAddress")
	private String macAddress;
	
	@JsonProperty("date")
	private String date;
	
	@JsonProperty("lottery")
	private String lottery;
	
	@JsonProperty("qrData")
	private String qrData;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public BigDecimal getNonCashAmount() {
		return nonCashAmount;
	}

	public void setNonCashAmount(BigDecimal nonCashAmount) {
		this.nonCashAmount = nonCashAmount;
	}

	public BigDecimal getCityTax() {
		return cityTax;
	}

	public void setCityTax(BigDecimal cityTax) {
		this.cityTax = cityTax;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public BillType getBillType() {
		return billType;
	}

	public void setBillType(BillType billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getReturnBillId() {
		return returnBillId;
	}

	public void setReturnBillId(String returnBillId) {
		this.returnBillId = returnBillId;
	}

	public TaxType getTaxType() {
		return taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getRegisterNo() {
		return registerNo;
	}

	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLottery() {
		return lottery;
	}

	public void setLottery(String lottery) {
		this.lottery = lottery;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}
	
}
