package nomin.loyaltyintegration.entity.posapi;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock {

	@JsonProperty
	private String code;
	@JsonProperty
	private String name;
	@JsonProperty
	private String measureUnit;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal qty;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal unitPrice;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal totalAmount;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal cityTax;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty
	private BigDecimal vat;

	@JsonProperty
	private String barCode;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getCityTax() {
		return cityTax;
	}

	public void setCityTax(BigDecimal cityTax) {
		this.cityTax = cityTax;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public String getBarCode() {
		return barCode == null ? "" : barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
}
