package nomin.loyaltyintegration.entity.pos;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
@Cacheable(false)
public class PosModBillResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="Code")
	private int Code;
	
	@Column(name="Message")
	private String Message;
	
	public int getCode() { return Code; }
	public void setCode(int Code) { this.Code = Code; }
	
	public String getMessage() { return Message; }
	public void setMessage(String Message) { this.Message = Message; }

}
