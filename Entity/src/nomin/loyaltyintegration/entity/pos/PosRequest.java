package nomin.loyaltyintegration.entity.pos;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PosRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("billKey")
	private long billKey;
	
	@JsonProperty("locationKey")
	private long locationKey;
	
	@JsonProperty("posKey")
	private long posKey;
	
	@JsonProperty("cashierKey")
	private long cashierKey;
	
	@JsonProperty("data")
	private String data;

	public long getBillKey() {
		return billKey;
	}

	public void setBillKey(long billKey) {
		this.billKey = billKey;
	}

	public long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(long locationKey) {
		this.locationKey = locationKey;
	}

	public long getPosKey() {
		return posKey;
	}

	public void setPosKey(long posKey) {
		this.posKey = posKey;
	}

	public long getCashierKey() {
		return cashierKey;
	}

	public void setCashierKey(long cashierKey) {
		this.cashierKey = cashierKey;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}

