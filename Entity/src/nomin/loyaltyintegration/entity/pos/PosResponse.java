package nomin.loyaltyintegration.entity.pos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.core.entity.businessentity.rest.BaseResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PosResponse extends BaseResponse {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@JsonProperty("result")
	private int result;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}