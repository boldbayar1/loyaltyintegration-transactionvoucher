package nomin.loyaltyintegration.entity.login;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.core.entity.businessentity.rest.BaseResponse;
import nomin.loyaltyintegration.entity.bill.BillResponse;
import nomin.loyaltyintegration.entity.card.info.CardType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse extends BaseResponse {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("invoiceId")
	private String invoiceId; 

	@JsonProperty("cardTypes")
	private List<CardType> cardTypes;
	
	@JsonProperty("bills")
	private List<BillResponse> bills;
	
	public String getInvoiceId() {
		return invoiceId;
	}
	
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	public List<CardType> getCardTypes(){
		return cardTypes;
	}
	
	public void setCardTypes(List<CardType> cardTypes) {
		this.cardTypes = cardTypes;
	}

	public List<BillResponse> getBills() {
		return bills;
	}

	public void setBills(List<BillResponse> bills) {
		this.bills = bills;
	}
	
	
	
}
