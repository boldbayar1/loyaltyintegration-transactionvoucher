package nomin.loyaltyintegration.entity.login;

import java.io.Serializable;
import javax.json.bind.annotation.JsonbProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceInfo implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("deviceId")
	private String deviceId;

	@JsonbProperty("language")
	private String language;
	
	@JsonbProperty("OrganizationKey")
	private String organizationKey;
	
	@JsonbProperty("PosKey")
	private String posKey;
	
	@JsonbProperty("IsProduction")
	private Integer production;
	
	@JsonbProperty("OrganizationType")
	private String organizationType;
	
	@JsonbProperty("Key")
	private String key;
	
	@JsonbProperty("phoneNumber")
	private String phoneNumber;
	
	@JsonbProperty("email")
	private String email;
	
	@JsonbProperty("noCardContinue")
	private Integer noCardContinue;
	
	@JsonbProperty("type")
	private String type;
	
	@JsonbProperty("ebarimtBillType")
	private Integer ebarimtBillType;
	
	@JsonbProperty("customerNo")
	private Long customerNo;

	public DeviceInfo() {
		super();
	}
	
	public DeviceInfo(String deviceId, String language, String organizationKey, String posKey, Integer production, String organizationType, String key, String phoneNumber, String email, Integer noCardContinue, String type, Integer ebarimtBillType,
			Long customerNo) {
		super();
		this.deviceId = deviceId;
		this.language = language;
		this.organizationKey = organizationKey;
		this.posKey = posKey;
		this.production = production;
		this.organizationType = organizationType;
		this.key = key;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.noCardContinue = noCardContinue;
		this.type = type;
		this.ebarimtBillType = ebarimtBillType;
		this.customerNo = customerNo;
	}

	//Default version of clone() method.
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOrganizationKey() {
		return organizationKey;
	}

	public void setOrganizationKey(String organizationKey) {
		this.organizationKey = organizationKey;
	}

	public String getPosKey() {
		return posKey;
	}

	public void setPosKey(String posKey) {
		this.posKey = posKey;
	}

	public Integer getProduction() {
		return production;
	}

	public void setProduction(Integer production) {
		this.production = production;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getNoCardContinue() {
		if(noCardContinue == null)
			noCardContinue = 1;
		return noCardContinue;
	}

	public void setNoCardContinue(Integer noCardContinue) {
		this.noCardContinue = noCardContinue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getEbarimtBillType() {
		return ebarimtBillType;
	}

	public void setEbarimtBillType(Integer ebarimtBillType) {
		this.ebarimtBillType = ebarimtBillType;
	}

	public Long getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(Long customerNo) {
		this.customerNo = customerNo;
	}
}
