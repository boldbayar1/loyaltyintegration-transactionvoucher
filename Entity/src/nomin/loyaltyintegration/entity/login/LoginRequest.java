package nomin.loyaltyintegration.entity.login;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.entity.bill.BillRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("requestId")
	private String requestId;
	
	@JsonProperty("card")
	private CardInfoRequest card;
	
	@JsonProperty("bill")
	private BillRequest bill;
	
	@JsonProperty("device")
	private DeviceInfo device; 
	
	public String getRequestId() { return requestId; }
	public void setRequestId(String requestId) { this.requestId = requestId; }
	
	public CardInfoRequest getCard() { return card; }
	public void setCard(CardInfoRequest card) { this.card = card; }
	
	public BillRequest getBillInfo() { return bill; }
	public void setBillInfo(BillRequest bill) { this.bill = bill; }

	public DeviceInfo getDevice() { return device; }
	public void setDevice(DeviceInfo device) { this.device = device; }
}
