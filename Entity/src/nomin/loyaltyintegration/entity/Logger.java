package nomin.loyaltyintegration.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.enums.Language;

@Entity
@Table(name = "Logger")
public class Logger implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PKey")
	private Long pKey;

	@Column(name = "DeviceId")
	private String deviceId;

	@Column(name = "InvoiceId")
	private String invoiceId;

	@Column(name = "RequestId")
	private String requestId;

	@Column(name = "BillKey")
	private Long billKey;
	
	@Column(name = "OrganizationType")
	private String organizationType;
	
	@Column(name = "Production")
	private Integer production;
	
	@Column(name = "CardNumber")
	private String cardNumber;

	@Column(name = "TransactionDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;
	
	@Column(name = "Type")
	private String type;
	
	@Column(name = "Status")
	private String status;
	
	@Column(name = "Request")
	private String request;
	
	@Column(name = "RequestDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestDate;
	
	@Column(name = "Response")
	private String response;

	@Column(name = "ResponseDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date responseDate;
	
	@Column(name = "Error")
	private String error;
	
	@Transient
	private Integer infoStatus;
	
	@Transient
	private Long organizationKey;
	
	@Transient
	private Long posKey;
	
	@Transient
	private Integer posCode;
	
	@Transient
	private Long locationKey;
	
	@Transient
	private Long cashierKey;
	
	@Transient
	private Long sectionKey;
	
	@Transient
	private String keyAES;
	
	@Transient
	private String cardType;
	
	@Transient
	private String billStatus;
	
	@Transient
	private Long locationKeyL2;
	
	@Transient
	private Language language;
	
	@Transient
	private String typeWhere;
	
	@Transient
	private String phoneNumber;
	
	@Transient
	private String email;
	
	@Transient
	private String token;
	
	@Transient
	private String qrData;
	
	@Transient
	private CardInfoRequest cardInfoRequest;
	
	@Transient
	private String internalId;
	
	@Transient
	private String registerNo;
	
	@Transient
	private String districtCode;
	
	@Transient
	private String deviceName;

	public Logger() {
		super();
	}

	public Logger(Long pKey, String deviceId, String invoiceId, String requestId, Long billKey, String organizationType, Integer production, String cardNumber, Date transactionDate, String type, String status, String request, Date requestDate,
			String response, Date responseDate, String error, Integer infoStatus, Long organizationKey, Long posKey, Integer posCode, Long locationKey, Long cashierKey, Long sectionKey, String keyAES, String cardType, String billStatus,
			Long locationKeyL2, Language language, String typeWhere, String phoneNumber, String email, String token, String qrData, String internalId, String registerNo, String districtCode, String deviceName) { // String info
		this.pKey = pKey;
		this.deviceId = deviceId;
		this.invoiceId = invoiceId;
		this.requestId = requestId;
		this.billKey = billKey;
		this.organizationType = organizationType;
		this.production = production;
		this.cardNumber = cardNumber;
		this.transactionDate = transactionDate;
		this.type = type;
		this.status = status;
		this.request = request;
		this.requestDate = requestDate;
		this.response = response;
		this.responseDate = responseDate;
		this.error = error;
		this.infoStatus = infoStatus;
		this.organizationKey = organizationKey;
		this.posKey = posKey;
		this.posCode = posCode;
		this.locationKey = locationKey;
		this.cashierKey = cashierKey;
		this.sectionKey = sectionKey;
		this.keyAES = keyAES;
		this.cardType = cardType;
		this.billStatus = billStatus;
		this.locationKeyL2 = locationKeyL2;
		this.language = language;
		this.typeWhere = typeWhere;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.token = token;
		this.qrData = qrData;
		this.internalId = internalId;
		this.registerNo = registerNo;
		this.districtCode = districtCode;
		this.deviceName = deviceName;
	}

	// copy constructor
	public Logger(Logger copyObject) {
        pKey = copyObject.pKey;
		deviceId = copyObject.deviceId;
		invoiceId = copyObject.invoiceId;
		requestId = copyObject.requestId;
		billKey = copyObject.billKey;
		organizationType = copyObject.organizationType;
		production = copyObject.production;
		cardNumber = copyObject.cardNumber;
		transactionDate = copyObject.transactionDate;
		type = copyObject.type;
		status = copyObject.status;
		request = copyObject.request;
		requestDate = copyObject.requestDate;
		response = copyObject.response;
		responseDate = copyObject.responseDate;
		error = copyObject.error;
		infoStatus = copyObject.infoStatus;
		organizationKey = copyObject.organizationKey;
		posKey = copyObject.posKey;
		posCode = copyObject.posCode;
		locationKey = copyObject.locationKey;
		cashierKey = copyObject.cashierKey;
		sectionKey = copyObject.sectionKey;
		keyAES = copyObject.keyAES;
		cardType = copyObject.cardType;
		billStatus = copyObject.billStatus;
		locationKeyL2 = copyObject.locationKeyL2;
		language = copyObject.language;
		typeWhere = copyObject.type;
		phoneNumber = copyObject.phoneNumber;
		email = copyObject.email;
		token = copyObject.token;
		qrData = copyObject.qrData;
		cardInfoRequest = copyObject.cardInfoRequest;
		internalId = copyObject.internalId;
		registerNo = copyObject.registerNo;
		districtCode = copyObject.districtCode;
		deviceName = copyObject.deviceName;
    }
	
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public Long getpKey() {
		return pKey;
	}

	public void setpKey(Long pKey) {
		this.pKey = pKey;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Long getBillKey() {
		return billKey;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public Integer getProduction() {
		return production;
	}

	public void setProduction(Integer production) {
		this.production = production;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public Integer getInfoStatus() {
		return infoStatus;
	}

	public void setInfoStatus(Integer infoStatus) {
		this.infoStatus = infoStatus;
	}

	public Long getOrganizationKey() {
		return organizationKey;
	}

	public void setOrganizationKey(Long organizationKey) {
		this.organizationKey = organizationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public Integer getPosCode() {
		return posCode;
	}

	public void setPosCode(Integer posCode) {
		this.posCode = posCode;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getCashierKey() {
		return cashierKey;
	}

	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}

	public Long getSectionKey() {
		return sectionKey;
	}

	public void setSectionKey(Long sectionKey) {
		this.sectionKey = sectionKey;
	}

	public String getKeyAES() {
		return keyAES;
	}

	public void setKeyAES(String keyAES) {
		this.keyAES = keyAES;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public Long getLocationKeyL2() {
		return locationKeyL2;
	}

	public void setLocationKeyL2(Long locationKeyL2) {
		this.locationKeyL2 = locationKeyL2;
	}

	public Language getLanguage() {
		if(language == null)
			language = Language.MN;
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getTypeWhere() {
		return typeWhere;
	}

	public void setTypeWhere(String typeWhere) {
		this.typeWhere = typeWhere;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public CardInfoRequest getCardInfoRequest() {
		return cardInfoRequest;
	}

	public void setCardInfoRequest(CardInfoRequest cardInfoRequest) {
		this.cardInfoRequest = cardInfoRequest;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getRegisterNo() {
		return registerNo;
	}

	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
}
