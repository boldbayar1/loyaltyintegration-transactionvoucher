package nomin.loyaltyintegration.tools;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class DateAsStringSerializer extends StdSerializer<Date> implements ContextualSerializer{

	private String pattern;

    public DateAsStringSerializer() {
        super(Date.class);
    }

    public DateAsStringSerializer(String pattern) {
        this();
        this.pattern = pattern;
    }

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(new SimpleDateFormat(pattern).format(value));
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        return new DateAsStringSerializer(property.getAnnotation(JsonFormat.class).pattern());
    }
}
