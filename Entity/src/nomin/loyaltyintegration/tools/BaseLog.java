package nomin.loyaltyintegration.tools;

import java.util.Date;

public abstract class BaseLog {
	
	private String type;
	private Date requestDate;
	private String request;
	private Date responseDate;
	private String response;
	private String error;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public Date getResponseDate() {
		return responseDate;
	}
	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
		responseDate = new Date();
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
		if (responseDate == null)
			responseDate = new Date();
	}
}
