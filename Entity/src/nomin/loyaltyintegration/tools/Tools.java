package nomin.loyaltyintegration.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Tools implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;

	public static Long newPkey() {
		try {
			Date newDate = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
			String pKey = String.format("%s%s%s%s%s%s%s", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), cal.get(Calendar.MILLISECOND));
			return Long.parseLong(pKey);
		} catch (Exception e) {
			return (long) 0;
		}
	}

	public static String getUUID() {
		String invoiceId = UUID.randomUUID().toString();
		invoiceId = invoiceId.replaceAll("-", "");
		invoiceId = invoiceId.toUpperCase();
		return invoiceId;
	}

	public static Object deepClone(Object object) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
