package nomin.loyaltyintegration.enums;

public enum TestCards {

	card112233000("112233000"),
	card112233001("112233001"),
	card112233002("112233002"),
	card112233003("112233003"),
	card112233004("112233004"),
	card112233005("112233005"),
	card112233006("112233006"),
	card112233007("112233007"),
	card112233008("112233008"),
	card112233009("112233009"),
	card112233010("112233010"),
	card2020005("2020005"),
	card2020008("2020008");
	
	private String text;
	
	TestCards(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
	
	public static TestCards fromString(String text) {
		for (TestCards testCards : TestCards.values()) {
			if (testCards.text.equalsIgnoreCase(text))
				return testCards;
		}
		return null;
	}

}
