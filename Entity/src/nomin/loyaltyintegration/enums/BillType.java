package nomin.loyaltyintegration.enums;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonSerialize(using = BillType.BillTypeSerializer.class)
@JsonDeserialize(using = BillType.BillTypeDeserializer.class)
public enum BillType {

    /**
     * Байгууллагаас хувь иргэнд
      */
    Person(1),
    /**
     * Байгууллагаас байгууллагад
     */
    Organization(3),
    /**
     * Нэхэмжлэхээр борлуулсан
     */
    Invoice(5);

    private int type;

    BillType(int type) {
        this.type = type;
    }

    public int getValue() {
        return type;
    }

    @Override
    public String toString() {
        return String.valueOf(type);
    }

    public static class BillTypeSerializer extends StdSerializer<BillType> {

        public BillTypeSerializer() {
            this(null);
        }

        public BillTypeSerializer(Class<BillType> type) {
            super(type);
        }

        @Override
        public void serialize(BillType value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeString(String.valueOf(value.getValue()));
        }
    }

    public static class BillTypeDeserializer extends StdDeserializer<BillType> {

        public BillTypeDeserializer() {
            this(null);
        }

        public BillTypeDeserializer(Class<BillType> type) {
            super(type);
        }

        @Override
        public BillType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            int type = Integer.parseInt(p.getText());
            if(type == Person.getValue())
                return Person;
            else if(type == Organization.getValue())
                return Organization;
            else if(type == Invoice.getValue())
                return Invoice;
            return null;
        }
    }
}