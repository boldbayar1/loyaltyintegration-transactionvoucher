package nomin.loyaltyintegration.enums;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@JsonSerialize(using = TaxType.TaxTypeSerializer.class)
@JsonDeserialize(using = TaxType.TaxTypeDeserializer.class)
public enum TaxType {
    /**
     * НӨАТ тооцох баримт
     */
    Tax(1),
    /**
     * НӨАТ-аас чөлөөлөгдөх
     */
    TaxFreeItem(2),
    /**
     * НӨАТ 0% тооцох
     */
    TaxFreeCalculate(3);

    private int type;

    TaxType(int type) {
        this.type = type;
    }

    public int getValue() {
        return type;
    }

    public static class TaxTypeSerializer extends StdSerializer<TaxType> {

        public TaxTypeSerializer() {
            this(null);
        }

        public TaxTypeSerializer(Class<TaxType> type) {
            super(type);
        }

        @Override
        public void serialize(TaxType value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeString(String.valueOf(value.getValue()));
        }
    }

    public static class TaxTypeDeserializer extends StdDeserializer<TaxType> {

        private List<TaxType> lstTaxType = Arrays.asList(Tax, TaxFreeItem, TaxFreeCalculate);

        public TaxTypeDeserializer() {
            super(TaxType.class);
        }

        @Override
        public TaxType deserialize(JsonParser p, DeserializationContext context) throws IOException, JsonProcessingException {
            Integer type = Integer.valueOf(p.getText());
            for (TaxType taxType : lstTaxType) {
                if (taxType.getValue() == type)
                    return taxType;
            }
            return null;
        }
    }
}