package nomin.loyaltyintegration.helper;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonHelper {

	public static ObjectMapper objectMapperStatic = new ObjectMapper();

	static {
		objectMapperStatic.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		objectMapperStatic.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
		objectMapperStatic.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapperStatic.setSerializationInclusion(Include.NON_NULL);
	}

	public static String toJson(Object data) {
		String rawData = "";
		try {
			rawData = objectMapperStatic.writeValueAsString(data);
		} catch (JsonProcessingException ex) {
			System.err.println("***Exception: nomin.core.utils.Json.toJson: " + ex.getMessage());
			rawData = null;
		}
		return rawData;
	}
	
	public static <T> T toJsonObject(String rawData, Class<T> type) {
		try {
			T result = objectMapperStatic.readValue(rawData, type);
			return result;
		} catch (Exception ex) {
			System.err.println("***Exception: nomin.core.utils.Json.toJsonObject: " + ex.getMessage());
		}
		return null;
	}
	
	public static <T> List<T> toJsonList(String rawData, Class<T> type) {
		try {
			return objectMapperStatic.readValue(rawData, objectMapperStatic.getTypeFactory().constructCollectionType(List.class, type));
		} catch (Exception ex) {
			System.err.println("***Exception: nomin.core.utils.Json.toJsonList: " + ex.getMessage());
		}
		return null;
	}
	
	public static String modifyJson(String json) throws Exception {
		if (json == null) return null;
		JsonNode parentNode = objectMapperStatic.readTree(json);
        if (parentNode == null) return null;
		ObjectNode node;
        node = (ObjectNode) parentNode;
        if (node.has("qrData"))
            node.put("qrData", "");
        if (node.has("lottery"))
            node.put("lottery", "");
        return parentNode.toString();
	}
	
}
