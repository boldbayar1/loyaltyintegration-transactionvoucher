package nomin.loyaltyintegration.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    public static class CardBonusCalculator {
        public static BigDecimal calculate(BigDecimal amount, BigDecimal cardPercent) {
            return amount.multiply(cardPercent).divide(new BigDecimal("100.00"), 0, RoundingMode.HALF_UP);
        }
    }

    public static class GiveAwayItemQtyCalculator {

        public static BigDecimal calculateByQty(BigDecimal qty, BigDecimal inQty, BigDecimal giveAwayItemQty) {
            return calculate(qty, inQty, giveAwayItemQty);
        }

        public static BigDecimal calculateByAmount(BigDecimal amount, BigDecimal inAmount, BigDecimal giveAwayItemQty) {
            return calculate(amount, inAmount, giveAwayItemQty);
        }

        protected static BigDecimal calculate(BigDecimal divider, BigDecimal divisor, BigDecimal multiplier) {
            return divider.divide(divisor, 0, RoundingMode.DOWN).multiply(multiplier);
        }
    }

    public static class InAmountQtyCalculator {
        public static BigDecimal getQty(BigDecimal intervalAmount, BigDecimal maxAmount) {
            return maxAmount.divide(intervalAmount, 0, RoundingMode.DOWN);
        }
    }

    public static class TaxCalculator {

        public static BigDecimal noVatPercent = new BigDecimal("100");
        public static BigDecimal vatPercent = new BigDecimal("10");

        public static BigDecimal getCityTaxAmount(BigDecimal amount, BigDecimal vatPercent, BigDecimal cityTaxPercent) {
            return getCityTaxAmount(amount, vatPercent, cityTaxPercent, Helper.NumericHelper.DefaultScale);
        }

        public static BigDecimal getCityTaxAmount(BigDecimal amount, BigDecimal vatPercent, BigDecimal cityTaxPercent, int scale) {
            if(Helper.NumericHelper.nonNullOrZero(cityTaxPercent))
                return getDivAmount(getNoVatAmount(amount, vatPercent, scale).multiply(cityTaxPercent), noVatPercent, 0, RoundingMode.HALF_UP).setScale(scale);
            return BigDecimal.ZERO.setScale(scale);
        }

        public static BigDecimal getNoVatAmount(BigDecimal amount, BigDecimal vatPercent) {
            return getNoVatAmount(amount, vatPercent, Helper.NumericHelper.DefaultScale);
        }

        public static BigDecimal getNoVatAmount(BigDecimal amount, BigDecimal vatPercent, int scale) {
            if(Helper.NumericHelper.nonNullOrZero(vatPercent))
                return getDivAmount(amount.multiply(noVatPercent), getHasVatPercent(vatPercent), scale);
            return amount.setScale(scale);
        }

        public static BigDecimal getVatAmount(BigDecimal amount, BigDecimal vatPercent) {
            return getVatAmount(amount, vatPercent, Helper.NumericHelper.DefaultScale);
        }

        protected static BigDecimal getVatAmount(BigDecimal amount, BigDecimal vatPercent, int scale) {
            if(Helper.NumericHelper.nonNullOrZero(vatPercent))
                return getDivAmount(amount.multiply(vatPercent), getHasVatPercent(vatPercent), scale);
            return BigDecimal.ZERO.setScale(scale);
        }

        protected static BigDecimal getDivAmount(BigDecimal divider, BigDecimal divisor, int scale) {
            return divider.divide(divisor, scale, RoundingMode.HALF_UP);
        }

        protected static BigDecimal getDivAmount(BigDecimal divider, BigDecimal divisor, int scale, RoundingMode roundingMode) {
            return divider.divide(divisor, scale, roundingMode);
        }

        protected static BigDecimal getHasVatPercent(BigDecimal vatPercent) {
            BigDecimal percent = TaxCalculator.noVatPercent;
            if(Helper.NumericHelper.nonNullOrZero(vatPercent))
                percent = percent.add(vatPercent);
            return percent;
        }
    }

    public static class BonusCalculator {
        public static BigDecimal getBonusAmountByPercent(BigDecimal amount, BigDecimal bonusPercent) {
            return amount.multiply(bonusPercent).divide(new BigDecimal("100"), 0, RoundingMode.HALF_UP);
        }
    }
}