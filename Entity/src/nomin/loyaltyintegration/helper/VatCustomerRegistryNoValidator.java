package nomin.loyaltyintegration.helper;

import java.util.regex.Pattern;

public interface VatCustomerRegistryNoValidator extends Validator {
    static VatCustomerRegistryNoValidator getInstance() {
        return VatCustomerRegistryNoValidatorImpl.instance;
    }
    boolean isPerson(String regNo);
    boolean isEasyRegistration(String regNo);

    final class VatCustomerRegistryNoValidatorImpl implements VatCustomerRegistryNoValidator {

        private static VatCustomerRegistryNoValidator instance = new VatCustomerRegistryNoValidatorImpl();

        private VatCustomerRegistryNoValidatorImpl() {
        }

        String person = "[А-ЯЁӨҮ]{2}[0-9]{8}";
        String organization = "[0-9]{7}";
        String easyRegistration = "[0-9]{8}";

        Pattern pattern = Pattern.compile("("+ person +")|("+ organization +")|("+ easyRegistration +")");
        Pattern patternPerson = Pattern.compile(person);
        Pattern patternEasyRegistration = Pattern.compile(easyRegistration);

        @Override
        public boolean isPerson(String regNo) {
            return patternPerson.matcher(regNo).matches();
        }

        @Override
        public boolean validate(String regNo) {
            return pattern.matcher(regNo).matches();
        }

        @Override
        public boolean isEasyRegistration(String regNo) {
            return patternEasyRegistration.matcher(regNo).matches();
        }

        @Override
        public boolean validateOnType(String value) {
            return true;
        }
    }
}
