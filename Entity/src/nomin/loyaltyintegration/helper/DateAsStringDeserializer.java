package nomin.loyaltyintegration.helper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAsStringDeserializer extends StdDeserializer<Date> implements ContextualDeserializer {

    private String pattern;

    public DateAsStringDeserializer() {
        super(Date.class);
    }

    public DateAsStringDeserializer(String pattern) {
        this();
        this.pattern = pattern;
    }

    @Override
    public Date deserialize(JsonParser p, DeserializationContext context) throws IOException, JsonProcessingException {
        try {
            return new SimpleDateFormat(pattern).parse(p.getText());
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext context, BeanProperty property) throws JsonMappingException {
        return new DateAsStringDeserializer(property.getAnnotation(JsonFormat.class).pattern());
    }
}