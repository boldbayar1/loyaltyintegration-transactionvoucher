package nomin.loyaltyintegration.helper;

import java.util.HashMap;
import java.util.Map;

import nomin.loyaltyintegration.enums.Language;
import nomin.loyaltyintegration.enums.StatusCode;


public class MessageHelper {

	private static Map<StatusCode, String> statusMessages;
	private static Map<StatusCode, String> statusMessagesEN;

	static {
		statusMessages = new HashMap<>();
		statusMessages.put(StatusCode.SUCCESS, "Амжилтай"); // 1
		statusMessages.put(StatusCode.FAIL, "Амжилтгүй"); // 1
		statusMessages.put(StatusCode.BODY_EMPTY, "Body талбарт утга дамжуулах шаардлагатай !"); // 400
		statusMessages.put(StatusCode.CARD_EMPTY, "Картын дугаар хоосон байна !"); //401
		
		statusMessages.put(StatusCode.REQUEST_INCORRECT, "%s талбар хоосон байна. "); // 440 -402 bolgono
		
		statusMessages.put(StatusCode.DEVICE_INACTIVE, "Идэвхгүй төхөөрөмж байна. "); // 110
		statusMessages.put(StatusCode.DEVICE_NOTLICENSED, "Төхөөрөмж зөвшөөрөлгүй байна. "); // 111
		statusMessages.put(StatusCode.EBARIMT_ERROR, "Ебаримт руу хадгалаж чадсангүй ! %s"); //112
		statusMessages.put(StatusCode.BILL_ERROR, "Биллийн мэдээлэл олдсонгүй! %s" );//115
		statusMessages.put(StatusCode.GENERAL_ERROR, "Алдаатай мэдээлэл ! Шалтгаан нь: %s");//116
		
		statusMessages.put(StatusCode.USER_INFO, "Харилцагчийн мэдээлэл дутуу байна! %s"); //114
		statusMessages.put(StatusCode.CARD_NOT_FOUND, "Картны мэдээлэл олдсонгүй !"); //404
		statusMessages.put(StatusCode.FINGER_DONT_MATCH, "Хурууны хээ таарахгүй байна !"); //405
		statusMessages.put(StatusCode.WRONG_PINCODE, "Пин код буруу !"); //406
		
		statusMessages.put(StatusCode.VOUCHER_NOT_FOUND, "Воучир мэдээлэл олдсонгүй !"); //408
		statusMessages.put(StatusCode.SAVE_VOUCHER_ERROR, "Воучир мэдээлэл хадгалагдаагүй !"); //409
		statusMessages.put(StatusCode.RETURN_VOUCHER_ERROR, "Воучирийн зөрүү хасагдсангүй тул гүйлгээ хийх боломжгүй байна !"); // 410
		statusMessages.put(StatusCode.CASH_TRANSACTION_ARE_CLOSED, "Бэлэн мөнгөний гүйлгээ хаалттай байна !");
		
		statusMessages.put(StatusCode.QR_WRONG_FORMAT, "QR картын дугаар буруу байна !"); //403
		statusMessages.put(StatusCode.SERVER_RESULT, "Хүсэлт амжилтгүй байна ! %s"); // 403
		statusMessages.put(StatusCode.CARD_INACTIVE, "Идэвхгүй карт байна !"); //403
		statusMessages.put(StatusCode.DATABASE_NOT_TRANSMIT, "Өгөгдөлийн сангаас хоосон утга ирсэн байна !"); // 404
		statusMessages.put(StatusCode.DATABASE_INCORRECT_VALUE, "Өгөгдөлийн сангаас буруу утга ирсэн байна !"); // 421
		statusMessages.put(StatusCode.DATABASE_INCORRECT_VALUE_2, "Өгөгдөлийн сангаас буруу утга ирсэн байна !"); // 422
		statusMessages.put(StatusCode.CARD_INFORMATION_ERROR, "Картын мэдээлэл авч чадсангүй !"); // 406
		statusMessages.put(StatusCode.BONUS_DEDUCTION_ERROR, "Бонус хасах тооцоололд алдаа !"); // 407
		statusMessages.put(StatusCode.QR_COULDNOT_READ, "QR код уншуулаагүй байна !"); // 411
		statusMessages.put(StatusCode.CHECK_REFERENCE_ALREADY_READ, "Reference код уншигдцан байна !");
		
		statusMessages.put(StatusCode.CONVERT_ERROR, "Хөрвүүлэлтийн алдаа гарлаа ! %s");
		statusMessages.put(StatusCode.CONVERT_IS_RIGHT, "Хөрвүүлэлт чадсангүй ! %s");
		
		statusMessages.put(StatusCode.DATABASE_ERROR, "Өгөгдөлийн сангийн алдаа. %s"); // 423
		
		statusMessages.put(StatusCode.BONUS_DEDUCTION_ERROR, "Бонус хасах тооцоололд алдаа !"); // 407
		
		statusMessages.put(StatusCode.CHECK_VOUCHER_ERROR, "Воучерийн мэдээлэл шалгаж чадсангүй ! %s"); // 414
		statusMessages.put(StatusCode.PUT_VOUCHER_ERROR, "Воучерийн мэдээлэл хасаж чадсангүй ! Шалтгаан: %s"); // 415
		statusMessages.put(StatusCode.RETURN_VOUCHER_FAIL_ERROR, "Воучерийн мэдээлэл буцааж чадсангүй! %s"); // 416
		statusMessages.put(StatusCode.VOUCHER_SUM, "0 дүнтэй воучерийн гүйлгээ хийх боломжгүй ! "); // 417
		statusMessages.put(StatusCode.NOT_ALLOWED, "Зөвшөөрөгдөөгүй байна !!!"); // 405
		
		statusMessages.put(StatusCode.RUNTIME_EXCEPTION, "Алдаа гарлаа. %s"); // 500
		
		statusMessages.put(StatusCode.SERVER_NO_SEND, "Мэдээлэл илгээхэд алдаа гарлаа! %s"); // 501
		statusMessages.put(StatusCode.SERVER_ERROR, "Майл сервер ажиллахгүй байна! %s"); // 503
		
		statusMessages.put(StatusCode.UPOINT_ERROR, "ЮПойнтын гүйлгээ хийхэд алдаа гарлаа!");	//418

		statusMessagesEN = new HashMap<>();
		statusMessagesEN.put(StatusCode.BODY_EMPTY, "The body field needs to transmit values !!!"); 
		statusMessagesEN.put(StatusCode.CARD_EMPTY, "The card number is empty !!!"); 
		
		statusMessagesEN.put(StatusCode.REQUEST_INCORRECT, "%s field is empty."); 
		
		statusMessagesEN.put(StatusCode.DEVICE_INACTIVE, "Inactive device."); 
		statusMessagesEN.put(StatusCode.DEVICE_NOTLICENSED, "The device is not licensed."); 
		statusMessagesEN.put(StatusCode.EBARIMT_ERROR, "Could not send to Ebarimt !!! %s"); 
		statusMessagesEN.put(StatusCode.CARD_NOT_FOUND, "Card information not found !!! %s"); 
		statusMessagesEN.put(StatusCode.FINGER_DONT_MATCH, "FingerPrint do not match !!! "); 
		statusMessagesEN.put(StatusCode.WRONG_PINCODE, "Wrong pincode !!!"); 
		
		statusMessagesEN.put(StatusCode.QR_WRONG_FORMAT, "The QR card number is incorrect !!! ");
		statusMessagesEN.put(StatusCode.CARD_INACTIVE, "There is an inactive card !!! %s");
		statusMessagesEN.put(StatusCode.DATABASE_NOT_TRANSMIT, "An empty value came from the database !!! %s"); 
		statusMessagesEN.put(StatusCode.DATABASE_INCORRECT_VALUE, "An incorrect value came from database !!! %s"); 
		statusMessagesEN.put(StatusCode.DATABASE_INCORRECT_VALUE_2, "An incorrect value came from database !!! %s"); 
		statusMessagesEN.put(StatusCode.CARD_INFORMATION_ERROR, "Couldn't get card information !!! %s"); 
		statusMessagesEN.put(StatusCode.BONUS_DEDUCTION_ERROR, "Bonus deduction error !!! %s"); 
		statusMessagesEN.put(StatusCode.QR_COULDNOT_READ, "QR couldn't read ."); 
		statusMessagesEN.put(StatusCode.DATABASE_ERROR, "DB error. %s"); 
		
		statusMessagesEN.put(StatusCode.RUNTIME_EXCEPTION, "Error. %s");
		
	}

	public static String getStatusMessage(StatusCode statusCode, Language language, Object... args) {
		String message = "Мессеж тохируулагдаагүй байна!";
		try {
			switch (language) {
			case MN:
				message = statusMessages.get(statusCode);
				break;
			case EN:
				message = statusMessagesEN.get(statusCode);
				break;
			}
		} catch (Exception e) {
			message = "Мессеж тохируулагдаагүй байна!";
		}
		
		if(args != null && args.length > 0) {
			message = String.format(message, args);
		}
		
		return message;
	}

}
