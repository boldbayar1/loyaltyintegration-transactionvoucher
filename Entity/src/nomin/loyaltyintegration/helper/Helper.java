package nomin.loyaltyintegration.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class Helper {
	
	public static class StringHelper {

        public static boolean nullOrEmpty(String str) {
            return str == null || str.isEmpty();
        }

        public static boolean nonNullOrEmpty(String str) {
            return !nullOrEmpty(str);
        }

        public static boolean equals(String str1, String str2) {
            if(str1 == null) return false;
            if(str2 == null) return false;
            return str1.equals(str2);
        }

        public static boolean equalsNullOrEmpty(String str1, String str2) {
            if((str1 == null || str1.isEmpty()) && (str2 == null || str2.isEmpty())) return true;
            return str1 != null && !str1.isEmpty() && str2 != null && !str2.isEmpty() && str1.equals(str2);
        }

        public static boolean equalsCaseInsensitive(String str1, String str2) {
            if(str1 != null && str2 != null)
                return str1.toLowerCase().equals(str2.toLowerCase());
            return false;
        }

        public static String setLength(String str, int length) {
            StringBuilder stringBuilder = new StringBuilder();
            if (str != null && str.length() > length) {
                stringBuilder.append(str.subSequence(0, length));
            } else {
                stringBuilder.append(str);
            }
            return stringBuilder.toString();
        }

        public static String concat(String separator, String ... strings) {
            return concat(separator, Arrays.asList(strings));
        }

        public static String concat(String separator, List<String> strings) {
            StringBuilder stringBuilder = new StringBuilder();
            if (ListHelper.nonNullOrEmpty(strings)) {
                for (String str : strings) {
                    if (stringBuilder.length() > 0) stringBuilder.append(separator);
                    stringBuilder.append(str);
                }
            }
            return stringBuilder.toString();
        }
    }

	public static class ListHelper {

        public static <T> List<T> find(List<T> lst, Findable<T> findable) {
            List<T> lstNew = new ArrayList<>();
            if(lst != null && !lst.isEmpty())
                lstNew.addAll(lst.stream().filter(i -> findable.match(i)).collect(Collectors.toList()));
            return lstNew;
        }

        public interface Findable<T> {
            boolean match(T t);
        }

        public static boolean nullOrEmpty(List<?> lst) {
            return lst == null || lst.isEmpty();
        }

        public static boolean nonNullOrEmpty(List<?> lst) {
            return !nullOrEmpty(lst);
        }
    }
	
	public static class NumericHelper {

        public static Integer DefaultScale = 2;
        public static BigDecimal Zero = BigDecimal.ZERO.setScale(DefaultScale);
        public static BigDecimal LargeAmount = new BigDecimal("100000000000000");
        public static BigDecimal Hundred = new BigDecimal("100");
        public static BigDecimal MaxChangeAmount = new BigDecimal("20000");

        public static boolean nullOrZero(BigDecimal number) {
            return number == null || number.compareTo(BigDecimal.ZERO) == 0;
        }

        public static boolean nonNullOrZero(BigDecimal number) {
            return !nullOrZero(number);
        }

        public static boolean nullOrZero(Long number) {
            return number == null || number == 0;
        }

        public static boolean nonNullOrZero(Long number) {
            return !nullOrZero(number);
        }

        public static boolean equals(Long long1, Long long2) {
            if(long1 != null && long2 != null) {
                return long1.compareTo(long2) == 0;
            }
            return false;
        }

        public static boolean equals(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
            if(bigDecimal1 != null && bigDecimal2 != null) {
                return bigDecimal1.compareTo(bigDecimal2) == 0;
            }
            return false;
        }

        public static BigDecimal add(BigDecimal ... numbers) {
            BigDecimal sum = BigDecimal.ZERO;
            if(numbers != null && numbers.length > 0) {
                for(int i = 0; i < numbers.length; i++)
                    if(nonNullOrZero(numbers[i]))
                        sum = sum.add(numbers[i]);
            }
            return sum;
        }

        public static BigDecimal min(BigDecimal ... numbers) {
            BigDecimal tmp = null;
            if(numbers != null && numbers.length > 0) {
                tmp = numbers[0];
                for(int i = 1; i < numbers.length; i++) {
                    if (numbers[i].compareTo(tmp) < 0)
                        tmp = numbers[i];
                }
            }
            return tmp;
        }

        public static boolean isLargeAmount(BigDecimal amount) {
            //  decimal(15.4)
            return amount.compareTo(LargeAmount) >= 0;
        }

        public static BigDecimal getNoScaleAmount(BigDecimal amount) {
            if (amount == null) return BigDecimal.ZERO;
            else
                return amount.setScale(0, RoundingMode.DOWN);
        }

        public static String getZeroFilledNumber(Object number, int length) {
            if (number == null) return null;
            StringBuilder numberBuilder = new StringBuilder(number.toString());
            while (numberBuilder.length() < length) {
                numberBuilder.insert(0,"0");
            }
            return numberBuilder.toString();
        }

        public static BigDecimal average(int scale, RoundingMode roundingMode, BigDecimal ... numbers) {
            if (numbers == null || numbers.length == 0) return BigDecimal.ZERO;
            return add(numbers).divide(new BigDecimal(numbers.length), scale, roundingMode);
        }

        public static boolean isLargeAmount(String amount) {
            try {
                return isLargeAmount(new BigDecimal(amount));
            } catch (Exception ex) {
                return true;
            }
        }

        public static BigDecimal getBigDecimalAmount(String number) {
            try {
                return new BigDecimal(number);
            } catch (Exception ex) {
                return null;
            }
        }

        public static int getBigDecimalLength(BigDecimal number) {
            String numberLength = number.toString();
            try {
                return numberLength.length();
            } catch (Exception ex) {
                return 0;
            }
        }
    }
	
    public static class NumberBuilder {
        public static String getNo(String no, int length) {
            StringBuilder noBuilder = new StringBuilder(no);
            while (noBuilder.length() < length) noBuilder.insert(0, "0");
            return noBuilder.toString();
        }
    }
}
