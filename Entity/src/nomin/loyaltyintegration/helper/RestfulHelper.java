package nomin.loyaltyintegration.helper;

import javax.servlet.http.HttpServletRequest;

import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.enums.Language;
import nomin.loyaltyintegration.enums.StatusCode;

public class RestfulHelper {

	//TODO getInfo Logic oos avdag bolgokh
	final static String URL_UPOINT_CHECK_INFO = "http://10.0.16.135:8080/UPoint/rest/pos/check-info";
	final static String URL_UPOINT_CONSUMER_INFO = "http://10.0.16.135:8080/UPoint/rest/pos/consumer-info";
	final static String URL_UPOINT_TRANSACTION = "http://10.0.16.135:8080/UPoint/rest/pos/process-transaction";
	final static String URL_UPOINT_CANCEL_TRANSACTION = "http://10.0.16.135:8080/UPoint/rest/pos/cancel-transaction";
	
	final static String URL_UPOINT_TOKEN = "https://auth.upoint.mn/thirdparty/authorize";

	final static String URL_FINGER_VERIFY = "http://10.0.10.117:8666/api/finger/verify";

	final static String URL_EBARIMT_REAL = new StringBuilder(getPOSApiUrl(true)).append("/api/posapi/put").toString();
	final static String URL_EBARIMT_TEST = new StringBuilder(getPOSApiUrl(false)).append("/api/posapi/put").toString();
	final static String URL_EBARIMT_RETURN_REAL = new StringBuilder(getPOSApiUrl(true)).append("/api/posapi/returnBill").toString();
	final static String URL_EBARIMT_RETURN_TEST = new StringBuilder(getPOSApiUrl(false)).append("/api/posapi/returnBill").toString();

	final static String URL_LOYALTY2_REAL_CHECK = new StringBuilder(getSpectreApiUrl(true)).append("/voucher/check").toString();
	final static String URL_LOYALTY2_REAL_PUT = new StringBuilder(getSpectreApiUrl(true)).append("/voucher/use").toString();
	final static String URL_LOYALTY2_REAL_RETURN = new StringBuilder(getSpectreApiUrl(true)).append("/voucher/return").toString();
	final static String URL_LOYALTY2_REAL_SMS = new StringBuilder(getSpectreApiUrl(true)).append("/voucher/sendsms").toString();

	final static String URL_SMS_EMAIL_SET = new StringBuilder(getSMSApiUrl(true)).append("/setmail").toString();
	final static String URL_SMS_PHONE = new StringBuilder(getSMSApiUrl(true)).append("/setmessage").toString();
	final static String URL_SMS_EMAIL_WITH_FILE = new StringBuilder(getSMSApiUrl(true)).append("/sendMail").toString();

	final static String SMS_AUTHORIZATION_KEY_EVcharger = "60DNEZDM4VTgVfL1ZdwI12y2HNASmcsUWCNpWLaA";
	final static String SMS_TOKEN_KEY_EVcharger = "ulamPGvrUXRSCg2G4nUJ"; // 95EpwcNmpYui3qI3O5Nqw4g5NJ4cZ4GbWLqY7DkgoSoKi3FdOD8fID3LMWJi
	final static String SMS_SEND_MAIL_TOKEN = "NWXyjDJYFcpGfcXWfiDHz5auiq5hOq899PkJM6wV0UohKEog8NctUgWKOhuf";

	final static String SMS_FROM_MAIL = "reportsms@nomin.net";
	final static String SMS_FROM_MAIL_TEST = "test@nomin.net";

	final static String POS_REPORT_BILL_IMAGE = "http://10.0.10.182:8009/POSReport/api/report/bill";
	final static String POS_CHECK_REGISTER_NO = "http://10.0.12.56:8088/api/posapi/checkRegNo";

	final static String URL_QPAY_REAL = "https://api.qpay.mn/v1";

	final static String URL_SOCIALPAY_REAL = "https://instore.golomtbank.com";
	final static String SOCIALPAY_AUTH = "Z6>au9IrZ,9*Q^}+2wO*q64!i^_xhD";
	final static String SOCIALPAY_TERMIAL = "19204653";

	public static String getSpectreApiUrl(boolean isReal) {
		if (isReal)
			return "https://sandbox.nomin.mn/rest";
		return "https://sandbox.nomin.mn/rest";
	}

	public static String getPOSApiUrl(boolean isReal) {
		if (isReal)
			return "http://10.0.10.182:8444";
		return "http://10.0.12.56:8088";
	}

	public static String getSMSApiUrl(boolean isReal) {
		if (isReal)
			return "https://sms-email.nomin.mn/api";
		return "https://sms-email.nomin.mn/api";
	}

	public static LoyaltyBaseResponse isEmptyBody(String body) {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		if (body == null || body.isEmpty()) {
			response.setStatus(400);
			response.setMessage(MessageHelper.getStatusMessage(StatusCode.BODY_EMPTY, Language.MN));
		} else {
			response.setStatus(1);
		}
		return response;
	}

	public static final String[] HEADERS_TO_TRY = { "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP", "HTTP_FORWARDED_FOR", "HTTP_FORWARDED",
			"HTTP_VIA", "REMOTE_ADDR" };

	public String getClientIpAddress(HttpServletRequest request) {
		for (String header : HEADERS_TO_TRY) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}

		return request.getRemoteAddr();
	}

	public static String getUrlUpointCheckInfo() {
		return URL_UPOINT_CHECK_INFO;
	}

	public static String getUrlUpointConsumerInfo() {
		return URL_UPOINT_CONSUMER_INFO;
	}

	public static String getUrlUpointTransaction() {
		return URL_UPOINT_TRANSACTION;
	}

	public static String getUrlUpointCancelTransaction() {
		return URL_UPOINT_CANCEL_TRANSACTION;
	}

	public static String getUrlFingerVerify() {
		return URL_FINGER_VERIFY;
	}

	public static String getUrlEbarimtReal() {
		return URL_EBARIMT_REAL;
	}

	public static String getUrlEbarimtTest() {
		return URL_EBARIMT_TEST;
	}

	public static String getUrlEbarimtReturnReal() {
		return URL_EBARIMT_RETURN_REAL;
	}

	public static String getUrlEbarimtReturnTest() {
		return URL_EBARIMT_RETURN_TEST;
	}

	public static String getUrlLoyalty2RealCheck() {
		return URL_LOYALTY2_REAL_CHECK;
	}

	public static String getUrlLoyalty2RealPut() {
		return URL_LOYALTY2_REAL_PUT;
	}

	public static String getUrlLoyalty2RealReturn() {
		return URL_LOYALTY2_REAL_RETURN;
	}

	public static String getUrlLoyalty2RealSms() {
		return URL_LOYALTY2_REAL_SMS;
	}

	public static String getUrlSmsEmailSet() {
		return URL_SMS_EMAIL_SET;
	}

	public static String getUrlSmsPhone() {
		return URL_SMS_PHONE;
	}

	public static String getUrlSmsEmailWithFile() {
		return URL_SMS_EMAIL_WITH_FILE;
	}

	public static String getSmsAuthorizationKeyEvcharger() {
		return SMS_AUTHORIZATION_KEY_EVcharger;
	}

	public static String getSmsTokenKeyEvcharger() {
		return SMS_TOKEN_KEY_EVcharger;
	}

	public static String getSmsSendMailToken() {
		return SMS_SEND_MAIL_TOKEN;
	}

	public static String getSmsFromMail() {
		return SMS_FROM_MAIL;
	}

	public static String getSmsFromMailTest() {
		return SMS_FROM_MAIL_TEST;
	}

	public static String getPosReportBillImage() {
		return POS_REPORT_BILL_IMAGE;
	}

	public static String getPosCheckRegisterNo() {
		return POS_CHECK_REGISTER_NO;
	}

	public static String getUrlQpayReal() {
		return URL_QPAY_REAL;
	}

	public static String getUrlSocialpayReal() {
		return URL_SOCIALPAY_REAL;
	}

	public static String getSocialpayAuth() {
		return SOCIALPAY_AUTH;
	}

	public static String getSocialpayTermial() {
		return SOCIALPAY_TERMIAL;
	}

	public static String getUrlUpointToken() {
		return URL_UPOINT_TOKEN;
	}
}