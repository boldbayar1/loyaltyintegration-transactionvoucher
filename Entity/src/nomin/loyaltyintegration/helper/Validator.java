package nomin.loyaltyintegration.helper;

public interface Validator {
	boolean validate(String value);
    boolean validateOnType(String value);
}
