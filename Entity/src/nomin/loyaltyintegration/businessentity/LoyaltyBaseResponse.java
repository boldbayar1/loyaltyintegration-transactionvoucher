package nomin.loyaltyintegration.businessentity;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoyaltyBaseResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private Integer status;

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("state")
	private Integer state;

	public LoyaltyBaseResponse() {
		super();
	}

	public LoyaltyBaseResponse(Integer status, String message, Integer state) {
		this.status = status;
		this.message = message;
		this.state = state;
	}

	// copy constructor
	public LoyaltyBaseResponse(LoyaltyBaseResponse copyObject) {
		status = copyObject.status;
		message = copyObject.message;
		state = copyObject.state;
    }

	public Integer getStatus() {
		if(status == null)
			status = 404;
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		if(message == null)
			message = "Мессеж тохируулаагүй";
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}
