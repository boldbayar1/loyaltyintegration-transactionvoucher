package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Cacheable(false)
public class Info implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Id")
	private String id;
	
	@Column(name = "Status")
	private Integer status;
	
	@Column(name = "DeviceId")
	private String deviceId;
	
	@Column(name = "OrganizationType")
	private String organizationType;

	@Column(name = "OrganizationKey")
	private Long organizationKey;

	@Column(name = "PosKey")
	private Long posKey;
	
	@Column(name = "PosCode")
	private Integer posCode;
	
	@Column(name = "LocationKey")
	private Long locationKey;
	
	@Column(name = "CashierKey")
	private Long cashierKey;
	
	@Column(name = "SectionKey")
	private Long sectionKey;

	@Column(name = "IsProduction")
	private Integer production;

	@Column(name = "KeyAES")
	private String keyAES;
	
	@Column(name = "RequestId")
	private String requestId;
	
	@Column(name = "CardType")
	private String cardType;
	
	@Column(name = "CardNumber")
	private String cardNumber;
	
	@Column(name = "InvoiceId")
	private String invoiceId;
	
	@Column(name = "BillKey")
	private Long billKey;

	@Column(name = "BillStatus")
	private String billStatus;
	
	@Column(name = "LocationKeyL2")
	private Long locationKeyL2;
	
	@Column(name = "InternalId")
	private String internalId;
	
	@Column(name = "RegisterNo")
	private String registerNo;
	
	@Column(name = "districtCode")
	private String districtCode;
	
	@Column(name = "deviceName")
	private String deviceName;
	
	@Transient
	private String voucherStatus;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public Long getOrganizationKey() {
		return organizationKey;
	}

	public void setOrganizationKey(Long organizationKey) {
		this.organizationKey = organizationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public Integer getPosCode() {
		return posCode;
	}

	public void setPosCode(Integer posCode) {
		this.posCode = posCode;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getCashierKey() {
		return cashierKey;
	}

	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}

	public Long getSectionKey() {
		return sectionKey;
	}

	public void setSectionKey(Long sectionKey) {
		this.sectionKey = sectionKey;
	}

	public Integer getProduction() {
		return production;
	}

	public void setProduction(Integer production) {
		this.production = production;
	}

	public String getKeyAES() {
		return keyAES;
	}

	public void setKeyAES(String keyAES) {
		this.keyAES = keyAES;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Long getBillKey() {
		return billKey;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getVoucherStatus() {
		return voucherStatus;
	}

	public void setVoucherStatus(String voucherStatus) {
		this.voucherStatus = voucherStatus;
	}

	public Long getLocationKeyL2() {
		return locationKeyL2;
	}

	public void setLocationKeyL2(Long locationKeyL2) {
		this.locationKeyL2 = locationKeyL2;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getRegisterNo() {
		return registerNo;
	}

	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
}
