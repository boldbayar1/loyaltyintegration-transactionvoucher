package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.spectre.response.VoucherResponse;

@Entity
@Cacheable(false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalcBonusResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Status")
	private Integer status;

	@Column(name = "Data")
	private String data;
	
	@Column(name = "Message")
	private String message;
	
	@Column(name = "BillKey")
	private String billKey;

	@Column(name = "InvoiceId")
	private String invoiceId;
	
	@Column(name = "ResultValue")
	private String resultValue;
	
	@Column(name = "InfoMessage")
	private String infoMessage;
	
	@Column(name = "SendMessage")
	private String sendMessage;
	
	@Transient
	private String qrDataImage;
	
	@Transient
	private String qrDataImageExt;
	
	@Transient
	private BigDecimal totalVoucherAmount;
	
	@Transient
	private String checkInfo;
	
	@Transient
	@JsonProperty("voucherResult")
	VoucherResponse voucherResult;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public BigDecimal getTotalVoucherAmount() {
		return totalVoucherAmount;
	}

	public void setTotalVoucherAmount(BigDecimal totalVoucherAmount) {
		this.totalVoucherAmount = totalVoucherAmount;
	}

	public String getBillKey() {
		return billKey;
	}

	public void setBillKey(String billKey) {
		this.billKey = billKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResultValue() {
		return resultValue;
	}

	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}

	public String getCheckInfo() {
		return checkInfo;
	}

	public void setCheckInfo(String checkInfo) {
		this.checkInfo = checkInfo;
	}

	public String getQrDataImage() {
		return qrDataImage;
	}

	public String getQrDataImageExt() {
		return qrDataImageExt;
	}

	public void setQrDataImage(String qrDataImage) {
		this.qrDataImage = qrDataImage;
	}

	public void setQrDataImageExt(String qrDataImageExt) {
		this.qrDataImageExt = qrDataImageExt;
	}

	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getSendMessage() {
		return sendMessage;
	}

	public void setSendMessage(String sendMessage) {
		this.sendMessage = sendMessage;
	}

	public VoucherResponse getVoucherResult() {
		return voucherResult;
	}

	public void setVoucherResult(VoucherResponse voucherResult) {
		this.voucherResult = voucherResult;
	}
}
