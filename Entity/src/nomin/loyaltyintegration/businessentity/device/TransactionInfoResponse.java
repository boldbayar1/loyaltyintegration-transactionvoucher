package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Cacheable(false)
public class TransactionInfoResponse implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Column(name = "cardUaId")
	private String cardUaId;
	
	@Column(name = "cardHolderId")
	private String cardHolderId;
	
	@Column(name = "uaId")
	private String uaId;
	
	@Column(name = "isRelated")
	private int isRelated;

	@Id
	@Column(name = "billKey")
	private Long billKey;
	
	@Column(name = "locationKey")
	private Long locationKey;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Column(name = "paymentAmount")
	private BigDecimal paymentAmount;
	
	@Column(name = "paymentVoucher")
	private BigDecimal paymentVoucher;
	
	@Column(name = "paymentUpoint")
	private BigDecimal paymentUpoint;
	
	@Column(name = "paymentBonus")
	private BigDecimal paymentBonus;
	
	@Column(name = "addBonus")
	private BigDecimal addBonus;
	
	@Column(name = "addUpoint")
	private BigDecimal addUpoint;
	
	@Column(name = "balanceBonus")
	private BigDecimal balanceBonus;
	
	@Column(name = "balanceUpoint")
	private BigDecimal balanceUpoint;
	
	@Column(name = "calcBonusAmount")
	private BigDecimal calcBonusAmount;
	
	@Column(name = "uPointGiveAwayData")
	private String uPointGiveAwayData;

	public String getCardUaId() {
		return cardUaId;
	}

	public String getCardHolderId() {
		return cardHolderId;
	}

	public String getUaId() {
		return uaId;
	}

	public Long getBillKey() {
		return billKey;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public BigDecimal getPaymentVoucher() {
		return paymentVoucher;
	}

	public BigDecimal getPaymentUpoint() {
		return paymentUpoint;
	}

	public BigDecimal getPaymentBonus() {
		return paymentBonus;
	}

	public BigDecimal getAddBonus() {
		return addBonus;
	}

	public BigDecimal getAddUpoint() {
		return addUpoint;
	}

	public BigDecimal getBalanceBonus() {
		return balanceBonus;
	}

	public BigDecimal getBalanceUpoint() {
		return balanceUpoint;
	}

	public BigDecimal getCalcBonusAmount() {
		return calcBonusAmount;
	}

	public void setCardUaId(String cardUaId) {
		this.cardUaId = cardUaId;
	}

	public void setCardHolderId(String cardHolderId) {
		this.cardHolderId = cardHolderId;
	}

	public void setUaId(String uaId) {
		this.uaId = uaId;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public void setPaymentVoucher(BigDecimal paymentVoucher) {
		this.paymentVoucher = paymentVoucher;
	}

	public void setPaymentUpoint(BigDecimal paymentUpoint) {
		this.paymentUpoint = paymentUpoint;
	}

	public void setPaymentBonus(BigDecimal paymentBonus) {
		this.paymentBonus = paymentBonus;
	}

	public void setAddBonus(BigDecimal addBonus) {
		this.addBonus = addBonus;
	}

	public void setAddUpoint(BigDecimal addUpoint) {
		this.addUpoint = addUpoint;
	}

	public void setBalanceBonus(BigDecimal balanceBonus) {
		this.balanceBonus = balanceBonus;
	}

	public void setBalanceUpoint(BigDecimal balanceUpoint) {
		this.balanceUpoint = balanceUpoint;
	}

	public void setCalcBonusAmount(BigDecimal calcBonusAmount) {
		this.calcBonusAmount = calcBonusAmount;
	}

	public String getuPointGiveAwayData() {
		return uPointGiveAwayData;
	}

	public void setuPointGiveAwayData(String uPointGiveAwayData) {
		this.uPointGiveAwayData = uPointGiveAwayData;
	}

	public int getIsRelated() {
		return isRelated;
	}

	public void setIsRelated(int isRelated) {
		this.isRelated = isRelated;
	}
	
}
