package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Cacheable(false)
public class CheckDevice implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="Status")
	private Integer status;
	
	@Column(name="OrganizationKey")
	private String organizationKey;
	
	@Column(name="PosKey")
	private String posKey;
	
	@Column(name="IsProduction")
	private Integer production;
	
	@Column(name="OrganizationType")
	private String organizationType;
	
	@Column(name="Key")
	private String key;
	
	@Column(name="CardNumber")
	private String CardNumber;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOrganizationKey() {
		return organizationKey;
	}

	public void setOrganizationKey(String organizationKey) {
		this.organizationKey = organizationKey;
	}

	public Integer getProduction() {
		return production;
	}

	public void setProduction(Integer production) {
		this.production = production;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCardNumber() {
		return CardNumber;
	}

	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}

	public String getPosKey() {
		return posKey;
	}

	public void setPosKey(String posKey) {
		this.posKey = posKey;
	}
}
