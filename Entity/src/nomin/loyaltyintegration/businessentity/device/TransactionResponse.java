package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import nomin.loyaltyintegration.businessentity.ebarimt.EbarimtRequest;

@Entity
@Cacheable(false)
public class TransactionResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Status")
	private Integer status;

	@Column(name = "Data")
	private EbarimtRequest data;
	
	@Column(name = "InvoiceId")
	private String invoiceId;
	
	public TransactionResponse() {
		super();
	}

	public TransactionResponse(Integer status, EbarimtRequest data, String invoiceId) {
		super();
		this.status = status;
		this.data = data;
		this.invoiceId = invoiceId;
	}

	// copy constructor
	public TransactionResponse(TransactionResponse copyObject) {
		status = copyObject.status;
		data = copyObject.data;
		invoiceId = copyObject.invoiceId;
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public EbarimtRequest getData() {
		return data;
	}

	public void setData(EbarimtRequest data) {
		this.data = data;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
}
