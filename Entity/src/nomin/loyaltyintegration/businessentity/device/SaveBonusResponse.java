package nomin.loyaltyintegration.businessentity.device;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Cacheable(false)
public class SaveBonusResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Status")
	private Integer status;

	@Column(name = "Data")
	private String data;

	public SaveBonusResponse() {
		super();
	}

	public SaveBonusResponse(Integer status, String data) {
		this.status = status;
		this.data = data;
	}

	// copy constructor
	public SaveBonusResponse(SaveBonusResponse copyObject) {
		status = copyObject.status;
		data = copyObject.data;
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
