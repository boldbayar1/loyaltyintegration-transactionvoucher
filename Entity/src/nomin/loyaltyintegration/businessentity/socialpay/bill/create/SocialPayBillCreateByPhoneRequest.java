package nomin.loyaltyintegration.businessentity.socialpay.bill.create;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayBillCreateByPhoneRequest extends SocialPayBillCreateRequest {

	@JsonProperty("phone")
	private String phone;
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}