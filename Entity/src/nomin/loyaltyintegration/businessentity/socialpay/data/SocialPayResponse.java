package nomin.loyaltyintegration.businessentity.socialpay.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayResponse {
	
	@JsonProperty("header")
	private Header header;
	
	@JsonProperty("body")
	private SocialPayResponseBody body;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public SocialPayResponseBody getBody() {
		return body;
	}

	public void setBody(SocialPayResponseBody body) {
		this.body = body;
	}
}