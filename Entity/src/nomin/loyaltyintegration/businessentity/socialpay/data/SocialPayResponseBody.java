package nomin.loyaltyintegration.businessentity.socialpay.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayResponseBody {
	@JsonProperty("response")
	private SocialPayResponseBodyResponse response;
	
	@JsonProperty("error")
	private SocialPayResponseBodyError error;

	public SocialPayResponseBodyResponse getResponse() {
		return response;
	}

	public void setResponse(SocialPayResponseBodyResponse response) {
		this.response = response;
	}

	public SocialPayResponseBodyError getError() {
		return error;
	}

	public void setError(SocialPayResponseBodyError error) {
		this.error = error;
	}
}