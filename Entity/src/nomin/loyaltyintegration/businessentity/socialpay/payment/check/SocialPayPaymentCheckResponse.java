package nomin.loyaltyintegration.businessentity.socialpay.payment.check;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.socialpay.data.Header;
import nomin.loyaltyintegration.businessentity.socialpay.payment.check.data.SocialPayPaymentCheckResponseBody;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayPaymentCheckResponse {
	@JsonProperty("header")
	private Header header;
	
	@JsonProperty("body")
	private SocialPayPaymentCheckResponseBody body;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public SocialPayPaymentCheckResponseBody getBody() {
		return body;
	}

	public void setBody(SocialPayPaymentCheckResponseBody body) {
		this.body = body;
	}
}