package nomin.loyaltyintegration.businessentity.socialpay.payment.check.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBodyError;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayPaymentCheckResponseBody {
	
	@JsonProperty("response")
	private SocialPayPaymentCheckResponseBodyResponse response;
	
	@JsonProperty("error")
	private SocialPayResponseBodyError error;

	public SocialPayPaymentCheckResponseBodyResponse getResponse() {
		return response;
	}

	public void setResponse(SocialPayPaymentCheckResponseBodyResponse response) {
		this.response = response;
	}

	public SocialPayResponseBodyError getError() {
		return error;
	}

	public void setError(SocialPayResponseBodyError error) {
		this.error = error;
	}
}