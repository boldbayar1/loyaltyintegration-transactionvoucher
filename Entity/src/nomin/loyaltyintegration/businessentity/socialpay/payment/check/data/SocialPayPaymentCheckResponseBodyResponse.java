package nomin.loyaltyintegration.businessentity.socialpay.payment.check.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialPayPaymentCheckResponseBodyResponse {
	@JsonProperty("approval_code")
	private String approvalCode;
	@JsonProperty("amount")
	private String amount;
	@JsonProperty("card_number")
	private String cardNumber;
	@JsonProperty("resp_desc")
	private String respDesc;
	@JsonProperty("resp_code")
	private String respCode;
	@JsonProperty("checksum")
	private String checksum;
	@JsonProperty("invoice")
	private String invoice;
	@JsonProperty("terminal")
	private String terminal;
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getRespDesc() {
		return respDesc;
	}
	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
}