package nomin.loyaltyintegration.businessentity.upoint.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInfoDetailRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("card_number")
	private String cardNumber;

	@JsonProperty("mobile")
	private String mobile;

	@JsonProperty("pin_code")
	private String pinCode;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
}
