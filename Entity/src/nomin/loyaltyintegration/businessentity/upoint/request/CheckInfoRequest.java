package nomin.loyaltyintegration.businessentity.upoint.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInfoRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("locationKey")
	private Long locationKey;
	
	@JsonProperty("posKey")
	private Long posKey;
	
	@JsonProperty("data")
	private String data;

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
