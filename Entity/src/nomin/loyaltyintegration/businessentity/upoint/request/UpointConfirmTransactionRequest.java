package nomin.loyaltyintegration.businessentity.upoint.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpointConfirmTransactionRequest {
	
	@JsonProperty("bill_number")
	private String billNumber;
	
	@JsonProperty("status")
	private String status;

	public String getBillNumber() {
		return billNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}