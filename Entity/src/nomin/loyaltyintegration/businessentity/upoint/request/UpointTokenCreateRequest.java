package nomin.loyaltyintegration.businessentity.upoint.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpointTokenCreateRequest {
	
	@JsonProperty("ua_id")
	private String ua_id;
	
	@JsonProperty("nomin_card_number")
	private String nomin_card_number;
	
	@JsonProperty("upoint_card_number")
	private String upoint_card_number;

	public String getUa_id() {
		return ua_id;
	}

	public String getNomin_card_number() {
		return nomin_card_number;
	}

	public String getUpoint_card_number() {
		return upoint_card_number;
	}

	public void setUa_id(String ua_id) {
		this.ua_id = ua_id;
	}

	public void setNomin_card_number(String nomin_card_number) {
		this.nomin_card_number = nomin_card_number;
	}

	public void setUpoint_card_number(String upoint_card_number) {
		this.upoint_card_number = upoint_card_number;
	}	
}