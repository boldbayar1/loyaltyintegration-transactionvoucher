package nomin.loyaltyintegration.businessentity.upoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessTransactionResponseItem {

    @JsonProperty("barcode")
    private String itemCode;

    @JsonProperty("qty")
    private BigDecimal qty;

    @JsonProperty("unit_point")
    private BigDecimal unitPoint;

    @JsonProperty("point")
    private BigDecimal point;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitPoint() {
        return unitPoint;
    }

    public void setUnitPoint(BigDecimal unitPoint) {
        this.unitPoint = unitPoint;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }
}