package nomin.loyaltyintegration.businessentity.upoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpointConfirmTransactionResponse {
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("merchant_seal__id")
	private String merchantSealId;

	public String getMessage() {
		return message;
	}

	public String getMerchantSealId() {
		return merchantSealId;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMerchantSealId(String merchantSealId) {
		this.merchantSealId = merchantSealId;
	}
}