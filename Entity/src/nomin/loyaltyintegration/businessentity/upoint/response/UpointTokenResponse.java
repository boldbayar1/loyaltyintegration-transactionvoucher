package nomin.loyaltyintegration.businessentity.upoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpointTokenResponse {
	
	@JsonProperty("error")
	private String error;
	
	@JsonProperty("error_description")
	private String error_description;
	
	@JsonProperty("access_token")
	private String access_token;
	
	@JsonProperty("expires_in")
	private String expires_in;
	
	@JsonProperty("refresh_token")
	private String refresh_token;
	
	@JsonProperty("scope")
	private String scope;
	
	@JsonProperty("token_type")
	private String token_type;

	public String getError() {
		return error;
	}

	public String getError_description() {
		return error_description;
	}

	public String getAccess_token() {
		return access_token;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public String getScope() {
		return scope;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
}