package nomin.loyaltyintegration.businessentity.upoint.response.cancel;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.upoint.response.UPointBank;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelTransactionResponseData {

    @JsonProperty("bill_number")
    private Long billKey;

    @JsonProperty("receipt_id")
    private String receiptId;

    @JsonProperty("return_receipt_id")
    private String returnReceiptId;

    @JsonProperty("refund_spend_point")
    private BigDecimal payPoint;

    @JsonProperty("refund_bonus_amount")
    private BigDecimal bonusAmount;

    @JsonProperty("point_balance")
    private BigDecimal pointBalance;

    @JsonProperty("bank_amount")
    private BigDecimal bankAmount;

    @JsonProperty("manufacturer_amount")
    private BigDecimal manufacturerAmount;

    @JsonProperty("item_amount")
    private BigDecimal itemAmount;

    @JsonProperty("items")
    private List<CancelTransactionResponseItem> lstItem;

    @JsonProperty("bank")
    private List<UPointBank> lstBank;

    @JsonProperty("manufacturer")
    private List<CancelTransactionResponseManufacturer> lstManufacturer;

    @JsonProperty
    private Integer result;

    @JsonProperty
    private String message;

    public Long getBillKey() {
        return billKey;
    }

    public void setBillKey(Long billKey) {
        this.billKey = billKey;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getReturnReceiptId() {
        return returnReceiptId;
    }

    public void setReturnReceiptId(String returnReceiptId) {
        this.returnReceiptId = returnReceiptId;
    }

    public BigDecimal getPayPoint() {
        return payPoint;
    }

    public void setPayPoint(BigDecimal payPoint) {
        this.payPoint = payPoint;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BigDecimal getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(BigDecimal pointBalance) {
        this.pointBalance = pointBalance;
    }

    public BigDecimal getBankAmount() {
        return bankAmount;
    }

    public void setBankAmount(BigDecimal bankAmount) {
        this.bankAmount = bankAmount;
    }

    public BigDecimal getManufacturerAmount() {
        return manufacturerAmount;
    }

    public void setManufacturerAmount(BigDecimal manufacturerAmount) {
        this.manufacturerAmount = manufacturerAmount;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public List<CancelTransactionResponseItem> getLstItem() {
        return lstItem;
    }

    public void setLstItem(List<CancelTransactionResponseItem> lstItem) {
        this.lstItem = lstItem;
    }

    public List<UPointBank> getLstBank() {
        return lstBank;
    }

    public void setLstBank(List<UPointBank> lstBank) {
        this.lstBank = lstBank;
    }

    public List<CancelTransactionResponseManufacturer> getLstManufacturer() {
        return lstManufacturer;
    }

    public void setLstManufacturer(List<CancelTransactionResponseManufacturer> lstManufacturer) {
        this.lstManufacturer = lstManufacturer;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}