package nomin.loyaltyintegration.businessentity.upoint.response.cancel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelTransactionResponse {

    @JsonProperty
    private String message;

    @JsonProperty("readEntity")
    private CancelTransactionResponseData data;

    @JsonProperty
    private Integer status;

    @JsonProperty
    private Integer result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CancelTransactionResponseData getData() {
        return data;
    }

    public void setData(CancelTransactionResponseData data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}