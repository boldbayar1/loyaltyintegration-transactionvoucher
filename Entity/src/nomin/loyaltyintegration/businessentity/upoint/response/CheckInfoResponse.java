package nomin.loyaltyintegration.businessentity.upoint.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInfoResponse implements Serializable  {

	private static final long serialVersionUID = 1L;

	@JsonProperty("ua_id")
	private String upointCardNumber;
	
	@JsonProperty("card_number")
	private String cardNumber;
	
	@JsonProperty("result")
	private Integer result;
	
	@JsonProperty("mobile")
	private String mobile;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("balance")
	private String upointBalance;
	
	@JsonProperty("card_status")
	private Integer cardStatus;
	
	@JsonProperty("created_at")
	private String createdAt;

	public String getUpointCardNumber() {
		return upointCardNumber;
	}

	public void setUpointCardNumber(String upointCardNumber) {
		this.upointCardNumber = upointCardNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUpointBalance() {
		return upointBalance;
	}

	public void setUpointBalance(String upointBalance) {
		this.upointBalance = upointBalance;
	}

	public Integer getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(Integer cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}	
}
