package nomin.loyaltyintegration.businessentity.upoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessTransactionResponseData {

	@Column(name = "receipt_id")
	@JsonProperty("receipt_id")
	private String receiptId;

	@Column(name = "card_number")
	@JsonProperty("card_number")
	private String cardNo;
	
	@Column(name = "manufacturer_items_point")
	@JsonProperty("manufacturer_items_point")
	private BigDecimal manufacturerItemsPoint;

	@Column(name = "point_balance")
	@JsonProperty("point_balance")
	private BigDecimal balancePoint;

	@Column(name = "merchant_point")
	@JsonProperty("merchant_point")
	private BigDecimal merchantPoint;

	@Column(name = "manufacturerPoint")
	@JsonProperty("manufacturer_point")
	private BigDecimal manufacturerPoint;

	@Id
	@Column(name = "bill_number")
	@JsonProperty("bill_number")
	private String billNo;

	@Column(name = "spend_point")
	@JsonProperty("spend_point")
	private BigDecimal payPoint;

	@Column(name = "result")
	@JsonProperty
	private Integer result;

	@Column(name = "date")
	@JsonProperty("date")
	private String date;

	@Column(name = "message")
	@JsonProperty
	private String message;

	@Column(name = "bank_point")
	@JsonProperty("bank_point")
	private BigDecimal bankPoint;

	@Column(name = "total_point")
	@JsonProperty("total_point")
	private BigDecimal totalPoint;

	@Column(name = "items")
	@JsonProperty("items")
	private List<ProcessTransactionResponseItem> lstItem;

	@Column(name = "manufacturer")
	@JsonProperty("manufacturer")
	private List<ProcessTransactionResponseManufacturer> lstManufacturer;

	@Column(name = "bank")
	@JsonProperty("bank")
	private List<UPointBank> lstBank;

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public BigDecimal getManufacturerItemsPoint() {
		return manufacturerItemsPoint;
	}

	public void setManufacturerItemsPoint(BigDecimal manufacturerItemsPoint) {
		this.manufacturerItemsPoint = manufacturerItemsPoint;
	}

	public BigDecimal getBalancePoint() {
		return balancePoint;
	}

	public void setBalancePoint(BigDecimal balancePoint) {
		this.balancePoint = balancePoint;
	}

	public BigDecimal getMerchantPoint() {
		return merchantPoint;
	}

	public void setMerchantPoint(BigDecimal merchantPoint) {
		this.merchantPoint = merchantPoint;
	}

	public BigDecimal getManufacturerPoint() {
		return manufacturerPoint;
	}

	public void setManufacturerPoint(BigDecimal manufacturerPoint) {
		this.manufacturerPoint = manufacturerPoint;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getPayPoint() {
		return payPoint;
	}

	public void setPayPoint(BigDecimal payPoint) {
		this.payPoint = payPoint;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BigDecimal getBankPoint() {
		return bankPoint;
	}

	public void setBankPoint(BigDecimal bankPoint) {
		this.bankPoint = bankPoint;
	}

	public BigDecimal getTotalPoint() {
		return totalPoint;
	}

	public void setTotalPoint(BigDecimal totalPoint) {
		this.totalPoint = totalPoint;
	}

	public List<ProcessTransactionResponseItem> getLstItem() {
		return lstItem;
	}

	public void setLstItem(List<ProcessTransactionResponseItem> lstItem) {
		this.lstItem = lstItem;
	}

	public List<ProcessTransactionResponseManufacturer> getLstManufacturer() {
		return lstManufacturer;
	}

	public void setLstManufacturer(List<ProcessTransactionResponseManufacturer> lstManufacturer) {
		this.lstManufacturer = lstManufacturer;
	}

	public List<UPointBank> getLstBank() {
		return lstBank;
	}

	public void setLstBank(List<UPointBank> lstBank) {
		this.lstBank = lstBank;
	}
}