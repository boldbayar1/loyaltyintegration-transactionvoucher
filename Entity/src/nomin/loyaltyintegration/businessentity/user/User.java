package nomin.loyaltyintegration.businessentity.user;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("phoneNumber")
	private String phoneNumber;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("noCardContinue")
	private String noCardContinue;
	
	@JsonProperty("ebarimtBillType")
	private String ebarimtBillType;
	
	@JsonProperty("customerNo")
	private String customerNo;
	
	@JsonProperty("register")
	private String register;
	
	@JsonProperty("cardholder")
	private String cardholder;
	
	public User() {
		super();
	}

	public User(String phoneNumber, String email, String noCardContinue, String ebarimtBillType, String customerNo, String register, String cardholder) {
		super();
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.noCardContinue = noCardContinue;
		this.ebarimtBillType = ebarimtBillType;
		this.customerNo = customerNo;
		this.register = register;
		this.cardholder = cardholder;
	}

	public String getPhoneNumber() {
		if(phoneNumber == null)
			phoneNumber = "";
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		if(email == null)
			email = "";
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoCardContinue() {
		if(noCardContinue == null)
			noCardContinue = "false";
		return noCardContinue;
	}

	public void setNoCardContinue(String noCardContinue) {
		this.noCardContinue = noCardContinue;
	}

	public String getEbarimtBillType() {
		if(ebarimtBillType == null)
			ebarimtBillType = "";
		return ebarimtBillType;
	}

	public void setEbarimtBillType(String ebarimtBillType) {
		this.ebarimtBillType = ebarimtBillType;
	}

	public String getCustomerNo() {
		if(customerNo == null)
			customerNo = "";
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getRegister() {
		if(register == null)
			register = "";
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getCardholder() {
		if(cardholder == null)
			cardholder = "";
		return cardholder;
	}

	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}
}
