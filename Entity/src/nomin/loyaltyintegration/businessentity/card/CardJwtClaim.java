package nomin.loyaltyintegration.businessentity.card;

import java.io.Serializable;

public class CardJwtClaim implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String cardType;
	private String cardNumber;
	private String pinType;
	private String pinData;
	
	public CardJwtClaim() {
		super();
	}

	public CardJwtClaim(String cardType, String cardNumber, String pinType, String pinData) {
		super();
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.pinType = pinType;
		this.pinData = pinData;
	}
	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getPinType() {
		return pinType;
	}
	public void setPinType(String pinType) {
		this.pinType = pinType;
	}
	public String getPinData() {
		return pinData;
	}
	public void setPinData(String pinData) {
		this.pinData = pinData;
	}
}
