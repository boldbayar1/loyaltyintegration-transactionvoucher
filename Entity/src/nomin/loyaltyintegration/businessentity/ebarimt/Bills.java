package nomin.loyaltyintegration.businessentity.ebarimt;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bills implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("billIdSuffix")
	private String billIdSuffix;

	@JsonProperty("amount")
	private String amount;

	@JsonProperty("vat")
	private String vat;
	
	@JsonProperty("cashAmount")
	private String cashAmount;
	
	@JsonProperty("nonCashAmount")
	private String nonCashAmount;
	
	@JsonProperty("cityTax")
	private String cityTax;
	
	@JsonProperty("districtCode")
	private String districtCode;
	
	@JsonProperty("billType")
	private String billType;
	
	@JsonProperty("posNo")
	private String posNo;
	
	@JsonProperty("customerNo")
	private String customerNo;
	
	@JsonProperty("returnBillId")
	private String returnBillId;
	
	@JsonProperty("internalId")
	private String internalId;
	
	@JsonProperty("registerNo")
	private String registerNo;
	
	@JsonProperty("stocks")
	private List<Stocks> stocks;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(String cashAmount) {
		this.cashAmount = cashAmount;
	}

	public String getNonCashAmount() {
		return nonCashAmount;
	}

	public void setNonCashAmount(String nonCashAmount) {
		this.nonCashAmount = nonCashAmount;
	}

	public String getCityTax() {
		return cityTax;
	}

	public void setCityTax(String cityTax) {
		this.cityTax = cityTax;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getReturnBillId() {
		return returnBillId;
	}

	public void setReturnBillId(String returnBillId) {
		this.returnBillId = returnBillId;
	}

	public List<Stocks> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stocks> stocks) {
		this.stocks = stocks;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getRegisterNo() {
		return registerNo;
	}

	public void setRegisterNo(String registerNo) {
		this.registerNo = registerNo;
	}	
}
