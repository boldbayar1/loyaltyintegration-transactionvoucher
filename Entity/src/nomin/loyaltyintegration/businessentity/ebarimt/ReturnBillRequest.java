package nomin.loyaltyintegration.businessentity.ebarimt;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import nomin.loyaltyintegration.tools.DateAsStringSerializer;

public class ReturnBillRequest {

	@JsonProperty
	private String returnBillId;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonSerialize(using = DateAsStringSerializer.class)
	@JsonProperty("date")
	private Date billDate;

	public String getReturnBillId() {
		return returnBillId;
	}

	public void setReturnBillId(String returnBillId) {
		this.returnBillId = returnBillId;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}
}
