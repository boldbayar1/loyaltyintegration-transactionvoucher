package nomin.loyaltyintegration.businessentity.ebarimt;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnBillResponse {

	@JsonProperty
	private boolean success;

	@JsonProperty
	private Integer errorCode;

	@JsonProperty
	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
