package nomin.loyaltyintegration.businessentity.ebarimt;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EbarimtRequest implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("group")
	private boolean group;
	
	@JsonProperty("billIdSuffix")
	private String billIdSuffix;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("vat")
	private String vat;
	
	@JsonProperty("cashAmount")
	private String cashAmount;
	
	@JsonProperty("nonCashAmount")
	private String nonCashAmount;
	
	@JsonProperty("cityTax")
	private String cityTax;
	
	@JsonProperty("billType")
	private String billType;
	
	@JsonProperty("posNo")
	private String posNo;
	
	@JsonProperty("bills")
	private List<Bills> bills;

	public boolean isGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillIdSuffix() {
		return billIdSuffix;
	}

	public void setBillIdSuffix(String billIdSuffix) {
		this.billIdSuffix = billIdSuffix;
	}

	public String getPosNo() {
		return posNo;
	}

	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	public List<Bills> getBills() {
		return bills;
	}

	public void setBills(List<Bills> bills) {
		this.bills = bills;
	}

	public String getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(String cashAmount) {
		this.cashAmount = cashAmount;
	}

	public String getNonCashAmount() {
		return nonCashAmount;
	}

	public void setNonCashAmount(String nonCashAmount) {
		this.nonCashAmount = nonCashAmount;
	}

	public String getCityTax() {
		return cityTax;
	}

	public void setCityTax(String cityTax) {
		this.cityTax = cityTax;
	}	
	
}




	
	
