package nomin.loyaltyintegration.businessentity.ebarimt;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckRegisterResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private Integer status;
	
	@JsonProperty("message")
	private String message;
		
	@JsonProperty("vatpayerRegisteredDate")
	private String vatpayerRegisteredDate;
	
	@JsonProperty("lastReceiptDate")
	private String lastReceiptDate;
	
	@JsonProperty("receiptFound")
	private String receiptFound;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("found")
	private String found;
	
	@JsonProperty("citypayer")
	private String citypayer;
	
	@JsonProperty("vatpayer")
	private String vatpayer;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getVatpayerRegisteredDate() {
		return vatpayerRegisteredDate;
	}

	public void setVatpayerRegisteredDate(String vatpayerRegisteredDate) {
		this.vatpayerRegisteredDate = vatpayerRegisteredDate;
	}

	public String getLastReceiptDate() {
		return lastReceiptDate;
	}

	public void setLastReceiptDate(String lastReceiptDate) {
		this.lastReceiptDate = lastReceiptDate;
	}

	public String getReceiptFound() {
		return receiptFound;
	}

	public void setReceiptFound(String receiptFound) {
		this.receiptFound = receiptFound;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFound() {
		return found;
	}

	public void setFound(String found) {
		this.found = found;
	}

	public String getCitypayer() {
		return citypayer;
	}

	public void setCitypayer(String citypayer) {
		this.citypayer = citypayer;
	}

	public String getVatpayer() {
		return vatpayer;
	}

	public void setVatpayer(String vatpayer) {
		this.vatpayer = vatpayer;
	}		
}
