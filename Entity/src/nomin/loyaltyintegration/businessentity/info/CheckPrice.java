package nomin.loyaltyintegration.businessentity.info;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Cacheable(false)
public class CheckPrice implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name="Status")
	private Integer status;
		
	@Column(name="Message")
	private String message;
		
	@Column(name="Name")
	private String name;
	
	@Column(name="ItemId")
	private String itemId;
	
	@Column(name="Measure")
	private String measure;
	
	@Column(name="Price")
	private Double price;
	
	@Column(name="WholeQty")
	private Double wholeQty;
	
	@Column(name="WholePrice")
	private Double wholePrice;
	
	@Column(name="DisPrice")
	private Double disPrice;
	
	@Column(name="WholeDisQty")
	private Double wholeDisQty;
	
	@Column(name="WholeDisPrice")
	private Double wholeDisPrice;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getWholeQty() {
		return wholeQty;
	}

	public void setWholeQty(Double wholeQty) {
		this.wholeQty = wholeQty;
	}

	public Double getWholePrice() {
		return wholePrice;
	}

	public void setWholePrice(Double wholePrice) {
		this.wholePrice = wholePrice;
	}

	public Double getDisPrice() {
		return disPrice;
	}

	public void setDisPrice(Double disPrice) {
		this.disPrice = disPrice;
	}

	public Double getWholeDisQty() {
		return wholeDisQty;
	}

	public void setWholeDisQty(Double wholeDisQty) {
		this.wholeDisQty = wholeDisQty;
	}

	public Double getWholeDisPrice() {
		return wholeDisPrice;
	}

	public void setWholeDisPrice(Double wholeDisPrice) {
		this.wholeDisPrice = wholeDisPrice;
	}
}
