package nomin.loyaltyintegration.businessentity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.spectre.response.VoucherResponse;
import nomin.loyaltyintegration.entity.payment.VoucherPaymentResponse;


public class Response implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private int status;

	@JsonProperty("data")
	private String data;
	
	@JsonProperty("billKey")
	private String billKey;
	
	@JsonProperty("totalVoucherAmount")
	private BigDecimal totalVoucherAmount;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("voucherList")
	private List<VoucherPaymentResponse> voucherList;
	
	@JsonProperty("voucherPaymentsResultL2")
	List<VoucherResponse> lstVoucherPaymentL2Response;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public BigDecimal getTotalVoucherAmount() {
		if(totalVoucherAmount == null)
			totalVoucherAmount = BigDecimal.ZERO;
		return totalVoucherAmount;
	}

	public void setTotalVoucherAmount(BigDecimal totalVoucherAmount) {
		this.totalVoucherAmount = totalVoucherAmount;
	}

	public String getBillKey() {
		return billKey;
	}

	public void setBillKey(String billKey) {
		this.billKey = billKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<VoucherPaymentResponse> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<VoucherPaymentResponse> voucherList) {
		this.voucherList = voucherList;
	}

	public List<VoucherResponse> getLstVoucherPaymentL2Response() {
		return lstVoucherPaymentL2Response;
	}

	public void setLstVoucherPaymentL2Response(List<VoucherResponse> lstVoucherPaymentL2Response) {
		this.lstVoucherPaymentL2Response = lstVoucherPaymentL2Response;
	}
}
