package nomin.loyaltyintegration.businessentity.log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.ebarimt.EbarimtRequest;
import nomin.loyaltyintegration.businessentity.qpay.payment.cancel.QPayPaymentCancelRequest;
import nomin.loyaltyintegration.businessentity.qpay.payment.check.pos.QPayPaymentCheckRequest;
import nomin.loyaltyintegration.businessentity.qpay.pos.QPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.socialpay.bill.create.pos.SocialPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.pos.SocialPayPaymentCancelRequest;
import nomin.loyaltyintegration.businessentity.socialpay.payment.check.pos.SocialPayPaymentCheckRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherCheckRequest;
import nomin.loyaltyintegration.businessentity.user.User;
import nomin.loyaltyintegration.entity.bill.Bills;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.login.DeviceInfo;
import nomin.loyaltyintegration.entity.payment.PaymentInfo;
import nomin.loyaltyintegration.entity.payment.Voucher;
import nomin.posreport.web.businessentity.bill.BillReportRequest;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("requestId")
	private String requestId;
	
	@JsonProperty("qrData")
	private String qrData;
	
	@JsonProperty("invoiceId")
	private String invoiceId;
	
	@JsonProperty("device")
	private DeviceInfo device;

	@JsonProperty("voucherPaymentsL1")
	private List<Voucher> voucherPayments;
	
	@JsonProperty("voucherPaymentsL2")
	private List<VoucherCheckRequest> voucherPaymentsL2;
	
	@JsonProperty("bill")
	private Bills bill;
	
	@JsonIgnoreProperties("payments")
	private List<PaymentInfo> payments;
	
	@JsonProperty("card")
	private CardInfoRequest card;
	
	@JsonProperty("user")
	private User user;
	
	@JsonProperty("ebarimt")
	private EbarimtRequest ebarimt;
	
	@JsonProperty("billImage")
	private BillReportRequest billImage;
	
	@JsonProperty("qpay")
	private QPayBillCreateRequest qPayBillCreateRequest;
	
	@JsonProperty("qpayCheck")
	private QPayPaymentCheckRequest qPayPaymentCheckRequest;
	
	@JsonProperty("qpayCancel")
	private QPayPaymentCancelRequest qPayPaymentCancelRequest;
	
	@JsonProperty("socialPay")
	private SocialPayBillCreateRequest socialPayBillCreateRequest;
	
	@JsonProperty("socialPayCancel")
	private SocialPayPaymentCancelRequest socialPayPaymentCancelRequest;
	
	@JsonProperty("socialPayCheck")
	private SocialPayPaymentCheckRequest socialPayPaymentCheckRequest;
	
	@JsonProperty("sendEbarimt")
	private boolean sendEbarimt;
	
	@JsonProperty("upointAlwaysAdded")
	private boolean upointAlwaysAdded;
		
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

	public List<Voucher> getVoucherPayments() {
		return voucherPayments;
	}

	public void setVoucherPayments(List<Voucher> voucherPayments) {
		this.voucherPayments = voucherPayments;
	}

	public List<VoucherCheckRequest> getVoucherPaymentsL2() {
		return voucherPaymentsL2;
	}

	public void setVoucherPaymentsL2(List<VoucherCheckRequest> voucherPaymentsL2) {
		this.voucherPaymentsL2 = voucherPaymentsL2;
	}
	
	public Bills getBill() {
		return bill;
	}

	public void setBill(Bills bill) {
		this.bill = bill;
	}

	public CardInfoRequest getCard() {
		if(card == null)
			card = new CardInfoRequest();
		return card;
	}

	public void setCard(CardInfoRequest card) {
		this.card = card;
	}

	public List<PaymentInfo> getPayments() {
		if(payments == null)
			payments = new ArrayList<>();
		return payments;
	}

	public void setPayments(List<PaymentInfo> payments) {
		this.payments = payments;
	}

	public User getUser() {
		if(user == null)
			user = new User();
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EbarimtRequest getEbarimt() {
		if(ebarimt == null)
			ebarimt = new EbarimtRequest();
		return ebarimt;
	}

	public void setEbarimt(EbarimtRequest ebarimt) {
		this.ebarimt = ebarimt;
	}

	public BillReportRequest getBillImage() {
		if(billImage == null)
			billImage = new BillReportRequest();
		return billImage;
	}

	public void setBillImage(BillReportRequest billImage) {
		this.billImage = billImage;
	}

	public QPayBillCreateRequest getqPayBillCreateRequest() {
		if(qPayBillCreateRequest == null)
			qPayBillCreateRequest = new QPayBillCreateRequest();
		return qPayBillCreateRequest;
	}

	public void setqPayBillCreateRequest(QPayBillCreateRequest qPayBillCreateRequest) {
		this.qPayBillCreateRequest = qPayBillCreateRequest;
	}

	public QPayPaymentCheckRequest getqPayPaymentCheckRequest() {
		if(qPayPaymentCheckRequest == null)
			qPayPaymentCheckRequest = new QPayPaymentCheckRequest();
		return qPayPaymentCheckRequest;
	}

	public void setqPayPaymentCheckRequest(QPayPaymentCheckRequest qPayPaymentCheckRequest) {
		this.qPayPaymentCheckRequest = qPayPaymentCheckRequest;
	}

	public SocialPayBillCreateRequest getSocialPayBillCreateRequest() {
		if(socialPayBillCreateRequest == null)
			socialPayBillCreateRequest = new SocialPayBillCreateRequest();
		return socialPayBillCreateRequest;
	}

	public void setSocialPayBillCreateRequest(SocialPayBillCreateRequest socialPayBillCreateRequest) {
		this.socialPayBillCreateRequest = socialPayBillCreateRequest;
	}
	
	public SocialPayPaymentCheckRequest getSocialPayPaymentCheckRequest() {
		if(socialPayPaymentCheckRequest == null)
			socialPayPaymentCheckRequest = new SocialPayPaymentCheckRequest();
		return socialPayPaymentCheckRequest;
	}

	public void setSocialPayPaymentCheckRequest(SocialPayPaymentCheckRequest socialPayPaymentCheckRequest) {
		this.socialPayPaymentCheckRequest = socialPayPaymentCheckRequest;
	}

	public QPayPaymentCancelRequest getqPayPaymentCancelRequest() {
		if(qPayPaymentCancelRequest == null)
			qPayPaymentCancelRequest = new QPayPaymentCancelRequest();
		return qPayPaymentCancelRequest;
	}

	public void setqPayPaymentCancelRequest(QPayPaymentCancelRequest qPayPaymentCancelRequest) {
		this.qPayPaymentCancelRequest = qPayPaymentCancelRequest;
	}

	public SocialPayPaymentCancelRequest getSocialPayPaymentCancelRequest() {
		if(socialPayPaymentCancelRequest == null)
			socialPayPaymentCancelRequest = new SocialPayPaymentCancelRequest();
		return socialPayPaymentCancelRequest;
	}

	public void setSocialPayPaymentCancelRequest(SocialPayPaymentCancelRequest socialPayPaymentCancelRequest) {
		this.socialPayPaymentCancelRequest = socialPayPaymentCancelRequest;
	}

	public boolean isSendEbarimt() {
		return sendEbarimt;
	}

	public void setSendEbarimt(boolean sendEbarimt) {
		this.sendEbarimt = sendEbarimt;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public boolean isUpointAlwaysAdded() {
		return upointAlwaysAdded;
	}

	public void setUpointAlwaysAdded(boolean upointAlwaysAdded) {
		this.upointAlwaysAdded = upointAlwaysAdded;
	}
}
