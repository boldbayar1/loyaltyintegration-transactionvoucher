package nomin.loyaltyintegration.businessentity.spectre.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherTransactionRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("locationKey")
	private Long locationKey;

	@JsonProperty("posKey")
	private Long posKey;

	@JsonProperty("cashierKey")
	private Long cashierKey;

	@JsonProperty("billKey")
	private Long billKey;
	
	@JsonProperty("systemId")
	private String systemId;
	
	@JsonProperty("phoneNumber")
	private String phoneNumber;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("suspend")
	private Integer suspend;
	
	@JsonProperty("details")
	private List<VoucherDetails> details;

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public Long getCashierKey() {
		return cashierKey;
	}

	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}

	public Long getBillKey() {
		return billKey;
	}

	public void setBillKey(Long billKey) {
		this.billKey = billKey;
	}

	public String getSystemId() {
		if(systemId == null)
			systemId = "Integration";
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSuspend() {
		return suspend;
	}

	public void setSuspend(Integer suspend) {
		this.suspend = suspend;
	}

	public List<VoucherDetails> getDetails() {
		if(details == null)
			details = new ArrayList<>();
		return details;
	}

	public void setDetails(List<VoucherDetails> details) {
		this.details = details;
	}
}
