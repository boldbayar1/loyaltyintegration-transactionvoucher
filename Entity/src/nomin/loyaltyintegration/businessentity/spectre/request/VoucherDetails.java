package nomin.loyaltyintegration.businessentity.spectre.request;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("voucherId")
	private String voucherId;

	@JsonProperty("amount")
	private BigDecimal amount;

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
