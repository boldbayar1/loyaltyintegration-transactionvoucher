package nomin.loyaltyintegration.businessentity.spectre.request;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherSmsRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("type")
	private String type;
	
	@JsonProperty("voucherId")
	private String voucherId;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("phoneNumber")
	private String phoneNumber;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}	
}
