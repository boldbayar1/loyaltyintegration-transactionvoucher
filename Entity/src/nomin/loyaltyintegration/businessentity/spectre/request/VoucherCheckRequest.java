package nomin.loyaltyintegration.businessentity.spectre.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherCheckRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("voucherId")
	private String voucherId;
	
	@JsonProperty("locationKey")
	private Long locationKey;
	
	@JsonProperty("posKey")
	private Long posKey;
	
	@JsonProperty("cashierKey")
	private Long cashierKey;

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public Long getLocationKey() {
		return locationKey;
	}

	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}

	public Long getPosKey() {
		return posKey;
	}

	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}

	public Long getCashierKey() {
		return cashierKey;
	}

	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}
}
