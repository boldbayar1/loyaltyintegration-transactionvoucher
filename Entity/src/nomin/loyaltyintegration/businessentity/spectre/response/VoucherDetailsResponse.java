package nomin.loyaltyintegration.businessentity.spectre.response;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VoucherDetailsResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private Integer status;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("balance")
	private BigDecimal balance;
	
	@JsonProperty("voucherId")
	private String voucherId;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}
}
