package nomin.loyaltyintegration.businessentity.qr;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QrResponse {

	@JsonProperty("status")
	private int status;

	@JsonProperty("message")
	private String message;

	@JsonProperty("qrData")
	private String qrData;
	
	@JsonProperty("qrDataImageBase64")
	private String qrDataImageBase64;
	
	@JsonProperty("qrDataImageExt")
	private String qrDataImageExt;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public String getQrDataImageBase64() {
		return qrDataImageBase64;
	}

	public void setQrDataImageBase64(String qrDataImageBase64) {
		this.qrDataImageBase64 = qrDataImageBase64;
	}

	public String getQrDataImageExt() {
		return qrDataImageExt;
	}

	public void setQrDataImageExt(String qrDataImageExt) {
		this.qrDataImageExt = qrDataImageExt;
	}
}