package nomin.loyaltyintegration.businessentity.qr;

import nomin.loyaltyintegration.entity.login.DeviceInfo;

public class QrRequest {

	private DeviceInfo device;

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

}