package nomin.loyaltyintegration.businessentity.qr;

import nomin.loyaltyintegration.entity.login.DeviceInfo;

public class QrReadRequest {

	private DeviceInfo device;
	private String qrData;
	private String cardNumber;

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
}