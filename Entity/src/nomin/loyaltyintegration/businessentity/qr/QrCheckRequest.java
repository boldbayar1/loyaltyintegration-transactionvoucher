package nomin.loyaltyintegration.businessentity.qr;

import java.io.Serializable;
import nomin.loyaltyintegration.entity.login.DeviceInfo;

public class QrCheckRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private DeviceInfo device;
	private String qrData;
	private String data;

	public DeviceInfo getDevice() {
		return device;
	}

	public void setDevice(DeviceInfo device) {
		this.device = device;
	}

	public String getQrData() {
		return qrData;
	}

	public void setQrData(String qrData) {
		this.qrData = qrData;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
