package nomin.loyaltyintegration.businessentity.qr;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class QrCheckResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private int status;

	@JsonProperty("message")
	private String message;

	@JsonProperty("email")
	private String email;
	
	@JsonProperty("phone")
	private String phone;
	
	@JsonProperty("register")
	private String register;
	
	@JsonProperty("nominCard")
	private String nominCard;
	
	@JsonProperty("upointCard")
	private String upointCard;
	
	@JsonProperty("firstName")
	private String firstName;
	
	@JsonProperty("lastName")
	private String lastName;
	
	@JsonProperty("vatCustomerNumber")
	private String vatCustomerNumber;
	
	@JsonProperty("cardPercent")
	private Double cardPercent;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getNominCard() {
		return nominCard;
	}

	public void setNominCard(String nominCard) {
		this.nominCard = nominCard;
	}

	public String getUpointCard() {
		return upointCard;
	}

	public void setUpointCard(String upointCard) {
		this.upointCard = upointCard;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getVatCustomerNumber() {
		return vatCustomerNumber;
	}

	public void setVatCustomerNumber(String vatCustomerNumber) {
		this.vatCustomerNumber = vatCustomerNumber;
	}

	public Double getCardPercent() {
		return cardPercent;
	}

	public void setCardPercent(Double cardPercent) {
		this.cardPercent = cardPercent;
	}
}
