package nomin.loyaltyintegration.businessentity.credential;

import java.util.List;

public class User {

	private Long userKey;
	private String username;
	private String password;

	private List<String> roles;

	public User() {

	}

	public User(Long userKey, String username, List<String> roles) {
		super();
		this.userKey = userKey;
		this.username = username;
		this.roles = roles;
	}
	
	public User(Long userKey, String username, String password, List<String> roles) {
		super();
		this.userKey = userKey;
		this.username = username;
		this.password = password;
		this.roles = roles;		
	}

	public Long getUserKey() {
		return userKey;
	}

	public void setUserKey(Long userKey) {
		this.userKey = userKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}