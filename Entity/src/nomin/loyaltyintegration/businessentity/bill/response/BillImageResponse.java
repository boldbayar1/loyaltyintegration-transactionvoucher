package nomin.loyaltyintegration.businessentity.bill.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.posreport.web.businessentity.report.ReportResponse;

public class BillImageResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private Integer status;

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("billImage")
	private String billImage;
		
	@JsonProperty("billImageExt")
	private String billImageExt;
	
	@JsonProperty("sendMessage")
	private String sendMessage;
	
	@JsonProperty("sendMail")
	private String sendMail;
	
	@JsonProperty("billInfo")
	private String billInfo;
	
	@JsonProperty("billState")
	private String billState;
	
	@JsonProperty("billImageBmp")
	private ReportResponse billImageBmp;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getBillImage() {
		return billImage;
	}

	public void setBillImage(String billImage) {
		this.billImage = billImage;
	}

	public String getBillImageExt() {
		return billImageExt;
	}

	public void setBillImageExt(String billImageExt) {
		this.billImageExt = billImageExt;
	}

	public String getSendMessage() {
		return sendMessage;
	}

	public void setSendMessage(String sendMessage) {
		this.sendMessage = sendMessage;
	}

	public String getSendMail() {
		return sendMail;
	}

	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}

	public String getBillInfo() {
		return billInfo;
	}

	public void setBillInfo(String billInfo) {
		this.billInfo = billInfo;
	}

	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}

	public ReportResponse getBillImageBmp() {
		return billImageBmp;
	}

	public void setBillImageBmp(ReportResponse billImageBmp) {
		this.billImageBmp = billImageBmp;
	}
}
