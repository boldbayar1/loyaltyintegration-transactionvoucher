package nomin.loyaltyintegration.businessentity.qpay.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QpayAuthTokenResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("token_type")
	private String tokenType;
	
	@JsonProperty("refresh_expires_in")
	private String refreshExpiresIn;
	
	@JsonProperty("refresh_token")
	private String refreshToken;
	
	@JsonProperty("access_token")
	private String accessToken;
	
	@JsonProperty("expires_in")
	private String expiresIn;
	
	@JsonProperty("scope")
	private String scope;
	
	@JsonProperty("not-before-policy")
	private Integer notBeforePolicy;
	
	@JsonProperty("session_state")
	private String sessionState;

	public String getTokenType() {
		return tokenType;
	}

	public String getRefreshExpiresIn() {
		return refreshExpiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public Integer getNotBeforePolicy() {
		return notBeforePolicy;
	}

	public String getSessionState() {
		return sessionState;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public void setRefreshExpiresIn(String refreshExpiresIn) {
		this.refreshExpiresIn = refreshExpiresIn;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public void setNotBeforePolicy(Integer notBeforePolicy) {
		this.notBeforePolicy = notBeforePolicy;
	}

	public void setSessionState(String sessionState) {
		this.sessionState = sessionState;
	}
}



