package nomin.loyaltyintegration.businessentity.qpay.payment.check;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.qpay.data.PaymentInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QPayPaymentCheckResponse {
	@JsonProperty("payment_info")
	private PaymentInfo paymentInfo;
	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
}