package nomin.loyaltyintegration.businessentity.qpay.pos;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QPayBillCreateRequest {

	private Long locationKey;
	private Long posKey;
	private Long cashierKey;
	private String orderNo;
	private BigDecimal amount;
	private String paymentMethod;
	private String customerQr;
	
	public Long getLocationKey() {
		return locationKey;
	}
	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}
	public Long getPosKey() {
		return posKey;
	}
	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}
	public Long getCashierKey() {
		return cashierKey;
	}
	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCustomerQr() {
		return customerQr;
	}
	public void setCustomerQr(String customerQr) {
		this.customerQr = customerQr;
	}
}