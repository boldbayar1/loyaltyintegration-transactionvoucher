package nomin.loyaltyintegration.businessentity.qpay.request;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class QpayCreateInvoiceRequest  implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("invoice_code")
	private String invoiceCode;

	@JsonProperty("sender_invoice_no")
	private String senderInvoiceNumber;
	
	@JsonProperty("invoice_receiver_code")
	private String invoiceReceiverCode;
	
	@JsonProperty("invoice_description")
	private String invoiceDescription;
	
	@JsonProperty("amount")
	private BigDecimal amount;
	
	@JsonProperty("callback_url")
	private String callbackUrl;

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public String getSenderInvoiceNumber() {
		return senderInvoiceNumber;
	}

	public String getInvoiceReceiverCode() {
		return invoiceReceiverCode;
	}

	public String getInvoiceDescription() {
		return invoiceDescription;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public void setSenderInvoiceNumber(String senderInvoiceNumber) {
		this.senderInvoiceNumber = senderInvoiceNumber;
	}

	public void setInvoiceReceiverCode(String invoiceReceiverCode) {
		this.invoiceReceiverCode = invoiceReceiverCode;
	}

	public void setInvoiceDescription(String invoiceDescription) {
		this.invoiceDescription = invoiceDescription;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
}