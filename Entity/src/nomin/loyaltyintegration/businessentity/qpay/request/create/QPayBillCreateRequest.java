package nomin.loyaltyintegration.businessentity.qpay.request.create;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import nomin.loyaltyintegration.businessentity.qpay.data.BranchInfo;
import nomin.loyaltyintegration.businessentity.qpay.data.GoodsDetail;
import nomin.loyaltyintegration.businessentity.qpay.data.VatInfo;
import nomin.loyaltyintegration.businessentity.qpay.data.Config;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QPayBillCreateRequest {

	@JsonProperty("template_id")
	private String templateId;
	
	@JsonProperty("merchant_id")
	private String merchantId;
	
	@JsonProperty("branch_info")
	private BranchInfo branchInfo;
	
	@JsonProperty("vat_info")
	private VatInfo vatInfo;
	
	@JsonProperty("order_no")
	private String orderNo;
	
	@JsonProperty("allow_partial_payment")
	private boolean allowPartialPayment;
	
	@JsonProperty("allow_tip")
	private boolean allowTip;
	
	@JsonProperty("currency_type")
	private String currencyType;
	
	@JsonProperty("goods_detail")
	private List<GoodsDetail> lstGoodDetail;
	
	@JsonProperty("note")
	private String note;
	
	@JsonProperty("attach")
	private String attach;
	
	@JsonProperty("payment_method")
	private String paymentMethod;
	
	@JsonProperty("customer_qr")
	private String customerQr;
	
	@JsonProperty("device_info")
	private String deviceInfo;
	
	@JsonProperty("nonce_str")
	private String nonceStr;
	
	@JsonProperty("logo_url")
	private String logoUrl;
	
	@JsonProperty("callback_url")
	private String callbackUrl;
	
	@JsonProperty("inform_email")
	private String informEmail;
	
	@JsonProperty("config")
	private Config config;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public BranchInfo getBranchInfo() {
		return branchInfo;
	}

	public void setBranchInfo(BranchInfo branchInfo) {
		this.branchInfo = branchInfo;
	}

	public VatInfo getVatInfo() {
		return vatInfo;
	}

	public void setVatInfo(VatInfo vatInfo) {
		this.vatInfo = vatInfo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public boolean isAllowPartialPayment() {
		return allowPartialPayment;
	}

	public void setAllowPartialPayment(boolean allowPartialPayment) {
		this.allowPartialPayment = allowPartialPayment;
	}

	public boolean isAllowTip() {
		return allowTip;
	}

	public void setAllowTip(boolean allowTip) {
		this.allowTip = allowTip;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public List<GoodsDetail> getLstGoodDetail() {
		return lstGoodDetail;
	}

	public void setLstGoodDetail(List<GoodsDetail> lstGoodDetail) {
		this.lstGoodDetail = lstGoodDetail;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getCustomerQr() {
		return customerQr;
	}

	public void setCustomerQr(String customerQr) {
		this.customerQr = customerQr;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getInformEmail() {
		return informEmail;
	}

	public void setInformEmail(String informEmail) {
		this.informEmail = informEmail;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}
}