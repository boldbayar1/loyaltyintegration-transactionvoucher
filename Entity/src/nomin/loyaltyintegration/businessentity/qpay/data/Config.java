package nomin.loyaltyintegration.businessentity.qpay.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Config {
	
	@JsonProperty("response")
	private ConfigResponse configResponse;

	public ConfigResponse getConfigResponse() {
		return configResponse;
	}

	public void setConfigResponse(ConfigResponse configResponse) {
		this.configResponse = configResponse;
	}
}