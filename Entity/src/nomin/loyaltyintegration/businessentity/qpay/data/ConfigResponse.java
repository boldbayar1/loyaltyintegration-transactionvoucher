package nomin.loyaltyintegration.businessentity.qpay.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigResponse {
	@JsonProperty("ignore_fields")
	private List<String> lstIgnoreField;

	public List<String> getLstIgnoreField() {
		return lstIgnoreField;
	}

	public void setLstIgnoreField(List<String> lstIgnoreField) {
		this.lstIgnoreField = lstIgnoreField;
	}
}