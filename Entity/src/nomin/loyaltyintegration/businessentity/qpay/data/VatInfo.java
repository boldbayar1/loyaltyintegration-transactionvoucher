package nomin.loyaltyintegration.businessentity.qpay.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VatInfo {
	@JsonProperty("is_auto_generate")
	private String isAutoGenerate;
	@JsonProperty("tax_register_no")
	private String taxRegisterNo;
	public String getIsAutoGenerate() {
		return isAutoGenerate;
	}
	public void setIsAutoGenerate(String isAutoGenerate) {
		this.isAutoGenerate = isAutoGenerate;
	}
	public String getTaxRegisterNo() {
		return taxRegisterNo;
	}
	public void setTaxRegisterNo(String taxRegisterNo) {
		this.taxRegisterNo = taxRegisterNo;
	}
}