package nomin.loyaltyintegration.businessentity.qpay.data;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentLog {
	private String pgName;
	private Long referenceNo;
	private Long locationKey;
	private Long posKey;
	private Long cashierKey;
	private BigDecimal amount;
	private Date requestTime;
	private String request;
	private Date responseTime;
	private String response;
	private String errorMessage;
	public String getPgName() {
		return pgName;
	}
	public void setPgName(String pgName) {
		this.pgName = pgName;
	}
	public Long getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(Long referenceNo) {
		this.referenceNo = referenceNo;
	}
	public Long getLocationKey() {
		return locationKey;
	}
	public void setLocationKey(Long locationKey) {
		this.locationKey = locationKey;
	}
	public Long getPosKey() {
		return posKey;
	}
	public void setPosKey(Long posKey) {
		this.posKey = posKey;
	}
	public Long getCashierKey() {
		return cashierKey;
	}
	public void setCashierKey(Long cashierKey) {
		this.cashierKey = cashierKey;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Date getRequestTime() {
		return requestTime;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
		this.requestTime = new Date();
	}
	public Date getResponseTime() {
		return responseTime;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
		this.responseTime = new Date();
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (responseTime == null) responseTime = new Date();
	}
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
}