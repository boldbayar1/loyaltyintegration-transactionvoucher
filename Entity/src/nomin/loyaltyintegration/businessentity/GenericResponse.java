package nomin.loyaltyintegration.businessentity;

import java.io.Serializable;

public class GenericResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private int statusCode;
	private String resultData;

	public GenericResponse() {
	}

	public GenericResponse(int statusCode, String resultData) {
		super();
		this.statusCode = statusCode;
		this.resultData = resultData;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getResultData() {
		return resultData;
	}

	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
}
