package com.nomin.loyaltyintegration.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.Credential;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nomin.loyaltyintegration.web.rest.authentication.interfaces.IAuthenticationMechanism;

@WebFilter(urlPatterns = {"*"})
public class AuthFilter extends HttpFilter {
	
	private static final long serialVersionUID = 1L;

	protected static final String loginUrl = "/index.html";
	protected static final String resourceUrl = "javax.faces.resource";
	protected static final String apiUrl = "/api/";
	
	protected static final String[] urls = new String[] {
		loginUrl, resourceUrl, apiUrl
	};
	
	@Inject
	IAuthenticationMechanism authenticationMechanism;
	
	@Inject
	SecurityContext securityContext;
	
	protected boolean isPermittedAtAll(String checkUri) {
		for(String url : urls)
			if(checkUri.contains(url)) return true;
		return false;
	}
	
	@Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		if(request.getRemoteUser() == null) {
			Credential credential = authenticationMechanism.getCredential(request);
			if(credential != null)
				securityContext.authenticate(request, response, new AuthenticationParameters().credential(credential));
		}
        if (request.getRemoteUser() != null || isPermittedAtAll(request.getRequestURI())) {
            filterChain.doFilter(request, response);
            if(request.getRemoteUser() != null && request.getRequestURI().contains(apiUrl)) {
            	request.logout();
            }
        } else {
        	response.sendRedirect(request.getContextPath()+loginUrl);
        }
	}
}