package com.nomin.loyaltyintegration.web.rest;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businesslogic.interfaces.ILoyaltyLogic;
import nomin.loyaltyintegration.helper.RestfulHelper;

@Path(value = "/payment")
@Stateless
public class PaymentRestful {

	
	@Inject ILoyaltyLogic loyaltyLogic;
	
	public PaymentRestful() {		
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/qpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response createQpayInvoice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.QpayCreateInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/cancel/qpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response cancelQpayInvoice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.QpayCancelInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/socialpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response createSocialPayInvoice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.SocialPayCreateInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/cancel/socialpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response cancelSocialPayInvoice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.SocialPayCancelInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
}
