package com.nomin.loyaltyintegration.web.rest.security;

import java.util.HashSet;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;

import nomin.loyaltyintegration.businessentity.credential.User;
import nomin.loyaltyintegration.businesslogic.interfaces.IUserCredentialLogicLocal;

@ApplicationScoped
public class RestIdentityStore implements IdentityStore {
	
	@Inject
	IUserCredentialLogicLocal userCredentialLogic;
	
	@Override
	public CredentialValidationResult validate(Credential credential) {
		try {
			UsernamePasswordCredential userCredential = (UsernamePasswordCredential) credential;
			User user = userCredentialLogic.login(userCredential.getCaller(), userCredential.getPasswordAsString());
			if(user != null) {
				System.err.println("Logged successfully.");
				return new CredentialValidationResult(user.getUsername(), new HashSet<>(user.getRoles()));
			}	
			System.err.println("Logged invalid");
			return CredentialValidationResult.INVALID_RESULT;
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return CredentialValidationResult.NOT_VALIDATED_RESULT;
	}
}