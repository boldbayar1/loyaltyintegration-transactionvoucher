package com.nomin.loyaltyintegration.web.rest.security;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ApplicationScoped
@AutoApplySession
public class RestServiceAuthenticationMechanism implements HttpAuthenticationMechanism {

	@Inject
	IdentityStore identityStore;	
	
	@Override
	public AuthenticationStatus validateRequest(HttpServletRequest request, HttpServletResponse response,
			HttpMessageContext context) throws AuthenticationException {
		Credential credential = context.getAuthParameters().getCredential();
        if (credential != null) {
        	CredentialValidationResult validation = identityStore.validate(credential);
        	if(validation != null && CredentialValidationResult.INVALID_RESULT.getStatus().equals(validation.getStatus())) {
        		throw new AuthenticationException("Login failed");
        	}   
        	return context.notifyContainerAboutLogin(validation);
        } else {
        	return context.doNothing();
        }
	}
}