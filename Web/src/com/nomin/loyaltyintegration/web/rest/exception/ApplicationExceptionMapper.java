package com.nomin.loyaltyintegration.web.rest.exception;

import javax.ejb.EJBAccessException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import nomin.loyaltyintegration.entity.payment.ErrorResponse;

@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<EJBAccessException> {

	@Override
	public Response toResponse(EJBAccessException exception) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatus(401);
		errorResponse.setMessage("Хандах эрх хүрэхгүй байна.");
		return Response.status(Response.Status.UNAUTHORIZED).entity(errorResponse).build();
	}
}
