package com.nomin.loyaltyintegration.web.rest;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.fasterxml.jackson.databind.ObjectMapper;

import nomin.loyaltyintegration.businessentity.GenericResponse;
import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businesslogic.interfaces.ICardLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ILoyaltyLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.IService;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IUPointLogicLocal;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.card.info.TestRequest;
import nomin.loyaltyintegration.entity.payment.ErrorResponse;
import nomin.loyaltyintegration.helper.JsonHelper;
import nomin.loyaltyintegration.helper.RestfulHelper;

@Path(value = "/bonus")
@Stateless
public class LoyaltyRestful {


	@Inject
	ICardLogic cardLogic;


	@Inject
	ITransactionLog logger;

	@Inject
	IUPointLogicLocal upoinLogic;

	@Inject
	ILoyaltyLogic loyaltyLogic;

	@Inject
	IService service;

	@Path("/info")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@PermitAll
	public Response info() {
		Response response = null;
		try {
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setStatus(1);
			errorResponse.setMessage("Амжилттай");
			response = Response.ok().entity(errorResponse).build();
		} catch (Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/calcBonus")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response calcBonus(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.CalcBonus(rawData, 0);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******CALCBONUS******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******CALCBONUS*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/calcBonus/change")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response calcBonusExchange(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.CalcBonusExchange(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******CALCBONUS-CHANGE******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******CALCBONUS-CHANGE*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/putBonus")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response putBonus2(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutBonus(rawData);
				
				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******PUTBONUS******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******PUTBONUS*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/putBonus/change")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response putBonusChange2(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutBonusChange(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******PUTBONUS-CHANGE******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******PUTBONUS-CHANGE*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/test/info")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response getCardInfo(String rawData) {
		System.err.println("/putBonus");
		System.err.println(rawData);
		Response response = null;
		Logger log = new Logger();
		try {
			TestRequest testRequest = new TestRequest();
			ObjectMapper objectMapper = JsonHelper.objectMapperStatic;
			testRequest = objectMapper.readValue(rawData, TestRequest.class);
			log.setDeviceId(testRequest.getDeviceId());

			CardInfoRequest request = new CardInfoRequest();
			request.setCardNumber(testRequest.getCardNumber());
			request.setCardType(testRequest.getCardType());
			request.setPinData(testRequest.getPinCode());
			request.setPinType(testRequest.getPinType());

			response = Response.ok().entity(cardLogic.getCardInfo2(request, log)).build();

		} catch (Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/getQr")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response getQr(String rawData) throws Exception {
		GenericResponse response = loyaltyLogic.GetQR(rawData);
		return Response.status(response.getStatusCode()).entity(response.getResultData()).build();
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/readQr")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response readQr(String rawData) throws Exception {
		GenericResponse response = loyaltyLogic.ReadQR(rawData);
		return Response.status(response.getStatusCode()).entity(response.getResultData()).build();
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/checkQr")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkQr(String rawData) throws Exception {
		GenericResponse response = loyaltyLogic.CheckQR(rawData);
		return Response.status(response.getStatusCode()).entity(response.getResultData()).build();
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/checkCardInfo")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkCardInfo(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.CheckCardInfo(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******CheckCardInfo******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******CheckCardInfo*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/checkRegisterNo")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkRegisterNo(String rawData) throws Exception {
		GenericResponse response = loyaltyLogic.CheckRegisterNo(rawData);
		return Response.status(response.getStatusCode()).entity(response.getResultData()).build();
	}
}
