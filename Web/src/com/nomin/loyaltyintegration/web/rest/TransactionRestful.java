package com.nomin.loyaltyintegration.web.rest;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businesslogic.interfaces.ILoyaltyLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.helper.RestfulHelper;

@Path(value = "/transaction")
@Stateless
public class TransactionRestful {

	@Inject ILoyaltyLogic loyaltyLogic;

	@Inject ITransactionLog logger;

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/bonus")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response transactionBonus2(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutTransaction(rawData, 1);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******BONUS******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******BONUS*******END******");
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/sales")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response transactionSales(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutTransaction(rawData, 0);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******SALES******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******SALES*******END******");
		}
		return response;
	}
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/return")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response transactionReturn2(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.ReturnTransaction(rawData);
				
				if(loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******RETURN******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******RETURN*******END******");
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/put/voucher")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response putVoucher(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutVoucher(rawData);
				
				if(loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******PUT-VOUCHER******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******PUT-VOUCHER*******END******");
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/put/direct/voucher")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response puDirectVoucher(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.PutVoucher(rawData);
				
				if(loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******PUT-VOUCHER******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******PUT-VOUCHER*******END******");
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/return/voucher")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response returnVoucher(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.ReturnVoucher(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******RETURN-VOUCHER******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******RETURN-VOUCHER*******END******");
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/sales/voucher")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response transactionVoucher(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.TransactionVoucher(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******SALES-VOUCHER******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******SALES-VOUCHER*******END******");
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/get/billImage")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response getBillImage(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.GetBillImage(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
			System.err.println("*************LOYALTYINTEGRATION******SALES-VOUCHER******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******SALES-VOUCHER*******END******");
		}
		return response;
	}
	
}
