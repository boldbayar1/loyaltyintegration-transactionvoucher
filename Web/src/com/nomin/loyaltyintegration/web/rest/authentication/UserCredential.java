package com.nomin.loyaltyintegration.web.rest.authentication;

import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.nomin.loyaltyintegration.web.rest.authentication.interfaces.ICredential;

@Named
@ApplicationScoped
public class UserCredential implements ICredential {

	byte[] salt = new byte[] { 72, 105, 78, 111, 109, 105, 110 };
	byte[] iv = new byte[] { 5, -3, 117, 17, -39, -8, 40, -74, 9, 112, -63, 109, -106, 108, 111, -55 };

	Cipher cipher;
	SecretKey key;

	String charset = "UTF-8";

	@PostConstruct
	public void postConstruct() {
		try {
			key = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec("HelloNomin".toCharArray(), salt, 1024, 256)).getEncoded(), "AES");
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String encode(String userName, String password) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
			String base64 = getBase64Encoded(userName, password);
			byte[] bytes = base64.getBytes();
			return getBase64Encoded(cipher.doFinal(bytes));
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public String[] decode(String encoded) {
//		try {
//			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
//			byte[] bytes = cipher.doFinal(getBase64Decoded(encoded));
//			return getDecodedFromBase64(bytes);
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			return null;
//		}
		
		String[] result = null;
		try {
//			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
//			byte[] bytes = cipher.doFinal(getBase64Decoded(encoded));
//			return getDecodedFromBase64(bytes);
			String usernameAndPassword = null;
			try {
				byte[] decodedBytes = Base64.getDecoder().decode(encoded);
				usernameAndPassword = new String(decodedBytes, "UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
			}
			final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
			result = new String[2];
			result[0] = tokenizer.nextToken();
			result[1] = tokenizer.nextToken();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return result;
	}

	protected String getBase64Encoded(String userName, String password) throws Exception {
		return getBase64Encoded(userName) + "-" + getBase64Encoded(password);
	}

	protected String getBase64Encoded(String toEncode) throws Exception {
		return getBase64Encoded(getBytes(toEncode));
	}

	protected String getBase64Encoded(byte[] bytes) throws Exception {
		return getString(Base64.getEncoder().encode(bytes));
	}

	protected byte[] getBytes(String str) throws Exception {
		return str.getBytes(charset);
	}

	protected String getString(byte[] bytes) throws Exception {
		return new String(bytes, charset);
	}

	protected byte[] getBase64Decoded(String encoded) throws Exception {
		return getBas64Decoded(getBytes(encoded));
	}

	protected byte[] getBas64Decoded(byte[] bytes) throws Exception {
		return Base64.getDecoder().decode(bytes);
	}

	protected String[] getDecodedFromBase64(byte[] bytes) throws Exception {
		String[] strs = getString(bytes).split("-");
		strs[0] = getString(getBase64Decoded(strs[0]));
		strs[1] = getString(getBase64Decoded(strs[1]));
		return strs;
	}
}
