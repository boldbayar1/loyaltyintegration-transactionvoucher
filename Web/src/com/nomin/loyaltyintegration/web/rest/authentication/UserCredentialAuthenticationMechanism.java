package com.nomin.loyaltyintegration.web.rest.authentication;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.http.HttpServletRequest;

import com.nomin.loyaltyintegration.web.rest.authentication.interfaces.IAuthenticationMechanism;
import com.nomin.loyaltyintegration.web.rest.authentication.interfaces.ICredential;

@Named
@ApplicationScoped
public class UserCredentialAuthenticationMechanism implements IAuthenticationMechanism {

	@Inject
	ICredential credential;
	
	@Override
	public UsernamePasswordCredential getCredential(HttpServletRequest request) {
		String auth = request.getHeader("Authorization");
		if(auth != null && !auth.isEmpty() && auth.contains(ICredential.USER_CREDENTIAL_TYPE)) {
			String[] credentials = credential.decode(auth.replace(ICredential.USER_CREDENTIAL_TYPE+" ", ""));
			if(credentials != null)
				return new UsernamePasswordCredential(credentials[0], credentials[1]);
		}
		return null;
	}
}