package com.nomin.loyaltyintegration.web.rest.authentication.interfaces;

import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.http.HttpServletRequest;

public interface IAuthenticationMechanism {
	public UsernamePasswordCredential getCredential(HttpServletRequest request);
}