package com.nomin.loyaltyintegration.web.rest.authentication.interfaces;

public interface ICredential {
	
	String USER_CREDENTIAL_TYPE = "Basic";
	
	String encode(String userName, String password);
	String[] decode(String encoded);
}