package com.nomin.loyaltyintegration.web.rest;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businesslogic.interfaces.ICryptoLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ILoyaltyLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.entity.payment.ErrorResponse;
import nomin.loyaltyintegration.enums.Language;
import nomin.loyaltyintegration.enums.StatusCode;
import nomin.loyaltyintegration.helper.MessageHelper;
import nomin.loyaltyintegration.helper.RestfulHelper;

@Path(value = "/info")
@Stateless
public class InfoRestful {

	
	@Inject ILoyaltyLogic loyaltyLogic;
	
	@Inject ICryptoLogic cryptoLogic;
	
	@Inject ITransactionLog logger;
	
	public InfoRestful() {
	}
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")	
	@PermitAll
	public Response info() {
		Response response = null;
		try {
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setStatus(StatusCode.SUCCESS.getValue());
			errorResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
			response = Response.ok().entity(errorResponse).build();
		} catch(Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
		
	}
	
	@Path("/authentication")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@RolesAllowed(value = "loyalty")
	public Response authentication() {
		Response response = null;
		try {
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setStatus(StatusCode.SUCCESS.getValue());
			errorResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
			response = Response.ok().entity(errorResponse).build();
		} catch(Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
	}
	
	@POST
	@Path("/test/encrypt")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@RolesAllowed(value = "loyalty")
	public Response encrypt(String rawData) {
		Response response = null;
		try {			
			String result = cryptoLogic.encryptAES(rawData, "A853DB7CD573444A82120F5304202D23");
			response = Response.ok().entity(result).build();
		} catch(Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
	}
	
	@Path("/test/decrypt")
	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	@RolesAllowed(value = "loyalty")
	public Response decrypt(String rawData) {
		Response response = null;
		try {
			String result =  cryptoLogic.decryptAES(rawData, "A853DB7CD573444A82120F5304202D23");
			response = Response.ok().entity(result).build();
		} catch(Exception ex) {
			response = Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build();
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/check/voucher")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkVoucher(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse voucherResponse = loyaltyLogic.CheckVoucher(rawData);

				if (voucherResponse.getStatus() != 1) {
					baseResponse.setStatus(voucherResponse.getStatus());
					baseResponse.setMessage(voucherResponse.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(voucherResponse.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/check/price")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkPrice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.CheckPrice(rawData);

				if (loyaltyRespose.getStatus() != 1) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}

	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/check/qpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkQpayInvoice(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.QpayCheckInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
	
	@RolesAllowed(value = "loyalty")
	@POST
	@Path(value = "/check/socialpay")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response checkSocialPay(String rawData) throws Exception {
		Response response = null;
		LoyaltyBaseResponse baseResponse = new LoyaltyBaseResponse();
		try {
			baseResponse = RestfulHelper.isEmptyBody(rawData);
			if (baseResponse.getStatus() == 1) {
				LoyaltyBaseResponse loyaltyRespose = loyaltyLogic.SocialPayCheckInvoice(rawData);

				if (loyaltyRespose.getState() == null) {
					baseResponse.setStatus(loyaltyRespose.getStatus());
					baseResponse.setMessage(loyaltyRespose.getMessage());
					response = Response.ok().entity(baseResponse).build();
				} else {
					response = Response.ok().entity(loyaltyRespose.getMessage()).build();
				}
			} else {
				response = Response.ok().entity(baseResponse).build();
			}
		} catch (Exception ex) {
			baseResponse.setStatus(500);
			baseResponse.setMessage(ex.getMessage().length() < 80 ? ex.getMessage() : ex.getMessage().substring(0, 80));
			response = Response.ok().entity(baseResponse).build();
		}
		return response;
	}
}
