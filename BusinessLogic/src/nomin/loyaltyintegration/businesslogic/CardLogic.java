package nomin.loyaltyintegration.businesslogic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import org.eclipse.persistence.exceptions.DatabaseException;
import nomin.core.entity.businessentity.rest.BaseResponse;
import nomin.core.logic.BaseLogic;
import nomin.core.logic.RestClient;
import nomin.core.utils.Doubles;
import nomin.core.utils.Json;
import nomin.core.utils.Strings;
import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoDetailRequest;
import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoRequest;
import nomin.loyaltyintegration.businessentity.upoint.response.CheckInfoResponse;
import nomin.loyaltyintegration.businesslogic.interfaces.ICardLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.IService;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IUPointLogicLocal;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.card.info.CardInfoProcedureResponse;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfoResponse;
import nomin.loyaltyintegration.entity.card.info.UACheckInfoResponse;
import nomin.loyaltyintegration.entity.card.info.register.CardRegisterRequest;
import nomin.loyaltyintegration.entity.card.info.register.UACardResponse;
import nomin.loyaltyintegration.entity.card.info.register.UAConsumerResponse;
import nomin.loyaltyintegration.entity.finger.FingerRequest;
import nomin.loyaltyintegration.entity.pos.PosCheckInfoRequest;
import nomin.loyaltyintegration.entity.pos.PosRequest;
import nomin.loyaltyintegration.entity.pos.PosResponse;
import nomin.loyaltyintegration.enums.StatusCode;
import nomin.loyaltyintegration.enums.TestCards;
import nomin.loyaltyintegration.helper.MessageHelper;
import nomin.loyaltyintegration.helper.RestfulHelper;

@Stateless(name = "CardLogic")
public class CardLogic extends BaseLogic implements ICardLogic {

	private static final String UNIT_NAME = "LoyaltyIntegration";

	@Inject
	IUPointLogicLocal upointLogic;

	@Inject
	IService service;

	@Inject
	ITransactionLog logger;

	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CardInfoResponse getCardInfo2(CardInfoRequest request, Logger log) {
		CardInfoResponse response = new CardInfoResponse();
		Logger saveLog = new Logger(log);
		saveLog.setType("getCardInfo");
		saveLog.setRequest(Json.toJson(request));
		saveLog.setRequestDate(new Date());
		boolean isUpointCard = false;
		boolean isPhone = false;
		boolean isNfc = false;
		try {
			String paramIdCardHolder = request.getCardNumber();
			String paramWithFingerPrint = "N";
			String pinCode = "" + request.getPinData();
			switch (request.getCardType()) {
			case "Phone":
				paramIdCardHolder = "999999" + request.getCardNumber();
				isPhone = true;
				paramWithFingerPrint = "Y";
				break;
			case "NominQR":
				request.setPinType("QrData");
				paramWithFingerPrint = "Q";
				break;
			case "NFC":
				isNfc = true;
				switch (request.getPinType()) {
				case "PinCode":
					paramWithFingerPrint = "N";
					break;
				case "FingerPrint":
					paramWithFingerPrint = "Y";
					break;
				}
				break;
			case "CardNumber":
			default:
				if (!Strings.isNullOrEmpty(request.getCardNumber())) {
					int len = request.getCardNumber().length();
					switch (len) {
					case 12:
						isUpointCard = true;
						paramIdCardHolder = "990000" + request.getCardNumber();
						break;
					case 16:
						isUpointCard = true;
						paramIdCardHolder = "99" + request.getCardNumber();
						break;
					}
				}
				if (request.getPinType() != null) {
					switch (request.getPinType()) {
					case "PinCode":
						paramWithFingerPrint = "N";
						break;
					case "FingerPrint":
						paramWithFingerPrint = "Y";
						break;
					}
					break;
				}
			}
			boolean isTestCard = TestCards.fromString(request.getCardNumber()) != null;
			PosCheckInfoRequest checkInfo = new PosCheckInfoRequest();
			if (isNfc) {
				try {
					PosResponse checkInfoResponse = setCardInfo(request, paramWithFingerPrint, isNfc, isUpointCard, pinCode, null, checkInfo, saveLog, isTestCard);
					if (checkInfoResponse.getStatus() != 0) {
						response.setMessage(checkInfoResponse.getMessage());
						return response;
					}

					if (!setUaResponse(checkInfoResponse, response, request, paramIdCardHolder, null, isNfc, isPhone, isUpointCard, paramWithFingerPrint, checkInfo, saveLog))
						return response;

				} catch (Exception ex) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, saveLog.getLanguage(), ex.getMessage()));
					return response;
				}
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("ID_CARDHOLDER", paramIdCardHolder);
			parameters.put("WITH_FP", paramWithFingerPrint);
			parameters.put("ID_POS", saveLog.getPosKey());
			parameters.put("ID_LOCATION", saveLog.getLocationKey());
			parameters.put("TYPE", "GET_CARDINFO");

			List<CardInfoProcedureResponse> list = getByProcedure("[dbo].[GET_INFO]", parameters, CardInfoProcedureResponse.class);
			if (list == null || list.size() == 0) {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, saveLog.getLanguage()));
				return response;
			}
			CardInfoProcedureResponse cardInfo = list.get(0);

			if (saveLog.getDeviceId() != null && "Eshop".equals(saveLog.getOrganizationType())) {
				cardInfo.setUseUpoint("N");
			}
			if (isTestCard) {
				cardInfo.setUaCardId(cardInfo.getCardHolderId());
			}

			if (cardInfo.getPinCode() == null && cardInfo.getUaCardId() == null && cardInfo.getCardAccountId() == null && !isTestCard && !"Eshop".equals(saveLog.getOrganizationType()) && isUpointCard) {
				CardRegisterRequest cardRegisterRequest = new CardRegisterRequest();
				UACardResponse uACardResponse = new UACardResponse();
				cardRegisterRequest.setPosKey(1L);
				cardRegisterRequest.setLocationKey(1L);
				cardRegisterRequest.setCashierKey(1L);
				uACardResponse.setUaId(request.getCardNumber());
				cardRegisterRequest.setData(uACardResponse);

//				BaseResponse baseRegister = service.RegisterCard(cardRegisterRequest, saveLog);

			}
			if ("Y".equals(cardInfo.getUseUpoint())) {
				PosResponse checkInfoResponse = setCardInfo(request, paramWithFingerPrint, isNfc, isUpointCard, pinCode, cardInfo, checkInfo, saveLog, isTestCard);
				if (!setUaResponse(checkInfoResponse, response, request, paramIdCardHolder, cardInfo, isNfc, isPhone, isUpointCard, paramWithFingerPrint, checkInfo, saveLog))
					return response;
			}

			if (("PinCode".equals(request.getPinType()) && !"Eshop".equals(saveLog.getOrganizationType()) && !request.isNoCardContinue()) || (Strings.isNullOrEmpty(request.getPinType()) && !request.isNoCardContinue())) {
				if (!isUpointCard && !pinCode.toLowerCase().equals(cardInfo.getPinCode())) {
					response.setMessage("Пин код буруу !!!");
					return response;
				}
			} else if (("FingerPrint".equals(request.getPinType()) && !request.isNoCardContinue()) || (!request.isNoCardContinue() && Strings.isNullOrEmpty(request.getPinType()))) {
				if ("DABCFDC81A1E4903B95FA2C67EF751A6".equals(saveLog.getDeviceId())) {
					cardInfo.setFingerPrintData(
							"允䅁䉁䅑䅁啁䅂䅁允十䅁䅍䅚䅁䅁䅁䅄䅑偁偎婎潌㤸瑦䵍晧䵚㑺煹猪㡩樶䉎⨰圪䙪太䉂晆穑书䕤㝲ㅗ琵湧癦桂戴婬偳䬸晰睢睫捃䌳㜸潖䨴捣潸㍆䡋搸朷救砵⼵椸㌲汘瑥䉖䱋杋⽮兖版佸偬䉄睬㕉煡獑横佇祮杂⽕故㘹㙗椸㉆呙䩓㥶㝅㕂塊ㅔ剥畹乬䉨桺㐷䅃浨䥱瀰⩍潫䩅卮畕歬敹䝓㉫杁兮氱晍偫䉁䉡䙦睊晈牉㥚獤癕橶歚楁䉇穤䭆䐵硔㝅砲摊煰先捉地乬噐䕋圵䴳䩘㥅䵘敒猪乍橪栲䈯䩊⩦䡡㑐㑧㕰启䑒剮䩆䕆䨶㍦㐸橦䥕䙁獉䙐救瑦噙剗ぱ瑣潈䉘㥫剗扰䑙捧灇䅍潥桔䩱晳啚䩣⼯䩦甶汅朳坷㠵乑噣摰栳䍑獓䍊牺剪㙌浤欴畨副免焱⨵牨橕橄䥦敉硚穔㉊牱卪橑㐶䱋牋㝧楸ㅉ潇獱啎楐噌㑌㍹硢に久䙇硱乲渳䉊眱畡䅆娷剨慬晎瑵䱡䙧㡤獫䕎啹杉㑤坭乶慴㝐㝩䝔敶慏敖偦婨楎塑㌲偭剬奴㠰㥒汱呦䉹㡌び䵊畵䑹呐串䌱⩋㈸慨偃ㅁ㥪獂剧䭁煸晐丯ㅵ偃做ふ⩰䍌昵牂祥㑂䭸䭉牫浗㕮獆䅔㑁兕⽆煕ㅸ㠴啵歳䑴潌敄㘰䑺䰰䙹娱䕰乨㕭浗樹㡂乚㙙䥆剤住祺奎橦歺佪愶獣橺䭑圴汴奒㝱婤㍎瑁㍣睤ㅱ瑢獪男塋㝊朴湱ㅥ危券䥅煐㙣卢⨴煵㙕略佴慤⽓兺䙸摡潩䝵牅穯慵䭷牱婦啚瘰汭䜵䌶ㅔ硩朷㥦⨸坳晧䕯䵢兏䑳䱆䅹䑥㑭攷愰湳ち爸低䝚塬晷伴摴癡印匶獦癷㍦䱅偩牎㥎卓䅐䅯䙈捈汳樯䱒䥮乸〰唶摲睹敳硂㙇眷䙴浭㙇吪敫慄捒䭸呒灪瑌䍔䥣䡨湣杋汸㝃晁剬捁桅㝶瑋ㅌ剪剂穚畃汣牚捂硐䈷牆潏瑐灯䄷䴶㝇䑫䵢捄睉婏摕歪偃㙤八䙔堹䑁㝰⽧潸癖倰煉㕨敗獌䜹㍕歚橈剕䉊䭡摹䝆圶䭫硣䉹剳捇捔圴䭥䩹䑶湦圪晦偹剴態牘兖䙊獣卄䝴摡捧伱坧䵲䵏䬶湥捨桮朶啤獕㝨偏坔〴穭慦瑥䩦䨯䈸瑦㕒丱湘焲呈灏削畐䙵捨㥣㑃塱䝳祈児欯䱪䙫坐䈲奺⼸獊协兡嘲欴案祰扷⩤ㅢ湬呰獤⽺䉣䝴丸䡍䩥橇䥓匷㝥䡐猲煚灂楦祃⩏ㅂ楲㍍桥㕈ㅸ潢楬歳噰杭獈愱䉤潖㉪朹兊㕴硐䭦伪坩䥫穰啨呐牑䭈浥䝙䱑䰱⩕煊昱歵䅪䝰硉求䄯畵洵婔洪㌵慚吴㥆佬呲呪瘲杉⽲㕣摈㉅湭䉱婁睆㕘㥒爹畫");
				}
				if ("44F67A7C87264C728DCE176B5513676C".equals(saveLog.getDeviceId())) {
					cardInfo.setFingerPrintData(
							"允䅁䉁䅑䅁啂睁䅁允十䅁䅍䅚䅁䅁䅁䅒䅍佁卒数⼵䡬呦朷⼴樲捊獎㝊瀵杄剶㕄〶全㑧瑃唪漲ㅹ睔捈啊吵瘹橱晨楔牷慆䑩㠱㙎㕔敃礴兔牉塆倪啳呢嘸䩹捊瑱吴祒桋㤪㝢湧坧㝒䥯噖剑䉂䄱杲煭婫煘匵卂ㅫ㕆牆父剓㑶睄杦䔰呆䈯洪㤷㝷䕆䠰朷畫⩙儴獁䝗敊慑晰䱉啥坣じ㍳㍷噈敱儴⩨剎祉穵䥊東吸㥰橘儱乴䍙婱ㄱ敪夯橔杯䩕娯瑖慚洲汦䅫乳琵呫套偐睤睑摺煭䝗㍩琲湋ㅐ癧偨㉵獭慖硆摦扌敄扳啐祣塸癳祲晕䩌晡䱯橥䱓䥡㙮䙸浪扡穃婗䅉獵湲煆㕢牕特䐪啎均䕅㔳兊䑨昸硔婰湬杦浓䠶㤱祘捌橫浸朷椴䡐㑙畃湖敵䥈潡㙏慖坐捁䨴奬兙䡍〴渳㝲灔婐癷乸㥢公㐶䵐堸晰⽉扏⩡癢娹券湦硢児兹䡹坌佔䭎㑓匰啌塒㑡呲桄䜴䱁呁扡㑭偹奚㙨㙨睕橧䉣慰呪睉潕⽘火煺慐稰⽄礹位伪瑵湸楑㥪䙯䕪䕕呙坉敭煰㑇牁獮支乓堸硣橖桱歐⽲歲啧慄瑦桕摖䡮氲硱啷㙒塏䔯奆䙔㡅改䱸瑈䙃ㅲ坡䕂䝫䕦橓噓䘹䉏䍶佔偊䉧牭入捴硶晅爱㐯㙌晶昰ㅬ佊橘捊卖捣渰硔䬰㥅橖㤲ㅚ䝮乙䭥䡹㥙湶止煯楰㡵䭕塁㍙㙔䴳㕌㌸㙡愴挶氳䡘㍲㑕㙯療爲敚兺汖乨稸穵䅬䔪氹慰坶䉘桩㡨䩲⩭坏晡䔸乐㤸㙺㍓敔䉲穫䕐焰䕁㝗硵䱺摎匯灚䕭楘歘桎祢䅕摋䱮䉈硏癵㕷䡒䡇ご䕖眪穦䉬潋剐硯敬剋氲䍯㠰卤⩏硏捹䙶楆歅危味伹睫瑰潐獍奶浊渶䑥穦㥅啫橈婪潯慌浏朰奬乳橦㠱⨴㉋猹砰牦㥊偉扉桩湂祧䭊䉭啚ㄯ煷㑷橚橕电坉煦煒䅑䡺穯㉰搯乤婙卌䥈奢䥂つぅ⽺剗䑸䵖楓䅎歧ち啹兮浕兩兩䱏䘯橚䙉䉴摦睔佲偐㝒塮佒睂奺婁䉢獭乍䡯框䘳煒䄲琪敌丸硃桮㙓䥈䔰乊佳桊佔穌祢摑⩙浘瘵桔桄㔴桂牣癄戲");
				}

				FingerRequest fingerRequest = new FingerRequest();
				fingerRequest.setFingerData(request.getPinData());
				fingerRequest.setStoredData(cardInfo.getFingerPrintData());

				String fingerRequestRawData = Json.toJson(fingerRequest);
				BaseResponse fingerResponse = RestClient.postRequest(RestfulHelper.getUrlFingerVerify(), fingerRequestRawData, BaseResponse.class, MediaType.APPLICATION_JSON_TYPE, null);

				if (fingerResponse != null && fingerResponse.getReadEntity() != null) {
					if (!"1".equals(fingerResponse.getReadEntity().toString())) {
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.FINGER_DONT_MATCH, saveLog.getLanguage()));
						return response;
					}
				} else {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.WRONG_PINCODE, saveLog.getLanguage()));
					return response;
				}
			}
			setCardInfoResponse(cardInfo, response);
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				if (dbException.getInternalException() != null) {
					response.setMessage(dbException.getInternalException().getMessage());
				} else
					response.setMessage(dbException.getMessage());
			} else {
				String errorMessage = ex.getMessage();
				if (errorMessage.contains("Primary keys must not contain null")) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, saveLog.getLanguage(), ex.getMessage()));
					return response;
				}
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, saveLog.getLanguage()), ex.getMessage()));
			}
			saveLog.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, saveLog.getLanguage()), ex.getMessage()));
			saveLog.setError(ex.getMessage());
		} finally {
			saveLog.setResponseDate(new Date());
			saveLog.setResponse(response.getMessage());
			logger.log(saveLog);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CardInfoResponse getCardInfo3(CardInfoRequest request, Logger log) throws Exception {
		CardInfoResponse response = new CardInfoResponse();
		CardInfoProcedureResponse cardInfoProcedureResponse = new CardInfoProcedureResponse();
		Logger saveLog = new Logger(log);
		saveLog.setType("getCardInfo");
		saveLog.setRequest(Json.toJson(request));
		saveLog.setRequestDate(new Date());
		boolean isUpointCard = false;
		boolean isNfc = false;
		List<String> states = new ArrayList<>();
		try {
			String paramIdCardHolder = request.getCardNumber();
			String paramWithFingerPrint = "N";
			String pinCode = "" + request.getPinData();
			switch (request.getCardType()) {
			case "Phone":
				paramIdCardHolder = "999999" + request.getCardNumber();
				paramWithFingerPrint = "Y";
				break;
			case "NominQR":
				request.setPinType("QrData");
				paramWithFingerPrint = "Q";
				break;
			case "NFC":
				isNfc = true;
				switch (request.getPinType()) {
				case "PinCode":
					paramWithFingerPrint = "N";
					break;
				case "ElectronicSignature":
					paramWithFingerPrint = "Y";
					break;
				}
				break;
			case "CardNumber":
			default:
				if (!Strings.isNullOrEmpty(request.getCardNumber())) {
					int len = request.getCardNumber().length();
					switch (len) {
					case 12:
						isUpointCard = true;
						paramIdCardHolder = "990000" + request.getCardNumber();
						break;
					case 16:
						isUpointCard = true;
						paramIdCardHolder = "99" + request.getCardNumber();
						break;
					}
				}
				if (request.getPinType() != null) {
					switch (request.getPinType()) {
					case "PinCode":
						paramWithFingerPrint = "N";
						break;
					case "ElectronicSignature":
						paramWithFingerPrint = "Y";
						break;
					}
					break;
				}
			}

			if (isNfc) {
				CheckInfoRequest checkInfoRequest = getCheckInfoRequest(saveLog, null);
				CheckInfoResponse upointCheckInfoResponse = null;
				if(checkInfoRequest!=null)
					upointCheckInfoResponse = upointLogic.getCheckInfo(checkInfoRequest, saveLog);

				if (upointCheckInfoResponse == null || upointCheckInfoResponse.getCardNumber().length() == 12) {
					response.setMessage(upointCheckInfoResponse.getMessage() != null ? upointCheckInfoResponse.getMessage() : MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, saveLog.getLanguage()));
					return response;
				}
				if (upointCheckInfoResponse != null && upointCheckInfoResponse.getResult() == 0 && upointCheckInfoResponse.getCardStatus() == 1) {
					if (!setCardInfoResponseUpoint(response, upointCheckInfoResponse)) {
						return response;
					}
				} else {
					response.setUseUpoint(null);
				}
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("ID_CARDHOLDER", paramIdCardHolder);
			parameters.put("WITH_FP", paramWithFingerPrint);
			parameters.put("ID_POS", saveLog.getPosKey());
			parameters.put("ID_LOCATION", saveLog.getLocationKey());
			parameters.put("TYPE", "GET_CARDINFO");
			
			List<CardInfoProcedureResponse> list = getByProcedure(saveLog.getProduction() == 1 ? "[dbo].[GET_INFO]" : "[dbo].[GET_INFO_DEVELOPING]", parameters, CardInfoProcedureResponse.class);
			if (list == null || list.size() == 0) {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, saveLog.getLanguage()));
				return response;
			}
			cardInfoProcedureResponse = list.get(0);

			if (!setCardInfoResponseCardInfo(response, cardInfoProcedureResponse)) {
				return response;
			}
			response.setStatusCardInfoResponse(StatusCode.SUCCESS.getValue());

			boolean isTestCard = false;
			if (TestCards.fromString(cardInfoProcedureResponse.getCardHolderId()) != null) {
				isTestCard = true;
				cardInfoProcedureResponse.setUaCardId(cardInfoProcedureResponse.getCardHolderId());
			}

			if (cardInfoProcedureResponse.getPinCode() == null && cardInfoProcedureResponse.getUaCardId() == null && cardInfoProcedureResponse.getCardAccountId() == null && !isTestCard && !"Eshop".equals(saveLog.getOrganizationType())
					&& isUpointCard) {
				CardRegisterRequest cardRegisterRequest = new CardRegisterRequest();
				UACardResponse uACardResponse = new UACardResponse();
				cardRegisterRequest.setPosKey(1L);
				cardRegisterRequest.setLocationKey(1L);
				cardRegisterRequest.setCashierKey(1L);
				uACardResponse.setUaId(request.getCardNumber());
				cardRegisterRequest.setData(uACardResponse);
//TODO 			бүртгэлгүй карт бүртгэх	
//				BaseResponse baseRegister = service.RegisterCard(cardRegisterRequest, saveLog);
			}

			if ("Y".equals(cardInfoProcedureResponse.getUseUpoint())) {
				CheckInfoRequest checkInfoRequest = getCheckInfoRequest(saveLog, cardInfoProcedureResponse.getUaCardId());
				CheckInfoResponse upointCheckInfoResponse = null;
				if(checkInfoRequest!=null)
					upointCheckInfoResponse = upointLogic.getCheckInfo(checkInfoRequest, saveLog);

				if (upointCheckInfoResponse == null || upointCheckInfoResponse.getCardNumber().length() == 12) {
					response.setMessage(upointCheckInfoResponse.getMessage() != null ? upointCheckInfoResponse.getMessage() : MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, saveLog.getLanguage()));
					return response;
				}

				if (upointCheckInfoResponse != null && upointCheckInfoResponse.getResult() == 0 && upointCheckInfoResponse.getCardStatus() == 1) {
					if (!setCardInfoResponseUpoint(response, upointCheckInfoResponse)) {
						return response;
					}
				} else {
					response.setUseUpoint(null);
				}
			}

			if (("PinCode".equals(request.getPinType()) && !"Eshop".equals(saveLog.getOrganizationType()) && !request.isNoCardContinue()) || (Strings.isNullOrEmpty(request.getPinType()) && !request.isNoCardContinue())) {
				if (!isUpointCard && !pinCode.toLowerCase().equals(cardInfoProcedureResponse.getPinCode())) {
					response.setMessage("Пин код буруу !!!");
					return response;
				}
			} else if ("ElectronicSignature".equals(request.getPinType())) {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.WRONG_PINCODE, saveLog.getLanguage()));
				return response;
			}
		} catch (PersistenceException ex) {
			saveLog.setError(ex.getMessage());
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatusCardInfoResponse(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(ex.getMessage());
					} else {
						response.setStatusCardInfoResponse(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatusCardInfoResponse(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				String errorMessage = ex.getMessage();
				if (errorMessage.contains("Primary keys must not contain null")) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, saveLog.getLanguage(), ex.getMessage()));
					return response;
				} else {
					response.setStatusCardInfoResponse(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
					states.add("SET_MESSAGE");
				}
			}
		} catch (Exception ex) {
			response.setStatusCardInfoResponse(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatusCardInfoResponse().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	private PosResponse setCardInfo(CardInfoRequest request, String paramWithFingerPrint, boolean isNfc, boolean isUpointCard, String pinCode,
			CardInfoProcedureResponse cardInfo, PosCheckInfoRequest checkInfo, Logger log, boolean isTestCard) {
		PosRequest checkInfoRequest = new PosRequest();
		checkInfoRequest.setLocationKey(!isTestCard ? 100L : 201403060000000001L);
		checkInfoRequest.setPosKey(100L);

		checkInfo.setCardNumber(request.getCardNumber());
		checkInfo.setMobile("");
		if (isNfc) {
			checkInfo.setPinCode("Y".equals(paramWithFingerPrint) ? "NominApproved" : request.getPinData());
		} else {
			String uaCardId = cardInfo.getUaCardId();
			if (!isUpointCard) {
				if (Strings.isNullOrEmpty(uaCardId))
					checkInfo.setCardNumber(cardInfo.getCardHolderId());
				else {
//					int uaCardLength = uaCardId.length();
//					if (uaCardLength < 12) {
//						uaCardId = Strings.leftPad(uaCardId, 12, "0");
//					} else if (uaCardLength > 12 && uaCardLength < 16) {
//						uaCardId = Strings.leftPad(uaCardId, 16, "0");
//					}
					checkInfo.setCardNumber(uaCardId);
				}
			}
			cardInfo.setUaCardId(checkInfo.getCardNumber());
			if (isUpointCard && "N".equals(paramWithFingerPrint)) {
				checkInfo.setPinCode(pinCode);
			} else {
				checkInfo.setPinCode("NominApproved");
			}
		}
		String checkInfoRequestRawData = Json.toJson(checkInfo);
		checkInfoRequest.setData(checkInfoRequestRawData);
		checkInfoRequestRawData = Json.toJson(checkInfoRequest);

		PosResponse checkInfoResponse = upointLogic.getCardInfo(checkInfoRequestRawData, log);
		return checkInfoResponse;
	}

	public boolean setCardInfoResponseUpoint(CardInfoResponse setCardInfoResponse, CheckInfoResponse checkInfoResponse) {
		try {
			setCardInfoResponse.setUaId(checkInfoResponse.getUpointCardNumber());
			setCardInfoResponse.setUaCardId(checkInfoResponse.getCardNumber());
			setCardInfoResponse.setUaResult(checkInfoResponse.getResult());
			setCardInfoResponse.setUaPhone(checkInfoResponse.getMobile());
			setCardInfoResponse.setUaCheckInfoMessage(checkInfoResponse.getMessage());
			setCardInfoResponse.setUaBalance(checkInfoResponse.getUpointBalance().length() > 0 ? Double.parseDouble(checkInfoResponse.getUpointBalance()) : Double.MIN_VALUE);
			setCardInfoResponse.setUaCardStatus(checkInfoResponse.getCardStatus());
			setCardInfoResponse.setUaCreatedDate(checkInfoResponse.getCreatedAt());
			setCardInfoResponse.setUaCheckInfoStatus("1");
		} catch (Exception e) {
			setCardInfoResponse.setUseUpoint(null);
			setCardInfoResponse.setMessage(e.getMessage());
			setCardInfoResponse.setUaCheckInfoStatus("-1");
			return false;
		}
		return true;
	}

	private boolean setCardInfoResponseCardInfo(CardInfoResponse setCardInfoResponse, CardInfoProcedureResponse cardInfoProcedureResponse) {
		try {
			setCardInfoResponse.setCardHolderId(cardInfoProcedureResponse.getCardHolderId());
			setCardInfoResponse.setCardAccountId(cardInfoProcedureResponse.getCardAccountId());
			setCardInfoResponse.setFirstName(cardInfoProcedureResponse.getFirstName());
			setCardInfoResponse.setLastName(cardInfoProcedureResponse.getLastName());
			setCardInfoResponse.setVatCustomerNo(cardInfoProcedureResponse.getVatCustomerNo());
			setCardInfoResponse.setVatCustomerNoTemp(cardInfoProcedureResponse.getVatCustomerNoTemp());
			setCardInfoResponse.setBonusUse(cardInfoProcedureResponse.getBonusUse());
			setCardInfoResponse.setOpenDate(cardInfoProcedureResponse.getOpenDate());
			setCardInfoResponse.setBonusCancelDayCount(cardInfoProcedureResponse.getBonusCancelDayCount());
			setCardInfoResponse.setIsDvip(cardInfoProcedureResponse.getIsDvip());
			setCardInfoResponse.setUseDvip(cardInfoProcedureResponse.getUseDvip());
			setCardInfoResponse.setCalcBonusPercentPurchaseAmount(cardInfoProcedureResponse.getCalcBonusPercentPurchaseAmount());
			setCardInfoResponse.setTimedPurchaseAmount(cardInfoProcedureResponse.getTimedPurchaseAmount());
			setCardInfoResponse.setBonusPecent(cardInfoProcedureResponse.getBonusPecent());
			setCardInfoResponse.setBonusBalance(cardInfoProcedureResponse.getBonusBalance());
			setCardInfoResponse.setDvipBalance(cardInfoProcedureResponse.getDvipBalance());
			setCardInfoResponse.setDvipSavedBalance(cardInfoProcedureResponse.getDvipSavedBalance());
			setCardInfoResponse.setCoinAllBalance(cardInfoProcedureResponse.getCoinAllBalance());
			setCardInfoResponse.setCoinMonthBalance(cardInfoProcedureResponse.getCoinMonthBalance());
			setCardInfoResponse.setCoinDayBalance(cardInfoProcedureResponse.getCoinDayBalance());
			setCardInfoResponse.setItemBonusBalance(cardInfoProcedureResponse.getItemBonusBalance());
			setCardInfoResponse.setAddItemBonus(cardInfoProcedureResponse.getAddItemBonus());
			setCardInfoResponse.setUseItemBonus(cardInfoProcedureResponse.getUseItemBonus());
			setCardInfoResponse.setItemBonusHeaderId(cardInfoProcedureResponse.getItemBonusHeaderId());
			setCardInfoResponse.setIsBirthDay(cardInfoProcedureResponse.getIsBirthDay());
			setCardInfoResponse.setIsIdPhoneDuplicated(cardInfoProcedureResponse.getIsIdPhoneDuplicated());
			setCardInfoResponse.setIsRequestPhoneUpdate(cardInfoProcedureResponse.getIsRequestPhoneUpdate());
			setCardInfoResponse.setUseUpoint(cardInfoProcedureResponse.getUseUpoint());
			setCardInfoResponse.setFingerPrintData(cardInfoProcedureResponse.getFingerPrintData());
			setCardInfoResponse.setQrResult(cardInfoProcedureResponse.getQrResult());
			setCardInfoResponse.setItemBonusX2Info(cardInfoProcedureResponse.getItemBonusX2Info());
			setCardInfoResponse.setCardNumber(cardInfoProcedureResponse.getCardNumber());
			setCardInfoResponse.setMessage("success");
			setCardInfoResponse.setVendorBalance(cardInfoProcedureResponse.getVendorBalance());
			setCardInfoResponse.setVendorPercent(cardInfoProcedureResponse.getVendorPercent());
			setCardInfoResponse.setCardType2(cardInfoProcedureResponse.getCardType());
			setCardInfoResponse.setEndDate(cardInfoProcedureResponse.getEndDate());
			setCardInfoResponse.setIsRefreshPinCode(cardInfoProcedureResponse.getIsRefreshPinCode());
			setCardInfoResponse.setAdditionalJson(cardInfoProcedureResponse.getAdditionalJson());
			setCardInfoResponse.setUaCardId(cardInfoProcedureResponse.getUaCardId());
		} catch (Exception e) {
			setCardInfoResponse.setMessage(e.getMessage());
			return false;
		}
		return true;
	}

	public CheckInfoRequest getCheckInfoRequest(Logger logger, String uaCardNumber) {
		CheckInfoRequest checkInfoRequest = new CheckInfoRequest();
		String uaCardId = null;
		try {
			if (uaCardNumber == null) {
				uaCardId = logger.getCardInfoRequest().getCardNumber();
			} else {
				uaCardId = uaCardNumber;
			}
			checkInfoRequest.setLocationKey(100L);
			checkInfoRequest.setPosKey(100L);
			CheckInfoDetailRequest checkInfoDetailRequest = new CheckInfoDetailRequest();
			checkInfoDetailRequest.setPinCode("NominApproved");
			checkInfoDetailRequest.setMobile("");
			if (uaCardNumber != null || "CardNumber".equals(logger.getCardInfoRequest().getCardNumber())) {
				if (TestCards.fromString(logger.getCardInfoRequest().getCardNumber()) == null || (TestCards.fromString(uaCardNumber) == null && uaCardNumber != null)) {
					int uaCardLength = uaCardId.length();
					if (uaCardLength < 12) {
						uaCardId = Strings.leftPad(uaCardId, 12, "0");
					} else if (uaCardLength > 12 && uaCardLength < 16) {
						uaCardId = Strings.leftPad(uaCardId, 16, "0");
					}
					checkInfoDetailRequest.setCardNumber(uaCardId);
				} else {
					checkInfoDetailRequest.setCardNumber(uaCardId);
				}
			} else {
				checkInfoDetailRequest.setCardNumber("");
				checkInfoDetailRequest.setMobile(logger.getCardInfoRequest().getCardNumber());
			}
			checkInfoRequest.setData(Json.toJson(checkInfoDetailRequest));
		} catch (Exception e) {
			checkInfoRequest = null;
		}
		return checkInfoRequest;
	}

	@SuppressWarnings("unused")
	private boolean setUaResponse(PosResponse checkInfoResponse, CardInfoResponse response, CardInfoRequest request, String paramIdCardHolder, CardInfoProcedureResponse cardInfo, boolean isNfc, boolean isPhone, boolean isUpointCard,
			String paramWithFingerPrint, PosCheckInfoRequest checkInfo, Logger log) {
		PosResponse posResponse = Json.toJsonObject(checkInfoResponse.getReadEntity(), PosResponse.class);
		UACheckInfoResponse uaResponse = Json.toJsonObject(posResponse.getReadEntity(), UACheckInfoResponse.class);

		response.setUseUpoint("1");
		response.setUaCheckInfoMessage("");
		response.setUaCheckInfoStatus("");
		response.setUaCardId(uaResponse.getCard_number());
		response.setUaBalance(uaResponse.getBalance().length() > 0 ? Double.parseDouble(uaResponse.getBalance()) : Double.MIN_VALUE);

		if (isNfc) {
			if (uaResponse != null) {
				response.setUseUpoint("1");
				response.setUaCheckInfoMessage(uaResponse.getMessage());
				response.setUaCheckInfoStatus("" + checkInfoResponse.getResult());
				response.setUaCardId(uaResponse.getCard_number());
				response.setUaBalance(Doubles.toDouble(uaResponse.getBalance()));

				int len = uaResponse.getCard_number().length();
				if (len == 12) {
					isUpointCard = true;
					paramIdCardHolder = "990000" + uaResponse.getCard_number();
				} else {
					isUpointCard = true;
					paramIdCardHolder = "99" + uaResponse.getCard_number();
				}
			} else {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, log.getLanguage(), checkInfoResponse.getMessage()));
				return false;
			}
		} else {
			if ("Y".equals(cardInfo.getUseUpoint())) {
				response.setUseUpoint(cardInfo.getUseUpoint());
				if ("Y".equals(paramWithFingerPrint) && isPhone) {
					response.setUseUpoint("N");
					if (isUpointCard && "N".equals(paramWithFingerPrint)) {
						response.setMessage(posResponse.getMessage());
						return false;
					} else {
						response.setUseUpoint("N");
					}
				} else {

				}
			} else {
				response.setUaCheckInfoMessage(checkInfoResponse.getMessage());
				response.setUaCheckInfoStatus("" + checkInfoResponse.getResult());
				if (posResponse != null) {
					if (uaResponse != null) {
						response.setUseUpoint("1");
						response.setUaCheckInfoMessage(uaResponse.getMessage());
						response.setUaCheckInfoStatus("" + checkInfoResponse.getResult());
						response.setUaCardId(checkInfo.getCardNumber());
						response.setUaBalance(Doubles.toDouble(uaResponse.getBalance()));
					} else {
						response.setUseUpoint("0");
					}
				}
			}
		}
		return true;
	}

	private void setCardInfoResponse(CardInfoProcedureResponse request, CardInfoResponse response) {
		response.setCardHolderId(request.getCardHolderId());
		response.setUaCardId(request.getUaCardId());
		response.setCardAccountId(request.getCardAccountId());
		response.setFirstName(request.getFirstName());
		response.setLastName(request.getLastName());
		response.setVatCustomerNo(request.getVatCustomerNo());
		response.setVatCustomerNoTemp(request.getVatCustomerNoTemp());
		response.setBonusUse(request.getBonusUse());
		response.setOpenDate(request.getOpenDate());
		response.setBonusCancelDayCount(request.getBonusCancelDayCount());
		response.setIsDvip(request.getIsDvip());
		response.setUseDvip(request.getUseDvip());
		response.setCalcBonusPercentPurchaseAmount(request.getCalcBonusPercentPurchaseAmount());
		response.setTimedPurchaseAmount(request.getTimedPurchaseAmount());
		response.setBonusPecent(request.getBonusPecent());
		response.setBonusBalance(request.getBonusBalance());
		response.setDvipBalance(request.getDvipBalance());
		response.setDvipSavedBalance(request.getDvipSavedBalance());
		response.setCoinAllBalance(request.getCoinAllBalance());
		response.setCoinMonthBalance(request.getCoinMonthBalance());
		response.setCoinDayBalance(request.getCoinDayBalance());
		response.setItemBonusBalance(request.getItemBonusBalance());
		response.setAddItemBonus(request.getAddItemBonus());
		response.setUseItemBonus(request.getUseItemBonus());
		response.setItemBonusHeaderId(request.getItemBonusHeaderId());
		response.setIsBirthDay(request.getIsBirthDay());
		response.setIsIdPhoneDuplicated(request.getIsIdPhoneDuplicated());
		response.setIsRequestPhoneUpdate(request.getIsRequestPhoneUpdate());
		response.setFingerPrintData(request.getFingerPrintData());
		response.setQrResult(request.getQrResult());
		response.setItemBonusX2Info(request.getItemBonusX2Info());
		response.setCardNumber(request.getCardNumber());
		response.setMessage("success");
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public CardInfoProcedureResponse registerCard(UACardResponse uaCardResponse, UAConsumerResponse uaConsumerResponse, Logger log) throws Exception {
		CardInfoProcedureResponse response = new CardInfoProcedureResponse();
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("ID_UACARD", uaCardResponse.getCardNumber());
			parameters.put("UAID", uaCardResponse.getUaId());
			parameters.put("UA_MOBILE", uaCardResponse.getMobile());
			parameters.put("UPOINT_FIRSTNAME", uaConsumerResponse != null ? uaConsumerResponse.getFirstName() : null);
			parameters.put("UPOINT_LASTNAME", uaConsumerResponse != null ? uaConsumerResponse.getLastName() : null);
			parameters.put("UPOINT_REGISTERNO", uaConsumerResponse != null ? uaConsumerResponse.getRegNo() : null);
			parameters.put("UPOINT_ADDRESS", uaConsumerResponse != null ? uaConsumerResponse.getAddress() : null);

			execProcedure("[dbo].[CreateCardholderByOrganization]", parameters);
		} catch (Exception e) {
			response = null;
		}
		return response;
	}

}
