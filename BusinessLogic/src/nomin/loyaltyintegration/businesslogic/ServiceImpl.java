package nomin.loyaltyintegration.businesslogic;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Asynchronous;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;

import nomin.core.entity.businessentity.rest.BaseResponse;
import nomin.core.logic.RestClient;
import nomin.core.utils.Json;
import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.CheckRegisterResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.EbarimtRequest;
import nomin.loyaltyintegration.businessentity.qpay.data.BranchInfo;
import nomin.loyaltyintegration.businessentity.qpay.data.GoodsDetail;
import nomin.loyaltyintegration.businessentity.qpay.data.VatInfo;
import nomin.loyaltyintegration.businessentity.qpay.pos.QPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.qpay.request.auth.QPayAuthRequest;
import nomin.loyaltyintegration.businessentity.qpay.request.auth.QPayAuthResponse;
import nomin.loyaltyintegration.businessentity.qpay.response.create.QPayBillCreateResponse;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherCheckRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherSmsRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherTransactionRequest;
import nomin.loyaltyintegration.businessentity.spectre.response.VoucherResponse;
import nomin.loyaltyintegration.businesslogic.helper.AbstractHttpClient;
import nomin.loyaltyintegration.businesslogic.interfaces.ICardLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.IService;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IUPointLogicLocal;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.bill.ReturnBillRequest;
import nomin.loyaltyintegration.entity.bill.ReturnBillResponse;
import nomin.loyaltyintegration.entity.card.info.register.CardRegisterRequest;
import nomin.loyaltyintegration.entity.card.info.register.ConsumerRequest;
import nomin.loyaltyintegration.entity.card.info.register.UAConsumerRequest;
import nomin.loyaltyintegration.entity.card.info.register.UAConsumerResponse;
import nomin.loyaltyintegration.entity.pos.PosCheckInfoRequest;
import nomin.loyaltyintegration.entity.pos.PosRequest;
import nomin.loyaltyintegration.entity.pos.PosResponse;
import nomin.loyaltyintegration.entity.posapi.PutResponse;
import nomin.loyaltyintegration.enums.Language;
import nomin.loyaltyintegration.enums.StatusCode;
import nomin.loyaltyintegration.helper.JsonHelper;
import nomin.loyaltyintegration.helper.MessageHelper;
import nomin.loyaltyintegration.helper.RestfulHelper;
import nomin.pos.common.helper.ByteHelper;
import nomin.posreport.web.businessentity.bill.BillReportRequest;
import nomin.posreport.web.businessentity.report.ReportResponse;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

@RequestScoped
public class ServiceImpl extends AbstractHttpClient implements IService {

	@Inject
	ITransactionLog logger;

	@Inject
	IUPointLogicLocal upoinLogic;

	@Inject
	ICardLogic cardLogic;

	@Override
	public String getApiUrl() {
		return "";
	}

	protected void SetLogError(VoucherResponse response, Logger log) {
		if (response != null) {
			if (response.getStatus() == 0) {
				log.setError("");
			}
			if (response.getStatus() != 0) {
				if (response.getMessage() != null && !response.getMessage().isEmpty())
					log.setError(response.getMessage());
			}
		}
	}

	protected String getError(String errorMessage) {
		try {
			nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse response = readResponse(errorMessage,
					nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse.class);
			return response.getMessage();
		} catch (Exception e) {
			return errorMessage;
		}
	}

	@Override
	public ReturnBillResponse ProcessReturnTransactionMobile(ReturnBillRequest request) {
		PosResponse response = new PosResponse();
		ReturnBillResponse newRes = new ReturnBillResponse();
		try {
			PosResponse baseRes = RestClient.request(HttpMethod.POST, RestfulHelper.getUrlEbarimtReturnReal(), request,
					PosResponse.class, MediaType.APPLICATION_JSON_TYPE, null);
			if (baseRes == null)
				return null;
			newRes = Json.toJsonObject(baseRes.getReadEntity(), ReturnBillResponse.class);
			return newRes;
		} catch (Exception ex) {
			response.setResult(100);
			response.setMessage("Ebarimt сервис функц дуудахад алдаа гарлаа. Сервис төлөв: " + response.getStatus());
		}
		return newRes;
	}

	@Override
	public BaseResponse RegisterCard(CardRegisterRequest request, Logger log) throws Exception {
		BaseResponse baseRes = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("RegisterCard");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		UAConsumerResponse uaConsumerResponse = null;
//		UACardResponse uACardResponse = new UACardResponse();
		try {
			UAConsumerRequest uaConsumerRequest = new UAConsumerRequest();
			uaConsumerRequest.setUaId(request.getData().getUaId());
			ConsumerRequest consumerRequest = new ConsumerRequest();
			consumerRequest.setLocationKey(request.getLocationKey());
			consumerRequest.setPosKey(request.getPosKey());
			consumerRequest.setData("{\"ua_id\":\"" + request.getData().getUaId() + "\"}");

			baseRes = RestClient.request(HttpMethod.POST, RestfulHelper.getUrlUpointConsumerInfo(), consumerRequest,
					BaseResponse.class, MediaType.APPLICATION_JSON_TYPE, null);
			savelog.setStatus(String.valueOf(baseRes.getStatus()));

			if (baseRes.getStatus() == 0) {
				BaseResponse uaResponse = Json.toJsonObject(baseRes.getReadEntity(), BaseResponse.class);
				uaConsumerResponse = Json.toJsonObject(uaResponse.getReadEntity(), UAConsumerResponse.class);

				PosRequest checkInfoRequest = new PosRequest();
				checkInfoRequest.setLocationKey(100L);
				checkInfoRequest.setPosKey(100L);

				PosCheckInfoRequest checkInfo = new PosCheckInfoRequest();
				checkInfo.setCardNumber(uaConsumerResponse.getCardNumber());
				checkInfo.setMobile("");
				checkInfo.setPinCode("NominApproved");
				checkInfoRequest.setData(Json.toJson(checkInfo));

//				PosResponse checkInfoResponse = cardLogic.getCardInfo2(Json.toJson(checkInfoRequest), log);
//
//				BaseResponse checkInfoReadEntity = Json.toJsonObject(checkInfoResponse.getReadEntity(), BaseResponse.class);
//				UACheckInfoResponse uaCheckInfo = Json.toJsonObject(checkInfoReadEntity.getReadEntity(), UACheckInfoResponse.class);
//				uACardResponse.setMobile(uaCheckInfo.getMobile());
//				uACardResponse.setUaId(uaCheckInfo.getUa_id());
//				uACardResponse.setCardNumber(uaCheckInfo.getCard_number());
//
//				CardInfoProcedureResponse response = cardLogic.registerCard2(uACardResponse, uaConsumerResponse, log);

				baseRes.setStatus(0);
			} else {

			}
		} catch (Exception ex) {
			baseRes.setResult(-1);
			baseRes.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
		} finally {
			savelog.setResponse(Json.toJson(baseRes));
			savelog.setResponseDate(new Date());
			savelog.setStatus(savelog.getStatus());
		}
		return baseRes;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public VoucherResponse CheckVoucherLoyalty2(VoucherCheckRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("CheckVoucher-L2-Rest");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		VoucherResponse response = null;
		try {
			TrustManager[] TrustMgr = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				// No Checking required
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				// No Checking required
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Installing the Trast manager
			SSLContext SSLCont = SSLContext.getInstance("SSL");
			SSLCont.init(null, TrustMgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(SSLCont.getSocketFactory());

			// Creating an all-trusting verifier for Host Name
			HostnameVerifier ValidHost = new HostnameVerifier() {
				public boolean verify(String HostName, SSLSession MySession) {
					return true;
				}
			};

			// Installing an all-trusting verifier for the HOST
			HttpsURLConnection.setDefaultHostnameVerifier(ValidHost);

			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlLoyalty2RealCheck()).openConnection();
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000); // set timeout to 60 seconds
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), VoucherResponse.class);
			if (response != null)
				savelog.setResponse(Json.toJson(response));
			SetLogError(response, savelog);
		} catch (Exception ex) {
			if (response == null)
				response = new VoucherResponse();
			response.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public VoucherResponse PutVoucherLoyalty2(VoucherTransactionRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		if (savelog.getStatus() != null && "RETURN_TO_CORRECT".equals(savelog.getStatus())) {
			savelog.setType("PutVoucher-L2-Rest-RETURN_TO_CORRECT");
		} else {
			savelog.setType("PutVoucher-L2-Rest");
		}
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		VoucherResponse response = null;
		try {
			TrustManager[] TrustMgr = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				// No Checking required
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				// No Checking required
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Installing the Trast manager
			SSLContext SSLCont = SSLContext.getInstance("SSL");
			SSLCont.init(null, TrustMgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(SSLCont.getSocketFactory());

			// Creating an all-trusting verifier for Host Name
			HostnameVerifier ValidHost = new HostnameVerifier() {
				public boolean verify(String HostName, SSLSession MySession) {
					return true;
				}
			};

			// Installing an all-trusting verifier for the HOST
			HttpsURLConnection.setDefaultHostnameVerifier(ValidHost);
			
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlLoyalty2RealPut()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), VoucherResponse.class);
			if (response != null)
				savelog.setResponse(Json.toJson(response));
			SetLogError(response, savelog);
		} catch (Exception ex) {
			if (response == null)
				response = new VoucherResponse();
			response.setStatus(100);
			response.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public VoucherResponse ReturnVoucherLoyalty2(VoucherTransactionRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		if (savelog.getStatus() != null && "ACTIVE_TO_RETURN".equals(savelog.getStatus())) {
			savelog.setType("ReturnVoucher-L2-Rest-ACTIVE_TO_RETURN");
		} else {
			savelog.setType("ReturnVoucher-L2-Rest");
		}
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		VoucherResponse response = null;
		try {
			TrustManager[] TrustMgr = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				// No Checking required
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				// No Checking required
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Installing the Trast manager
			SSLContext SSLCont = SSLContext.getInstance("SSL");
			SSLCont.init(null, TrustMgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(SSLCont.getSocketFactory());

			// Creating an all-trusting verifier for Host Name
			HostnameVerifier ValidHost = new HostnameVerifier() {
				public boolean verify(String HostName, SSLSession MySession) {
					return true;
				}
			};

			// Installing an all-trusting verifier for the HOST
			HttpsURLConnection.setDefaultHostnameVerifier(ValidHost);
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlLoyalty2RealReturn()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), VoucherResponse.class);
			if (response != null)
				savelog.setResponse(Json.toJson(response));
			SetLogError(response, savelog);
		} catch (Exception ex) {
			if (response == null)
				response = new VoucherResponse();
			response.setStatus(100);
			response.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public VoucherResponse SmsLoyalty2(VoucherSmsRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("SmsVoucher-L2-Rest");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		VoucherResponse response = null;
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlLoyalty2RealSms()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), VoucherResponse.class);
			if (response != null)
				savelog.setResponse(Json.toJson(response));
			SetLogError(response, savelog);
		} catch (Exception ex) {
			if (response == null)
				response = new VoucherResponse();
			response.setStatus(100);
			response.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyBaseResponse SendEmail(String request, Logger log) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("SendEmail-Rest");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		try {
			if ("Electric".equals(log.getOrganizationType())) {
				connection = (HttpURLConnection) new URL(RestfulHelper.getUrlSmsEmailWithFile()).openConnection();
			}
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlSmsEmailSet()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			if ("Electric".equals(log.getOrganizationType())) {
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestProperty("token", RestfulHelper.getSmsSendMailToken());
				connection.setRequestProperty("email", RestfulHelper.getSmsFromMail());
				connection.setRequestProperty("subject", "Hello");
				connection.setRequestProperty("text", request);
				connection.setRequestProperty("withFile", "1");
			}
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());

			if (200 == connection.getResponseCode()) {
				if ("0".equals(savelog.getResponse())) {
					response.setStatus(StatusCode.SUCCESS.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
				} else {
					response.setStatus(StatusCode.SERVER_NO_SEND.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_NO_SEND, log.getLanguage(),
							savelog.getResponse()));
				}
			} else {
				response.setStatus(StatusCode.SERVER_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_ERROR, log.getLanguage(),
						savelog.getResponse()));
			}
			if (response != null)
				savelog.setResponse(Json.toJson(response));
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(
					MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyBaseResponse SendMessage(String request, Logger log) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("SendMessage-Rest");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlSmsPhone()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			if ("Electric".equals(log.getOrganizationType())) {
				connection.setRequestProperty("Authorization", RestfulHelper.getSmsAuthorizationKeyEvcharger());
			}
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());

			if (200 == connection.getResponseCode()) {
				if ("0".equals(savelog.getResponse())) {
					response.setStatus(StatusCode.SUCCESS.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
				} else {
					response.setStatus(StatusCode.SERVER_NO_SEND.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_NO_SEND, log.getLanguage(),
							savelog.getResponse()));
				}
			} else {
				response.setStatus(StatusCode.SERVER_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_ERROR, log.getLanguage(),
						savelog.getResponse()));
			}
			if (response != null)
				savelog.setResponse(Json.toJson(response));
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(
					MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			savelog.setError(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
			savelog.setStatus(response.getStatus().toString());
			logger.log(savelog);
		}
		return response;
	}

	public String SetMessage(HashMap<?, ?> map) throws Exception {
		String requestData = "";
		String token = "";
		try {
			if ("Electric".equals(map.get("type"))) {
				token = RestfulHelper.getSmsTokenKeyEvcharger();
			} else if ("Test".equals(map.get("type"))) {
				token = RestfulHelper.getSmsTokenKeyEvcharger();
			}

			requestData = "{\r\n" + "  \"token\": \"" + token + "\", " + "\r\n" + "  \"number\": \""
					+ map.get("phoneNumber") + "\"," + "\r\n" + "  \"text\": \"Таны баримт: " + map.get("amount") + ", "
					+ map.get("ddtd") + ", " + map.get("lottery")
					+ " Таны худалдан авсан баримтыг илгээллээ. Web: bonus.nomin.mn. Lavlakh utas: 7799-1111.\"\r\n"
					+ "}";
			System.err.println("**********************************");
			System.out.println(requestData);
			System.err.println("**********************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestData;
	}

	@Asynchronous
	public String SetMail(HashMap<?, ?> map) throws Exception {
		String requestData = "";
		try {
			Logger log = (Logger) map.get("log");
			if ("Electric".equals(log.getOrganizationType())) {

			} else if ("Test".equals(map.get("type"))) {

			}

			requestData = "<div style='background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;margin:0; padding:0;'> "
					+ "<table cellspacing='0' cellpadding='0' border='0' width='100%'> "
					+ "<tr> <td align='center' valign='top' style='padding:20px 0 20px 0'> "
					+ "<table bgcolor='#FFFFFF' cellspacing='0' cellpadding='10' border='0' width='650' style='border:1px solid #E0E0E0;'> "
					+ "<tr> <td valign='top'>" + "<a href='https://bonus.nomin.mn'>"
					+ "<img src='https://bonus.nomin.mn/img/logo.68050aff.png' alt='nomin.bonus' style='margin-bottom:10px;' border='0'/>"
					+ "</a></td> </tr> <tr> <td valign='top'>"
					+ "<h4 style='font-size:14px;font-weight:normal; line-height:14px; margin:0 0 11px 0;''>Хэрэглэгч танд энэ өдрийн мэнд хүргэе</h4>"
					+ "<h4 style='font-size:14px; font-weight:normal; margin:0;'>Таны transDate <small>цагт худалдан авсан талоны мэдэгдэл.</small></h4></tr> <tr>"
					+ "<td>" + "<table cellspacing='0' cellpadding='0' border='0' width='650'><thead> <tr> "
					+ "<th align='left' width='325' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>QRCODE</th>"
					+ "<th width='10'></th> "
					+ "<th align='left' width='325' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>НӨАТ</th> </tr> </thead> "
					+ "<tbody> <tr> <td valign='top' style='font-size:12px; padding:15px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;'>"
					+ "<p style='width:50%; margin:0px auto;'>"
					+ "<img height='120' width='120' src='https://api.qrserver.com/v1/create-qr-code/?data="
					+ map.get("qrData") + "&amp;size=120x120' alt='' /></p></td> " + "<td>&nbsp;</td> "
					+ "<td valign='top' style='font-size:12px; padding:7px 9px 9px 9px; border-left:1px solid #EAEAEA; border-bottom:1px solid #EAEAEA; border-right:1px solid #EAEAEA;'> <p>"
					+ "<b>Сугалааны дугаар:</b> " + map.get("lottery") + " </p> " + "<p><b>ДДТД:</b>  "
					+ map.get("billId") + "  </p> " + "<p><b>ebarimt.mn -д бүртгүүлэх дүн:</b>  " + map.get("amount")
					+ " ₮ </p><p><b>Ebarimt нэвтрэх нэр:</b>  " + map.get("customerNo")
					+ "  </p>  </td> </tr> </tbody> </table> </td> </tr> <tr> <td> "
					+ "<table cellspacing='0' cellpadding='0' border='0' width='650'> " + "<thead> "
					+ "<tr> <th align='left' width='325' bgcolor='#EAEAEA' style='font-size:13px; padding:5px 9px 6px 9px; line-height:1em;'>Таны худалдан авалтын мэдээлэл</th> </table> <br/> "
					+ "<p style='padding: 0 9px;'><b>Худалдан авагчийн картын дугаар:</b>  " + map.get("cardholderId")
					+ " </p> " + "<p style='padding: 0 9px;'><b>Худалдан авагчийн нэр:</b> firstName </p> "
					+ "<p style='padding: 0 9px;'><b>Бонусын дүн:</b>  amount ₮ </p> "
					+ "<p style='padding: 0 9px;'><b>vat:</b> " + map.get("vatAmount") + " ₮ </p> "
					+ "<br/> </td> </tr> "
					+ "<tr> <td bgcolor='#EAEAEA' align='center' style='background:#EAEAEA; text-align:center;'>"
					+ "<p style='font-size:12px; margin:0;'>Манай үйлчилгээг сонгосон танд баярлалаа, <strong>NOMIN.MN</strong></p></center></td> </tr> "
					+ "<td bgcolor='#EAEAEA' align='left' style='background:#EAEAEA; text-align:left;'>"
					+ "<p style='font-size:12px; margin:0;'>\"Номин Холдинг\" ХХК-ийн Номин Юнайтед оффис Хан-Уул дүүрэг, Чингисийн өргөн чөлөө, Улаанбаатар 17042, Монгол Улс, 210136, Ш/Ч-2316 "
					+ "<p> Утас: 1800 2888, Факс: 976 75778888 И-мэйл: nomin@nomin.net </p> </p></center></td> </tr> </table> </td> </tr> </table>  </div>";

			System.err.println("******************************************************");
			System.out.println(requestData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return requestData;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public nomin.loyaltyintegration.entity.posapi.PutResponse putEbarimt(EbarimtRequest request, Logger log)
			throws Exception {
		nomin.loyaltyintegration.entity.posapi.PutResponse response = new nomin.loyaltyintegration.entity.posapi.PutResponse();
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("Ebarimt-Rest");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		String url = null;
		try {
			if (savelog.getProduction() == 0) {
				url = RestfulHelper.getUrlEbarimtTest();
			} else {
				url = RestfulHelper.getUrlEbarimtReal();
			}
			url = RestfulHelper.getUrlEbarimtTest();
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), PutResponse.class);
			if (response != null && "true".equals(response.getSuccess())) {
				nomin.loyaltyintegration.entity.posapi.PutResponse responseLog = readResponse(savelog.getResponse(),
						PutResponse.class);
				if (responseLog != null) {
					responseLog.setLottery("");
					responseLog.setQrData("");
					for (int i = 0; i < responseLog.getLstGroup().size(); i++) {
						responseLog.getLstGroup().get(i).setLottery("");
						responseLog.getLstGroup().get(i).setQrData("");
					}
					savelog.setResponse(Json.toJson(responseLog));
				} else {
					savelog.setResponse(AES.encrypt(Json.toJson(response), "BD165553659A48F99192262692146D6E"));
				}
				savelog.setStatus("1");
			} else {
				savelog.setStatus("0");
				if (response.getErrorCode() != null) {
					savelog.setStatus(response.getErrorCode().toString());
				}
			}
		} catch (Exception ex) {
			response.setMessage(ex.getMessage());
			savelog.setError(ex.getMessage());
			savelog.setStatus("100");
		} finally {
			if (connection != null)
				connection.disconnect();
			logger.log(savelog);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReportResponse GetBillImage(BillReportRequest request, Logger log) throws Exception {
		ReportResponse response = new ReportResponse();
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("Pos-Report-Rest");
		savelog.setRequest(JsonHelper.toJson(request));
		savelog.setRequestDate(new Date());
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getPosReportBillImage()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Connection", "keep-alive");
			connection.setRequestProperty("Content-Length", "<calculated when request is sent>");
			connection.setRequestProperty("Postman-Token", "<calculated when request is sent>");
			connection.setRequestProperty("Cache-Control", "no-cache");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			if (connection.getResponseCode() == 200) {
				response = readResponse(savelog.getResponse(), ReportResponse.class);
			}
		} catch (Exception ex) {
			response = null;
			savelog.setError(ex.getMessage());
			savelog.setStatus("100");
		} finally {
			logger.log(savelog);
			if (connection != null)
				connection.disconnect();
		}
		return response;
	}

	@SuppressWarnings("unused")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReturnBillResponse returnEbarimt(ReturnBillRequest returnBillRequest, Logger log) throws Exception {
		HttpURLConnection connection = null;
		ReturnBillResponse response = new ReturnBillResponse();
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("returnEbarimt-Rest");
		savelog.setRequest(Json.toJson(returnBillRequest));
		savelog.setRequestDate(new Date());
		String url = null;
		try {
			if (savelog.getProduction() == 0) {
				url = RestfulHelper.getUrlEbarimtReturnTest();
			} else {
				url = RestfulHelper.getUrlEbarimtReturnReal();
			}
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlEbarimtReturnTest()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			writeRequest(connection, returnBillRequest);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			response = readResponse(savelog.getResponse(), ReturnBillResponse.class);
			if (!response.isSuccess()) {
				throw new Exception(response.getMessage());
			}
			return response;
		} finally {
			logger.log(savelog);
			if (connection != null)
				connection.disconnect();
		}
	}

	@SuppressWarnings({ "unused", "null" })
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CheckRegisterResponse CheckRegisterNo(String request) throws Exception {
		HttpURLConnection connection = null;
		CheckRegisterResponse response = new CheckRegisterResponse();
		String result = "";
		try {
			connection = (HttpURLConnection) new URL(new StringBuilder(RestfulHelper.getPosCheckRegisterNo())
					.append("?regno=").append(request).toString()).openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json");
			checkError(connection);
			response = readResponse(connection, CheckRegisterResponse.class);
			if (response != null && "true".equals(response.getFound())) {
				response.setStatus(StatusCode.SUCCESS.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
			} else if (response != null) {
				response.setStatus(404);
				response.setMessage("Олдсонгүй");
			} else {
				response.setStatus(501);
				response.setMessage("Серверт алдаа гарлаа");
			}
		} catch (Exception ex) {
			response.setStatus(500);
			response.setMessage("Сервер алдаа " + ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
		}
		return response;
	}

	protected String QpayGetAuthToken() throws Exception {
		URL url = new URL(new StringBuilder().append(RestfulHelper.getUrlQpayReal()).append("/auth/token").toString());
		HttpURLConnection connection = null;
		try {
			QPayAuthRequest request = new QPayAuthRequest();
			request.setClientId("D903CDC7-2B88-18BA-38A2-FB78F3866800");
			request.setClientSecret("0431EE39-140D-1154-2F3A-9A83610E6C98");
			request.setGrantType("client");
			request.setRefreshToken("");

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			writeRequest(connection, request);
			checkError(connection);
			QPayAuthResponse authResponse = readResponse(connection, QPayAuthResponse.class);
			return authResponse.getAccessToken();
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public QPayBillCreateResponse QpayCreate(QPayBillCreateRequest request) throws Exception {
		String token = QpayGetAuthToken();
		URL url = new URL(
				new StringBuilder().append(RestfulHelper.getUrlQpayReal()).append("/in_store/create").toString());
		HttpURLConnection connection = null;

		nomin.loyaltyintegration.businessentity.qpay.request.create.QPayBillCreateRequest createRequest = new nomin.loyaltyintegration.businessentity.qpay.request.create.QPayBillCreateRequest();
		createRequest.setTemplateId("NOMIN_MN_INVOICE");
		createRequest.setMerchantId("NOMIN_MN");
		BranchInfo branchInfo = new BranchInfo();
		branchInfo.setId("1");
		branchInfo.setEmail("info@nomin.mn");
		branchInfo.setName("Nomin");
		nomin.loyaltyintegration.businessentity.qpay.data.Phone phone = new nomin.loyaltyintegration.businessentity.qpay.data.Phone();
		phone.setCountryCode("+976");
		phone.setPhoneNumber("0");
		branchInfo.setPhone(phone);
		createRequest.setBranchInfo(branchInfo);
		VatInfo vatInfo = new VatInfo();
		vatInfo.setTaxRegisterNo("0");
		vatInfo.setIsAutoGenerate("0");
		createRequest.setVatInfo(vatInfo);
		createRequest.setOrderNo(request.getOrderNo());
		createRequest.setAllowPartialPayment(false);
		createRequest.setAllowTip(false);
		createRequest.setCurrencyType("MNT");
		List<GoodsDetail> lstGoodDetail = new ArrayList<GoodsDetail>();
		GoodsDetail goodsDetail = new GoodsDetail();
		goodsDetail.setId("1");
		goodsDetail.setName("Payment");
		goodsDetail.setBarCode("");
		goodsDetail.setQuantity(1);
		goodsDetail.setUnitPrice(request.getAmount().intValue());
		goodsDetail.setBody("");
		lstGoodDetail.add(goodsDetail);
		createRequest.setLstGoodDetail(lstGoodDetail);
		createRequest.setNote("");
		createRequest.setAttach("");
		createRequest.setPaymentMethod("MPM"); // request.getPaymentMethod()
		createRequest.setCustomerQr(request.getCustomerQr() != null ? request.getCustomerQr() : "");
		createRequest.setDeviceInfo("DF1354DDI");
		createRequest.setNonceStr("DFD46842131");
		createRequest.setLogoUrl("");
		createRequest.setCallbackUrl("http://www.sample.mn/order/invoice_id=123");
		createRequest.setInformEmail("info@info.mn");
		nomin.loyaltyintegration.businessentity.qpay.data.Config config = new nomin.loyaltyintegration.businessentity.qpay.data.Config();
		nomin.loyaltyintegration.businessentity.qpay.data.ConfigResponse configResponse = new nomin.loyaltyintegration.businessentity.qpay.data.ConfigResponse();
		configResponse.setLstIgnoreField(Arrays.asList("qr_image"));
		config.setConfigResponse(configResponse);
		createRequest.setConfig(config);

		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("QP");
		paymentLog.setRequest("QPAYSENDINVOICE " + getRequest(createRequest));
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setAmount(request.getAmount());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(6000);
			connection.setRequestProperty("Authorization", new StringBuilder("Bearer ").append(token).toString());
			writeRequest(connection, createRequest);
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			return readResponse(paymentLog.getResponse(), QPayBillCreateResponse.class);
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw new Exception(getError(e.getMessage()));
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			logger.saveTransaction(paymentLog);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse QpayCancel(
			nomin.loyaltyintegration.businessentity.qpay.payment.cancel.QPayPaymentCancelRequest request)
			throws Exception {
		String token = QpayGetAuthToken();
		URL url = new URL(new StringBuilder().append(RestfulHelper.getUrlQpayReal()).append("/payment/")
				.append(request.getId()).toString());
		HttpURLConnection connection = null;
		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("QP");
		paymentLog.setRequest("QPAYCANCELINVOICE " + request.getId());
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		paymentLog.setAmount(request.getAmount());
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("DELETE");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(6000);
			connection.setRequestProperty("Authorization", new StringBuilder("Bearer ").append(token).toString());
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			return readResponse(paymentLog.getResponse(),
					nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse.class);
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw new Exception(getError(e.getMessage()));
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			logger.saveTransaction(paymentLog);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public nomin.loyaltyintegration.businessentity.qpay.data.PaymentInfo QpayCheck(
			nomin.loyaltyintegration.businessentity.qpay.check.pos.QPayPaymentCheckRequest request) throws Exception {
		String token = QpayGetAuthToken();
		URL url = new URL(
				new StringBuilder().append(RestfulHelper.getUrlQpayReal()).append("/in_store/check").toString());
		HttpURLConnection connection = null;
		nomin.loyaltyintegration.businessentity.qpay.payment.check.QPayPaymentCheckRequest checkRequest = new nomin.loyaltyintegration.businessentity.qpay.payment.check.QPayPaymentCheckRequest();
		checkRequest.setMerchantId("NOMIN_MN");
		checkRequest.setOrderNo(request.getOrderNo());

		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("QP");
		paymentLog.setRequest("QPAYCHECKPAYMENT " + getRequest(checkRequest));
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		paymentLog.setAmount(request.getAmount());
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(6000);
			connection.setRequestProperty("Authorization", new StringBuilder("Bearer ").append(token).toString());
			writeRequest(connection, checkRequest);
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			nomin.loyaltyintegration.businessentity.qpay.payment.check.QPayPaymentCheckResponse response = readResponse(
					paymentLog.getResponse(),
					nomin.loyaltyintegration.businessentity.qpay.payment.check.QPayPaymentCheckResponse.class);
			return response.getPaymentInfo();
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw new Exception(getError(e.getMessage()));
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			if (request.getRequestDate() != null) {
				paymentLog.setRequestTime(request.getRequestDate());
				paymentLog.setResponseTime(request.getRequestDate());
			}
			logger.saveTransaction(paymentLog);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBody SocialPayCreate(
			nomin.loyaltyintegration.businessentity.socialpay.bill.create.pos.SocialPayBillCreateRequest request)
			throws Exception {
		String requestPath = null;
		String requestName = null;
		nomin.loyaltyintegration.businessentity.socialpay.bill.create.SocialPayBillCreateRequest billCreateRequest = null;
		if (request.getPhoneNo() != null && !request.getPhoneNo().isEmpty()) {
			nomin.loyaltyintegration.businessentity.socialpay.bill.create.SocialPayBillCreateByPhoneRequest tmpRequest = new nomin.loyaltyintegration.businessentity.socialpay.bill.create.SocialPayBillCreateByPhoneRequest();
			tmpRequest.setPhone(request.getPhoneNo());
			billCreateRequest = tmpRequest;
			requestName = "SOCIALPAYSENDINVOICEBYPHONE";
			requestPath = "/pos/invoice/phone";
		}
		if (billCreateRequest == null) {
			billCreateRequest = new nomin.loyaltyintegration.businessentity.socialpay.bill.create.SocialPayBillCreateRequest();
			requestName = "SOCIALPAYSENDINVOICEBYQRCODE";
			requestPath = "/pos/invoice/qr";
		}
		billCreateRequest.setAmount(request.getAmount().toString());
		billCreateRequest.setInvoice(request.getOrderNo());
		billCreateRequest.setTerminal(RestfulHelper.getSocialpayTermial());
		billCreateRequest.setChecksum(getCheckSum(request.getOrderNo(), request.getAmount(), request.getPhoneNo()));

		URL url = new URL(
				new StringBuilder().append(RestfulHelper.getUrlSocialpayReal()).append(requestPath).toString());
		HttpURLConnection connection = null;

		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("SP");
		paymentLog.setRequest(requestName + " " + getRequest(billCreateRequest));
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setAmount(request.getAmount());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(10000);
			writeRequest(connection, billCreateRequest);
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponse response = readResponse(
					paymentLog.getResponse(),
					nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponse.class);
			return response.getBody();
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw e;
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			logger.saveTransaction(paymentLog);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBody SocialPayCancel(
			nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.pos.SocialPayPaymentCancelRequest request)
			throws Exception {
		URL url = new URL(new StringBuilder().append(RestfulHelper.getUrlSocialpayReal()).append("/pos/invoice/cancel")
				.toString());
		HttpURLConnection connection = null;

		nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.SocialPayPaymentCancelRequest cancelRequest = new nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.SocialPayPaymentCancelRequest();
		cancelRequest.setInvoice(request.getOrderNo());
		cancelRequest.setAmount(request.getAmount().toString());
		cancelRequest.setTerminal(RestfulHelper.getSocialpayTermial());
		cancelRequest.setChecksum(getCheckSum(request.getOrderNo(), request.getAmount(), null));

		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("SP");
		paymentLog.setRequest("SOCIALPAYCANCELINVOICE " + getRequest(cancelRequest));
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		paymentLog.setAmount(request.getAmount());
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(10000);
			writeRequest(connection, cancelRequest);
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponse response = readResponse(
					paymentLog.getResponse(),
					nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponse.class);
			return response.getBody();
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw e;
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			logger.saveTransaction(paymentLog);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public nomin.loyaltyintegration.businessentity.socialpay.payment.check.data.SocialPayPaymentCheckResponseBody SocialPayCheck(
			nomin.loyaltyintegration.businessentity.socialpay.payment.check.pos.SocialPayPaymentCheckRequest request)
			throws Exception {
		URL url = new URL(new StringBuilder().append(RestfulHelper.getUrlSocialpayReal()).append("/pos/invoice/check")
				.toString());
		HttpURLConnection connection = null;

		nomin.loyaltyintegration.businessentity.socialpay.payment.check.SocialPayPaymentCheckRequest checkRequest = new nomin.loyaltyintegration.businessentity.socialpay.payment.check.SocialPayPaymentCheckRequest();
		checkRequest.setInvoice(request.getOrderNo());
		checkRequest.setAmount(request.getAmount().toString());
		checkRequest.setTerminal(RestfulHelper.getSocialpayTermial());
		checkRequest.setChecksum(getCheckSum(request.getOrderNo(), request.getAmount(), null));

		nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog paymentLog = new nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog();
		paymentLog.setPgName("SP");
		paymentLog.setRequest("SOCIALPAYCHECKPAYMENT " + getRequest(checkRequest));
		paymentLog.setLocationKey(request.getLocationKey());
		paymentLog.setPosKey(request.getPosKey());
		paymentLog.setCashierKey(request.getCashierKey());
		paymentLog.setReferenceNo(new Long(request.getOrderNo()));
		paymentLog.setAmount(request.getAmount());
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(10000);
			writeRequest(connection, checkRequest);
			checkError(connection);
			paymentLog.setResponse(readResponse(connection, String.class));
			nomin.loyaltyintegration.businessentity.socialpay.payment.check.SocialPayPaymentCheckResponse response = readResponse(
					paymentLog.getResponse(),
					nomin.loyaltyintegration.businessentity.socialpay.payment.check.SocialPayPaymentCheckResponse.class);
			return response.getBody();
		} catch (Exception e) {
			paymentLog.setErrorMessage(e.getMessage());
			throw e;
		} finally {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
			if (request.getRequestDate() != null) {
				paymentLog.setRequestTime(request.getRequestDate());
				paymentLog.setResponseTime(request.getRequestDate());
			}
			logger.saveTransaction(paymentLog);
		}
	}

	protected String getCheckSum(String invoice, BigDecimal amount, String phone) throws Exception {
		// terminal + invoice + amount + phone
		StringBuilder sb = new StringBuilder();
		sb.append(RestfulHelper.getSocialpayTermial());
		sb.append(invoice);
		sb.append(amount.toString());
		if (phone != null)
			sb.append(phone);
		return getCheckSum(sb.toString(), RestfulHelper.getSocialpayAuth());
	}

	protected String getCheckSum(String message, String key) throws Exception {
		byte[] byteKey = key.getBytes("UTF-8");
		String HMAC = "HmacSHA256";
		SecretKeySpec keySpec = new SecretKeySpec(byteKey, HMAC);
		Mac mac = Mac.getInstance(HMAC);
		mac.init(keySpec);
		return ByteHelper.bytesToHex(mac.doFinal(message.getBytes("UTF-8"))).toLowerCase();
	}

	// Upoint Web Token
	/**
	 * //Upoint Web Token getUpointToken public UpointTokenResponse
	 * getUpointToken(UpointTokenCreateRequest upointTokenCreateRequest, Logger log)
	 * throws Exception { HttpURLConnection connection = null; Logger savelog =
	 * (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
	 * savelog.setType("upointToken-Rest");
	 * savelog.setRequest(Json.toJson(upointTokenCreateRequest));
	 * savelog.setRequestDate(new Date()); try { String encoded =
	 * Base64.getEncoder().encodeToString(("gOrI2KROrr6Csv" + ":" +
	 * "iE_ZlVzDB0Lqf6_kauem4ToTQvudCUitWdX9F0b7x7vVoLsrWvToC/DRk0IH").getBytes(StandardCharsets.UTF_8));
	 * URL url = new URL(new
	 * StringBuilder().append(RestfulHelper.getUrlUpointToken()).toString());
	 * connection = (HttpURLConnection) url.openConnection();
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Content-Type", "application/json");
	 * connection.setRequestProperty("Authorization", "Basic " + encoded);
	 * connection.setConnectTimeout(5000); connection.setReadTimeout(5000);
	 * writeRequest(connection, upointTokenCreateRequest); checkError(connection);
	 * 
	 * savelog.setResponse(readResponse(connection, String.class));
	 * savelog.setResponseDate(new Date()); UpointTokenResponse response =
	 * readResponse(savelog.getResponse(), UpointTokenResponse.class); if
	 * (response.getError() != null) { throw new Exception("UpointToken авахад алдаа
	 * гарлаа"+response.getError_description()); } return response; } finally {
	 * logger.log(savelog); if (connection != null) connection.disconnect(); } }
	 * 
	 * public ProcessTransactionResponseData
	 * UpointProcessTransaction(UpointTransactionData UpointTransactionData, String
	 * token, Logger log) throws Exception { HttpURLConnection connection = null;
	 * Logger savelog = (Logger)
	 * nomin.loyaltyintegration.tools.Tools.deepClone(log);
	 * savelog.setType("upointProcess-Rest");
	 * savelog.setRequest(Json.toJson(UpointTransactionData));
	 * savelog.setRequestDate(new Date()); try { URL url = new URL(new
	 * StringBuilder().append("https://api1.upoint.mn:10000/v1/resources/process_transaction").toString());
	 * connection = (HttpURLConnection) url.openConnection();
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Content-Type", "application/json");
	 * connection.setRequestProperty("Authorization", new StringBuilder("Bearer
	 * ").append(token).toString()); connection.setConnectTimeout(6000);
	 * connection.setReadTimeout(10000); writeRequest(connection,
	 * UpointTransactionData); checkError(connection);
	 * 
	 * savelog.setResponse(readResponse(connection, String.class));
	 * savelog.setResponseDate(new Date()); ProcessTransactionResponseData response
	 * = readResponse(savelog.getResponse(), ProcessTransactionResponseData.class);
	 * if (response.getResult() != 0) { throw new Exception(response.getMessage());
	 * } return response; } finally { logger.log(savelog); if (connection != null)
	 * connection.disconnect(); } }
	 * 
	 * public UpointConfirmTransactionResponse
	 * UpointConfirmTransaction(UpointConfirmTransactionRequest
	 * upointConfirmTransactionRequest, Logger log) throws Exception {
	 * HttpURLConnection connection = null; Logger savelog = (Logger)
	 * nomin.loyaltyintegration.tools.Tools.deepClone(log);
	 * savelog.setType("upointConfirm-Rest");
	 * savelog.setRequest(Json.toJson(upointConfirmTransactionRequest));
	 * savelog.setRequestDate(new Date()); try { URL url = new URL(new
	 * StringBuilder().append("https://api1.upoint.mn:10000/v1/resources/confirm_transaction").toString());
	 * connection = (HttpURLConnection) url.openConnection();
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Content-Type", "application/json");
	 * connection.setRequestProperty("Authorization", "merchantToken");// TODO
	 * getMerchantToken connection.setConnectTimeout(6000);
	 * connection.setReadTimeout(10000); writeRequest(connection,
	 * upointConfirmTransactionRequest); checkError(connection);
	 * 
	 * savelog.setResponse(readResponse(connection, String.class));
	 * savelog.setResponseDate(new Date()); UpointConfirmTransactionResponse
	 * response = readResponse(savelog.getResponse(),
	 * UpointConfirmTransactionResponse.class); if
	 * (!"Амжилттай".equals(response.getMessage())) { throw new
	 * Exception(response.getMessage()); } return response; } finally {
	 * logger.log(savelog); if (connection != null) connection.disconnect(); } }
	 * 
	 * public ProcessTransactionResponseData
	 * UpointCancelTransaction(UpointTransactionData UpointTransactionData, Logger
	 * log) throws Exception { HttpURLConnection connection = null; Logger savelog =
	 * (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
	 * savelog.setType("upointCancel-Rest");
	 * savelog.setRequest(Json.toJson(UpointTransactionData));
	 * savelog.setRequestDate(new Date()); try { URL url = new URL(new
	 * StringBuilder().append("https://api1.upoint.mn:10000/v1/resources/cancel_transaction").toString());
	 * connection = (HttpURLConnection) url.openConnection();
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Content-Type", "application/json");
	 * connection.setRequestProperty("Authorization", "merchantToken");// TODO
	 * getMerchantToken connection.setConnectTimeout(6000);
	 * connection.setReadTimeout(10000); writeRequest(connection,
	 * UpointTransactionData); checkError(connection);
	 * 
	 * savelog.setResponse(readResponse(connection, String.class));
	 * savelog.setResponseDate(new Date()); ProcessTransactionResponseData response
	 * = readResponse(savelog.getResponse(), ProcessTransactionResponseData.class);
	 * if (response.getResult() != 0) { throw new Exception(response.getMessage());
	 * } return response; } finally { logger.log(savelog); if (connection != null)
	 * connection.disconnect(); } }
	 **/
}
