package nomin.loyaltyintegration.businesslogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import nomin.loyaltyintegration.businessentity.credential.User;
import nomin.loyaltyintegration.businesslogic.interfaces.IUserCredentialLogicLocal;

@Stateless
public class UserCredentialLogic implements IUserCredentialLogicLocal {
	
	private static List<User> users = new ArrayList<User>();
	
	static {
		users = new ArrayList<User>();
		users.add(new User(1L, "api", "api", Arrays.asList("api")));
		users.add(new User(2L, "test-eshop", "123456", Arrays.asList("loyalty")));
		users.add(new User(3L, "test-eshop-2", "A^#jQ7un!t", Arrays.asList("loyalty")));
		users.add(new User(4L, "test-oil", "1234562020", Arrays.asList("loyalty")));		
		users.add(new User(6L, "test-service", "20202020", Arrays.asList("loyalty")));
		users.add(new User(7L, "test-mobile", "20202020", Arrays.asList("loyalty")));
		
		users.add(new User(5L, "oil", "P@550il20@0", Arrays.asList("loyalty")));
		users.add(new User(5L, "qrReal", "QrRe@l2@2@", Arrays.asList("loyalty")));
		users.add(new User(5L, "qrTest", "QrTest2@2@", Arrays.asList("loyalty")));
		users.add(new User(8L, "EVcharger", "Ev-Ch@rgeR", Arrays.asList("loyalty")));
		users.add(new User(8L, "EVcharger-Test", "1234562021", Arrays.asList("loyalty")));
	}

	@Override
	public User login(String username, String password) throws Exception {
		if(username == null || password == null) return null;
		if(username != null && username.isEmpty()) return null;
		if(password != null && password.isEmpty()) return null;
		
		for (User user : users) {
			if(user.getUsername().equals(username)) {
				if(user.getPassword().equals(password)) {
					return new User(user.getUserKey(), user.getUsername(), user.getRoles());
				}
				else return null;				
			}
		}
		return null;
	}
}