package nomin.loyaltyintegration.businesslogic;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import nomin.core.logic.RestClient;
import nomin.core.utils.Json;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IUPointLogicLocal;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.card.info.CancelTransactionRequest;
import nomin.loyaltyintegration.entity.card.info.UPointTransactionRequest;
import nomin.loyaltyintegration.entity.pos.PosResponse;
import nomin.loyaltyintegration.helper.RestfulHelper;
import nomin.loyaltyintegration.businessentity.BaseJsonResponse;
import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoRequest;
import nomin.loyaltyintegration.businessentity.upoint.response.CheckInfoResponse;
import nomin.loyaltyintegration.businessentity.upoint.response.ProcessTransactionResponseData;
import nomin.loyaltyintegration.businessentity.upoint.response.cancel.CancelTransactionResponseData;
import nomin.loyaltyintegration.businesslogic.helper.AbstractHttpClient;


@Stateless(name = "UPointLogic")
public class UPointLogic extends AbstractHttpClient implements IUPointLogicLocal {

	@Inject
	ITransactionLog logger;

	@Override
	public String getApiUrl() {
		return "";
	}

	// TODO getCardInfo дуудаж байгааг болиулж getCheckInfo солих
	@Override
	public PosResponse getCardInfo(String rawData, Logger log) {
		PosResponse response = new PosResponse();
		Logger saveLog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		saveLog.setType("UPOINT_CHECK_INFO");
		saveLog.setRequest(rawData);
		saveLog.setRequestDate(new Date());
		try {
			response = RestClient.postRequest(RestfulHelper.getUrlUpointCheckInfo(), rawData, PosResponse.class, MediaType.APPLICATION_JSON_TYPE, null);

			if (response != null && response.getStatus() == 0) {
				if (response.getResult() != 0)
					response.setStatus(response.getResult());
			} else {
				response.setResult(1);
				response.setMessage("U-Point check-info функц дуудахад алдаа гарлаа. Сервис төлөв: " + response.getStatus());
			}
		} catch (Exception ex) {
			response.setResult(100);
			response.setMessage("U-Point check-info функц дуудахад алдаа гарлаа. Сервис төлөв: " + response.getStatus());
			saveLog.setError(ex.getMessage());
		} finally {
			saveLog.setResponseDate(new Date());
			saveLog.setResponse(Json.toJson(response));
			logger.log(saveLog);
		}
		return response;
	}

	// upointCheckInfo
	@Override
	public CheckInfoResponse getCheckInfo(CheckInfoRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("UPOINT_CHECK_INFO");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		CheckInfoResponse response = null;
		BaseJsonResponse baseResponse = null;
		if (log.getProduction() == 0) {
			request.setLocationKey(201403060000000001L);
		}
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlUpointCheckInfo()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			baseResponse = readResponse(savelog.getResponse(), BaseJsonResponse.class);
			savelog.setStatus(String.valueOf(baseResponse.getStatus()));
			if (baseResponse != null && 0 == baseResponse.getStatus()) {
				response = Json.toJsonObject(baseResponse.getReadEntity(), CheckInfoResponse.class);
				savelog.setResponse(Json.toJson(response));
			}
		} catch (Exception ex) {
			if (response == null)
				response = new CheckInfoResponse();
			response.setMessage(ex.getMessage());
			response.setResult(500);
			savelog.setError(ex.getMessage());
		} finally {
			logger.log(savelog);
		}
		return response;
	}
	
	// upoint ProcessTransaction
	@Override
	public ProcessTransactionResponseData upointProcessTransaction(UPointTransactionRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("UPOINT_TRANSACTION");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		ProcessTransactionResponseData response = null;
		BaseJsonResponse baseResponse = null;
		if (log.getProduction() == 0) {
			request.setLocationKey(201403060000000001L);
		}
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlUpointTransaction()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			baseResponse = readResponse(savelog.getResponse(), BaseJsonResponse.class);
			savelog.setStatus(String.valueOf(baseResponse.getStatus()));
			
			if (baseResponse != null) {
				if(0 == baseResponse.getStatus()) {
					response = Json.toJsonObject(baseResponse.getReadEntity(), ProcessTransactionResponseData.class);
					savelog.setResponse(Json.toJson(response));
				} else {
					response = new ProcessTransactionResponseData();
					response.setMessage(baseResponse.getMessage());
					response.setResult(baseResponse.getResult());
				}
			}
		} catch (Exception ex) {
			if (response == null)
				response = new ProcessTransactionResponseData();
			response.setMessage(ex.getMessage());
			response.setResult(500);
			savelog.setError(ex.getMessage());
		} finally {
			logger.log(savelog);
		}
		return response;
	}
	
	// upoint CancelTransaction
	@Override
	public CancelTransactionResponseData upointCancelTransaction(CancelTransactionRequest request, Logger log) throws Exception {
		HttpURLConnection connection = null;
		Logger savelog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		savelog.setType("UPOINT_CANCEL_TRANSACTION");
		savelog.setRequest(Json.toJson(request));
		savelog.setRequestDate(new Date());
		CancelTransactionResponseData response = null;
		BaseJsonResponse baseResponse = null;
		if (log.getProduction() == 0) {
			request.setLocationKey(201403060000000001L);
		}
		try {
			connection = (HttpURLConnection) new URL(RestfulHelper.getUrlUpointCancelTransaction()).openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setReadTimeout(60000);
			writeRequest(connection, request);
			checkError(connection);
			savelog.setResponse(readResponse(connection, String.class));
			savelog.setResponseDate(new Date());
			baseResponse = readResponse(savelog.getResponse(), BaseJsonResponse.class);
			savelog.setStatus(String.valueOf(baseResponse.getStatus()));
			if (baseResponse != null) {
				if(0 == baseResponse.getStatus()) {
					response = Json.toJsonObject(baseResponse.getReadEntity(), CancelTransactionResponseData.class);
					savelog.setResponse(Json.toJson(response));
				} else {
					response = new CancelTransactionResponseData();
					response.setMessage(baseResponse.getMessage());
					response.setResult(baseResponse.getResult());
				}
			}
		} catch (Exception ex) {
			if (response == null)
				response = new CancelTransactionResponseData();
			response.setMessage(ex.getMessage());
			response.setResult(500);
			savelog.setError(ex.getMessage());
		} finally {
			logger.log(savelog); 
		}
		return response;
	}
}