package nomin.loyaltyintegration.businesslogic;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import nomin.core.logic.BaseLogic;
import nomin.loyaltyintegration.businessentity.card.CardJwtClaim;
import nomin.loyaltyintegration.businesslogic.interfaces.ICryptoLogic;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;

@Stateless(name = "CryptoLogic")
public class CryptoLogic extends BaseLogic implements ICryptoLogic, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String UNIT_NAME = "LoyaltyIntegration";
	private static SecretKeySpec secretKey;
	private static byte[] key;
	public String keyJWT = "6D2104F2120B48B58C6FD1DAEB80A549";

	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	public CryptoLogic() {

	}

	@Override
	public String encryptAES(String strToEncrypt, String secret) {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}

	@Override
	public String decryptAES(String strToDecrypt, String secret) {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			System.out.println("Error while decrypting: " + e.toString());
		}
		return null;
	}

	public static void setKey(String myKey) {
		try {
			key = myKey.getBytes("UTF-8");
			secretKey = new SecretKeySpec(key, "AES");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String encryptCardJWT(CardJwtClaim claim) {
		try {
			Map<String, Object> headerClaims = new HashMap<String, Object>();
			headerClaims.put("cardType", claim.getCardType());
			headerClaims.put("cardNumber", claim.getCardNumber());
			headerClaims.put("pinType", claim.getPinType());
			headerClaims.put("pinData", claim.getPinData());
			Algorithm algorithm = Algorithm.HMAC256(keyJWT);
			String token = JWT.create().withIssuer("auth0").withHeader(headerClaims).sign(algorithm);
			return token;
		} catch (Exception e) {
			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}

	@Override
	public CardInfoRequest decryptCardJWT(String strToDecrypt) {
		DecodedJWT jwt = null;
		CardInfoRequest cardInfo = null;
		try {
			Algorithm algorithm = Algorithm.HMAC256(keyJWT);
			JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
			jwt = verifier.verify(strToDecrypt);

			cardInfo = new CardInfoRequest();
			cardInfo.setCardType(jwt.getHeaderClaim("cardType").asString());
			cardInfo.setCardNumber(jwt.getHeaderClaim("cardNumber").asString());
			cardInfo.setPinType(jwt.getHeaderClaim("pinType").asString());
			cardInfo.setPinData(jwt.getHeaderClaim("pinData").asString());

		} catch (Exception e) {
			System.out.println("Error while decrypting: " + e.getMessage());
		}
		return cardInfo;
	}

}
