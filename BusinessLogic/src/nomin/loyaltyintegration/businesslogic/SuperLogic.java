package nomin.loyaltyintegration.businesslogic;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

public abstract class SuperLogic {

	public abstract EntityManager getEntityManager();
	
	private void setParameters(Query query, Map<String, Object> parameters) {
		if(parameters == null || parameters.isEmpty()) return;
		Set<Parameter<?>> lstParameter = query.getParameters();
		if(lstParameter == null || lstParameter.isEmpty()) return;
		
		for(Parameter<?> parameter : query.getParameters()) {
			if(parameters.containsKey(parameter.getName()))
				query.setParameter(parameter.getName(), parameters.get(parameter.getName()));
		}
	}
	
	private void setLimit(Query query, int firstResult, int maxResults) {
		if(maxResults > 0) {
			query.setFirstResult(firstResult);
			query.setMaxResults(maxResults);
		}
	}
	
	public <T> List<T> getByQuery(Class<T> type, String jpql, Map<String, Object> parameters) throws Exception {
		return getByQuery(type, jpql, parameters, 0, 0);
	}
	
	@Transactional(value = TxType.NOT_SUPPORTED)
	public <T> List<T> getByQuery(Class<T> type, String jpql, Map<String, Object> parameters, int firstResult, int maxResults) throws Exception {
		TypedQuery<T> query = getEntityManager().createQuery(jpql, type);
		setParameters(query, parameters);
		setLimit(query, firstResult, maxResults);
		return query.getResultList();
	}
	
	@Transactional(value = TxType.NOT_SUPPORTED)
	public Object getBySingleResult(String jpql, Map<String, Object> parameters) {
		Query query = getEntityManager().createQuery(jpql);
		setParameters(query, parameters);
		return query.getSingleResult();
	}
	
	@Transactional(value = TxType.NOT_SUPPORTED)
	public <T> List<T> getAll(Class<T> type) throws Exception {
		return getByQuery(type, "SELECT a FROM "+type.getSimpleName()+" a", null);
	}
	
	
	public void insert(List<?> lstObject) throws Exception {
		for(Object obj : lstObject) {
			getEntityManager().persist(obj);
		}
		getEntityManager().flush();
	}
	
	public void update(List<?> lstObject) throws Exception {
		for(Object obj : lstObject) {
			getEntityManager().merge(obj);
		}
		getEntityManager().flush();
	}
	
	public <T> void insert(T object) throws Exception {
		getEntityManager().persist(object);
		getEntityManager().flush();
	}
	
	public <T> void update(T object) throws Exception {
		getEntityManager().merge(object);
		getEntityManager().flush();
	}
	
	public void executeNonQuery(String jpql, Map<String, Object> parameters) throws Exception {
		Query query = getEntityManager().createQuery(jpql);
		setParameters(query, parameters);
		query.executeUpdate();
	}
}