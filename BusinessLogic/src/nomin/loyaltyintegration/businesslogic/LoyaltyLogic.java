package nomin.loyaltyintegration.businesslogic;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.SQLException;
import java.text.Bidi;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.eclipse.persistence.exceptions.DatabaseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import nomin.loyaltyintegration.businessentity.GenericResponse;
import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businessentity.Response;
import nomin.loyaltyintegration.businessentity.bill.response.BillImageResponse;
import nomin.loyaltyintegration.businessentity.device.CalcBonusResponse;
import nomin.loyaltyintegration.businessentity.device.CheckDevice;
import nomin.loyaltyintegration.businessentity.device.Info;
import nomin.loyaltyintegration.businessentity.device.SaveBonusResponse;
import nomin.loyaltyintegration.businessentity.device.TransactionInfoResponse;
import nomin.loyaltyintegration.businessentity.device.TransactionResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.CheckRegisterResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.EbarimtRequest;
import nomin.loyaltyintegration.businessentity.ebarimt.Stocks;
import nomin.loyaltyintegration.businessentity.log.LogRequest;
import nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse;
import nomin.loyaltyintegration.businessentity.qpay.pos.QPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.qpay.response.create.QPayBillCreateResponse;
import nomin.loyaltyintegration.businessentity.qr.QrCheckResponse;
import nomin.loyaltyintegration.businessentity.qr.QrReadRequest;
import nomin.loyaltyintegration.businessentity.qr.QrReadResponse;
import nomin.loyaltyintegration.businessentity.qr.QrRequest;
import nomin.loyaltyintegration.businessentity.qr.QrResponse;
import nomin.loyaltyintegration.businessentity.socialpay.bill.create.pos.SocialPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBody;
import nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.pos.SocialPayPaymentCancelRequest;
import nomin.loyaltyintegration.businessentity.socialpay.payment.check.data.SocialPayPaymentCheckResponseBody;
import nomin.loyaltyintegration.businessentity.socialpay.payment.check.pos.SocialPayPaymentCheckRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherCheckRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherDetails;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherTransactionRequest;
import nomin.loyaltyintegration.businessentity.spectre.response.VoucherResponse;
import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoDetailRequest;
import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoRequest;
import nomin.loyaltyintegration.businessentity.upoint.response.CheckInfoResponse;
import nomin.loyaltyintegration.businessentity.upoint.response.ProcessTransactionResponseData;
import nomin.loyaltyintegration.businessentity.upoint.response.cancel.CancelTransactionResponseData;
import nomin.loyaltyintegration.businessentity.user.User;
import nomin.core.logic.BaseLogic;
import nomin.core.utils.Dates;
import nomin.core.utils.Json;
import nomin.core.utils.Strings;
import nomin.core.utils.Tools;
import nomin.loyaltyintegration.businesslogic.helper.StringHelper;
import nomin.loyaltyintegration.businesslogic.interfaces.ICardLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ILoyaltyLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.IService;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IUPointLogicLocal;
import nomin.loyaltyintegration.businesslogic.interfaces.IVoucherLogic;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.bill.BillDetailRequest;
import nomin.loyaltyintegration.entity.bill.BillRequest;
import nomin.loyaltyintegration.entity.bill.Bills;
import nomin.loyaltyintegration.entity.bill.CalcResult;
import nomin.loyaltyintegration.entity.bill.Sales;
import nomin.loyaltyintegration.entity.card.info.Bank;
import nomin.loyaltyintegration.entity.card.info.CancelTransactionRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfo;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfoResponse;
import nomin.loyaltyintegration.entity.card.info.Item;
import nomin.loyaltyintegration.entity.card.info.Manufacturer;
import nomin.loyaltyintegration.entity.card.info.QrCardInfoProcedureResponse;
import nomin.loyaltyintegration.entity.card.info.UPointTransactionRequest;
import nomin.loyaltyintegration.entity.card.info.UpointTransactionData;
import nomin.loyaltyintegration.entity.invoice.InvoiceBill;
import nomin.loyaltyintegration.entity.invoice.InvoiceCustomerInfo;
import nomin.loyaltyintegration.entity.invoice.InvoiceEbarimt;
import nomin.loyaltyintegration.entity.invoice.InvoiceRequest;
import nomin.loyaltyintegration.entity.login.DeviceInfo;
import nomin.loyaltyintegration.entity.payment.CalcPaymentRequest;
import nomin.loyaltyintegration.entity.payment.CalcPaymentRequestExchange;
import nomin.loyaltyintegration.entity.payment.PaymentData;
import nomin.loyaltyintegration.entity.payment.PaymentInfo;
import nomin.loyaltyintegration.entity.payment.PaymentInfoNext;
import nomin.loyaltyintegration.entity.payment.PaymentRequest;
import nomin.loyaltyintegration.entity.payment.PaymentRequestChange;
import nomin.loyaltyintegration.entity.payment.PaymentRequestLog;
import nomin.loyaltyintegration.entity.payment.VoucherPaymentResponse;
import nomin.loyaltyintegration.entity.posapi.PutGroup;
import nomin.loyaltyintegration.entity.posapi.PutRequest;
import nomin.loyaltyintegration.entity.posapi.PutResponse;
import nomin.loyaltyintegration.entity.transaction.ReturnTransactionRequest;
import nomin.loyaltyintegration.entity.voucher.InvoiceVoucherPayments;
import nomin.loyaltyintegration.enums.Language;
import nomin.loyaltyintegration.enums.StatusCode;
import nomin.loyaltyintegration.enums.TestCards;
import nomin.loyaltyintegration.helper.JsonHelper;
import nomin.loyaltyintegration.helper.MessageHelper;
import nomin.loyaltyintegration.helper.VatCustomerRegistryNoValidator;
import nomin.posreport.web.businessentity.bill.BillItemDtl;
import nomin.posreport.web.businessentity.bill.BillItemGroup;
import nomin.posreport.web.businessentity.bill.BillItems;
import nomin.posreport.web.businessentity.bill.BillReport;
import nomin.posreport.web.businessentity.bill.BillReportRequest;
import nomin.posreport.web.businessentity.bill.CardData;
import nomin.posreport.web.businessentity.bill.CardItem;
import nomin.posreport.web.businessentity.bill.CardItems;
import nomin.posreport.web.businessentity.bill.VoucherData;
import nomin.posreport.web.businessentity.bill.VoucherDataDtl;
import nomin.posreport.web.businessentity.report.ReportResponse;

@Stateless(name = "LoyaltyLogic")
public class LoyaltyLogic extends BaseLogic implements ILoyaltyLogic, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String UNIT_NAME = "LoyaltyIntegration";
	public static final ObjectMapper objectMapper = JsonHelper.objectMapperStatic;
	VatCustomerRegistryNoValidator vatCustomerRegistryNoValidator = VatCustomerRegistryNoValidator.getInstance();

	@Inject
	ICardLogic cardLogic;

	@Inject
	IUPointLogicLocal upointLogic;

	@Inject
	IService service;

	@Inject
	IVoucherLogic voucherLoyaltyOne;

	@Inject
	ITransactionLog logger;

	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unused")
	@Override
	public LoyaltyBaseResponse CalcBonus(String rawData, Integer isEbarimt) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CardInfoResponse cardResponse = new CardInfoResponse();
		QrCardInfoProcedureResponse qrCardResponse = new QrCardInfoProcedureResponse();
		CalcBonusResponse calcBonusResponse = new CalcBonusResponse();
		BigDecimal totalAmount = new BigDecimal(0);
		Logger log = new Logger();
		log.setType("CalcBonus");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		String cardInfoRaw = "";
		String billDataRaw = "";
		CalcPaymentRequest request = new CalcPaymentRequest();
		List<String> states = new ArrayList<>();
		Boolean isQrData = false;
		List<CalcBonusResponse> list = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, CalcPaymentRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setCardInfoRequest(request.getCard());

			if (!CheckDevice(request, response, log))
				return response;

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getQrData() == null || request.getQrData().length() != 18) {
				if (request.getUser().getNoCardContinue() == null) {
					request.getCard().setNoCardContinue(false);
				} else {
					request.getCard().setNoCardContinue("true".equals(request.getUser().getNoCardContinue()) ? true : false);
				}
				if (request == null || request.getCard() == null || StringHelper.isNullOrEmpty(request.getCard().getCardNumber())) {
					response.setStatus(StatusCode.CARD_EMPTY.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_EMPTY, log.getLanguage()));
					return response;
				}

				request.getCard().setCardNumber(request.getCard().getCardNumber().replace(":", ""));
				log.setCardNumber(request.getCard().getCardNumber() != null ? request.getCard().getCardNumber() : "");

				if (!"Eshop".equals(request.getDevice().getOrganizationType())) {
					if (!request.getCard().isNoCardContinue()) {
						System.err.println("encrypt pincode");
						String decryptedText = AES.decrypt(request.getCard().getPinData(), log.getKeyAES());
						request.getCard().setPinData(decryptedText);
					}
				}
				cardResponse = cardLogic.getCardInfo3(request.getCard(), log);
			} else {
				HashMap<String, Object> parameters = new HashMap<>();
				parameters.put("QrData", request.getQrData());
				parameters.put("Type", "CheckReference");
				parameters.put("DeviceId", request.getDevice().getDeviceId());
				parameters.put("Language", String.valueOf(log.getLanguage()));

				List<QrCardInfoProcedureResponse> qrGetCardInfo = getByProcedure("CHECK_QR", parameters, QrCardInfoProcedureResponse.class);
				qrCardResponse = qrGetCardInfo.get(0);

				if (qrGetCardInfo == null || qrGetCardInfo.size() == 0 || !"SUCCESS".equals(qrCardResponse.getQrResult())) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, log.getLanguage()));
					response.setState(StatusCode.QR_COULDNOT_READ.getValue());
					return response;
				}
				log.setCardNumber(qrCardResponse.getCardHolderId());
				cardResponse = SetCardInfoResponse(qrCardResponse);

				if ("Y".equals(cardResponse.getUseUpoint())) {
					CheckInfoRequest checkInfoRequest = cardLogic.getCheckInfoRequest(log, cardResponse.getUaCardId());
					CheckInfoResponse upointCheckInfoResponse = null;
					if(checkInfoRequest!=null)
						upointCheckInfoResponse = upointLogic.getCheckInfo(checkInfoRequest, log);
					//Upoint карт амжилттай дуудагдсан үед мэдээллийг бөглөнө эсрэг тохиолдолд UsePoint-г null бөглөнө
					if (upointCheckInfoResponse != null && upointCheckInfoResponse.getResult() == 0 && upointCheckInfoResponse.getCardStatus() == 1) {
						cardResponse.setUaId(upointCheckInfoResponse.getUpointCardNumber());
						cardResponse.setUaCardId(upointCheckInfoResponse.getCardNumber());
						cardResponse.setUaResult(upointCheckInfoResponse.getResult());
						cardResponse.setUaPhone(upointCheckInfoResponse.getMobile());
						cardResponse.setUaCheckInfoMessage(upointCheckInfoResponse.getMessage());
						cardResponse.setUaBalance(upointCheckInfoResponse.getUpointBalance().length() > 0 ? Double.parseDouble(upointCheckInfoResponse.getUpointBalance()) : Double.MIN_VALUE);
						cardResponse.setUaCardStatus(upointCheckInfoResponse.getCardStatus());
						cardResponse.setUaCreatedDate(upointCheckInfoResponse.getCreatedAt());
						cardResponse.setUaCheckInfoStatus("1");
					}else {
						cardResponse.setUseUpoint(null);
					}
				}
				isQrData = true;
			}

			if (!CheckCalcBonus(request, response, log))
				return response;

			totalAmount = request.getBill().getTotalAmount();
			billDataRaw = JsonHelper.toJson(request.getBill().getBillDetails());

			if (cardResponse != null && ("success".equals(cardResponse.getMessage()) || "АМЖИЛТТАЙ".equals(cardResponse.getMessage()))) {
				cardResponse.setBillNo(request.getBill().getBillNo());
				if (request.getQrData() != null && request.getQrData().length() == 18) {
					cardResponse.setCardType("CardNumber");
				} else {
					cardResponse.setCardType(request.getCard().getCardType() != null ? request.getCard().getCardType() : "");
				}
				cardResponse.setPinType(request.getCard().getPinType());
				cardResponse.setTotalAmount(totalAmount);
				cardResponse.setVoucher(request.getBill().getVoucher());

				HashMap<String, Object> parameters = new HashMap<>();
				parameters.put("RequestId", request.getRequestId());
				parameters.put("DeviceId", request.getDevice().getDeviceId());
				parameters.put("CardInfo", JsonHelper.toJson(cardResponse));
				parameters.put("BillData", billDataRaw);
				if(isEbarimt == 1) {
					parameters.put("EbarimtFormat", "2");
				}
				if (request.getUser().getNoCardContinue() == null) {
					parameters.put("NoCardContinue", "true");
				} else {
					parameters.put("NoCardContinue", String.valueOf(request.getUser().getNoCardContinue()));
				}
				parameters.put("Language", String.valueOf(log.getLanguage()));
				if (log.getProduction() == 0) {
					list = getByProcedure("CalculateBonus_DEVELOPING", parameters, CalcBonusResponse.class);
				} else {
					list = getByProcedure("CalculateBonus", parameters, CalcBonusResponse.class);
				}
				CalcBonusResponse result = list.get(0);
				if (list == null || list.size() == 0 || result.getStatus() != 1) {
					response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
					return response;
				}
				response.setStatus(result.getStatus());
				response.setMessage(result.getData());
				response.setState(1);
				log.setInvoiceId(result.getInvoiceId());
				log.setResponse(result.getData());
			} else if (cardResponse != null && "failed".equals(cardResponse.getMessage())) {
				response.setStatus(StatusCode.CARD_INFORMATION_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_INFORMATION_ERROR, log.getLanguage()) + cardResponse.getMessage());
			} else {
				if (MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, log.getLanguage()).equals(cardResponse.getMessage())) {
					response.setStatus(StatusCode.QR_COULDNOT_READ.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, log.getLanguage()));
				} else {
					if (cardResponse != null) {
						response.setStatus(StatusCode.CARD_INFORMATION_ERROR.getValue());
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_INFORMATION_ERROR, log.getLanguage()) + cardResponse.getMessage());
					} else {
						response.setStatus(StatusCode.CARD_INFORMATION_ERROR.getValue());
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_INFORMATION_ERROR, log.getLanguage()));
					}
				}
			}
		} catch (PersistenceException ex) {
			log.setError(ex.getMessage());
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(ex.getMessage());
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	private boolean CheckCalcBonus(CalcPaymentRequest request, LoyaltyBaseResponse response, Logger log) {
		if (StringHelper.isNullOrEmpty(request.getRequestId())) {
			response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
			response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "requestId"));
			return false;
		}

		if (request.getBill() == null) {
			response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
			response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "bill"));
			return false;
		}

		if (request.getBill().getBillDetails() == null || request.getBill().getBillDetails().size() == 0) {
			response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
			response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "bill.billDetail"));
			return false;
		}
		return true;
	}

	// Төхөөрөмжийн мэдээлэл шалгах
	private boolean CheckDevice(CalcPaymentRequest request, LoyaltyBaseResponse response, Logger log) {
		try {
			InetAddress ip;
			ip = InetAddress.getLocalHost();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("DeviceId", request.getDevice().getDeviceId());
			parameters.put("ServerName", ip.getHostName());
			parameters.put("CardNumber", request.getCard().getCardNumber());
			parameters.put("Language", String.valueOf(log.getLanguage()));
			List<CheckDevice> list = getByProcedure("CheckDevice", parameters, CheckDevice.class);

			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DEVICE_INACTIVE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DEVICE_INACTIVE, log.getLanguage()));
				return false;
			}
			CheckDevice result = list.get(0);
			request.getDevice().setOrganizationKey(result.getOrganizationKey());
			request.getDevice().setProduction(result.getProduction());
			request.getDevice().setOrganizationType(result.getOrganizationType());
			request.getDevice().setKey(result.getKey());
			request.getCard().setCardNumber(result.getCardNumber());
			request.getDevice().setPosKey(result.getPosKey());
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
			}
			return false;
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			return false;
		}
		return true;
	}

	private CardInfoResponse SetCardInfoResponse(QrCardInfoProcedureResponse qrResponse) {
		CardInfoResponse response = new CardInfoResponse();
		try {
			response.setCardHolderId(qrResponse.getCardHolderId());
			response.setCardAccountId(qrResponse.getCardAccountId());
			response.setFirstName(qrResponse.getFirstName());
			response.setLastName(qrResponse.getLastName());
			response.setVatCustomerNo(qrResponse.getVatCustomerNo());
//			response.setVatCustomerNoTemp(qrResponse.getVatCustomerNoTemp());
			response.setBonusUse(qrResponse.getBonusUse());
			response.setOpenDate(qrResponse.getOpenDate());
			response.setBonusCancelDayCount(qrResponse.getBonusCancelDayCount());
			response.setIsDvip(qrResponse.getIsDvip());
			response.setUseDvip(qrResponse.getUseDvip());
			response.setCalcBonusPercentPurchaseAmount(qrResponse.getCalcBonusPercentPurchaseAmount());
			response.setTimedPurchaseAmount(qrResponse.getTimedPurchaseAmount());
			response.setBonusPecent(qrResponse.getBonusPecent());
			response.setBonusBalance(qrResponse.getBonusBalance());
			response.setDvipBalance(qrResponse.getDvipBalance());
			response.setDvipSavedBalance(qrResponse.getDvipSavedBalance());
			response.setCoinAllBalance(qrResponse.getCoinAllBalance());
			response.setCoinMonthBalance(qrResponse.getCoinMonthBalance());
			response.setCoinDayBalance(qrResponse.getCoinDayBalance());
			response.setItemBonusBalance(qrResponse.getItemBonusBalance());
			response.setAddItemBonus(qrResponse.getAddItemBonus());
			response.setUseItemBonus(qrResponse.getUseItemBonus());
			response.setItemBonusHeaderId(qrResponse.getItemBonusHeaderId());
			response.setIsBirthDay(qrResponse.getIsBirthDay());
			response.setIsIdPhoneDuplicated(qrResponse.getIsIdPhoneDuplicated());
			response.setIsRequestPhoneUpdate(qrResponse.getIsRequestPhoneUpdate());
			response.setUseUpoint(qrResponse.getUseUpoint());
			response.setFingerPrintData(qrResponse.getFingerPrintData());
			response.setQrResult(qrResponse.getQrResult());
			response.setItemBonusX2Info(qrResponse.getItemBonusX2Info());
			response.setCardNumber(Long.parseLong(qrResponse.getCardHolderId()));
//			response.setAdditionalJson();
			response.setMessage("success");

			response.setUaCardId(qrResponse.getUaCardId());
			response.setUaBalance(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	// Картын мэдээлэл
	@SuppressWarnings("unused")
	private String SetCardInfoJson(CalcPaymentRequest request, CardInfoResponse cardResponse, BigDecimal totalAmount, Boolean isQrData) {
		CardInfo cardInfoData = new CardInfo();
		try {
			cardInfoData.setBillNo(request.getBill().getBillNo());
			if (isQrData) {
				cardInfoData.setCardType("CardNumber");
			} else {
				cardInfoData.setCardType(request.getCard().getCardType());
			}
			cardInfoData.setCardNumber(request.getCard().getCardNumber() != null ? request.getCard().getCardNumber() : "");
			cardInfoData.setCardPercent(BigDecimal.valueOf(cardResponse.getBonusPecent()));
			cardInfoData.setCardHolderId(cardResponse.getCardHolderId() != null ? cardResponse.getCardHolderId() : "");
			cardInfoData.setCardUAId(cardResponse.getUaCardId() != null ? cardResponse.getUaCardId() : "");
			cardInfoData.setPinType(request.getCard().getPinType());
			cardInfoData.setTotalAmount(totalAmount);
			cardInfoData.setVoucher(request.getBill().getVoucher());
			cardInfoData.setBonusBalance(String.valueOf(cardResponse.getBonusBalance()));
			cardInfoData.setDvipBalance(BigDecimal.valueOf(cardResponse.getDvipBalance()));
			cardInfoData.setUseDVip(cardResponse.getUseDvip());
			cardInfoData.setCoinDayBalance(BigDecimal.valueOf(cardResponse.getCoinDayBalance()));
			cardInfoData.setCoinMonthBalance(BigDecimal.valueOf(cardResponse.getCoinMonthBalance()));
			cardInfoData.setUpointBalance(BigDecimal.valueOf(cardResponse.getUaBalance()));
			cardInfoData.setUseUPoint(cardResponse.getUseUpoint());
			cardInfoData.setItemBonusBalance(cardResponse.getItemBonusBalance());
			cardInfoData.setAddItemBonus(cardResponse.getAddItemBonus());
			cardInfoData.setUseItemBonus(cardResponse.getUseItemBonus());
			cardInfoData.setItemBonusX2Info(cardResponse.getItemBonusX2Info());
		} catch (Exception e) {
			return "" + e.getMessage();
		}
		return Json.toJson(cardInfoData);
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public LoyaltyBaseResponse CalcBonusExchange(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		String billDataRaw = "";
		String billInfo = "";
		CalcPaymentRequestExchange request = new CalcPaymentRequestExchange();
		Language language = Language.MN;
		Logger log = new Logger();
		log.setType("CalcBonusExchange");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, CalcPaymentRequestExchange.class);
			billDataRaw = Json.toJson(request.getBillInfo().getBillDetails());
			billInfo = Json.toJson(request.getBill());
			language = request.getDeviceInfo().getLanguage() != null ? Language.valueOf(request.getDeviceInfo().getLanguage()) : language;

			log.setLanguage(language);
			log.setDeviceId(request.getDeviceInfo().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceIdChange());

			if (!GetDevice(response, log)) {
				return response;
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("RequestId", request.getRequestId());
			parameters.put("DeviceId", request.getDeviceInfo().getDeviceId());
			parameters.put("InvoiceIdChange", request.getInvoiceIdChange());
			parameters.put("BillData", billDataRaw);
			parameters.put("BillInfo", "[" + billInfo + "]");
			parameters.put("Language", String.valueOf(language));
			List<CalcBonusResponse> list = null;
			if (log.getProduction() == 0) {
				list = getByProcedure("CalculateBonus_Change_DEVELOPING", parameters, CalcBonusResponse.class);
			} else {
				list = getByProcedure("CalculateBonus_Change", parameters, CalcBonusResponse.class);
			}
			CalcBonusResponse result = list.get(0);

			if (list == null || list.size() == 0 || result.getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, language));
				return response;
			}
			response.setStatus(result.getStatus());
			response.setMessage(result.getData());
			response.setState(1);
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, language), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, language), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, language), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse PutBonus(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		PaymentRequest request = new PaymentRequest();
		String paymentRawData = "";
		TransactionInfoResponse transactionInfoResponse = null;
		ProcessTransactionResponseData processTransactionResponseData = null;
		Logger log = new Logger();
		log.setType("PutBonus");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		List<TransactionInfoResponse> list = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, PaymentRequest.class);
			paymentRawData = Json.toJson(request.getPayments());
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());
			log.setCardType(request.getCardType());

			if (!GetDevice(response, log)) {
				return response;
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("InvoiceId", request.getInvoiceId());
			parameters.put("DeviceId", request.getDevice().getDeviceId());
			parameters.put("CardType", request.getCardType());
			parameters.put("Language", String.valueOf(log.getLanguage()));
			parameters.put("TransactionType", "PutBonus");
			if (log.getProduction() == 0) {
				list = getByProcedure("TransactionInfo_DEVELOPING", parameters, TransactionInfoResponse.class);
			} else {
				list = getByProcedure("TransactionInfo", parameters, TransactionInfoResponse.class);
			}

			transactionInfoResponse = list.get(0);
			request.getDevice().setOrganizationKey(String.valueOf(transactionInfoResponse.getLocationKey()));

			if (list == null || list.size() == 0 || transactionInfoResponse == null || transactionInfoResponse.getBillKey() == null || Long.valueOf(transactionInfoResponse.getBillKey()).compareTo(Long.valueOf(100)) < 0) {
				response.setStatus(StatusCode.BONUS_DEDUCTION_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BONUS_DEDUCTION_ERROR, log.getLanguage()));
				return response;
			}
			if (transactionInfoResponse.getBillKey() != null) {
				log.setBillKey(transactionInfoResponse.getBillKey());
			}

			if (transactionInfoResponse.getPaymentUpoint().intValue() > 0) {
				UPointTransactionRequest requestTransactionRawData = SetTransactionData(transactionInfoResponse, null, log);
				processTransactionResponseData = upointLogic.upointProcessTransaction(requestTransactionRawData, log);

				if (processTransactionResponseData != null && processTransactionResponseData.getResult() == 0) {
					states.add("PUT_UPOINT_SUCCESS");
					response = SaveBonus(request, transactionInfoResponse.getBillKey(), paymentRawData, Json.toJson(processTransactionResponseData), log);
				} else {
					response = SaveBonus(request, (long) 0, paymentRawData, "", log);
				}
			} else {
				response = SaveBonus(request, (long) 0, paymentRawData, "", log);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);

			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}

			if (response.getStatus() != 1 && states.contains("PUT_UPOINT_SUCCESS")) {
				CancelTransactionRequest cancelTransactionRequest = new CancelTransactionRequest();
				cancelTransactionRequest.setLocationKey(transactionInfoResponse.getLocationKey());
				cancelTransactionRequest.setData("{\"bill_number\":\"" + transactionInfoResponse.getBillKey() + "\"}");
				CancelTransactionResponseData cancelTransactionResponse = upointLogic.upointCancelTransaction(cancelTransactionRequest, log);
				if (cancelTransactionResponse == null) {
					System.err.println("*************LOYALTYINTEGRATION******PUTBONUS**cancelTransactionResponse****ERROR***START********");
					System.err.println(JsonHelper.toJson(cancelTransactionResponse));
					System.err.println("*************LOYALTYINTEGRATION******PUTBONUS**cancelTransactionResponse****ERROR***END********");
				}
			}
		}
		return response;
	}

	@SuppressWarnings("unused")
	private UPointTransactionRequest SetUPointTransactionRequest(TransactionInfoResponse transactionInfoResponse) {
		UPointTransactionRequest requestTransaction = new UPointTransactionRequest();
		try {
			UpointTransactionData transactionData = new UpointTransactionData();
			transactionData.setDate(Dates.convertDateToFormatString(new Date(), "YYYY/MM/dd hh:mm:ss"));
			transactionData.setManufacturer(new ArrayList<Manufacturer>());
			transactionData.setItems(new ArrayList<Item>());
			transactionData.setBank(new ArrayList<Bank>());
			transactionData.setCard_number(transactionInfoResponse.getCardUaId());
			transactionData.setBill_number(transactionInfoResponse.getBillKey());
			transactionData.setSpend_amount(transactionInfoResponse.getPaymentUpoint().intValue());
			transactionData.setBonus_amount(transactionInfoResponse.getCalcBonusAmount());
			transactionData.setBonus_point(transactionInfoResponse.getAddUpoint().intValue());
			transactionData.setTotal_amount(BigDecimal.ZERO);
			transactionData.setCash_amount(BigDecimal.ZERO);
			transactionData.setTerminal_id("000000041743952036");

			requestTransaction.setLocationKey(transactionInfoResponse.getLocationKey());
			requestTransaction.setData(Json.toJson(transactionData));
		} catch (Exception e) {
			return null;
		}
		return requestTransaction;
	}

	// Upoint руу явах
	private UPointTransactionRequest SetTransactionData(TransactionInfoResponse transactionInfoResponse, LogRequest request, Logger log) {
		UPointTransactionRequest requestTransaction = new UPointTransactionRequest();
		UpointTransactionData transactionData = new UpointTransactionData();
		try {
			List<Item> uPointGiveAwayItems = JsonHelper.toJsonList(transactionInfoResponse.getuPointGiveAwayData(), Item.class);
			if (uPointGiveAwayItems == null) uPointGiveAwayItems = new ArrayList<>();
			transactionData.setDate(Dates.convertDateToFormatString(new Date(), "YYYY/MM/dd hh:mm:ss"));
			transactionData.setManufacturer(new ArrayList<Manufacturer>());
			transactionData.setItems(uPointGiveAwayItems);
			transactionData.setBank(new ArrayList<Bank>());
			transactionData.setCard_number(transactionInfoResponse.getCardUaId());
			if (request != null && request.getBill() != null && request.getBill().getIdBill() != null)
				transactionData.setBill_number(request.getBill().getIdBill());
			else
				transactionData.setBill_number(transactionInfoResponse.getBillKey());
			transactionData.setSpend_amount(transactionInfoResponse.getPaymentUpoint().intValue());
			transactionData.setBonus_amount(transactionInfoResponse.getCalcBonusAmount());
			transactionData.setBonus_point(transactionInfoResponse.getAddUpoint().intValue());
			transactionData.setTotal_amount(BigDecimal.ZERO);
			transactionData.setCash_amount(BigDecimal.ZERO);
			transactionData.setTerminal_id(request != null && log.getOrganizationKey() != null 
					&& log.getOrganizationKey().equals(2323232L) && transactionInfoResponse.getIsRelated() == 1 ? "14151391451981516" : "000000041743952036");

			String transactionDataRaw = Json.toJson(transactionData);
			requestTransaction.setLocationKey(transactionInfoResponse.getLocationKey());
			requestTransaction.setData(transactionDataRaw);
		} catch (Exception e) {
			return null;
		}
		return requestTransaction;
	}

	@Override
	public LoyaltyBaseResponse PutBonusChange(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		PaymentRequestChange request = null;
		Logger log = new Logger();
		log.setType("PutBonusChange");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, PaymentRequestChange.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());
			log.setCardType(request.getCardType());

			if (!GetDevice(response, log)) {
				return response;
			}
			
			//	ЮПойнт өмнөх гүйлгээг цуцлах, шинэ гүйлгээг бүртгэх
			List<ProcessTransactionResponseData> upointBillDataList = null;
//			if (log.getProduction() == 0) {
				Logger tmpLog = new Logger();
				tmpLog.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
				tmpLog.setDeviceId(request.getDevice().getDeviceId());
				tmpLog.setInvoiceId(request.getInvoiceIdChange());
				tmpLog.setRequestId(request.getRequestId());
				tmpLog.setCardType(request.getCardType());
				if (!GetDevice(response, tmpLog))
					return response;
				upointBillDataList = getUpointBillData(tmpLog);

				if (upointBillDataList != null && upointBillDataList.size() > 0 && upointBillDataList.get(0).getResult() == 0) {
					CancelTransactionRequest cancelTransactionRequest = new CancelTransactionRequest();
					cancelTransactionRequest.setLocationKey(tmpLog.getLocationKey());
					cancelTransactionRequest.setData("{\"bill_number\":\"" + upointBillDataList.get(0).getBillNo() + "\"}");
					CancelTransactionResponseData cancelTransactionResponse = upointLogic.upointCancelTransaction(cancelTransactionRequest, tmpLog);
					
					if ( cancelTransactionResponse == null || (cancelTransactionResponse.getResult() != 0 && cancelTransactionResponse.getResult() != 101)) {
						response.setStatus(StatusCode.SERVER_RESULT.getValue());
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, tmpLog.getLanguage(), " upoint буцаалт амжилтгүй боллоо! "));
						return response;
					}
					
					states.add("RETURN_UPOINT_SUCCESS");
				}
//			}
			
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("InvoiceId", request.getInvoiceIdChange());
			parameters.put("DeviceId", request.getDevice().getDeviceId());
			parameters.put("Language", String.valueOf(log.getLanguage()));
			
	
			List<CalcBonusResponse> list = new ArrayList<CalcBonusResponse>();
			if (log.getProduction() == 0) {
				 list= getByProcedure("PutBonusChange_DEVELOPING", parameters, CalcBonusResponse.class);
			} else {
				 list = getByProcedure("PutBonusChange", parameters, CalcBonusResponse.class);
			}
			
			
			CalcBonusResponse result = list.get(0);

			if (list == null || list.size() == 0 || result.getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return response;
			}

			String resString = "";
			PaymentRequestChange paymentRequest = new PaymentRequestChange();
			paymentRequest.setRequestId(request.getRequestId());
			paymentRequest.setInvoiceId(request.getInvoiceId());
			paymentRequest.setCardType(request.getCardType());

			for (PaymentInfoNext name : request.getPayments()) {
				paymentRequest.getPayments().add(name);
			}
			paymentRequest.getDevice().setDeviceId(request.getDevice().getDeviceId());
			resString = Json.toJson(paymentRequest);

			LoyaltyBaseResponse resp = PutBonus(resString);
			response = new LoyaltyBaseResponse(resp);
			
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	public LoyaltyBaseResponse SaveBonus(PaymentRequest request, Long BillKey, String paymentRawData, String transactionResponse, Logger log) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		PaymentRequestLog saveBonusRequest = new PaymentRequestLog();
		saveBonusRequest.setRequest(request);
		saveBonusRequest.setBillKey(BillKey);
		saveBonusRequest.setPaymentRawData(paymentRawData);
		saveBonusRequest.setTransactionResponse(transactionResponse);
		saveBonusRequest.setInvoiceId(log.getInvoiceId());
		String rawData = "";
		Logger saveLog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		saveLog.setType("SaveBonus");
		saveLog.setRequestDate(new Date());
		saveLog.setBillKey(BillKey);
		List<String> states = new ArrayList<>();
		List<SaveBonusResponse> lstSaveBonus = new ArrayList<>();
		try {
			rawData = Json.toJson(saveBonusRequest);
			saveLog.setRequest(rawData);
//			for (int i = 0; i < request.getVoucherPayments().size(); i++) {
//				request.getVoucherPayments().get(i).setIdLocation(request.getDevice().getOrganizationKey());
//				request.getVoucherPayments().get(i).setXml("");
//				VoucherPaymentResponse voucherPaymentResponse = voucherLoyaltyOne.saveVoucher(request.getVoucherPayments().get(i), "SUSPEND+", saveLog);
//				lstVoucherPaymentResponse.add(voucherPaymentResponse);
//				if (voucherPaymentResponse.getStatus() == 1) {
//					lstVoucher.add(request.getVoucherPayments().get(i));
//				}
//			}
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("InvoiceId", request.getInvoiceId());
			parameters.put("DeviceId", request.getDevice().getDeviceId());
			parameters.put("CardType", request.getCardType());
			parameters.put("PaymentInfo", paymentRawData);
			parameters.put("BillKey", BillKey);
			parameters.put("UPointData", transactionResponse);
//			parameters.put("VoucherPayments", Json.toJson(lstVoucherPaymentResponse));
			parameters.put("Language", String.valueOf(saveLog.getLanguage()));
			if (log.getProduction() == 0) {
				lstSaveBonus = getByProcedure("SaveBonus_DEVELOPING", parameters, SaveBonusResponse.class);
			} else {
				lstSaveBonus = getByProcedure("SaveBonus", parameters, SaveBonusResponse.class);
			}

			SaveBonusResponse result = new SaveBonusResponse(lstSaveBonus.get(0));

			if (lstSaveBonus == null || lstSaveBonus.size() == 0 || result.getStatus() != 1) {
				response.setStatus(StatusCode.BONUS_DEDUCTION_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BONUS_DEDUCTION_ERROR, saveLog.getLanguage()));
				return response;
			}
			response.setMessage(result.getData());
			response.setStatus(result.getStatus());
			response.setState(1);
			saveLog.setResponse(response.getMessage());
		} catch (PersistenceException e) {
			if (e.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) e.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, saveLog.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, saveLog.getLanguage()), e.getMessage()));
				states.add("SET_MESSAGE");
			}
			saveLog.setError(e.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, saveLog.getLanguage()), ex.getMessage()));
			saveLog.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			saveLog.setResponseDate(new Date());
			saveLog.setStatus(response.getStatus().toString());
			saveLog.setResponse(Json.toJson(response));
			logger.log(saveLog);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	@SuppressWarnings("unused")
	@Override
	public LoyaltyBaseResponse ReturnTransaction(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		ReturnTransactionRequest request = new ReturnTransactionRequest();
		TransactionInfoResponse transactionInfoResponse = null;
		ProcessTransactionResponseData processTransactionResponseData = null;
		HashMap<String, Object> parameters = new HashMap<>();
		Logger log = new Logger();
		log.setType("ReturnTransaction");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		List<ProcessTransactionResponseData> upointBillDataList = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, ReturnTransactionRequest.class);
			log.setLanguage(request.getLanguage() != null ? Language.valueOf(request.getLanguage()) : Language.MN);
			log.setDeviceId(request.getDeviceId());
			log.setInvoiceId(request.getInvoiceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			parameters.put("checkInvoiceId", request.getInvoiceId());
			parameters.put("Type", "checkBillStatus");
			parameters.put("Language", String.valueOf(log.getLanguage()));
			execProcedure("GET_INFO", parameters);
			
//			if (log.getProduction() == 0) {
				upointBillDataList = getUpointBillData(log);

				if (upointBillDataList != null && upointBillDataList.size() > 0 && upointBillDataList.get(0).getResult() == 0) {
					CancelTransactionRequest cancelTransactionRequest = new CancelTransactionRequest();
					cancelTransactionRequest.setLocationKey(log.getLocationKey());
					cancelTransactionRequest.setData("{\"bill_number\":\"" + upointBillDataList.get(0).getBillNo() + "\"}");
					CancelTransactionResponseData cancelTransactionResponse = upointLogic.upointCancelTransaction(cancelTransactionRequest, log);
					if (cancelTransactionResponse == null || cancelTransactionResponse.getResult() != 0) {
						response.setStatus(StatusCode.SERVER_RESULT.getValue());
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage(), " upoint буцаалт амжилтгүй боллоо! "));
						return response;
					}
					states.add("RETURN_UPOINT_SUCCESS");
				}
//			}

			parameters = new HashMap<>();
			parameters.put("InvoiceId", request.getInvoiceId());
			parameters.put("DeviceId", request.getDeviceId());
			parameters.put("Language", String.valueOf(log.getLanguage()));
			List<CalcBonusResponse> list = null;
			if (log.getProduction() == 0)
				list = getByProcedure("ReturnTransaction_DEVELOPING", parameters, CalcBonusResponse.class);
			else
				list = getByProcedure("ReturnTransaction", parameters, CalcBonusResponse.class);

			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return response;
			}
			CalcBonusResponse result = list.get(0);
			response.setMessage(result.getMessage());
			response.setStatus(result.getStatus());
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}

			if (log.getProduction() == 0 && response.getStatus() != 1 && states.contains("RETURN_UPOINT_SUCCESS")) {
				logger.actionInvoiceBill("updateInvoiceBill", log.getInvoiceId(), log.getDeviceId(), "RETURN_UPOINT_SUCCESS");
			}

			if (states.contains("RETURN_UPOINT_SUCCESS")) {
				logger.actionUpointBillData("actionUpointBillData", log, upointBillDataList.get(0).getBillNo().toString(), "RETURN_UPOINT_SUCCESS");
			}
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse PutTransaction(String rawData, int isBonus) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		String requestRawData = "";
		log.setType("PutTransaction=" + isBonus);
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		HashMap<String, Object> parameters = new HashMap<>();
		CalcBonusResponse ebarimtRequest = new CalcBonusResponse();
		PutResponse ebarimtResponse = new PutResponse();
		TransactionInfoResponse transactionInfoResponse = null;
		ProcessTransactionResponseData processTransactionResponseData = null;
		List<InvoiceBill> invoiceBill = new ArrayList<>();
		List<CalcBonusResponse> list = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			requestRawData = rawData;
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setInvoiceId(request.getInvoiceId());

			if (!GetDevice(response, log)) {
				return response;
			}
			
			invoiceBill = getInvoiceBillList(log);
			if (isBonus == 1 && (invoiceBill == null || invoiceBill.size() <= 0)) {
				response.setStatus(StatusCode.BILL_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BILL_ERROR, log.getLanguage()));
				return response;
			}

			if (isBonus == 1) {
				parameters = new HashMap<>();
				parameters.put("InvoiceId", request.getInvoiceId());
				parameters.put("DeviceId", request.getDevice().getDeviceId());
				parameters.put("CardType", invoiceBill.get(0).getCardType());
				parameters.put("TransactionType", "PutTransaction");
				parameters.put("Language", String.valueOf(log.getLanguage()));
				List<TransactionInfoResponse> listTransactionInfoResponse = null;
				if (log.getProduction() == 0)
					listTransactionInfoResponse = getByProcedure("TransactionInfo_DEVELOPING", parameters, TransactionInfoResponse.class);
				else
					listTransactionInfoResponse = getByProcedure("TransactionInfo", parameters, TransactionInfoResponse.class);

				if (listTransactionInfoResponse == null || listTransactionInfoResponse.size() == 0) {
					response.setStatus(StatusCode.BONUS_DEDUCTION_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.BONUS_DEDUCTION_ERROR, log.getLanguage()));
					return response;
				}
				
				transactionInfoResponse = listTransactionInfoResponse.get(0);
				
				if (transactionInfoResponse == null || transactionInfoResponse.getBillKey() == null
						|| Long.valueOf(transactionInfoResponse.getBillKey()).compareTo(Long.valueOf(1)) < 0) {
					response.setStatus(StatusCode.BONUS_DEDUCTION_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.BONUS_DEDUCTION_ERROR, log.getLanguage()));
					return response;
				}
				
				request.getDevice().setOrganizationKey(String.valueOf(transactionInfoResponse.getLocationKey()));

				if (transactionInfoResponse.getAddUpoint().intValue() > 0 ||
					(!StringHelper.isNullOrEmpty(transactionInfoResponse.getuPointGiveAwayData()) && transactionInfoResponse.getuPointGiveAwayData().length() > 2)) {
					UPointTransactionRequest requestTransactionRawData = SetTransactionData(transactionInfoResponse, request, log);
					processTransactionResponseData = upointLogic.upointProcessTransaction(requestTransactionRawData, log);

					if (processTransactionResponseData == null || processTransactionResponseData.getResult() != 0) {
//						response.setStatus(StatusCode.UPOINT_ERROR.getValue());
//						response.setMessage(MessageHelper.getStatusMessage(StatusCode.UPOINT_ERROR, log.getLanguage()));
//						return response;
						processTransactionResponseData = null;
					}else {
						states.add("PUT_UPOINT_SUCCESS");
					}
				}
			}

			if (request.isSendEbarimt()) {
				parameters = new HashMap<>();
				parameters.put("type", "getEbarimtInformation");
				parameters.put("DeviceId", log.getDeviceId());
				parameters.put("checkInvoiceId", log.getInvoiceId());
				List<CalcBonusResponse> listCheckEbarimt = getByProcedure("GET_INFO", parameters, CalcBonusResponse.class);
				ebarimtRequest = listCheckEbarimt.get(0);

				if (listCheckEbarimt.get(0).getStatus() != 1 || ebarimtRequest == null) {
					response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR, log.getLanguage()));
					return response;
				}
				ebarimtResponse = service.putEbarimt(Json.toJsonObject(ebarimtRequest.getData(), EbarimtRequest.class), log);

				if (ebarimtResponse == null) {
					response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR, log.getLanguage()));
					return response;
				} else if (!"true".equals(ebarimtResponse.getSuccess())) {
					response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR, log.getLanguage(), ebarimtResponse.getMessage()));
					return response;
				}
				states.add("EBARIMT_SUCCESS");

				if (!SetBill(response, log, request, ebarimtResponse, ebarimtRequest)) {
					return response;
				}
				requestRawData = JsonHelper.toJson(request).toString();
			}

			parameters = new HashMap<>();
			parameters.put("Json", requestRawData);
			parameters.put("IsBonus", isBonus);
			parameters.put("Language", String.valueOf(log.getLanguage()));
			if (processTransactionResponseData != null)
				parameters.put("UPointData", Json.toJson(processTransactionResponseData));
			if (log.getProduction() == 0) {
				list = getByProcedure("PutTransaction_DEVELOPING", parameters, CalcBonusResponse.class);
			} else {
				list = getByProcedure("PutTransaction", parameters, CalcBonusResponse.class);
			}

			response.setMessage(list.get(0).getMessage());
			response.setStatus(list.get(0).getStatus());

			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return response;
			}

			response.setState(1);
			response.setMessage(JsonHelper.toJson(list.get(0)));
			response.setState(StatusCode.SUCCESS.getValue());
			states.add("BILL_SUCCESS");
			if (list.get(0).getBillKey() != null) {
				log.setBillKey(Long.parseLong(list.get(0).getBillKey()));
				log.setInvoiceId(list.get(0).getInvoiceId());
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}

			if (!states.contains("BILL_SUCCESS") && states.contains("EBARIMT_SUCCESS")) {
				nomin.loyaltyintegration.entity.bill.ReturnBillRequest returnBillRequest = new nomin.loyaltyintegration.entity.bill.ReturnBillRequest();
				nomin.loyaltyintegration.entity.bill.ReturnBillResponse returnBillResponse = new nomin.loyaltyintegration.entity.bill.ReturnBillResponse();
				try {
					returnBillRequest.setReturnBillId(ebarimtResponse.getBillId());
					returnBillRequest.setBillDate(new Date());
					returnBillResponse = service.returnEbarimt(returnBillRequest, log);
					if (!returnBillResponse.isSuccess()) {
						System.err.println("*************LOYALTYINTEGRATION******PutTransaction**ReturnBillResponse****ERROR***START***WRONG***RESULT******");
						System.err.println(JsonHelper.toJson(returnBillResponse));
						System.err.println("*************LOYALTYINTEGRATION******PutTransaction**ReturnBillResponse****ERROR***END***WRONG***RESULT*****");
					}
				} catch (Exception ex) {
					System.err.println("*************LOYALTYINTEGRATION******PutTransaction**ReturnBillResponse****ERROR***START********");
					ex.printStackTrace();
					System.err.println("*************LOYALTYINTEGRATION******PutTransaction**ReturnBillResponse****ERROR***END********");
				}
			}

			if (log.getProduction() == 1 && !states.contains("BILL_SUCCESS") && states.contains("PUT_UPOINT_SUCCESS")) {
				CancelTransactionRequest cancelTransactionRequest = new CancelTransactionRequest();
				cancelTransactionRequest.setLocationKey(transactionInfoResponse.getLocationKey());
				Long billKey = (request != null && request.getBill() != null && request.getBill().getIdBill() != null) ? request.getBill().getIdBill() : transactionInfoResponse.getBillKey();
				cancelTransactionRequest.setData("{\"bill_number\":\"" + billKey + "\"}");
				CancelTransactionResponseData cancelTransactionResponse = upointLogic.upointCancelTransaction(cancelTransactionRequest, log);
				if (cancelTransactionResponse == null || cancelTransactionResponse.getResult() != 0) {
					System.err.println("*************LOYALTYINTEGRATION******PutTransaction**cancelTransactionResponse****ERROR***START********");
					System.err.println(JsonHelper.toJson(cancelTransactionResponse));
					System.err.println("*************LOYALTYINTEGRATION******PutTransaction**cancelTransactionResponse****ERROR***END********");
				}
			}
		}
		return response;
	}

	/**
	 * mobile Ашиглахаа болисон
	 * 
	 * @Override public LoyaltyBaseResponse PutTransactionMobile(String rawData)
	 *           throws Exception { LoyaltyBaseResponse response = new
	 *           LoyaltyBaseResponse(); Language language =
	 *           Language.MN; @SuppressWarnings("unused") SaveMobileRequest
	 *           mobileRequest = null; EbarimtRequest ebarimtRequest;
	 *           ReturnBillRequest returnBillRequest = new ReturnBillRequest();
	 *           PutResponse putGroup = null; ReturnBillResponse returnBillResponse
	 *           = null; List<String> states = new ArrayList<>(); Logger log = new
	 *           Logger(); try { mobileRequest = JsonHelper.toJsonObject(rawData,
	 *           SaveMobileRequest.class); ebarimtRequest =
	 *           JsonHelper.toJsonObject(rawData, EbarimtRequest.class); putGroup =
	 *           service.putEbarimt(ebarimtRequest, log); if (putGroup != null &&
	 *           putGroup.isSuccess()) { states.add("PUT_SUCCESS"); String
	 *           responseData = Json.toJson(putGroup);
	 * 
	 *           HashMap<String, Object> parameters = new HashMap<>();
	 *           parameters.put("Json", responseData); parameters.put("IsMobile",
	 *           1); // parameters.put("invoiceId", invoiceId);
	 *           parameters.put("Language", String.valueOf(language));
	 *           List<CalcBonusResponse> list =
	 *           getByProcedure("EbarimtToPutTransaction_NOW", parameters,
	 *           CalcBonusResponse.class);
	 * 
	 *           response.setMessage(list.get(0).getData());
	 *           response.setStatus(list.get(0).getStatus());
	 * 
	 *           putGroup.setLottery(""); putGroup.setQrData(""); for (int i = 0; i
	 *           < putGroup.getLstGroup().size(); i++) {
	 *           putGroup.getLstGroup().get(i).setLottery("");
	 *           putGroup.getLstGroup().get(i).setQrData(""); }
	 * 
	 *           if (list == null || list.size() == 0 || list.get(0).getStatus() !=
	 *           1) {
	 *           response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
	 *           response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE,
	 *           language)); return response; } states.add("BILL_SUCCESS");
	 *           logger.InvoiceLog("Java", Json.toJson(putGroup), 1, "JMobile", "",
	 *           JsonHelper.modifyJson(Json.toJson(putGroup)), "BILL_SUCCESS", "");
	 *           } else { response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
	 *           response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR,
	 *           language, putGroup.getMessage())); } } catch (PersistenceException
	 *           ex) { if (ex.getCause() instanceof DatabaseException) {
	 *           DatabaseException dbException = (DatabaseException) ex.getCause();
	 *           SQLException sqlException = (SQLException) dbException.getCause();
	 *           String errorMessage = sqlException.getMessage(); if
	 *           (sqlException.getErrorCode() == 50000) { String[] allMessage =
	 *           errorMessage.split(";"); if (allMessage.length > 1) { String
	 *           statusCode = allMessage[0]; String message = allMessage[1];
	 *           response.setStatus(Integer.parseInt(statusCode));
	 *           response.setMessage(message); } else {
	 *           response.setStatus(StatusCode.DATABASE_ERROR.getValue());
	 *           response.setMessage(errorMessage); } } else {
	 *           response.setStatus(StatusCode.DATABASE_ERROR.getValue());
	 *           response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR,
	 *           language), errorMessage)); } } else {
	 *           response.setStatus(StatusCode.DATABASE_ERROR.getValue());
	 *           response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR,
	 *           language), ex.getMessage())); } } catch (Exception ex) {
	 *           response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
	 *           response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION,
	 *           language), ex.getMessage())); } finally { if
	 *           (!states.contains("BILL_SUCCESS") &&
	 *           states.contains("PUT_SUCCESS")) { String error = null; try {
	 *           returnBillRequest.setReturnBillId(putGroup.getBillId());
	 *           returnBillRequest.setBillDate(new Date()); returnBillResponse =
	 *           service.ProcessReturnTransactionMobile(returnBillRequest); if
	 *           (returnBillResponse.isSuccess()) { states.add("RETURN_SUCCESS"); }
	 *           else { error = returnBillResponse.getMessage(); } } catch
	 *           (Exception ex) { error = ex.getMessage(); } finally { if
	 *           (states.contains("RETURN_SUCCESS")) { logger.InvoiceLog("Java",
	 *           Json.toJson(putGroup), -1, "JMobile", "",
	 *           Json.toJson(returnBillResponse), "RETURN_SUCCESS", ""); } else {
	 *           logger.InvoiceLog("Java", Json.toJson(putGroup), -2, "JMobile", "",
	 *           Json.toJson(returnBillResponse), "RETURN_ERROR", error); } } } }
	 *           return response; }
	 **/

	@Override
	public GenericResponse ReadQR(String rawData) throws Exception {
		GenericResponse result = new GenericResponse();
		QrReadResponse response = new QrReadResponse();
		ObjectMapper objectMapper = new ObjectMapper();
		result.setStatusCode(200);
		Logger log = new Logger();
		log.setType("ReadQr");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		try {
			if (rawData == null || rawData.isEmpty()) {
				response.setStatus(101);
				response.setMessage("Body талбарт утга дамжуулах шаардлагатай!");
			} else {
				QrReadRequest request = objectMapper.readValue(rawData, QrReadRequest.class);
				if (Strings.isNullOrEmpty(request.getDevice().getDeviceId()) || Strings.isNullOrEmpty(request.getQrData()) || Strings.isNullOrEmpty(request.getCardNumber())) {
					response.setStatus(101);
					response.setMessage("Body талбарт параметер дамжуулаагүй байна!");
				} else {
					HashMap<String, Object> parameters = new HashMap<>();
					parameters.put("QrData", request.getQrData());
					parameters.put("CardNumber", request.getCardNumber());
					parameters.put("DeviceId", request.getDevice().getDeviceId());

					execProcedure("READ_QR", parameters);

					response.setStatus(StatusCode.SUCCESS.getValue());
					response.setQrData(request.getQrData());
					response.setCardNumber(request.getCardNumber());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
				}
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();

				response.setStatus(200);
				if (dbException.getInternalException() != null) {
					response.setMessage(dbException.getInternalException().getMessage());
					if (response.getMessage() != null) {
						response.setStatus(201);
					}
				} else
					response.setMessage(dbException.getMessage());
			} else {
				result.setStatusCode(500);
				response.setStatus(500);
				response.setMessage(ex.getMessage());
			}
		} catch (Exception e) {
			result.setStatusCode(400);
			response.setStatus(401);
			response.setMessage("Алдаа гарлаа.");
		}

		try {
			String responseData = objectMapper.writeValueAsString(response);
			result.setResultData(responseData);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public GenericResponse GetQR(String rawData) throws Exception {
		GenericResponse result = new GenericResponse();
		QrResponse response = new QrResponse();
		ObjectMapper objectMapper = JsonHelper.objectMapperStatic;
		result.setStatusCode(200);
		try {
			if (rawData == null || rawData.isEmpty()) {
				response.setStatus(101);
				response.setMessage("Body талбарт утга дамжуулах шаардлагатай!");
			} else {
				QrRequest request = objectMapper.readValue(rawData, QrRequest.class);
				if (Strings.isNullOrEmpty(request.getDevice().getDeviceId())) {
					response.setStatus(0);
					response.setMessage("Body талбарт deviceId утгыг дамжуулаагүй байна!");
				} else {
					Long newKey = Tools.createNewPKey();
					String strKey = Long.toString(newKey);
					StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("GET_QR");
					storedProcedure.registerStoredProcedureParameter("DeviceId", String.class, ParameterMode.IN);
					storedProcedure.registerStoredProcedureParameter("PosCode", Integer.class, ParameterMode.OUT);
					storedProcedure.setParameter("DeviceId", request.getDevice().getDeviceId());
					storedProcedure.execute();

					String posCode = Strings.createString(storedProcedure.getOutputParameterValue("PosCode"));

					response.setStatus(StatusCode.SUCCESS.getValue());
					response.setQrData(strKey.substring(0, 14) + posCode);
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
					response.setQrDataImageBase64(getQRCodeImage(response.getQrData(), 600, 600));
					response.setQrDataImageExt("png");
				}
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				response.setStatus(100);
				if (dbException.getInternalException() != null) {
					response.setMessage(dbException.getInternalException().getMessage());
					if (response.getMessage() != null) {
						response.setStatus(201);
					}
				} else
					response.setMessage(dbException.getMessage());
			} else {
				result.setStatusCode(500);
				response.setStatus(500);
				response.setMessage(ex.getMessage());
			}
		} catch (Exception e) {
			result.setStatusCode(400);
			response.setStatus(401);
			response.setMessage("Алдаа гарлаа.");
		}

		try {
			String responseData = objectMapper.writeValueAsString(response);
			result.setResultData(responseData);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public GenericResponse CheckQR(String rawData) throws Exception {
		GenericResponse result = new GenericResponse();
		QrCheckResponse response = new QrCheckResponse();
		result.setStatusCode(200);
		try {
			if (rawData == null || rawData.isEmpty()) {
				response.setStatus(101);
				response.setMessage("Body талбарт утга дамжуулах шаардлагатай!");
			} else {
				QrReadRequest request = JsonHelper.toJsonObject(rawData, QrReadRequest.class);

				if (Strings.isNullOrEmpty(request.getDevice().getDeviceId()) || Strings.isNullOrEmpty(request.getQrData())) {
					response.setStatus(101);
					response.setMessage("Body талбарт параметер дамжуулаагүй байна!");
				} else {
					HashMap<String, Object> parameters = new HashMap<>();
					parameters.put("QrData", request.getQrData());
					parameters.put("Type", "TakeCharger");
					parameters.put("DeviceId", request.getDevice().getDeviceId());
					parameters.put("Language", request.getDevice().getLanguage() != null ? request.getDevice().getLanguage().toString() : "MN");

					List<QrCardInfoProcedureResponse> list = getByProcedure("CHECK_QR", parameters, QrCardInfoProcedureResponse.class);
					QrCardInfoProcedureResponse cardInfo = list.get(0);

					if (list == null || list.size() == 0 || !"SUCCESS".equals(cardInfo.getQrResult())) {
						response.setStatus(StatusCode.QR_COULDNOT_READ.getValue());
						response.setMessage(MessageHelper.getStatusMessage(StatusCode.QR_COULDNOT_READ, Language.MN));
						String responseData = JsonHelper.toJson(response);
						result.setResultData(responseData);
						return result;
					}

					response.setEmail(cardInfo.getEmail());
					response.setPhone(cardInfo.getHolderByPhone());
					response.setRegister(cardInfo.getRegister());
					response.setNominCard(cardInfo.getCardHolderId());
					response.setUpointCard(cardInfo.getUaCardId());
					response.setFirstName(cardInfo.getFirstName());
					response.setLastName(cardInfo.getLastName());
					response.setVatCustomerNumber(cardInfo.getVatCustomerNo());
					response.setCardPercent(cardInfo.getBonusPecent());
					response.setStatus(StatusCode.SUCCESS.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, Language.MN));
				}
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();

				response.setStatus(200);
				if (dbException.getInternalException() != null) {
					response.setMessage(dbException.getInternalException().getMessage());
					if (response.getMessage() != null) {
						response.setStatus(201);
					}
				} else
					response.setMessage(dbException.getMessage());
			} else {
				result.setStatusCode(500);
				response.setStatus(500);
				response.setMessage(ex.getMessage());
			}
		} catch (Exception e) {
			result.setStatusCode(400);
			response.setStatus(401);
			response.setMessage("Алдаа гарлаа.");
		} finally {
			try {
				String responseData = JsonHelper.toJson(response);
				result.setResultData(responseData);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public LoyaltyBaseResponse CheckVoucher(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		Response result = new Response();
		LogRequest request = new LogRequest();
		List<VoucherResponse> lstVoucherPaymentL2Response = new ArrayList<>();
		List<VoucherPaymentResponse> lstVoucherPaymentResponse = new ArrayList<>();
		List<String> states = new ArrayList<>();
		Logger log = new Logger();
		log.setType("CheckVoucher");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if ("Electric".equals(log.getOrganizationType())) { //"Electric".equals(log.getOrganizationType())
				for (int i = 0; i < request.getVoucherPaymentsL2().size(); i++) {
					VoucherCheckRequest vRequest = new VoucherCheckRequest();
					vRequest.setVoucherId(request.getVoucherPaymentsL2().get(i).getVoucherId());
					vRequest.setLocationKey(log.getLocationKeyL2());
					vRequest.setPosKey(log.getPosKey());
					vRequest.setCashierKey(log.getCashierKey());

					VoucherResponse voucherCheckResponse = service.CheckVoucherLoyalty2(vRequest, log);
					lstVoucherPaymentL2Response.add(voucherCheckResponse);
					if (voucherCheckResponse.getStatus() == 0) {
						result.setTotalVoucherAmount(result.getTotalVoucherAmount().add(voucherCheckResponse.getBalance()));
						states.add("CHECK_SUCCESS");
					}
				}
				if (states.contains("CHECK_SUCCESS")) {
					result.setStatus(StatusCode.SUCCESS.getValue());
//					result.setData(JsonHelper.toJson(lstVoucherPaymentL2Response));// TODO устгаад документ засаж өгөх
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					result.setLstVoucherPaymentL2Response(lstVoucherPaymentL2Response);
					response.setMessage(JsonHelper.toJson(result));
					response.setStatus(1);
				} else {
					result.setStatus(StatusCode.FAIL.getValue());
//					result.setData(JsonHelper.toJson(lstVoucherPaymentL2Response));// TODO устгаад документ засаж өгөх
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
					result.setLstVoucherPaymentL2Response(lstVoucherPaymentL2Response);
					response.setMessage(JsonHelper.toJson(result));
					response.setStatus(1);
				}
			} else if ("Electric".equals(log.getOrganizationType())) {// "Eshop".equals(log.getOrganizationType())
				for (int i = 0; i < request.getVoucherPayments().size(); i++) {
					VoucherPaymentResponse voucherCheckResponse = voucherLoyaltyOne.actionVoucher(request.getVoucherPayments().get(i), "CHECKVOUCHER", log);
					lstVoucherPaymentResponse.add(voucherCheckResponse);
					if (voucherCheckResponse.getStatus() == 1) {
						result.setTotalVoucherAmount(result.getTotalVoucherAmount().add(new BigDecimal(voucherCheckResponse.getVoucherAmount())));
						states.add("CHECK_SUCCESS");
					}
				}
				result.setVoucherList(lstVoucherPaymentResponse);
				if (states.contains("CHECK_SUCCESS")) {
					result.setStatus(StatusCode.SUCCESS.getValue());
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setStatus(1);
				} else {
					result.setStatus(StatusCode.FAIL.getValue());
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setStatus(1);
				}
			} else {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.NOT_ALLOWED, log.getLanguage()));
				response.setStatus(StatusCode.NOT_ALLOWED.getValue());
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public LoyaltyBaseResponse PutVoucher(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		CalcBonusResponse result = new CalcBonusResponse();
		QrCardInfoProcedureResponse cardInfo = null;
		List<VoucherResponse> lstCheckResult = new ArrayList<>();
		VoucherTransactionRequest putRequest = new VoucherTransactionRequest();
		VoucherResponse putResult = new VoucherResponse();
		List<VoucherDetails> lstPutRequestDetails = new ArrayList<>();
		List<CalcBonusResponse> saveBill = null;
		Logger log = new Logger();
		log.setType("PutVoucher");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		HashMap<String, Object> parameters = new HashMap<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId() != null ? request.getDevice().getDeviceId() : "");
			log.setRequestId(request.getRequestId() != null ? request.getRequestId() : "");
			log.setPhoneNumber(request.getUser().getPhoneNumber() != null ? request.getUser().getPhoneNumber() : "");
			log.setEmail(request.getUser().getEmail() != null ? request.getUser().getEmail() : "");
			log.setCardNumber(request.getUser().getCardholder() != null ? request.getUser().getCardholder() : "");
			log.setQrData(request.getQrData() != null ? request.getQrData() : "");

			if ((request == null || request.getRequestId() == null || StringHelper.isNullOrEmpty(request.getDevice().getDeviceId()))) {
				response.setStatus(StatusCode.BODY_EMPTY.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BODY_EMPTY, log.getLanguage()));
				return response;
			}

			// Ямар хэрэглэгч энэ воучирийг түгжиж байгааг тэмдэглэв.
			if ((StringHelper.isNullOrEmpty(request.getUser().getEmail())) && (StringHelper.isNullOrEmpty(request.getUser().getCardholder())) && (StringHelper.isNullOrEmpty(request.getUser().getPhoneNumber()))) {
				response.setStatus(StatusCode.USER_INFO.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.USER_INFO, log.getLanguage()));
				return response;
			}

			if (!GetDevice(response, log)) {
				return response;
			}

			if (!CreateBillKey(response, log)) {
				return response;
			}

			if (request.getQrData() != null) {
				parameters = new HashMap<>();
				parameters.put("QrData", request.getQrData());
				parameters.put("Type", "CheckReference");
				parameters.put("DeviceId", request.getDevice().getDeviceId());
				parameters.put("Language", String.valueOf(log.getLanguage()));

				List<QrCardInfoProcedureResponse> list = getByProcedure("CHECK_QR", parameters, QrCardInfoProcedureResponse.class);
				cardInfo = list.get(0);
			} else if ("false".equals(request.getUser().getNoCardContinue()) && StringHelper.isNullOrEmpty(request.getUser().getCardholder())) {
				LogRequest logRequestCardInfo = (LogRequest) nomin.loyaltyintegration.tools.Tools.deepClone(request);
				CardInfoRequest cardInfoRequest = new CardInfoRequest();
				if (request.getUser().getCardholder() != null) {
					cardInfoRequest.setCardNumber(request.getUser().getCardholder());
					cardInfoRequest.setCardType("CardNumber");
				} else {
					cardInfoRequest.setCardNumber(request.getUser().getPhoneNumber());
					cardInfoRequest.setCardType("Phone");
				}
				logRequestCardInfo.setCard(cardInfoRequest);
				LoyaltyBaseResponse getCardInfoResponse = GetCardInfo(rawData);

				if (StatusCode.SUCCESS.getValue() == getCardInfoResponse.getStatus()) {
					cardInfo = JsonHelper.toJsonObject(getCardInfoResponse.getMessage(), QrCardInfoProcedureResponse.class);
				}
			}

			if ("false".equals(request.getUser().getNoCardContinue())) {
				if (cardInfo == null || !"SUCCESS".equals(cardInfo.getQrResult())) {
					result.setStatus(StatusCode.CARD_NOT_FOUND.getValue());
					result.setData(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}
			}

			if ("Electric".equals(log.getOrganizationType())) {
				for (int i = 0; i < request.getVoucherPaymentsL2().size(); i++) {
					VoucherCheckRequest checkRequest = new VoucherCheckRequest();
					checkRequest.setVoucherId(request.getVoucherPaymentsL2().get(i).getVoucherId());
					checkRequest.setLocationKey(log.getLocationKeyL2());
					checkRequest.setPosKey(log.getPosKey());
					checkRequest.setCashierKey(log.getCashierKey());

					VoucherResponse voucherCheckResponse = service.CheckVoucherLoyalty2(checkRequest, log);
					lstCheckResult.add(voucherCheckResponse);
					if (voucherCheckResponse.getStatus() == 0) {
						VoucherDetails details = new VoucherDetails();
						details.setVoucherId(voucherCheckResponse.getVoucherId());
						details.setAmount(voucherCheckResponse.getBalance());
						lstPutRequestDetails.add(details);
						states.add("CHECK_SUCCESS");
					}
				}

				if ((lstPutRequestDetails.size() <= 0 || lstPutRequestDetails == null || !states.contains("CHECK_SUCCESS"))) {
					result.setStatus(StatusCode.VOUCHER_NOT_FOUND.getValue());
					result.setData(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.VOUCHER_NOT_FOUND, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}

				BigDecimal checkAmount = lstCheckResult.stream().map(i -> i.getBalance()).reduce(BigDecimal.ZERO, BigDecimal::add);

				if (checkAmount.compareTo(BigDecimal.ZERO) == 0 || checkAmount.compareTo(BigDecimal.ZERO) == -1) {
					result.setStatus(StatusCode.VOUCHER_SUM.getValue());
					result.setData(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.VOUCHER_SUM, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}

				// PutVoucher service
				putRequest.setLocationKey(log.getLocationKeyL2());
				putRequest.setPosKey(log.getPosKey());
				putRequest.setCashierKey(log.getCashierKey());
				putRequest.setBillKey(log.getBillKey());
				putRequest.setSystemId("Integration");
				putRequest.setPhoneNumber(log.getPhoneNumber());
				putRequest.setEmail(log.getEmail());
				putRequest.setSuspend(1); // Түр ашиглаж байгаа
				putRequest.setDetails(lstPutRequestDetails);

				putResult = service.PutVoucherLoyalty2(putRequest, log);
				if (putResult.getStatus() == 0) {
					states.add("PUT_VOUCHER_SUCCESS");
					log.setInvoiceId(nomin.loyaltyintegration.tools.Tools.getUUID());
					result.setInvoiceId(log.getInvoiceId());
					result.setTotalVoucherAmount(checkAmount);
				} else {
					result.setStatus(StatusCode.PUT_VOUCHER_ERROR.getValue());
					result.setData(JsonHelper.toJson(putResult));
					result.setCheckInfo(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.PUT_VOUCHER_ERROR, log.getLanguage(), putResult.getMessage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}

				for (int i = 0; i < lstPutRequestDetails.size(); i++) {
					for (int j = 0; j < putResult.getDetails().size(); j++) {
						if (lstPutRequestDetails.get(i).getVoucherId().equals(putResult.getDetails().get(j).getVoucherId())) {
							putResult.getDetails().get(j).setBalance(lstPutRequestDetails.get(i).getAmount());
						}
					}
				}

				// SaveVoucher
				parameters = new HashMap<>();
				parameters.put("type", "PutVoucher");
				parameters.put("invoiceId", log.getInvoiceId());
				parameters.put("data", rawData);
				parameters.put("voucherResult", JsonHelper.toJson(putResult));
				parameters.put("language", String.valueOf(log.getLanguage()));
				parameters.put("voucherType", 0);
				
				if (log.getProduction() == 0) {
					saveBill = getByProcedure("VoucherTransaction_DEVELOPING", parameters, CalcBonusResponse.class);
				} else {
					saveBill = getByProcedure("VoucherTransaction", parameters, CalcBonusResponse.class);
				}				

				if (saveBill == null || saveBill.size() == 0 || saveBill.get(0).getStatus() != 1) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SAVE_VOUCHER_ERROR, log.getLanguage(), "fix: " + log.getInvoiceId()));
					response.setStatus(StatusCode.SAVE_VOUCHER_ERROR.getValue());
					return response;
				}
				states.add("SAVE_BILL_SUCCESS");
				result.setStatus(StatusCode.SUCCESS.getValue());
				result.setInvoiceId(log.getInvoiceId());
				putResult.setStatus(null);
				putResult.setMessage(null);
				result.setVoucherResult(putResult);
				response.setMessage(JsonHelper.toJson(result));
				response.setState(1);
				response.setStatus(1);
			} else {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.NOT_ALLOWED, log.getLanguage()));
				response.setStatus(StatusCode.NOT_ALLOWED.getValue());
				return response;
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			states.add("SET_MESSAGE");
			log.setError(ex.getMessage());
		} finally {
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			log.setStatus(String.valueOf(result.getStatus()));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
			if ("Electric".equals(log.getOrganizationType()) && states.contains("PUT_VOUCHER_SUCCESS") && !states.contains("SAVE_BILL_SUCCESS")) {
				Logger failLogger = new Logger(log);
				failLogger.setType("PutVoucher-Fix");
				failLogger.setRequest(rawData);
				failLogger.setRequestDate(new Date());
				VoucherTransactionRequest vRequest = new VoucherTransactionRequest();
				try {
					// ReturnVoucher service
					vRequest.setLocationKey(log.getLocationKeyL2());
					vRequest.setPosKey(log.getPosKey());
					vRequest.setCashierKey(log.getCashierKey());
					vRequest.setBillKey(log.getBillKey());
					vRequest.setDetails(lstPutRequestDetails);
					failLogger.setRequest(Json.toJson(vRequest));
					VoucherResponse voucherCheckResponse = service.ReturnVoucherLoyalty2(vRequest, log);
					failLogger.setResponse(Json.toJson(voucherCheckResponse));
					if (voucherCheckResponse.getStatus() == 0) {
						voucherCheckResponse.setStatus(StatusCode.SUCCESS.getValue());
					} else {
						voucherCheckResponse.setStatus(StatusCode.FAIL.getValue());
					}
				} catch (Exception e) {
					failLogger.setStatus("RETURN_VOUCHER_FAIL");
					failLogger.setError(e.getMessage());
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_START______________");
					e.printStackTrace();
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_JSON______________");
					System.err.println(Json.toJson(vRequest));
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_START______________");
				} finally {
					failLogger.setRequestDate(new Date());
					logger.log(failLogger);
				}
			}
		}
		return response;
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public LoyaltyBaseResponse PutDirectVoucher(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		CalcBonusResponse result = new CalcBonusResponse();
		List<VoucherResponse> lstCheckResult = new ArrayList<>();
		VoucherTransactionRequest putRequest = new VoucherTransactionRequest();
		VoucherResponse putResult = new VoucherResponse();
		List<VoucherDetails> lstPutRequestDetails = new ArrayList<>();
		List<CalcBonusResponse> saveBill = new ArrayList<>();
		Logger log = new Logger();
		log.setType("PutDirectVoucher");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId() != null ? request.getDevice().getDeviceId() : "");
			log.setRequestId(request.getRequestId() != null ? request.getRequestId() : "");
			log.setPhoneNumber(request.getUser().getPhoneNumber() != null ? request.getUser().getPhoneNumber() : "");
			log.setEmail(request.getUser().getEmail() != null ? request.getUser().getEmail() : "");
			log.setCardNumber(request.getUser().getCardholder() != null ? request.getUser().getCardholder() : "");

			if ((request == null || request.getRequestId() == null || StringHelper.isNullOrEmpty(request.getDevice().getDeviceId()))) {
				response.setStatus(StatusCode.BODY_EMPTY.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BODY_EMPTY, log.getLanguage()));
				return response;
			}

			if (!GetDevice(response, log)) {
				return response;
			}

			if (!CreateBillKey(response, log)) {
				return response;
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("QrData", request.getRequestId());
			parameters.put("Type", "CheckReference");
			parameters.put("DeviceId", request.getDevice().getDeviceId());
			parameters.put("Language", String.valueOf(log.getLanguage()));

			if ("Electric".equals(log.getOrganizationType())) {
				// CheckVoucher service
				for (int i = 0; i < request.getVoucherPaymentsL2().size(); i++) {
					VoucherCheckRequest checkRequest = new VoucherCheckRequest();
					checkRequest.setVoucherId(request.getVoucherPaymentsL2().get(i).getVoucherId());
					checkRequest.setLocationKey(log.getLocationKeyL2());
					checkRequest.setPosKey(log.getPosKey());
					checkRequest.setCashierKey(log.getCashierKey());

					VoucherResponse voucherCheckResponse = service.CheckVoucherLoyalty2(checkRequest, log);
					lstCheckResult.add(voucherCheckResponse);
					if (voucherCheckResponse.getStatus() == 0) {
						VoucherDetails details = new VoucherDetails();
						details.setVoucherId(voucherCheckResponse.getVoucherId());
						details.setAmount(voucherCheckResponse.getBalance());
						lstPutRequestDetails.add(details);
						states.add("CHECK_SUCCESS");
					}
				}

				if ((lstPutRequestDetails.size() <= 0 || lstPutRequestDetails == null || !states.contains("CHECK_SUCCESS"))) {
					result.setStatus(StatusCode.VOUCHER_NOT_FOUND.getValue());
					result.setData(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.VOUCHER_NOT_FOUND, log.getLanguage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}

				BigDecimal checkAmount = lstCheckResult.stream().map(i -> i.getBalance()).reduce(BigDecimal.ZERO, BigDecimal::add);

				// PutVoucher service
				putRequest.setLocationKey(log.getLocationKeyL2());
				putRequest.setPosKey(log.getPosKey());
				putRequest.setCashierKey(log.getCashierKey());
				putRequest.setBillKey(log.getBillKey());
				putRequest.setSystemId("Integration");
				putRequest.setPhoneNumber(log.getPhoneNumber());
				putRequest.setEmail(log.getEmail());
				putRequest.setSuspend(0); // Түр ашиглаж байгаа
				putRequest.setDetails(lstPutRequestDetails);

				putResult = service.PutVoucherLoyalty2(putRequest, log);
				if (putResult.getStatus() == 0) {
					states.add("PUT_VOUCHER_SUCCESS");
					log.setInvoiceId(nomin.loyaltyintegration.tools.Tools.getUUID());
					result.setInvoiceId(log.getInvoiceId());
					result.setTotalVoucherAmount(checkAmount);
				} else {
					result.setStatus(StatusCode.PUT_VOUCHER_ERROR.getValue());
					result.setData(JsonHelper.toJson(putResult));
					result.setCheckInfo(JsonHelper.toJson(lstCheckResult));
					result.setMessage(MessageHelper.getStatusMessage(StatusCode.PUT_VOUCHER_ERROR, log.getLanguage(), putResult.getMessage()));
					response.setMessage(JsonHelper.toJson(result));
					response.setState(1);
					return response;
				}

				for (int i = 0; i < lstPutRequestDetails.size(); i++) {
					for (int j = 0; j < putResult.getDetails().size(); j++) {
						if (lstPutRequestDetails.get(i).getVoucherId().equals(putResult.getDetails().get(j).getVoucherId())) {
							putResult.getDetails().get(j).setBalance(lstPutRequestDetails.get(i).getAmount());
						}
					}
				}

				// SaveVoucher
				parameters = new HashMap<>();
				parameters.put("type", "PutVoucher");
				parameters.put("invoiceId", log.getInvoiceId());
				parameters.put("data", rawData);
				parameters.put("voucherResult", JsonHelper.toJson(putResult));
				parameters.put("voucherType", 1);
				parameters.put("language", String.valueOf(log.getLanguage()));

				saveBill = getByProcedure("[dbo].[VoucherTransaction]", parameters, CalcBonusResponse.class);

				if (saveBill == null || saveBill.size() == 0 || saveBill.get(0).getStatus() != 1) {
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.SAVE_VOUCHER_ERROR, log.getLanguage(), "fix: " + log.getInvoiceId()));
					response.setStatus(StatusCode.SAVE_VOUCHER_ERROR.getValue());
					return response;
				}
				states.add("SAVE_BILL_SUCCESS");
				result.setStatus(StatusCode.SUCCESS.getValue());
				result.setData(JsonHelper.toJson(putResult));
				result.setInvoiceId(log.getInvoiceId());
				response.setMessage(JsonHelper.toJson(result));
				response.setState(1);
				response.setStatus(1);
			} else {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.NOT_ALLOWED, log.getLanguage()));
				response.setStatus(StatusCode.NOT_ALLOWED.getValue());
				return response;
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			states.add("SET_MESSAGE");
			log.setError(ex.getMessage());
		} finally {
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			log.setStatus(String.valueOf(result.getStatus()));
			logger.log(log);

			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}

			if ("Electric".equals(log.getOrganizationType()) && states.contains("PUT_VOUCHER_SUCCESS") && !states.contains("SAVE_BILL_SUCCESS")) {
				Logger failLogger = new Logger(log);
				failLogger.setType("PutVoucher-Fix");
				failLogger.setRequest(rawData);
				failLogger.setRequestDate(new Date());
				VoucherTransactionRequest vRequest = new VoucherTransactionRequest();
				try {
					// ReturnVoucher service
					vRequest.setLocationKey(log.getLocationKeyL2());
					vRequest.setPosKey(log.getPosKey());
					vRequest.setCashierKey(log.getCashierKey());
					vRequest.setBillKey(log.getBillKey());
					vRequest.setDetails(lstPutRequestDetails);
					failLogger.setRequest(Json.toJson(vRequest));
					VoucherResponse voucherCheckResponse = service.ReturnVoucherLoyalty2(vRequest, log);
					failLogger.setResponse(Json.toJson(voucherCheckResponse));
					if (voucherCheckResponse.getStatus() == 0) {
						voucherCheckResponse.setStatus(StatusCode.SUCCESS.getValue());
					} else {
						voucherCheckResponse.setStatus(StatusCode.FAIL.getValue());
					}
				} catch (Exception e) {
					failLogger.setStatus("RETURN_VOUCHER_FAIL");
					failLogger.setError(e.getMessage());
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_START______________");
					e.printStackTrace();
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_JSON______________");
					System.err.println(Json.toJson(vRequest));
					System.err.println("______________SAVE_LOYALTY_INTEGRATION_RETURN_LOG_FAILED_START______________");
				} finally {
					failLogger.setRequestDate(new Date());
					logger.log(failLogger);
				}
			}
		}
		return response;
	}

	@Override
	@Transactional(rollbackOn = Exception.class)
	public LoyaltyBaseResponse ReturnVoucher(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse result = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("ReturnVoucher");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		VoucherTransactionRequest returnRequest = new VoucherTransactionRequest();
		VoucherResponse returnResult = new VoucherResponse();
		List<VoucherDetails> lstReturnRequestDetails = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());

			if (request.getInvoiceId() == null || request.getDevice().getDeviceId() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage())));
				return response;
			}

			if (!GetDevice(response, log)) {
				return response;
			}

			List<InvoiceBill> invoiceBill = getInvoiceBillList(log);
			List<InvoiceVoucherPayments> voucherList = getInvoiceVoucherPaymentsList(log.getInvoiceId(), log.getDeviceId());
			if (invoiceBill == null || invoiceBill.size() <= 0 || voucherList == null || voucherList.size() <= 0) {
				response.setStatus(StatusCode.VOUCHER_NOT_FOUND.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.VOUCHER_NOT_FOUND, log.getLanguage())));
				return response;
			}

			if ("EBARIMT_SUCCESS".contains(invoiceBill.get(0).getStatus()) || "BILL_IMAGE_SUCCESS".contains(invoiceBill.get(0).getStatus()) || "BILL_SUCCESS".contains(invoiceBill.get(0).getStatus())
					|| "MESSAGE_SUCCESS".contains(invoiceBill.get(0).getStatus()) || "MAIL_SUCCESS".contains(invoiceBill.get(0).getStatus()) || "MESSAGE_MAIL_SUCCESS".contains(invoiceBill.get(0).getStatus())) {
				response.setStatus(StatusCode.RETURN_VOUCHER_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RETURN_VOUCHER_ERROR, log.getLanguage(), "TRANSACTION IF It has been called only once, it cannot be returned!!!")));
				return response;
			}

			for (int i = 0; i < voucherList.size(); i++) {
				VoucherDetails details = new VoucherDetails();
				details.setVoucherId(voucherList.get(i).getVoucherCode());
				details.setAmount(voucherList.get(i).getUsedAmount());
				lstReturnRequestDetails.add(details);
			}

			// ReturnVoucher
			returnRequest.setLocationKey(log.getLocationKeyL2());
			returnRequest.setPosKey(log.getPosKey());
			returnRequest.setCashierKey(log.getCashierKey());
			returnRequest.setBillKey(log.getBillKey());
			returnRequest.setSystemId("Integration");
			returnRequest.setPhoneNumber(log.getPhoneNumber());
			returnRequest.setEmail(log.getEmail());
			returnRequest.setSuspend(1);
			returnRequest.setDetails(lstReturnRequestDetails);

			logger.actionInvoiceBill("updateInvoiceBill", log.getInvoiceId(), log.getDeviceId(), "RETURN_TO_INIT");
			returnResult = service.ReturnVoucherLoyalty2(returnRequest, log);
			if (returnResult.getStatus() != 0) {
				logger.actionInvoiceBill("updateInvoiceBill", log.getInvoiceId(), log.getDeviceId(), "RETURN_VOUCHER_ERROR");
				result.setStatus(StatusCode.RETURN_VOUCHER_ERROR.getValue());
				result.setCheckInfo(JsonHelper.toJson(lstReturnRequestDetails));
				result.setMessage(MessageHelper.getStatusMessage(StatusCode.RETURN_VOUCHER_ERROR, log.getLanguage(), returnResult.getMessage()));
				returnResult.setStatus(null);
				returnResult.setMessage(null);
				result.setVoucherResult(returnResult);
				response.setMessage(JsonHelper.toJson(result));
				response.setState(1);
				return response;
			} else {
				logger.actionInvoiceBill("deleteInvoiceBill", log.getInvoiceId(), log.getDeviceId(), null);
				states.add("RETURN_VOUCHER_SUCCESS");
				result.setStatus(StatusCode.SUCCESS.getValue());
				result.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
				result.setData(JsonHelper.toJson(lstReturnRequestDetails));
				result.setTotalVoucherAmount(lstReturnRequestDetails.stream().map(i -> i.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add));
				returnResult.setStatus(null);
				returnResult.setMessage(null);
				result.setVoucherResult(returnResult);
				response.setMessage(JsonHelper.toJson(result));
				response.setState(1);
				response.setStatus(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			states.add("SET_MESSAGE");
			log.setError(ex.getMessage());
		} finally {
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			log.setStatus(String.valueOf(result.getStatus()));
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	@SuppressWarnings({ "null", "unused" })
	@Override
//	@Transactional(rollbackOn = Exception.class)
	public LoyaltyBaseResponse TransactionVoucher(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		VoucherTransactionRequest returnVoucherRequest = new VoucherTransactionRequest();
		VoucherResponse returnVoucher = new VoucherResponse();
		List<VoucherDetails> lstReturnDetails = new ArrayList<>();
		VoucherTransactionRequest putVoucherRequest = new VoucherTransactionRequest();
		BigDecimal setVoucherAmount = BigDecimal.ZERO;
		VoucherResponse putVoucher = new VoucherResponse();
		List<VoucherDetails> lstPutDetails = new ArrayList<>();
		List<TransactionResponse> saveBill = new ArrayList<>();
		List<CalcBonusResponse> lstCalcBonusResultNoCart = new ArrayList<>();
		List<CalcBonusResponse> lstPutTransaction = new ArrayList<>();
		DeviceInfo deviceInfo = new DeviceInfo();
		LogRequest calcBonusResponseInvoice = new LogRequest();
		LoyaltyBaseResponse calcBonusResult = new LoyaltyBaseResponse();
		CalcBonusResponse calcBonusResultNoCart = new CalcBonusResponse();
		CalcResult calcResponse = new CalcResult();
		LoyaltyBaseResponse putBonusResponse = new LoyaltyBaseResponse();
		LogRequest transactionRequest = new LogRequest();
		LoyaltyBaseResponse transactionResponse = new LoyaltyBaseResponse();
		LoyaltyBaseResponse transactionNoBonusResponse = new LoyaltyBaseResponse();
		EbarimtRequest ebarimtRequest = new EbarimtRequest();
		List<InvoiceEbarimt> listGetEbarimtInfo = new ArrayList<>();
		PutResponse ebarimtResponse = new PutResponse();
		BillReportRequest billReportRequest = new BillReportRequest();
		BillReport billRepoert = new BillReport();
		ReportResponse billImageResponse = new ReportResponse();
		BillImageResponse sendBillImageResponse = new BillImageResponse();
		List<InvoiceVoucherPayments> voucherList = new ArrayList<>();
		HashMap<String, Object> parameters = new HashMap<>();

		List<String> states = new ArrayList<>();
		Logger log = new Logger();
		log.setType("TransactionVoucher");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());
			 
			if (request.getBill().getInvref() == null || request.getBill().getPaymentVoucher() == null || request.getBill().getPaymentAmount() == null || request.getBill() == null || request.getBill().getSales() == null
					|| request.getDevice().getDeviceId() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage()));
				return response;
			}

			for (int i = 0; i < request.getBill().getSales().size(); i++) {
				if (request.getBill().getSales().get(i).getItemId() == null || request.getBill().getSales().get(i).getQuantity() == null || request.getBill().getSales().get(i).getIsWhole() == null
						|| request.getBill().getSales().get(i).getBasePrice() == null || request.getBill().getSales().get(i).getPrice() == null || request.getBill().getSales().get(i).getAmount() == null) {
					response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "sales"));
					return response;
				}
			}

			if (!GetDevice(response, log)) {
				return response;
			}

			// VoucherList
			List<InvoiceBill> invoiceBill = getInvoiceBillList(log);
			voucherList = getInvoiceVoucherPaymentsList(log.getInvoiceId(), log.getDeviceId());
			if (invoiceBill == null || invoiceBill.size() <= 0 || voucherList == null || voucherList.size() <= 0) {
				response.setStatus(StatusCode.VOUCHER_NOT_FOUND.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.VOUCHER_NOT_FOUND, log.getLanguage()));
				return response;
			}
			states.add(invoiceBill.get(0).getStatus());
			 
			// Бэлэн мөнгөөр төлөлт болон 0 дүнтэй воучир гүйлгээ хасав.
			if (voucherList.stream().filter(Objects::nonNull).map(InvoiceVoucherPayments::getVoucherAmount).filter(getVoucherAmount -> Objects.nonNull(getVoucherAmount)).reduce(BigDecimal.ZERO, BigDecimal::add)
					.compareTo(request.getBill().getPaymentVoucher()) == -1 && request.getBill().getPaymentVoucher().compareTo(BigDecimal.ZERO) == 0) {
				response.setStatus(StatusCode.CASH_TRANSACTION_ARE_CLOSED.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CASH_TRANSACTION_ARE_CLOSED, log.getLanguage()));
				return response;
			}
			
			// TODO zoruugeer ni guilgeed bolgokh
			if ("active".equals(invoiceBill.get(0).getStatus()) || "ACTIVE_TO_RETURN".equals(invoiceBill.get(0).getStatus())) {
				// ReturnVoucher
				returnVoucherRequest.setLocationKey(log.getLocationKeyL2());
				returnVoucherRequest.setPosKey(log.getPosKey());
				returnVoucherRequest.setCashierKey(log.getCashierKey());
				returnVoucherRequest.setBillKey(log.getBillKey());
				for (int i = 0; i < voucherList.size(); i++) {
					VoucherDetails details = new VoucherDetails();
					if (voucherList.get(i).getVoucherAmount().compareTo(BigDecimal.ZERO) == 1) {
						details.setAmount(voucherList.get(i).getVoucherAmount());
						details.setVoucherId(voucherList.get(i).getVoucherCode());
						lstReturnDetails.add(details);
					}
				}
				log.setStatus("ACTIVE_TO_RETURN");
				states.add("ACTIVE_TO_RETURN");
				returnVoucherRequest.setDetails(lstReturnDetails);
				returnVoucher = service.ReturnVoucherLoyalty2(returnVoucherRequest, log);

				if (returnVoucher.getStatus() != 0 || returnVoucher == null) {
					response.setStatus(StatusCode.RETURN_VOUCHER_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.RETURN_VOUCHER_ERROR, log.getLanguage()));
					return response;
				}
				states.add("RETURN_VOUCHER_SUCCESS");
			}
			setVoucherAmount = request.getBill().getPaymentVoucher();
			
			if (states.contains("RETURN_VOUCHER_SUCCESS") || "active".equals(invoiceBill.get(0).getStatus()) || "RETURN_VOUCHER_SUCCESS".equals(invoiceBill.get(0).getStatus()) || "ACTIVE_TO_RETURN".equals(invoiceBill.get(0).getStatus())
					|| "RETURN_TO_CORRECT".equals(invoiceBill.get(0).getStatus())) {
				// PutVoucher
				putVoucherRequest.setLocationKey(log.getLocationKeyL2());
				putVoucherRequest.setPosKey(log.getPosKey());
				putVoucherRequest.setCashierKey(log.getCashierKey());
				putVoucherRequest.setBillKey(log.getBillKey());
				putVoucherRequest.setSuspend(0); // Ашиглаж байгаа
				for (int i = 0; i < voucherList.size(); i++) {
					VoucherDetails details = new VoucherDetails();
					details.setVoucherId(voucherList.get(i).getVoucherCode());
					if (setVoucherAmount.compareTo(BigDecimal.ZERO) != 0) {
						if (setVoucherAmount.compareTo(voucherList.get(i).getVoucherAmount()) == 1) {
							details.setAmount(voucherList.get(i).getVoucherAmount());
						} else {
							details.setAmount(setVoucherAmount);
						}
						setVoucherAmount = setVoucherAmount.subtract(details.getAmount());
						lstPutDetails.add(details);
					}
				}
				log.setStatus("RETURN_TO_CORRECT");
				putVoucherRequest.setDetails(lstPutDetails);
				if(setVoucherAmount.compareTo(BigDecimal.ZERO) != 0) {
					response.setStatus(StatusCode.PUT_VOUCHER_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.PUT_VOUCHER_ERROR, log.getLanguage(), "Ашиглаж байгаа воучир дүн билл үнийн дүнтэй тэнцэхгүй байна"));
					return response;
				}
				putVoucher = service.PutVoucherLoyalty2(putVoucherRequest, log);
				if (putVoucher.getStatus() != 0 || putVoucher == null) {
					response.setStatus(StatusCode.PUT_VOUCHER_ERROR.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.PUT_VOUCHER_ERROR, log.getLanguage(), "/" + JsonHelper.toJson(putVoucher) + "/"));
					return response;
				}
				states.add("PUT_VOUCHER_SUCCESS");
			}

			parameters = new HashMap<>();
			parameters.put("type", "getInvoiceCustomerInfo");
			parameters.put("DeviceId", log.getDeviceId());
			parameters.put("checkInvoiceId", log.getInvoiceId());
			List<InvoiceCustomerInfo> invoiceCustomerInfo = getByProcedure("GET_INFO", parameters, InvoiceCustomerInfo.class);
			CardInfoRequest cardInfoRequest = new CardInfoRequest();
			if (!Strings.isNullOrEmpty(invoiceCustomerInfo.get(0).getCardHolderId())) {
				cardInfoRequest.setCardType("CardNumber");
				cardInfoRequest.setCardNumber(invoiceCustomerInfo.get(0).getCardHolderId());
				cardInfoRequest.setNoCardContinue(true);
			} else {
				cardInfoRequest.setCardType("Phone");
				cardInfoRequest.setCardNumber(invoiceCustomerInfo.get(0).getPhoneNumber());
				cardInfoRequest.setNoCardContinue(true);
			}

			CardInfoResponse cardResponse = cardLogic.getCardInfo3(cardInfoRequest, log);
			CalcPaymentRequest calcPaymentRequest = new CalcPaymentRequest();
			calcPaymentRequest.setRequestId(request.getRequestId());
			
			BillRequest billRequest = new BillRequest();
			billRequest.setBillNo(request.getBill().getInvref());
			billRequest.setVoucher(request.getBill().getPaymentVoucher());
			billRequest.setTotalAmount(request.getBill().getPaymentAmount());
			List<BillDetailRequest> lstBillDetailRequest = new ArrayList<>();
			for (int i = 0; i < request.getBill().getSales().size(); i++) {
				BillDetailRequest billDetailRequest = new BillDetailRequest();
				billDetailRequest.setItemId(request.getBill().getSales().get(i).getItemId());
				billDetailRequest.setQuantity(request.getBill().getSales().get(i).getQuantity());
				billDetailRequest.setIsWhole(request.getBill().getSales().get(i).getIsWhole());
				billDetailRequest.setHasNuat(request.getBill().getSales().get(i).getHasNuat());
				billDetailRequest.setBasePrice(request.getBill().getSales().get(i).getBasePrice());
				billDetailRequest.setPrice(request.getBill().getSales().get(i).getPrice());
				billDetailRequest.setAmount(request.getBill().getSales().get(i).getAmount());
				billDetailRequest.setSectionKey(new BigDecimal(log.getSectionKey()));
				billDetailRequest.setCustomerKey(new BigDecimal(0));
				lstBillDetailRequest.add(billDetailRequest);
			}
			billRequest.setBillDetail(lstBillDetailRequest);
			calcPaymentRequest.setBill(billRequest);
			deviceInfo.setDeviceId(log.getDeviceId());
			deviceInfo.setLanguage(log.getLanguage().toString());
			calcPaymentRequest.setDevice(deviceInfo);
			User user = new User();
			user.setNoCardContinue("true");
			calcPaymentRequest.setUser(user);
			
//			String noCartDataRaw = "{\"cardHolderId\":\"0\",\"cardAccountId\":\"0\",\"firstName\":\"\",\"lastName\":\"\",\"bonusUse\":\"N\",\"openDate\":\"\",\"bonusCancelDayCount\":30,\"isDvip\":\"N\",\"useDvip\":\"N\",\"calcBonusPercentPurchaseAmount\":0,\"timedPurchaseAmount\":-1.0,\"bonusPecent\":0,\"bonusBalance\":\"0\",\"dvipBalance\":0.0,\"dvipSavedBalance\":0.0,\"coinAllBalance\":0.0,\"coinMonthBalance\":0.0,\"coinDayBalance\":0.0,\"itemBonusBalance\":0.0,\"addItemBonus\":\"Y\",\"useItemBonus\":\"Y\",\"itemBonusHeaderId\":1,\"isBirthDay\":\"N\",\"isIdPhoneDuplicated\":\"N\",\"isRequestPhoneUpdate\":\"N\",\"itemBonusX2Info\":\"\",\"cardNumber\":0,\"uaBalance\":0,\"billNo\":0,\"cardType\":\"CardNumber\",\"pinType\":\"PinCode\",\"totalAmount\":" + request.getBill().getPaymentAmount() + ",\"voucher\":" + request.getBill().getPaymentVoucher() + 
//					",\"message\":\"success\"}";
//			String billDataRaw = JsonHelper.toJson(lstBillDetailRequest);
//			parameters = new HashMap<>();
//			parameters.put("RequestId", request.getRequestId());
//			parameters.put("DeviceId", request.getDevice().getDeviceId());
//			parameters.put("CardInfo", noCartDataRaw);
//			parameters.put("BillData", billDataRaw);
//			parameters.put("NoCardContinue", "true");
//			parameters.put("EbarimtFormat", "2");
//			parameters.put("Language", String.valueOf(log.getLanguage()));
//			lstCalcBonusResultNoCart = getByProcedure("CalculateBonus_DEVELOPING", parameters, CalcBonusResponse.class);
//			if (lstCalcBonusResultNoCart == null || lstCalcBonusResultNoCart.size() == 0 || lstCalcBonusResultNoCart.get(0).getStatus() != 1) {
//				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
//				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
//				return response;
//			}
//			calcResponse = JsonHelper.toJsonObject(lstCalcBonusResultNoCart.get(0).getData(), CalcResult.class);
//			states.add("CALCBONUS_SUCCESS");
			 
			if (cardResponse != null || ("success".equals(cardResponse.getMessage()) || "АМЖИЛТТАЙ".equals(cardResponse.getMessage())) || !"true".equals(invoiceCustomerInfo.get(0).getType())) {
				calcPaymentRequest.setRequestId(invoiceCustomerInfo.get(0).getReqeustId());
				calcPaymentRequest.setQrData(request.getQrData() != null ? request.getQrData() : "");
				calcPaymentRequest.setCard(cardInfoRequest);

				states.add("PUT_VOUCHER_TO_CALCBONUS");
				calcBonusResult = CalcBonus(JsonHelper.toJson(calcPaymentRequest), 1);
				calcBonusResponseInvoice = JsonHelper.toJsonObject(calcBonusResult.getMessage(), LogRequest.class);
				if (calcBonusResult.getStatus() != 1) {
					response = new LoyaltyBaseResponse(calcBonusResult);
					return response;
				}
				
				calcResponse = JsonHelper.toJsonObject(calcBonusResult.getMessage(), CalcResult.class);
				
				PaymentRequest paymentRequest = new PaymentRequest();
				paymentRequest.setRequestId(log.getRequestId());
				paymentRequest.setCardType("Bonus");
				paymentRequest.setPayments(new ArrayList<>());
				paymentRequest.setDevice(deviceInfo);
				paymentRequest.setInvoiceId(calcResponse.getInvoiceId());
				states.add("CALCBONUS_TO_PUT_BONUS");
				putBonusResponse = PutBonus(JsonHelper.toJson(paymentRequest));
				if (putBonusResponse.getStatus() != 1) {
					response = new LoyaltyBaseResponse(putBonusResponse);
					return response;
				}
				states.add("PUT_BONUS_SUCCESS");
			} else {
				String noCartDataRaw = "{\"cardHolderId\":\"0\",\"cardAccountId\":\"0\",\"firstName\":\"\",\"lastName\":\"\",\"bonusUse\":\"N\",\"openDate\":\"\",\"bonusCancelDayCount\":30,\"isDvip\":\"N\",\"useDvip\":\"N\",\"calcBonusPercentPurchaseAmount\":0,\"timedPurchaseAmount\":-1.0,\"bonusPecent\":0,\"bonusBalance\":\"0\",\"dvipBalance\":0.0,\"dvipSavedBalance\":0.0,\"coinAllBalance\":0.0,\"coinMonthBalance\":0.0,\"coinDayBalance\":0.0,\"itemBonusBalance\":0.0,\"addItemBonus\":\"Y\",\"useItemBonus\":\"Y\",\"itemBonusHeaderId\":1,\"isBirthDay\":\"N\",\"isIdPhoneDuplicated\":\"N\",\"isRequestPhoneUpdate\":\"N\",\"itemBonusX2Info\":\"\",\"cardNumber\":0,\"uaBalance\":0,\"billNo\":0,\"cardType\":\"CardNumber\",\"pinType\":\"PinCode\",\"totalAmount\":" + request.getBill().getPaymentAmount() + ",\"voucher\":" + request.getBill().getPaymentVoucher() + 
						",\"message\":\"success\"}";
				String billDataRaw = JsonHelper.toJson(lstBillDetailRequest);
				parameters = new HashMap<>();
				parameters.put("RequestId", request.getRequestId());
				parameters.put("DeviceId", request.getDevice().getDeviceId());
				parameters.put("CardInfo", noCartDataRaw);
				parameters.put("BillData", billDataRaw);
				parameters.put("NoCardContinue", "true");
				parameters.put("EbarimtFormat", "2");
				parameters.put("Language", String.valueOf(log.getLanguage()));
				if (log.getProduction() == 0) {
					lstCalcBonusResultNoCart = getByProcedure("CalculateBonus_DEVELOPING", parameters, CalcBonusResponse.class);
				} else {
					lstCalcBonusResultNoCart = getByProcedure("CalculateBonus", parameters, CalcBonusResponse.class);
				}
				if (lstCalcBonusResultNoCart == null || lstCalcBonusResultNoCart.size() == 0 || lstCalcBonusResultNoCart.get(0).getStatus() != 1) {
					response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
					return response;
				}
				calcResponse = JsonHelper.toJsonObject(lstCalcBonusResultNoCart.get(0).getData(), CalcResult.class);
				states.add("CALCBONUS_SUCCESS");
			}
			
			Long ebarimtBillKey = Tools.createNewPKey();
			String billIdSuffix1 = Tools.createNewPKey().toString().substring(6, 12).toString();
			Integer intBillIdSuffix2 = Integer.valueOf(billIdSuffix1) - 1;
			
			ebarimtRequest.setGroup(true);
			ebarimtRequest.setVat(calcResponse.getBill().get(0).getTotalNuat().toString());
			ebarimtRequest.setAmount(calcResponse.getBill().get(0).getTotalAmount().toString());
			
			ebarimtRequest.setBillType("1");
			ebarimtRequest.setBillIdSuffix(billIdSuffix1);
			String posNo = log.getPosCode().toString();
			if(posNo.length() < 6) {
				posNo = Strings.leftPad(posNo, 6, "0");
			}
			ebarimtRequest.setPosNo(posNo);
			nomin.loyaltyintegration.businessentity.ebarimt.Bills ebarimtBills = new nomin.loyaltyintegration.businessentity.ebarimt.Bills();
			List<nomin.loyaltyintegration.businessentity.ebarimt.Bills> lstEbarimtBills = new ArrayList<>();
			ebarimtBills.setAmount(calcResponse.getBill().get(0).getTotalAmount().toString());
			ebarimtBills.setVat(calcResponse.getBill().get(0).getTotalNuat().toString());
			ebarimtBills.setCashAmount(calcResponse.getBill().get(0).getTotalAmount().toString());
			ebarimtBills.setNonCashAmount("0.00");
			ebarimtBills.setCityTax(calcResponse.getBill().get(0).getTotalCityTax().toString());
			ebarimtBills.setDistrictCode(log.getDistrictCode());
			ebarimtBills.setInternalId(log.getInternalId());
			ebarimtBills.setRegisterNo(log.getRegisterNo());
			ebarimtBills.setCustomerNo("");
			ebarimtBills.setPosNo(posNo);
			ebarimtBills.setBillType("1");
			ebarimtBills.setBillIdSuffix(intBillIdSuffix2.toString());
			ebarimtBills.setReturnBillId("");			
			
			List<nomin.loyaltyintegration.businessentity.ebarimt.Stocks> lstStocks = new ArrayList<>();
			
			for(int i = 0; i < calcResponse.getBill().get(0).getBillDetail().size(); i++) {
				nomin.loyaltyintegration.businessentity.ebarimt.Stocks stock = new nomin.loyaltyintegration.businessentity.ebarimt.Stocks();
				stock.setCode(calcResponse.getBill().get(0).getBillDetail().get(i).getItemId());
				stock.setName(calcResponse.getBill().get(0).getBillDetail().get(i).getName());
				stock.setMeasureUnit(calcResponse.getBill().get(0).getBillDetail().get(i).getMeasureUnit());
				stock.setQty(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getQuantity().toString()));
				stock.setUnitPrice(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getPrice().toString()));
				stock.setTotalAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getAmountWithNuat()));
				stock.setCityTax(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getCityTax().toString()));
				stock.setVat(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getNuat().toString()));
				lstStocks.add(stock);
			}
			
			ebarimtBills.setStocks(lstStocks);
			lstEbarimtBills.add(ebarimtBills);
			ebarimtRequest.setBills(lstEbarimtBills);
			
			ebarimtResponse = service.putEbarimt(ebarimtRequest, log);			
			if (ebarimtResponse == null) {
				response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR, log.getLanguage()));
				return response;
			} else if (!"true".equals(ebarimtResponse.getSuccess())) {
				response.setStatus(StatusCode.EBARIMT_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.EBARIMT_ERROR, log.getLanguage(), ebarimtResponse.getMessage()));
				return response;
			} 
			
			states.add("EBARIMT_SUCCESS");
			billReportRequest.setExportType("image");
			BillReport billReport = new BillReport();
			billReport.setCopyName("1");
			billReport.setBillNo(request.getBill().getInvref().toString());
			billReport.setTtd(log.getInternalId());
			billReport.setBillDate(log.getRequestDate());
			if(states.contains("PUT_BONUS_SUCCESS") && !cardResponse.getVatCustomerNo().isEmpty()) {
				billRepoert.setCustomerTtd(cardResponse.getVatCustomerNo());
			}
			billReport.setDdtd(ebarimtResponse.getBillId());
			billReport.setQrCode(ebarimtResponse.getQrData());
			billReport.setLotteryNo(ebarimtResponse.getLottery());
			billReport.setPayAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalAmount().toString()));
			billReport.setAmountHasVat(new BigDecimal(calcResponse.getBill().get(0).getTotalAmount().toString()));
			billReport.setVatAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalNuat().toString()));
			billReport.setAmount(BigDecimal.ZERO);
			billReport.setPayAmount(BigDecimal.ZERO);
			billReport.setCashAmount(BigDecimal.ZERO);
			billReport.setChangeAmount(BigDecimal.ZERO);
			String billString3 = ebarimtBillKey.toString().substring(ebarimtBillKey.toString().length() -4 , ebarimtBillKey.toString().length()).toString();
			String billString2 = ebarimtBillKey.toString().substring(ebarimtBillKey.toString().length() -8 , ebarimtBillKey.toString().length() - 4).toString();
			String billString1 = ebarimtBillKey.toString().substring(ebarimtBillKey.toString().length() -12 , ebarimtBillKey.toString().length() - 8).toString();			
			billReport.setBillNo("# " +billString1 + " " + billString2 + " " + billString3); //"#1158 0604 3531"
			billReport.setPos(log.getPosCode().toString());
			billReport.setCash(log.getDeviceName());
			nomin.posreport.web.businessentity.bill.BillItems billItems = new nomin.posreport.web.businessentity.bill.BillItems();
			billItems.setAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalAmount().toString()));
			billItems.setCityTaxAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalCityTax().toString()));
			billItems.setCategoryCount(1);
			nomin.posreport.web.businessentity.bill.BillItem itemsHasVat = new nomin.posreport.web.businessentity.bill.BillItem();
			itemsHasVat.setAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalAmount().toString()));
			itemsHasVat.setVatAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalNuat().toString()));
			itemsHasVat.setTotalAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalAmount().toString()));
			if(states.contains("PUT_BONUS_SUCCESS")){
				itemsHasVat.setBonusAmount(new BigDecimal(calcResponse.getBill().get(0).getTotalAddBonus().toString()));
			}
			itemsHasVat.setHasVat(true);
			List<nomin.posreport.web.businessentity.bill.BillItemGroup> lstBillItemGroup = new ArrayList<>();
			nomin.posreport.web.businessentity.bill.BillItemGroup billItemGroup = new nomin.posreport.web.businessentity.bill.BillItemGroup();
			billItemGroup.setGroup("true");
			List<BillItemDtl> lstBillItemDtl = new ArrayList<>();
			nomin.posreport.web.businessentity.bill.VoucherData voucherData = new nomin.posreport.web.businessentity.bill.VoucherData();
			voucherData.setAmount(request.getBill().getPaymentVoucher());
			List<VoucherDataDtl> lstVoucherDataDtl = new ArrayList<>();
			for(int i = 0; i < putVoucherRequest.getDetails().size(); i ++) {
				VoucherDataDtl voucherDataDtl = new VoucherDataDtl();
				voucherDataDtl.setAmount(new BigDecimal(putVoucherRequest.getDetails().get(i).getAmount().toString()));
				voucherDataDtl.setVoucherCode(putVoucherRequest.getDetails().get(i).getVoucherId());
				lstVoucherDataDtl.add(voucherDataDtl);
			}
			voucherData.setLstDtl(lstVoucherDataDtl);
			billReport.setVoucherData(voucherData);
			
			for(int i = 0; i < calcResponse.getBill().get(0).getBillDetail().size(); i++) {
				BillItemDtl billItemDtl = new BillItemDtl();
				billItemDtl.setSection(calcResponse.getBill().get(0).getBillDetail().get(i).getSectionName());
				billItemDtl.setItem(calcResponse.getBill().get(0).getBillDetail().get(i).getItemId());
				billItemDtl.setQty(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getQuantity()));
				billItemDtl.setPrice(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getPrice().toString()));
				billItemDtl.setVatFreeItem(false);
				billItemDtl.setVatAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getNuat().toString()));
				billItemDtl.setDiscountAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getBasePrice().toString()).subtract(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getPrice().toString())));
				billItemDtl.setBonusAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getAddBonus().toString()));		
				billItemDtl.setBonusPayAmount(BigDecimal.ZERO);
				billItemDtl.setUpointAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getAddUPoint().toString()));
				billItemDtl.setUpointItemAmount(BigDecimal.ZERO);
				billItemDtl.setUpointPayAmount(BigDecimal.ZERO);
				billItemDtl.setAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getAmount().toString()));
				billItemDtl.setCityTaxAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getCityTax().toString()));
				billItemDtl.setItemBonusAmount(new BigDecimal(calcResponse.getBill().get(0).getBillDetail().get(i).getAddItemBonusAmount().toString()));
				
				lstBillItemDtl.add(billItemDtl);
			}
			if(states.contains("PUT_BONUS_SUCCESS")) {
				CardData cardData = new CardData();
				CardItems cardItems = new CardItems();
				CardItem cardItem = new CardItem();
				List<CardItem> lstCardItem = new ArrayList<>();
				cardItem.setBeginBalance(calcResponse.getCardTypes().get(0).getBalanceBonus());
				cardItem.setSubtracted(BigDecimal.ZERO);
				cardItem.setAdded(new BigDecimal(calcResponse.getBill().get(0).getTotalAddBonus()));
				cardItem.setEndBalance(calcResponse.getCardTypes().get(0).getBalanceBonus().add(new BigDecimal(calcResponse.getBill().get(0).getTotalAddBonus())));
				
				lstCardItem.add(cardItem);
				if(calcResponse.getCardTypes().get(0).getIsUPoint() == 1) {
					cardItems.setUpointCardNo(cardResponse.getUaCardId());
					cardItems.setLstDtl(lstCardItem);
				}
				cardData.setCard(cardItems);
				billReport.setCardData(cardData);
			}
			
			itemsHasVat.setLstGroup(lstBillItemGroup);
			billItems.setItemsHasVat(itemsHasVat);
			billReport.setBillItems(billItems);
			billReportRequest.setReport(billReport);
			
			billImageResponse = service.GetBillImage(billReportRequest, log);
			if (billImageResponse == null) {
				response.setStatus(StatusCode.BILL_ERROR.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.BILL_ERROR, log.getLanguage()));
				return response;
			}
			states.contains("BILLIMAGE_SUCCESS");

			transactionRequest.setRequestId(request.getRequestId());
			Bills transacitonBills = new Bills();
			transacitonBills.setIdBill(ebarimtBillKey);
			transacitonBills.setInvref(request.getBill().getInvref());
			transacitonBills.setPaymentAmount(request.getBill().getPaymentAmount());
			transacitonBills.setVatBillNo(ebarimtResponse.getBillId());
			transacitonBills.setVatAmount(new BigDecimal(ebarimtResponse.getVat().toString()));
			transacitonBills.setEbarimtBillType(1);
			transacitonBills.setVatCustomerTTD("");
			
			for (int i = 0; i < request.getBill().getSales().size(); i++) {
				for (int j = 0; j < ebarimtResponse.getLstGroup().get(0).getStocks().size(); j++) {
					if(request.getBill().getSales().get(i).getItemId().equals(ebarimtResponse.getLstGroup().get(0).getStocks().get(j).getCode())) {
						request.getBill().getSales().get(i).setVatBillNo(ebarimtResponse.getLstGroup().get(0).getBillId());
						request.getBill().getSales().get(i).setVatAmount(new BigDecimal(ebarimtResponse.getLstGroup().get(0).getStocks().get(j).getVat().toString()));
					}
				}
			}
			
			transacitonBills.setSales(request.getBill().getSales());
			List<VoucherPaymentResponse> lstVoucherPaymentResponse = new ArrayList<>();
			for(int i = 0; i < putVoucherRequest.getDetails().size(); i ++) {
				VoucherPaymentResponse voucherPaymentResponse = new VoucherPaymentResponse();
				voucherPaymentResponse.setVoucherCode(putVoucherRequest.getDetails().get(i).getVoucherId());
				voucherPaymentResponse.setVoucherType("P");
				voucherPaymentResponse.setVoucherPercent("0");
				voucherPaymentResponse.setIdItem("");
				voucherPaymentResponse.setVoucherAmount(putVoucherRequest.getDetails().get(i).getAmount().toString());
				voucherPaymentResponse.setUsedAmount(putVoucherRequest.getDetails().get(i).getAmount().toString());
				lstVoucherPaymentResponse.add(voucherPaymentResponse);
			}
			voucherData.setLstDtl(lstVoucherDataDtl);
			
			transacitonBills.setLstvoucherPayment(lstVoucherPaymentResponse);			
			transactionRequest.setBill(transacitonBills);
			List<PaymentInfo> lstPayments = new ArrayList<>();
			PaymentInfo paymentInfo = new PaymentInfo();
			paymentInfo.setType("Cash");
			paymentInfo.setAmount(BigDecimal.ZERO);
			PaymentData paymentData = new PaymentData();
			Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			paymentData.setTransactionDate(formatter.format(log.getRequestDate()));
			paymentData.setCardNo("");
			paymentData.setReferenceNo("");
			paymentData.setTraceNo("");
			paymentData.setBatchNo("");
			paymentData.setTerminalId("");
			paymentInfo.setData(paymentData);
			lstPayments.add(paymentInfo);
			transactionRequest.setPayments(lstPayments);
			transactionRequest.setDevice(deviceInfo);
			
			parameters = new HashMap<>();
			if (states.contains("PUT_BONUS_SUCCESS")) {
				transactionRequest.setInvoiceId(calcResponse.getInvoiceId());
				parameters.put("IsBonus", 1);
			} else {
				transactionRequest.setInvoiceId(request.getInvoiceId());
				parameters.put("IsBonus", 0);
			}
			parameters.put("Json", JsonHelper.toJson(transactionRequest));
			parameters.put("Language", String.valueOf(log.getLanguage()));
			System.out.println(Base64.getEncoder().encodeToString(billImageResponse.getResponse().get(0)));
			parameters.put("BillImageImageBase64", Base64.getEncoder().encodeToString(billImageResponse.getResponse().get(0)));
			if (log.getProduction() == 0) {
				lstPutTransaction = getByProcedure("PutTransaction_DEVELOPING", parameters, CalcBonusResponse.class);
			} else {
				lstPutTransaction = getByProcedure("PutTransaction", parameters, CalcBonusResponse.class);
			}

			if (lstPutTransaction == null || lstPutTransaction.size() == 0 || lstPutTransaction.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return response;
			}
			
			sendBillImageResponse.setStatus(StatusCode.SUCCESS.getValue());
			sendBillImageResponse.setBillImageExt("base64");
			sendBillImageResponse.setBillImageBmp(billImageResponse);
			sendBillImageResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
			sendBillImageResponse.setBillState(states.get(states.size() - 1));
			response.setMessage(JsonHelper.toJson(sendBillImageResponse));
			response.setStatus(1);
			response.setState(1);
			return response;
			
/****
	Mail, Message илгээхийг хаав		
***/
//			parameters = new HashMap<>();
//			parameters.put("phoneNumber", invoiceCustomerInfo.get(0).getPhoneNumber());
//			parameters.put("amount", ebarimtResponse.getAmount()); // ebarimtRequest.getBills().get(0).getAmount()
//			parameters.put("ddtd", ebarimtResponse.getLstGroup().get(0).getBillId());
//			parameters.put("lottery", ebarimtResponse.getLstGroup().get(0).getLottery());
//			parameters.put("type", log.getOrganizationType());
//			parameters.put("billId", ebarimtResponse.getLstGroup().get(0).getBillId());
//			parameters.put("customerNo", invoiceCustomerInfo.get(0).getCustomerNo());
//			parameters.put("cardholderId", invoiceCustomerInfo.get(0).getCardHolderId());
//			parameters.put("vatAmount", ebarimtResponse.getLstGroup().get(0).getVat());
//			parameters.put("email", invoiceCustomerInfo.get(0).getEmail());
//
//			CalcBonusResponse checkMessage = getInvoiceInfo("checkMessage", log.getInvoiceId());
//			if (checkMessage != null || checkMessage.getStatus() == -1) {
//				LoyaltyBaseResponse sendMessageResponse = service.SendMessage(service.SetMessage(parameters), log);
//				if (sendMessageResponse.getStatus() == 1) {
//					sendBillImageResponse.setSendMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
//				} else {
//					sendBillImageResponse.setSendMessage(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
//				}
//			} else {
//				sendBillImageResponse.setSendMessage(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
//			}
//			CalcBonusResponse checkMail = getInvoiceInfo("checkMail", log.getInvoiceId());
//			if (checkMail != null || checkMail.getStatus() == -1) {
//				LoyaltyBaseResponse sendMailResponse = service.SendEmail(service.SetMail(parameters), log);
//				if (sendMailResponse.getStatus() == 1) {
//					sendBillImageResponse.setSendMail(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
//				} else {
//					sendBillImageResponse.setSendMail(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
//				}
//			} else {
//				sendBillImageResponse.setSendMail(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
//			}
//
//			sendBillImageResponse.setStatus(StatusCode.SUCCESS.getValue());
//			sendBillImageResponse.setBillImageExt("bmp");
//			sendBillImageResponse.setBillImage(Json.toJson(billImageResponse.getResponse().get(0)).replace("\"", ""));
//			sendBillImageResponse.setBillImageBmp(billImageResponse);
//			sendBillImageResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
//			sendBillImageResponse.setBillInfo(JsonHelper.toJson(ebarimtResponse));
//			sendBillImageResponse.setBillState(states.get(states.size() - 1));
//			response.setMessage(JsonHelper.toJson(sendBillImageResponse));
//			response.setStatus(1);
//			response.setState(1);
//			return response;
		} catch (PersistenceException ex) {
			log.setError(ex.getMessage());
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}

			// Ebarimt буцаалт
			if (!states.contains("BILL_SUCCESS") && states.contains("EBARIMT_SUCCESS")) {
				nomin.loyaltyintegration.entity.bill.ReturnBillRequest returnBillRequest = new nomin.loyaltyintegration.entity.bill.ReturnBillRequest();
				nomin.loyaltyintegration.entity.bill.ReturnBillResponse returnBillResponse = new nomin.loyaltyintegration.entity.bill.ReturnBillResponse();
				try {
					returnBillRequest.setReturnBillId(ebarimtResponse.getBillId());
					returnBillRequest.setBillDate(new Date());
					returnBillResponse = service.returnEbarimt(returnBillRequest, log);
					if (!returnBillResponse.isSuccess()) {
						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnBillResponse****ERROR***START***WRONG***RESULT******");
						System.err.println(returnBillResponse.getMessage());
						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnBillResponse****ERROR***END***WRONG***RESULT*****");
					}
				} catch (Exception ex) {
					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnBillResponse****ERROR***START********");
					ex.printStackTrace();
					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnBillResponse****ERROR***END********");
				}
			}
			/****
			Воучир буцааж хэвийн болгодогийг хаав		
			 ***/
//			boolean IsPutVoucher = false;
//			if (!states.contains("BILL_SUCCESS") && states.contains("RETURN_VOUCHER_SUCCESS")) {
//				try {
//					VoucherTransactionRequest returnVoucherRequest1 = new VoucherTransactionRequest();
//					List<VoucherDetails> lstReturnDetails1 = new ArrayList<>();
//					returnVoucherRequest1.setLocationKey(log.getLocationKeyL2());
//					returnVoucherRequest1.setPosKey(log.getPosKey());
//					returnVoucherRequest1.setCashierKey(log.getCashierKey());
//					returnVoucherRequest1.setBillKey(log.getBillKey());
//					for (int i = 0; i < putVoucherRequest.getDetails().size(); i++) {
//						VoucherDetails details = new VoucherDetails();
//						details.setAmount(voucherList.get(i).getVoucherAmount());
//						details.setVoucherId(voucherList.get(i).getVoucherCode());
//						lstReturnDetails1.add(details);
//					}
//					returnVoucherRequest1.setDetails(lstReturnDetails1);
//					returnVoucher = service.ReturnVoucherLoyalty2(returnVoucherRequest1, log);	
//					if (returnVoucher.getStatus() != 0 || returnVoucher == null) {
//						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnVoucherResponse****ERROR***START***WRONG***RESULT******");
//						System.err.println(Json.toJson(returnVoucher));
//						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnVoucherResponse****ERROR***END***WRONG***RESULT*****");
//					}
//					IsPutVoucher = true;
//				} catch (Exception ex) {
//					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnVoucherResponse****ERROR***START********");
//					ex.printStackTrace();
//					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**ReturnVoucherResponse****ERROR***END********");
//				}
//			}
//			
//			if (!states.contains("BILL_SUCCESS") && states.contains("PUT_VOUCHER_SUCCESS") && IsPutVoucher) {
//				try {
//					VoucherTransactionRequest putVoucherRequest1 = new VoucherTransactionRequest();
//					List<VoucherDetails> lstPutDetails1 = new ArrayList<>();
//					putVoucherRequest1.setLocationKey(log.getLocationKeyL2());
//					putVoucherRequest1.setPosKey(log.getPosKey());
//					putVoucherRequest1.setCashierKey(log.getCashierKey());
//					putVoucherRequest1.setBillKey(log.getBillKey());
//					putVoucherRequest1.setSuspend(1);
//					
//					for (int i = 0; i < voucherList.size(); i++) {
//						VoucherDetails details = new VoucherDetails();
//						if (voucherList.get(i).getVoucherAmount().compareTo(BigDecimal.ZERO) == 1) {
//							details.setAmount(voucherList.get(i).getVoucherAmount());
//							details.setVoucherId(voucherList.get(i).getVoucherCode());
//							lstPutDetails1.add(details);
//						}
//					}
//					
//					putVoucherRequest1.setDetails(lstPutDetails1);
//					putVoucher = service.PutVoucherLoyalty2(putVoucherRequest1, log);
//					if (putVoucher.getStatus() != 0 || putVoucher == null) {
//						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**PutVoucherResponse****ERROR***START***WRONG***RESULT******");
//						System.err.println(Json.toJson(putVoucher));
//						System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**PutVoucherResponse****ERROR***END***WRONG***RESULT*****");
//					}
//				} catch (Exception ex) {
//					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**PutVoucherResponse****ERROR***START********");
//					ex.printStackTrace();
//					System.err.println("*************LOYALTYINTEGRATION******TransactionVoucher**PutVoucherResponse****ERROR***END********");
//				}
//			}

			if (states != null && !states.isEmpty()) {
				logger.actionInvoiceBill("updateInvoiceBill", log.getInvoiceId(), log.getDeviceId(), states.get(states.size() - 1));
			}
		}
		return response;
	}

//	SetBillSetBill
	public boolean SetBill(LoyaltyBaseResponse response, Logger log, LogRequest request, PutResponse ebarimtResponse, CalcBonusResponse ebarimtRequest) {
		try {
			log.getRequest();
			request.getBill().setVatBillNo(ebarimtResponse.getBillId());
			request.getBill().setVatCustomerTTD(ebarimtResponse.getCustomerNo());
			request.getBill().setVatAmount(ebarimtResponse.getAmount());
			request.getBill().setIsPosted('\0');

			PutGroup putGroup = JsonHelper.toJsonObject(ebarimtRequest.getData(), PutGroup.class);
			List<Sales> lstSales = new ArrayList<>();
			for (int i = 0; i < putGroup.getLstGroup().size(); i++) {
				Sales sales = new Sales();
				for (int j = 0; j < putGroup.getLstGroup().get(i).getStocks().size(); j++) {
					sales.setItemId(putGroup.getLstGroup().get(i).getStocks().get(j).getCode());
					sales.setVatAmount(putGroup.getLstGroup().get(i).getStocks().get(j).getVat());

					for (int k = 0; k < ebarimtResponse.getLstGroup().size(); k++) {
						for (int m = 0; m < ebarimtResponse.getLstGroup().get(k).getStocks().size(); m++) {
							if (putGroup.getLstGroup().get(i).getStocks().get(j).getCode().equals(ebarimtResponse.getLstGroup().get(k).getStocks().get(m).getCode())) {
								sales.setVatBillNo(ebarimtResponse.getLstGroup().get(k).getBillId());
							}
						}
					}
					lstSales.add(sales);
				}
			}
			request.getBill().setSales(lstSales);
		} catch (Exception ex) {
			response.setStatus(StatusCode.CONVERT_ERROR.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.CONVERT_ERROR, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			return false;
		}
		return true;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean CreateBillKey(LoyaltyBaseResponse response, Logger log) {
		try {
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("DeviceId", log.getDeviceId());
			List<CalcBonusResponse> list = getByProcedure("[dbo].[CreatePkey]", parameters, CalcBonusResponse.class);
			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return false;
			}
			log.setBillKey(Long.parseLong(list.get(0).getData()));
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
			}
			return false;
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			return false;
		}
		return true;
	}

	public boolean GetDevice(LoyaltyBaseResponse response, Logger log) throws Exception {
		try {
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("DeviceId", log.getDeviceId());
			parameters.put("InvoiceId", log.getInvoiceId());
			parameters.put("Language", String.valueOf(log.getLanguage()));
//			List<Info> list = getByProcedure("[dbo].[GET_DEVICE]", parameters, Info.class); real bolgokh ued procedure solikh
			List<Info> list = getByProcedure("[dbo].[GET_DEVICE_DEVELOPING]", parameters, Info.class);
			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1 || list.get(0).getStatus() == null) {
				response.setStatus(StatusCode.DEVICE_INACTIVE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DEVICE_INACTIVE, log.getLanguage()));
				return false;
			}
			log.setInfoStatus(list.get(0).getStatus());
			log.setBillKey(list.get(0).getBillKey());
			log.setOrganizationType(list.get(0).getOrganizationType());
			log.setProduction(list.get(0).getProduction());
			log.setCardNumber(list.get(0).getCardNumber());
			log.setOrganizationKey(list.get(0).getOrganizationKey());
			log.setPosKey(list.get(0).getPosKey());
			log.setPosCode(list.get(0).getPosCode());
			log.setLocationKey(list.get(0).getLocationKey());
			log.setCashierKey(list.get(0).getCashierKey());
			log.setSectionKey(list.get(0).getSectionKey());
			log.setKeyAES(list.get(0).getKeyAES());
			log.setCardType(list.get(0).getCardType());
			log.setBillStatus(list.get(0).getBillStatus());
			log.setLocationKeyL2(list.get(0).getLocationKeyL2());
			log.setInternalId(list.get(0).getInternalId());
			log.setRegisterNo(list.get(0).getRegisterNo());
			log.setDistrictCode(list.get(0).getDistrictCode());
			log.setDeviceName(list.get(0).getDeviceName());
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
			}
			return false;
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			return false;
		}
		return true;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<InvoiceVoucherPayments> getInvoiceVoucherPaymentsList(String invoiceId, String deviceId) throws Exception {
		List<InvoiceVoucherPayments> list = new ArrayList<>();
		Map<String, Object> parameters = new HashMap<>();
		try {
			parameters.put("Type", "getInvoiceVoucherPayments");
			parameters.put("checkInvoiceId", invoiceId);
			parameters.put("DeviceId", deviceId);
			parameters.put("Language", String.valueOf(Language.MN));
			list = getByProcedure("GET_INFO", parameters, InvoiceVoucherPayments.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<InvoiceRequest> getInvoiceRequest(String invoiceId, String deviceId) throws Exception {
		List<InvoiceRequest> list = new ArrayList<>();
		Map<String, Object> parameters = new HashMap<>();
		try {
			parameters.put("Type", "getInvoiceRequest");
			parameters.put("checkInvoiceId", invoiceId);
			parameters.put("DeviceId", deviceId);
			parameters.put("Language", String.valueOf(Language.MN));
			list = getByProcedure("GET_INFO", parameters, InvoiceRequest.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<InvoiceBill> getInvoiceBillList(Logger log) throws Exception {
		List<InvoiceBill> list = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Type", "getInvoiceBill");
			parameters.put("checkInvoiceId", log.getInvoiceId());
			parameters.put("DeviceId", log.getDeviceId());
			parameters.put("Language", String.valueOf(Language.MN));
			if (log.getProduction() == 0) {
				list = getByProcedure("GET_INFO_DEVELOPING", parameters, InvoiceBill.class);
			} else {
				list = getByProcedure("GET_INFO", parameters, InvoiceBill.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalcBonusResponse getInvoiceInfo(String type, String invoiceId) throws Exception {
		CalcBonusResponse response = null;
		try {
			Map<String, Object> parameters = new HashMap<>();
			List<CalcBonusResponse> list = new ArrayList<>();
			parameters.put("Type", type);
			parameters.put("checkInvoiceId", invoiceId);
			list = getByProcedure("GET_INFO", parameters, CalcBonusResponse.class);
			if (list != null) {
				response = list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ProcessTransactionResponseData> getUpointBillData(Logger log) throws Exception {
		List<ProcessTransactionResponseData> list = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Type", "getUpointBillData");
			parameters.put("RequestID_BILL", log.getBillKey());
			parameters.put("Language", String.valueOf(log.getLanguage()));
			if (log.getProduction() == 0) {
				list = getByProcedure("GET_INFO_DEVELOPING", parameters, ProcessTransactionResponseData.class);
			} else {
				list = getByProcedure("GET_INFO", parameters, ProcessTransactionResponseData.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public LoyaltyBaseResponse CheckCardInfo(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		QrCardInfoProcedureResponse cardInfoResponse = new QrCardInfoProcedureResponse();
		Logger log = new Logger();
		log.setType("CheckCardInfo");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			LoyaltyBaseResponse getCardInfoResponse = GetCardInfo(rawData);

			if (StatusCode.SUCCESS.getValue() == getCardInfoResponse.getStatus()) {
				cardInfoResponse = JsonHelper.toJsonObject(getCardInfoResponse.getMessage(), QrCardInfoProcedureResponse.class);
				cardInfoResponse.setEmail(null);
				cardInfoResponse.setHolderByPhone(null);
				cardInfoResponse.setIsBirthDay(null);
				cardInfoResponse.setRegister(null);
				cardInfoResponse.setVatCustomerNo(null);
				cardInfoResponse.setCardAccountId(null);
				cardInfoResponse.setPinCode(null);
				cardInfoResponse.setBonusUse(null);
				cardInfoResponse.setOpenDate(null);
				cardInfoResponse.setBonusCancelDayCount(null);
				cardInfoResponse.setIsDvip(null);
				cardInfoResponse.setUseDvip(null);
				cardInfoResponse.setCalcBonusPercentPurchaseAmount(null);
				cardInfoResponse.setTimedPurchaseAmount(null);
				cardInfoResponse.setDvipBalance(null);
				cardInfoResponse.setDvipSavedBalance(null);
				cardInfoResponse.setCoinAllBalance(null);
				cardInfoResponse.setCoinMonthBalance(null);
				cardInfoResponse.setItemBonusBalance(null);
				cardInfoResponse.setAddItemBonus(null);
				cardInfoResponse.setUseItemBonus(null);
				cardInfoResponse.setItemBonusHeaderId(null);
				cardInfoResponse.setIsIdPhoneDuplicated(null);
				cardInfoResponse.setIsRequestPhoneUpdate(null);
				cardInfoResponse.setFingerPrintData(null);
				cardInfoResponse.setQrResult(null);
				cardInfoResponse.setItemBonusX2Info(null);
				cardInfoResponse.setInvoiceId(null);
				cardInfoResponse.setStatus(StatusCode.SUCCESS.getValue());
				cardInfoResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
				response.setMessage(JsonHelper.toJson(cardInfoResponse));
				response.setState(1);
				response.setStatus(1);
			} else {
				response.setMessage(getCardInfoResponse.getMessage());
				response.setStatus(getCardInfoResponse.getStatus());
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			states.add("SET_MESSAGE");
			log.setError(ex.getMessage());
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			if (cardInfoResponse != null) {
				log.setCardNumber(cardInfoResponse.getCardHolderId() != null ? cardInfoResponse.getCardHolderId() : "");
			}
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyBaseResponse GetCardInfo(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		QrCardInfoProcedureResponse cardInfoResponse = new QrCardInfoProcedureResponse();
		Logger log = new Logger();
		log.setType("GetCardInfo");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();

		CalcPaymentRequest request = new CalcPaymentRequest();
		CalcPaymentRequest requestSet = null;
		CheckInfoRequest checkInfoRequest = new CheckInfoRequest();
		checkInfoRequest.setLocationKey(100L);
		checkInfoRequest.setPosKey(100L);
		CheckInfoDetailRequest checkInfoDetailRequest = new CheckInfoDetailRequest();
		checkInfoDetailRequest.setPinCode("NominApproved");
		checkInfoDetailRequest.setMobile("");
		CheckInfoResponse checkInfoResponse = new CheckInfoResponse();
		try {
			request = JsonHelper.toJsonObject(rawData, CalcPaymentRequest.class);
			requestSet = (CalcPaymentRequest) nomin.loyaltyintegration.tools.Tools.deepClone(request);
			log.setLanguage(requestSet.getDevice().getLanguage() != null ? Language.valueOf(requestSet.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(requestSet.getDevice().getDeviceId());
			log.setRequestId(requestSet.getRequestId());
			log.setCardNumber(request.getCard().getCardNumber());

			if (!CheckDevice(requestSet, response, log))
				return response;

			if (!GetDevice(response, log)) {
				return response;
			}

			if ("2020005".equals(request.getCard().getCardNumber())) {
				requestSet.getCard().setCardNumber("112233005");
			}

			if ("2020008".equals(request.getCard().getCardNumber())) {
				requestSet.getCard().setCardNumber("112233008");
			}

			switch (requestSet.getCard().getCardType()) {
			case "Phone":
				checkInfoDetailRequest.setMobile(request.getCard().getCardNumber());
				checkInfoDetailRequest.setCardNumber("");
				requestSet.getCard().setCardNumber("999999" + requestSet.getCard().getCardNumber());
				break;
			case "NFC":
				requestSet.getCard().setCardNumber(requestSet.getCard().getCardNumber().replace(":", ""));
				checkInfoDetailRequest.setCardNumber(requestSet.getCard().getCardNumber());
				break;
			case "CardNumber":
			default:
				if (!Strings.isNullOrEmpty(request.getCard().getCardNumber())) {
					checkInfoDetailRequest.setCardNumber(requestSet.getCard().getCardNumber());
					int len = requestSet.getCard().getCardNumber().length();
					switch (len) {
					case 12:
						requestSet.getCard().setCardNumber("990000" + requestSet.getCard().getCardNumber());
						break;
					case 16:
						requestSet.getCard().setCardNumber("99" + requestSet.getCard().getCardNumber());
						break;
					}
				}
				break;
			}

			if ("NFC".equals(requestSet.getCard().getCardType())) {
				checkInfoRequest.setData(Json.toJson(checkInfoDetailRequest));
				checkInfoResponse = upointLogic.getCheckInfo(checkInfoRequest, log);
				if (checkInfoResponse != null && checkInfoResponse.getCardNumber().length() == 12) {
					requestSet.getCard().setCardNumber("990000" + checkInfoResponse.getCardNumber());
				} else if (checkInfoResponse != null) {
					requestSet.getCard().setCardNumber("99" + checkInfoResponse.getCardNumber());
				} else {
					response.setStatus(StatusCode.CARD_NOT_FOUND.getValue());
					response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, log.getLanguage()));
					return response;
				}
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("Type", "getCardInfo");
			parameters.put("CardholderId", requestSet.getCard().getCardNumber());
			parameters.put("DeviceId", requestSet.getDevice().getDeviceId());
			parameters.put("Language", String.valueOf(log.getLanguage()));

			List<QrCardInfoProcedureResponse> list = getByProcedure("GET_INFO", parameters, QrCardInfoProcedureResponse.class);
			cardInfoResponse = list.get(0);

			if (list == null || list.size() == 0 || !"SUCCESS".equals(cardInfoResponse.getQrResult()) || cardInfoResponse.getPinCode() == null) {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.CARD_NOT_FOUND, log.getLanguage()));
				response.setStatus(StatusCode.CARD_NOT_FOUND.getValue());
				return response;
			}

			if (!"NFC".equals(request.getCard().getCardType())) {
				if (TestCards.fromString(request.getCard().getCardType()) != null) {
					int uaCardLength = cardInfoResponse.getUaCardId().length();
					String uaCardId = cardInfoResponse.getUaCardId();
					if (uaCardLength < 12) {
						uaCardId = Strings.leftPad(uaCardId, 12, "0");
					} else if (uaCardLength > 12 && uaCardLength < 16) {
						uaCardId = Strings.leftPad(uaCardId, 16, "0");
					}
					checkInfoDetailRequest.setCardNumber(uaCardId);
				}
				checkInfoRequest.setData(Json.toJson(checkInfoDetailRequest));
				checkInfoResponse = upointLogic.getCheckInfo(checkInfoRequest, log);
			}

			if (checkInfoResponse != null && 0 == checkInfoResponse.getResult()) {
				cardInfoResponse.setuPointBalance(Double.parseDouble(checkInfoResponse.getUpointBalance()));
			}

			if ("112233005".equals(request.getCard().getCardNumber()) && "NominOil".equals(log.getOrganizationType())) {
				cardInfoResponse.setCardHolderId("2020005");
			}

			if ("112233008".equals(request.getCard().getCardNumber()) && "NominOil".equals(log.getOrganizationType())) {
				cardInfoResponse.setCardHolderId("2020008");
			}

			response.setMessage(JsonHelper.toJson(cardInfoResponse));
			response.setStatus(StatusCode.SUCCESS.getValue());

		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			states.add("SET_MESSAGE");
			log.setError(ex.getMessage());
		} finally {
			log.setResponseDate(new Date());
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			if (request.getCard() != null) {
				log.setCardNumber(request.getCard().getCardNumber() != null ? request.getCard().getCardNumber() : "");
			}
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				response.setMessage(response.getMessage().substring(0, 80));
			}
		}
		return response;
	}

	private String getQRCodeImage(String text, int width, int height) throws WriterException, IOException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

		ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
		byte[] pngData = pngOutputStream.toByteArray();
		String encodedString = Base64.getEncoder().encodeToString(pngData);
		return encodedString;
	}

	@Override
	public LoyaltyBaseResponse CheckPrice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("CheckPrice");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		@SuppressWarnings("unused")
		nomin.loyaltyintegration.businessentity.info.CheckPrice checkPriceResponse = new nomin.loyaltyintegration.businessentity.info.CheckPrice();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("Type", "checkPrice");
			parameters.put("QrData", "1");
			parameters.put("location", log.getLocationKey());
			parameters.put("barcode", request.getRequestId());
			parameters.put("Language", String.valueOf(log.getLanguage()));

			List<nomin.loyaltyintegration.businessentity.info.CheckPrice> list = getByProcedure("GET_INFO", parameters, nomin.loyaltyintegration.businessentity.info.CheckPrice.class);
			checkPriceResponse = list.get(0);

			if (list == null || list.size() == 0 || list.get(0).getStatus() != 1) {
				response.setStatus(StatusCode.DATABASE_INCORRECT_VALUE.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.DATABASE_INCORRECT_VALUE, log.getLanguage()));
				return response;
			}

			response.setMessage(Json.toJson(list.get(0)));
			response.setState(1);
			response.setStatus(1);
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
		} finally {
			log.setRequest(rawData);
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (response.getStatus() != 1 && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse GetBillImage(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("GetBillImage");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		ReportResponse billImageResponse = new ReportResponse();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());
			log.setRequestId(request.getRequestId());
			log.setInvoiceId(request.getInvoiceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (log.getInvoiceId() == null) {
				response.setStatus(StatusCode.FAIL.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage())));
				return response;
			}

			billImageResponse = service.GetBillImage(request.getBillImage(), log);

			if (billImageResponse.getResponse().get(0) != null) {
				BillImageResponse sendBillImageResponse = new BillImageResponse();
				sendBillImageResponse.setStatus(StatusCode.SUCCESS.getValue());
				sendBillImageResponse.setBillImageExt("bmp");
				sendBillImageResponse.setBillImage(Json.toJson(billImageResponse.getResponse().get(0)).replace("\"", ""));
				sendBillImageResponse.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));

				response.setMessage(JsonHelper.toJson(sendBillImageResponse));
				response.setStatus(1);
				response.setState(1);
			} else {
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.FAIL, log.getLanguage()));
				response.setStatus(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
		} finally {
			log.setRequest(rawData);
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (response.getStatus() != 1 && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public GenericResponse CheckRegisterNo(String rawData) throws Exception {
		GenericResponse result = new GenericResponse();
		CheckRegisterResponse response = new CheckRegisterResponse();
		LogRequest request = new LogRequest();
		result.setStatusCode(200);
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			if (Strings.isNullOrEmpty(request.getRequestId())) {
				response.setStatus(101);
				response.setMessage("Body талбарт параметер дамжуулаагүй байна!");
			} else {
				response = service.CheckRegisterNo(request.getRequestId());
			}
		} catch (Exception e) {
			response.setStatus(400);
			response.setMessage("Алдаа гарлаа. " + e.getMessage());
		} finally {
			try {
				String responseData = JsonHelper.toJson(response);
				result.setResultData(responseData);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public LoyaltyBaseResponse QpayCreateInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("QpayCreate");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getqPayBillCreateRequest().getAmount() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "qpayCreate"));
			} else {
				if (!CreateBillKey(response, log)) {
					return response;
				}

				QPayBillCreateRequest qPayBillCreateRequest = new QPayBillCreateRequest();
				qPayBillCreateRequest.setOrderNo(log.getBillKey().toString());
				qPayBillCreateRequest.setAmount(request.getqPayBillCreateRequest().getAmount());
				qPayBillCreateRequest.setCustomerQr(request.getqPayBillCreateRequest().getCustomerQr() == null ? request.getqPayBillCreateRequest().getCustomerQr() : "");
				qPayBillCreateRequest.setLocationKey(log.getLocationKey());
				qPayBillCreateRequest.setPosKey(log.getPosKey());
				qPayBillCreateRequest.setCashierKey(log.getCashierKey());
				log.setInvoiceId(log.getBillKey().toString());
				log.setBillKey(null);
				setResult.setInvoiceId(log.getInvoiceId());
				setResult.setCheckInfo(null);
				QPayBillCreateResponse qPayBillCreateResponse = service.QpayCreate(qPayBillCreateRequest);

				if (qPayBillCreateResponse == null || qPayBillCreateResponse.getQrCode() == null) {
					setResult.setData(JsonHelper.toJson(qPayBillCreateResponse));
					setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage()));
					response.setStatus(StatusCode.SERVER_RESULT.getValue());
				} else {
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					setResult.setData(JsonHelper.toJson(qPayBillCreateResponse));
					setResult.setCheckInfo(qPayBillCreateResponse.getId());
					setResult.setQrDataImage(getQRCodeImage(qPayBillCreateResponse.getQrCode(), 600, 600));
					setResult.setQrDataImageExt("png");
					response.setStatus(StatusCode.SUCCESS.getValue());
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse QpayCancelInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("QpayCancel");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getqPayPaymentCancelRequest().getInvoiceId() == null || request.getqPayPaymentCancelRequest().getCheckInfo() == null || request.getqPayPaymentCancelRequest().getAmount() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "qpayCancel"));
			} else {
				log.setInvoiceId(request.getqPayPaymentCancelRequest().getInvoiceId());
				nomin.loyaltyintegration.businessentity.qpay.payment.cancel.QPayPaymentCancelRequest qPayPaymentCancelRequest = new nomin.loyaltyintegration.businessentity.qpay.payment.cancel.QPayPaymentCancelRequest();
				qPayPaymentCancelRequest.setId(request.getqPayPaymentCancelRequest().getCheckInfo());
				qPayPaymentCancelRequest.setLocationKey(log.getLocationKey());
				qPayPaymentCancelRequest.setPosKey(log.getPosKey());
				qPayPaymentCancelRequest.setCashierKey(log.getCashierKey());
				qPayPaymentCancelRequest.setOrderNo(request.getqPayPaymentCancelRequest().getInvoiceId());
				qPayPaymentCancelRequest.setAmount(request.getqPayPaymentCancelRequest().getAmount());

				setResult.setInvoiceId(log.getInvoiceId());
				setResult.setBillKey(null);
				setResult.setCheckInfo(null);
				QPayResponse qPayResponse = service.QpayCancel(qPayPaymentCancelRequest);

				if (qPayResponse != null && "PAYMENT_CANCELED".equals(qPayResponse.getName())) {
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					setResult.setData(JsonHelper.toJson(qPayResponse));
					response.setStatus(StatusCode.SUCCESS.getValue());
				} else {
					setResult.setData(JsonHelper.toJson(qPayResponse));
					setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage()));
					response.setStatus(StatusCode.SERVER_RESULT.getValue());
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse QpayCheckInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("QpayCheckInvoice");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getqPayPaymentCheckRequest().getInvoiceId() == null || request.getqPayPaymentCheckRequest().getRequestDate() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "qpayCheck"));
			} else {
				nomin.loyaltyintegration.businessentity.qpay.check.pos.QPayPaymentCheckRequest qPayPaymentCheckRequest = new nomin.loyaltyintegration.businessentity.qpay.check.pos.QPayPaymentCheckRequest();
				qPayPaymentCheckRequest.setLocationKey(log.getLocationKey());
				qPayPaymentCheckRequest.setPosKey(log.getPosKey());
				qPayPaymentCheckRequest.setLocationKey(log.getCashierKey());
				qPayPaymentCheckRequest.setOrderNo(request.getqPayPaymentCheckRequest().getInvoiceId());
				qPayPaymentCheckRequest.setAmount(request.getqPayPaymentCheckRequest().getAmount());
				qPayPaymentCheckRequest.setRequestDate(request.getqPayPaymentCheckRequest().getRequestDate());

				nomin.loyaltyintegration.businessentity.qpay.data.PaymentInfo paymentInfo = service.QpayCheck(qPayPaymentCheckRequest);
				if (paymentInfo != null && "PAID".equals(paymentInfo.getPaymentStatus())) {
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					setResult.setData(JsonHelper.toJson(paymentInfo));
					response.setStatus(StatusCode.SUCCESS.getValue());
				} else {
					if (paymentInfo != null && "NOT_PAID".equals(paymentInfo.getPaymentStatus())) {
						setResult.setData(JsonHelper.toJson(paymentInfo));
						setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
						setResult.setMessage("Хүсэлт амжилтгүй байна! Нэхэмжлэх төлөгдөөгүй байна.");
						response.setStatus(StatusCode.SERVER_RESULT.getValue());
					} else {
						setResult.setData(JsonHelper.toJson(paymentInfo));
						setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
						setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage(), paymentInfo.getPaymentStatus()));
						response.setStatus(StatusCode.SERVER_RESULT.getValue());
					}
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse SocialPayCreateInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("SocialCreate");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getSocialPayBillCreateRequest().getAmount() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "socailPayCreate"));
			} else {
				if (!CreateBillKey(response, log)) {
					return response;
				}

				SocialPayBillCreateRequest socialPayBillCreateRequest = new SocialPayBillCreateRequest();
				socialPayBillCreateRequest.setAmount(request.getSocialPayBillCreateRequest().getAmount());
				socialPayBillCreateRequest.setPhoneNo(request.getSocialPayBillCreateRequest().getPhoneNo());
				socialPayBillCreateRequest.setOrderNo(log.getBillKey().toString());
				socialPayBillCreateRequest.setLocationKey(log.getLocationKey());
				socialPayBillCreateRequest.setPosKey(log.getPosKey());
				socialPayBillCreateRequest.setCashierKey(log.getCashierKey());
				setResult.setInvoiceId(log.getBillKey().toString());
				setResult.setBillKey(null);
				setResult.setCheckInfo(null);

				SocialPayResponseBody socialPayResponseBody = service.SocialPayCreate(socialPayBillCreateRequest);
				if (socialPayResponseBody.getError() == null) {
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					setResult.setData(socialPayResponseBody.getResponse().getDesc());
					setResult.setQrDataImage(getQRCodeImage(socialPayResponseBody.getResponse().getDesc(), 600, 600));
					setResult.setQrDataImageExt("png");
					response.setStatus(StatusCode.SUCCESS.getValue());
				} else {
					setResult.setData(JsonHelper.toJson(socialPayResponseBody));
					setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage(), socialPayResponseBody.getError().getErrorDesc()));
					response.setStatus(StatusCode.SERVER_RESULT.getValue());
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse SocialPayCancelInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("SocialCancel");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getSocialPayPaymentCancelRequest().getInvoiceId() == null || request.getSocialPayPaymentCancelRequest().getAmount() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "socailPayCreate"));
			} else {
				SocialPayPaymentCancelRequest socialPayPaymentCancelRequest = new SocialPayPaymentCancelRequest();
				socialPayPaymentCancelRequest.setOrderNo(request.getSocialPayPaymentCancelRequest().getInvoiceId());
				socialPayPaymentCancelRequest.setAmount(request.getSocialPayPaymentCancelRequest().getAmount());
				socialPayPaymentCancelRequest.setLocationKey(log.getLocationKey());
				socialPayPaymentCancelRequest.setPosKey(log.getPosKey());
				setResult.setInvoiceId(null);
				setResult.setBillKey(null);
				setResult.setCheckInfo(null);

				SocialPayResponseBody socialPayResponseBody = service.SocialPayCancel(socialPayPaymentCancelRequest);
				if (socialPayResponseBody.getError() == null && "SUCCESS".equals(socialPayResponseBody.getResponse().getStatus())) {
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					setResult.setData(socialPayResponseBody.getResponse().getDesc());
					response.setStatus(StatusCode.SUCCESS.getValue());
				} else {
					setResult.setData(JsonHelper.toJson(socialPayResponseBody));
					setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage(), socialPayResponseBody.getError().getErrorDesc()));
					response.setStatus(StatusCode.SERVER_RESULT.getValue());
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

	@Override
	public LoyaltyBaseResponse SocialPayCheckInvoice(String rawData) throws Exception {
		LoyaltyBaseResponse response = new LoyaltyBaseResponse();
		CalcBonusResponse setResult = new CalcBonusResponse();
		LogRequest request = new LogRequest();
		Logger log = new Logger();
		log.setType("SocialCheck");
		log.setRequest(rawData);
		log.setRequestDate(new Date());
		List<String> states = new ArrayList<>();
		try {
			request = JsonHelper.toJsonObject(rawData, LogRequest.class);
			log.setLanguage(request.getDevice().getLanguage() != null ? Language.valueOf(request.getDevice().getLanguage()) : Language.MN);
			log.setDeviceId(request.getDevice().getDeviceId());

			if (!GetDevice(response, log)) {
				return response;
			}

			if (request.getSocialPayPaymentCheckRequest().getAmount() == null || request.getSocialPayPaymentCheckRequest().getInvoiceId() == null) {
				response.setStatus(StatusCode.REQUEST_INCORRECT.getValue());
				response.setMessage(MessageHelper.getStatusMessage(StatusCode.REQUEST_INCORRECT, log.getLanguage(), "socialPayCheck"));
			} else {
				log.setInvoiceId(request.getSocialPayPaymentCheckRequest().getOrderNo());
				SocialPayPaymentCheckRequest socialPayPaymentCheckRequest = new SocialPayPaymentCheckRequest();
				socialPayPaymentCheckRequest.setOrderNo(request.getSocialPayPaymentCheckRequest().getInvoiceId());
				socialPayPaymentCheckRequest.setAmount(request.getSocialPayPaymentCheckRequest().getAmount());
				socialPayPaymentCheckRequest.setLocationKey(log.getLocationKey());
				socialPayPaymentCheckRequest.setPosKey(log.getPosKey());
				socialPayPaymentCheckRequest.setCashierKey(log.getCashierKey());
				socialPayPaymentCheckRequest.setRequestDate(request.getSocialPayPaymentCheckRequest().getRequestDate());

				SocialPayPaymentCheckResponseBody socialPayPaymentCheckResponseBody = service.SocialPayCheck(socialPayPaymentCheckRequest);
				setResult.setInvoiceId(request.getSocialPayPaymentCheckRequest().getInvoiceId());
				setResult.setBillKey(null);
				setResult.setCheckInfo(null);
				if (socialPayPaymentCheckResponseBody.getError() == null) {
					setResult.setData(JsonHelper.toJson(socialPayPaymentCheckResponseBody));
					setResult.setStatus(StatusCode.SUCCESS.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
					response.setStatus(StatusCode.SUCCESS.getValue());

				} else {
					setResult.setData(JsonHelper.toJson(socialPayPaymentCheckResponseBody));
					setResult.setStatus(StatusCode.SERVER_RESULT.getValue());
					setResult.setMessage(MessageHelper.getStatusMessage(StatusCode.SERVER_RESULT, log.getLanguage(), socialPayPaymentCheckResponseBody.getError().getErrorDesc()));
					response.setStatus(StatusCode.SERVER_RESULT.getValue());
				}
				response.setMessage(JsonHelper.toJson(setResult));
				response.setState(1);
			}
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
						log.setError(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, log.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			log.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, log.getLanguage()), ex.getMessage()));
			log.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(rawData);
			log.setStatus(response.getStatus().toString());
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(log);
			if (states.contains("SET_MESSAGE") && response.getMessage().length() > 80)
				response.setMessage(response.getMessage().substring(0, 80));
		}
		return response;
	}

}
