package nomin.loyaltyintegration.businesslogic;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.eclipse.persistence.exceptions.DatabaseException;

import nomin.core.logic.BaseLogic;
import nomin.core.utils.Json;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.businesslogic.interfaces.IVoucherLogic;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.payment.Voucher;
import nomin.loyaltyintegration.entity.payment.VoucherPaymentResponse;
import nomin.loyaltyintegration.enums.StatusCode;
import nomin.loyaltyintegration.helper.MessageHelper;

@Stateless(name = "VoucherLogic")
public class VoucherLogic extends BaseLogic implements IVoucherLogic, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject ITransactionLog logger;

	@PersistenceContext(unitName = "LoyaltyIntegration")
	EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	// Воучир шалгах ашиглах
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public VoucherPaymentResponse actionVoucher(Voucher voucher, String type, Logger log) {
		VoucherPaymentResponse response = new VoucherPaymentResponse();
		Logger saveLog = (Logger) nomin.loyaltyintegration.tools.Tools.deepClone(log);
		saveLog.setType("Voucher-L1");
		saveLog.setRequest(Json.toJson(voucher));
		saveLog.setResponseDate(new Date());
		saveLog.setType(type);
		saveLog.setBillKey(voucher.getBillKey());
		response.setVoucherCode(voucher.getVoucherCode());
		List<String> states = new ArrayList<>();
		try {
			if ("CHECKVOUCHER".equals(type)) {
				voucher.setVoucherCode("SUSPENDC" + voucher.getVoucherCode());
			} else if ("SUSPEND+".equals(type)) {
				voucher.setVoucherCode("SUSPEND+" + voucher.getVoucherCode());
			} else if ("SUSPEND-".equals(type)) {
				voucher.setVoucherCode("SUSPEND-" + voucher.getVoucherCode());
			} else if ("CONFIRM".equals(type)) {
				voucher.setVoucherCode(voucher.getVoucherCode());
			}

			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("voucherCode", voucher.getVoucherCode());
			parameters.put("xml", voucher.getXml());
			parameters.put("billKey", voucher.getBillKey());
			parameters.put("type", "voucher");
			parameters.put("DeviceId", log.getDeviceId());
			List<VoucherPaymentResponse> result = getByProcedure("[dbo].[ACTION_INFO]", parameters, VoucherPaymentResponse.class);

			response = result.get(0);
			response.setStatus(1);
			response.setMessage(MessageHelper.getStatusMessage(StatusCode.SUCCESS, log.getLanguage()));
		} catch (PersistenceException ex) {
			if (ex.getCause() instanceof DatabaseException) {
				DatabaseException dbException = (DatabaseException) ex.getCause();
				SQLException sqlException = (SQLException) dbException.getCause();
				String errorMessage = sqlException.getMessage();
				if (sqlException.getErrorCode() == 50000) {
					String[] allMessage = errorMessage.split(";");
					if (allMessage.length > 1) {
						String statusCode = allMessage[0];
						String message = allMessage[1];
						response.setStatus(Integer.parseInt(statusCode));
						response.setMessage(message);
					} else {
						response.setStatus(StatusCode.DATABASE_ERROR.getValue());
						response.setMessage(errorMessage);
					}
				} else {
					response.setStatus(StatusCode.DATABASE_ERROR.getValue());
					response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, saveLog.getLanguage()), errorMessage));
					states.add("SET_MESSAGE");
				}
			} else {
				response.setStatus(StatusCode.DATABASE_ERROR.getValue());
				response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.DATABASE_ERROR, saveLog.getLanguage()), ex.getMessage()));
				states.add("SET_MESSAGE");
			}
			saveLog.setError(ex.getMessage());
		} catch (Exception ex) {
			response.setStatus(StatusCode.RUNTIME_EXCEPTION.getValue());
			response.setMessage(String.format(MessageHelper.getStatusMessage(StatusCode.RUNTIME_EXCEPTION, saveLog.getLanguage()), ex.getMessage()));
			saveLog.setError(ex.getMessage());
			states.add("SET_MESSAGE");
		} finally {
			log.setRequest(Json.toJson(voucher));
			log.setResponse(Json.toJson(response));
			log.setResponseDate(new Date());
			logger.log(saveLog);
			if(states.contains("SET_MESSAGE") && response.getMessage().length() > 80) {
				log.setError(response.getMessage());
			}
		}
		return response;
	}

}
