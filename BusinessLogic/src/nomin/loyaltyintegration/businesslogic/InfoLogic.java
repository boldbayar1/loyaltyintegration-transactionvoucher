package nomin.loyaltyintegration.businesslogic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import nomin.core.logic.BaseLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.IInfoLogic;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.entity.GeneralConfigs;
import nomin.loyaltyintegration.helper.JsonHelper;

@Stateless(name = "InfoLogic")
public class InfoLogic extends BaseLogic implements IInfoLogic, Serializable {

	private static final long serialVersionUID = 1L;
	private static final String UNIT_NAME = "LoyaltyIntegration";
	public static final ObjectMapper objectMapper = JsonHelper.objectMapperStatic;

	@Inject
	ITransactionLog logger;

	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public List<GeneralConfigs> GetInfo() throws Exception {
		List<GeneralConfigs> result = null;
		try {
			StringBuilder jpql = new StringBuilder();
			jpql.append("SELECT NEW nomin.loyaltyintegration.entity.GeneralConfigs(")
					.append("a.pKey, a.application, a.type, a.name, a.url, a.name, a.jsonValue, a.result, a.imageUrl, a.token, a.terminal, "
							+ "a.key, a.auth, a.basicAuthU, a.basicAuthP" + "")
					.append(") FROM GeneralConfigs a").append(" ORDER BY a.pKey ASC");
			result = getByQuery(GeneralConfigs.class, jpql.toString(), new HashMap<String, Object>());
		} catch (Exception ex) {
			System.err.println("*************LOYALTYINTEGRATION******GetInfo******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******GetInfo*******END******");
		}
		return result;
	}

	@Override
	public GeneralConfigs GetInfo(Long pkey) throws Exception {
		GeneralConfigs result = null;
		try {
			List<GeneralConfigs> resultValue = getByField(GeneralConfigs.class, "pkey", pkey);
			result = resultValue != null && !resultValue.isEmpty() ? resultValue.get(0) : null;
		} catch (Exception ex) {
			System.err.println("*************LOYALTYINTEGRATION******GetInfo******pkey******START********");
			ex.printStackTrace();
			System.err.println("*************LOYALTYINTEGRATION******GetInfo******pkey*******END******");
		}
		return result;
	}

}
