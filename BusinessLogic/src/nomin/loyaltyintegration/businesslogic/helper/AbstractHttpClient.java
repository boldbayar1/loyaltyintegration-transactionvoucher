package nomin.loyaltyintegration.businesslogic.helper;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import nomin.loyaltyintegration.businesslogic.helper.Helper;
import nomin.loyaltyintegration.businesslogic.helper.StringHelper;

public abstract class AbstractHttpClient {

    public abstract String getApiUrl();

    protected void checkError(HttpURLConnection connection) throws Exception {
        if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            try (InputStream inputStream = new BufferedInputStream(connection.getErrorStream())) {
                if(inputStream != null)
                    throw new Exception(StringHelper.read(inputStream));
            }
        }
    }

    protected <T> T readResponse(HttpURLConnection connection, Class<T> type) throws Exception {
        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            try (InputStream inputStream = connection.getInputStream()) {
                if (type == String.class) {
                    return type.cast(StringHelper.read(inputStream));
                } else {
                    return Helper.read(inputStream, type);
                }
            }
        }
        return null;
    }
    
    protected <T> T readResponse(String response, Class<T> type) throws Exception {
    	return Helper.read(response, type);
    }

    protected <T extends Object> List<T> readListResponse(HttpURLConnection connection, Class<T> type) throws Exception {
        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            try (InputStream inputStream = connection.getInputStream()) {
                return Helper.readList(inputStream, type);
            }
        }
        return null;
    }

    protected void writeRequest(HttpURLConnection connection, Object request) throws Exception {
        connection.setDoOutput(true);
        try (OutputStream outputStream = connection.getOutputStream()) {
            Helper.write(outputStream, request);
        }
    }
    
    protected void writeRequestString(HttpURLConnection connection, String request) throws Exception {
    	connection.setDoOutput(true);
        try (OutputStream outputStream = connection.getOutputStream()) {
            outputStream.write(request.getBytes("utf-8"));
        }
	}
    
    protected String getRequest(Object request) throws Exception {
    	return Helper.getString(request);
    }
    
    protected void writeRequestParameter(HttpURLConnection connection, Map<String, String> params) throws Exception {
    	connection.setDoOutput(true);
    	String postData = getRequestParameter(params);
        try (OutputStream outputStream = connection.getOutputStream()) {
            outputStream.write(postData.toString().getBytes("UTF-8"));
        }
    }
    
    private String getRequestParameter(Map<String, String> params) throws Exception {
    	StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, String> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        return postData.toString();
    }
}