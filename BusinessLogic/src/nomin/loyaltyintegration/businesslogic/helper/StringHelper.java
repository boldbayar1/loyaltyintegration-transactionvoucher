package nomin.loyaltyintegration.businesslogic.helper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class StringHelper {
	public static String read(InputStream inputStream) throws Exception {
		if (inputStream == null) return null;
		try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			int read = 0;
			int length = 4096;
			byte[] bytes = new byte[length];
			while ((read = inputStream.read(bytes, 0, length)) > -1) {
				outputStream.write(bytes, 0, read);
			}
			return outputStream.toString("utf-8");
		}
	}
	
	public static boolean isNullOrEmpty(String str) {
		return str == null || str.isEmpty();
	}
}