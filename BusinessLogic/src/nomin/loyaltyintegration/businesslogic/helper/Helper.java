package nomin.loyaltyintegration.businesslogic.helper;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public static void write(OutputStream outputStream, Object value) throws Exception {
		objectMapper.writeValue(outputStream, value);
	}
	
	public static byte[] getBytes(Object object) throws Exception {
		return objectMapper.writeValueAsBytes(object);
	}
	
	public static String getString(Object object) throws Exception {
		return objectMapper.writeValueAsString(object);
	}
	
	public static <T> T read(InputStream inputStream, Class<T> type) throws Exception {
		return objectMapper.readValue(inputStream, type);
	}
	
	public static <T> T read(String json, Class<T> type) throws Exception {
		return objectMapper.readValue(json, type);
	}
	
	public static <T> List<T> readList(InputStream inputStream, Class<T> type) throws Exception {
		return objectMapper.readValue(inputStream, objectMapper.getTypeFactory().constructCollectionType(List.class, type));
	}
}
