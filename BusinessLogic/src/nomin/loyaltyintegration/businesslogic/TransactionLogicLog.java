package nomin.loyaltyintegration.businesslogic;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import nomin.core.logic.BaseLogic;
import nomin.core.utils.Json;
import nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog;
import nomin.loyaltyintegration.businesslogic.interfaces.ITransactionLog;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.enums.Language;
import nomin.pos.common.helper.JsonHelper;

@Stateless(name = "TransactionLogicLog")
public class TransactionLogicLog extends BaseLogic implements ITransactionLog, Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "LoyaltyIntegration")
	EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	protected Exception getDatabaseException(Exception ex) {
		if (ex == null)
			return null;
		Throwable t = ex.getCause();
		if (t == null)
			return ex;
		if (t instanceof SQLException) {
			return (SQLException) t;
		} else if (t instanceof PersistenceException) {
			return getDatabaseException((PersistenceException) t);
		} else if (t.getClass().getName() != null && t.getClass().getName().equals("org.eclipse.persistence.exceptions.DatabaseException")) {
			return getDatabaseException((Exception) t);
		} else {
			return ex;
		}
	}

	// LOGGER хөтлөж байгаа
	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void log(Logger log) {
		try {
			log.setTransactionDate(new Date());
			insert(log);
		} catch (Exception e) {
			System.err.println("______________SAVE_LOYALTY_INTEGRATION_LOG_FAILED_START______________");
			Exception ex = getDatabaseException(e);
			System.err.println(ex.getMessage());
			try {
				System.err.println(Json.toJson(log));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			System.err.println("______________SAVE_LOYALTY_INTEGRATION_LOG_FAILED_END________________");
		}
	}

	// actionInvoiceBill хөтлөж байгаа 
	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actionInvoiceBill(String type, String invoiceId, String deviceId, String updateInfo) {
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Type", type);
			parameters.put("InvoiceId", invoiceId);
			parameters.put("DeviceId", deviceId);
			parameters.put("Language", String.valueOf(Language.MN));
			if ("updateInvoiceBill".equals(type)) {
				parameters.put("UpdateInfo", updateInfo);
			}
			execProcedure("ACTION_INFO", parameters);
		} catch (Exception e) {
			System.err.println("______________actionInvoiceBill_FAILED_START______________");
			e.printStackTrace();
			System.err.println("______________actionInvoiceBill_FAILED_END________________");
		}
	}

	// PG_LOG хөтлөж байгаа
	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveTransaction(PaymentLog paymentLog) {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(paymentLog.getRequestTime());
			calendar.set(Calendar.MILLISECOND, 0);
			Date requestTime = calendar.getTime();
			calendar.setTime(paymentLog.getResponseTime());
			calendar.set(Calendar.MILLISECOND, 0);
			Date responseTime = calendar.getTime();

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Type", "saveWork");
			parameters.put("PG_NAME", paymentLog.getPgName());
			parameters.put("REFERENCENO", paymentLog.getReferenceNo());
			parameters.put("ID_LOCATION", paymentLog.getLocationKey());
			parameters.put("ID_POS", paymentLog.getPosKey());
			parameters.put("ID_CASHIER", paymentLog.getCashierKey());
			parameters.put("AMOUNT", paymentLog.getAmount());
			parameters.put("REQUESTTIME", requestTime);
			parameters.put("REQUEST", paymentLog.getRequest());
			parameters.put("RESPONSETIME", responseTime);
			parameters.put("RESPONSE", paymentLog.getResponse());
			parameters.put("ERRORMSG", paymentLog.getErrorMessage());
			execProcedure("ACTION_INFO", parameters);
		} catch (Exception e) {
			System.out.println("______________SAVEWORK_FAILED_START______________");
			Exception ex = getDatabaseException(e);
			System.out.println(ex.getMessage());
			try {
				System.out.println(JsonHelper.getString(paymentLog));
			} catch (Exception e1) {
			}
			System.out.println("______________SAVEWORK_FAILED_END________________");
		}
	}

	// actionUpointBillData буцаалтийг тэмдэглэж байгаа
	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actionUpointBillData(String type, Logger logger, String billKey, String updateInfo) {
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Type", type);
			parameters.put("InvoiceId", logger.getInvoiceId());
			parameters.put("billKey", billKey);
			parameters.put("Language", String.valueOf(Language.MN));
			parameters.put("UpdateInfo", updateInfo);
			execProcedure("ACTION_INFO", parameters);
		} catch (Exception e) {
			System.err.println("______________actionInvoiceBill_FAILED_START______________");
			e.printStackTrace();
			System.err.println("______________actionInvoiceBill_FAILED_END________________");
		}
	}
}
