package nomin.loyaltyintegration.businesslogic;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptMD5 {

	String password = "mypassword";
	String salt = "Random$SaltValue#WithSpecialCharacters12@$@4&#%^$*";
	//String hash = decryptPassword(password + salt);
	public static String encryptPassword(String input) {
			String md5 = null;
			
		if(null == input) return null;
		
		try {
			
		//Create MessageDigest object for MD5
		MessageDigest digest = MessageDigest.getInstance("MD5");
		
		//Update input string in message digest
		digest.update(input.getBytes(), 0, input.length());

		//Converts message digest value in base 16 (hex) 
		md5 = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return md5;
	}
	
	 public byte[] encrypt(String message) throws Exception {
	        final MessageDigest md = MessageDigest.getInstance("md5");
	        final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
	                .getBytes("utf-8"));
	        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
	        for (int j = 0, k = 16; j < 8;) {
	            keyBytes[k++] = keyBytes[j++];
	        }

	        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
	        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
	        final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

	        final byte[] plainTextBytes = message.getBytes("utf-8");
	        final byte[] cipherText = cipher.doFinal(plainTextBytes);
	        // final String encodedCipherText = new sun.misc.BASE64Encoder()
	        // .encode(cipherText);

	        return cipherText;
	    }

	    public String decrypt(byte[] message) throws Exception {
	        final MessageDigest md = MessageDigest.getInstance("md5");
	        final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
	                .getBytes("utf-8"));
	        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
	        for (int j = 0, k = 16; j < 8;) {
	            keyBytes[k++] = keyBytes[j++];
	        }

	        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
	        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
	        final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	        decipher.init(Cipher.DECRYPT_MODE, key, iv);

	        // final byte[] encData = new
	        // sun.misc.BASE64Decoder().decodeBuffer(message);
	        final byte[] plainText = decipher.doFinal(message);

	        return new String(plainText, "UTF-8");
	    }
	
	
}
