package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoRequest;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.card.info.CardInfoProcedureResponse;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;
import nomin.loyaltyintegration.entity.card.info.CardInfoResponse;
import nomin.loyaltyintegration.entity.card.info.register.UACardResponse;
import nomin.loyaltyintegration.entity.card.info.register.UAConsumerResponse;

public interface ICardLogic {
	
	CardInfoResponse getCardInfo2(CardInfoRequest request, Logger log);
	CardInfoProcedureResponse registerCard(UACardResponse uaCardResponse, UAConsumerResponse uaConsumerResponse, Logger log) throws Exception;
	CardInfoResponse getCardInfo3(CardInfoRequest request, Logger log) throws Exception;

	CheckInfoRequest getCheckInfoRequest(Logger logger, String uaCardNumber);
}
