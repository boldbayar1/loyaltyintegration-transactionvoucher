package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.businessentity.upoint.request.CheckInfoRequest;
import nomin.loyaltyintegration.businessentity.upoint.response.CheckInfoResponse;
import nomin.loyaltyintegration.businessentity.upoint.response.ProcessTransactionResponseData;
import nomin.loyaltyintegration.businessentity.upoint.response.cancel.CancelTransactionResponseData;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.card.info.CancelTransactionRequest;
import nomin.loyaltyintegration.entity.card.info.UPointTransactionRequest;
import nomin.loyaltyintegration.entity.pos.PosResponse;

public interface IUPointLogicLocal {

	// upoint service
	PosResponse getCardInfo(String rawData, Logger log);

	// upoint service checkInfo
	public CheckInfoResponse getCheckInfo(CheckInfoRequest request, Logger log) throws Exception;
	public ProcessTransactionResponseData upointProcessTransaction(UPointTransactionRequest request, Logger log) throws Exception;
	public CancelTransactionResponseData upointCancelTransaction(CancelTransactionRequest request, Logger log) throws Exception;
}
