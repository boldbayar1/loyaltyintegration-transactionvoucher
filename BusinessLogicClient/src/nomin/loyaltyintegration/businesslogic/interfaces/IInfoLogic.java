package nomin.loyaltyintegration.businesslogic.interfaces;

import java.util.List;
import nomin.loyaltyintegration.entity.GeneralConfigs;

public interface IInfoLogic {

	List<GeneralConfigs> GetInfo() throws Exception;
	GeneralConfigs GetInfo(Long pkey) throws Exception;
}
