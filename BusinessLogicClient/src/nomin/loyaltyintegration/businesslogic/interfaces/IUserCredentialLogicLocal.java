package nomin.loyaltyintegration.businesslogic.interfaces;

import javax.ejb.Local;

import nomin.loyaltyintegration.businessentity.credential.User;

@Local
public interface IUserCredentialLogicLocal {
	public User login(String username, String password) throws Exception;
}