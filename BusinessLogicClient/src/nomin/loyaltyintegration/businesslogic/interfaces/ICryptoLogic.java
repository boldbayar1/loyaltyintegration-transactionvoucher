package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.businessentity.card.CardJwtClaim;
import nomin.loyaltyintegration.entity.card.info.CardInfoRequest;

public interface ICryptoLogic {
	
	// AES
	String encryptAES(String strToEncrypt, String secret);
	String decryptAES(String strToDecrypt, String secret);
	
	// jwt
	String encryptCardJWT(CardJwtClaim claim);
	CardInfoRequest decryptCardJWT(String strToDecrypt);
}
