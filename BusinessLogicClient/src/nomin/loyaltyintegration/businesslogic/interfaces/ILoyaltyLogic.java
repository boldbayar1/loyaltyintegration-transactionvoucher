package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.businessentity.GenericResponse;
import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;

public interface ILoyaltyLogic {

	// тооцоолол хийх
	public LoyaltyBaseResponse CalcBonus(String rawData, Integer isEbarimt) throws Exception;
	public LoyaltyBaseResponse CalcBonusExchange(String rawData) throws Exception;
	
	// бонус бүртгэл
	public LoyaltyBaseResponse PutBonus(String rawData) throws Exception;
	public LoyaltyBaseResponse PutBonusChange(String rawData) throws Exception;

	// бонус тооцолол буцаалт хийх
	public LoyaltyBaseResponse ReturnTransaction(String rawData) throws Exception;
	
	// билл болгох
	public LoyaltyBaseResponse PutTransaction(String rawData, int isBonus) throws Exception;
//	public LoyaltyBaseResponse PutTransactionMobile(String rawData) throws Exception;
	
	// Qr картын мэдээлэл авах
	public GenericResponse GetQR(String rawData) throws Exception;
	public GenericResponse ReadQR(String rawData) throws Exception;
	public GenericResponse CheckQR(String rawData) throws Exception;
	
	// Loyalty2 voucher гүйлгээ
	public LoyaltyBaseResponse CheckVoucher(String rawData) throws Exception;
	public LoyaltyBaseResponse PutVoucher(String rawData) throws Exception;
	public LoyaltyBaseResponse PutDirectVoucher(String rawData) throws Exception;
	public LoyaltyBaseResponse TransactionVoucher(String rawData) throws Exception;
	public LoyaltyBaseResponse ReturnVoucher(String rawData) throws Exception;
	
	// Картын мэдээлэл авчрах
	public LoyaltyBaseResponse CheckCardInfo(String rawData) throws Exception;
	public LoyaltyBaseResponse GetCardInfo(String rawData) throws Exception;
	
	// CheckPrice
	public LoyaltyBaseResponse CheckPrice(String rawData) throws Exception;
	
	// GetBillImage
	public LoyaltyBaseResponse GetBillImage(String rawData) throws Exception;
	
	// CheckRegisterNo
	public GenericResponse CheckRegisterNo(String rawData) throws Exception;
	
	// QPay
	public LoyaltyBaseResponse QpayCheckInvoice(String rawData) throws Exception;
	public LoyaltyBaseResponse QpayCreateInvoice(String rawData) throws Exception;
	public LoyaltyBaseResponse QpayCancelInvoice(String rawData) throws Exception;
	
	// SocialPay
	public LoyaltyBaseResponse SocialPayCreateInvoice(String rawData) throws Exception;
	public LoyaltyBaseResponse SocialPayCheckInvoice(String rawData) throws Exception;
	public LoyaltyBaseResponse SocialPayCancelInvoice(String rawData) throws Exception;
	
}
