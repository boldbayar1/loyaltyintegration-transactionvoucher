package nomin.loyaltyintegration.businesslogic.interfaces;

import java.util.HashMap;

import nomin.core.entity.businessentity.rest.BaseResponse;
import nomin.loyaltyintegration.businessentity.LoyaltyBaseResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.CheckRegisterResponse;
import nomin.loyaltyintegration.businessentity.ebarimt.EbarimtRequest;
import nomin.loyaltyintegration.businessentity.qpay.pos.QPayBillCreateRequest;
import nomin.loyaltyintegration.businessentity.qpay.response.create.QPayBillCreateResponse;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherCheckRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherSmsRequest;
import nomin.loyaltyintegration.businessentity.spectre.request.VoucherTransactionRequest;
import nomin.loyaltyintegration.businessentity.spectre.response.VoucherResponse;
import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.bill.ReturnBillRequest;
import nomin.loyaltyintegration.entity.bill.ReturnBillResponse;
import nomin.loyaltyintegration.entity.card.info.register.CardRegisterRequest;
import nomin.loyaltyintegration.entity.posapi.PutResponse;
import nomin.posreport.web.businessentity.bill.BillReportRequest;
import nomin.posreport.web.businessentity.report.ReportResponse;


public interface IService {
	
	// ebarimt service
//	PutResponse ProcessTransactionMobile(String rawData);
	ReturnBillResponse ProcessReturnTransactionMobile(ReturnBillRequest request);
	PutResponse putEbarimt(EbarimtRequest request, Logger log) throws Exception;
	ReturnBillResponse returnEbarimt(ReturnBillRequest returnBillRequest, Logger log) throws Exception;
	
	// card register service
//	BaseResponse RegisterCard(CardRegisterRequest request, String invoiceId) throws Exception;
	BaseResponse RegisterCard(CardRegisterRequest request, Logger log) throws Exception;

	// Loyalty2 voucher бүртгэх, буцаалт хийх	
	VoucherResponse CheckVoucherLoyalty2(VoucherCheckRequest request, Logger log) throws Exception;
	VoucherResponse PutVoucherLoyalty2(VoucherTransactionRequest request, Logger log) throws Exception;
	VoucherResponse ReturnVoucherLoyalty2(VoucherTransactionRequest request, Logger log) throws Exception;
	VoucherResponse SmsLoyalty2(VoucherSmsRequest request, Logger log) throws Exception;
	
	// mail, sms
	LoyaltyBaseResponse SendEmail(String request, Logger log) throws Exception;
	LoyaltyBaseResponse SendMessage(String request, Logger log) throws Exception;
	String SetMessage(HashMap<?, ?> map) throws Exception;
	String SetMail(HashMap<?, ?> map) throws Exception;
	
	// pos billImage 
	ReportResponse GetBillImage(BillReportRequest request, Logger log) throws Exception;
	
	// chekRegNo
	CheckRegisterResponse CheckRegisterNo(String request) throws Exception;
	
	// Qpay V1
	QPayBillCreateResponse QpayCreate(QPayBillCreateRequest request) throws Exception;
	nomin.loyaltyintegration.businessentity.qpay.data.PaymentInfo QpayCheck(nomin.loyaltyintegration.businessentity.qpay.check.pos.QPayPaymentCheckRequest request) throws Exception;
	nomin.loyaltyintegration.businessentity.qpay.data.QPayResponse QpayCancel(nomin.loyaltyintegration.businessentity.qpay.payment.cancel.QPayPaymentCancelRequest request) throws Exception;
	
	// SocialPay
	nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBody SocialPayCreate(nomin.loyaltyintegration.businessentity.socialpay.bill.create.pos.SocialPayBillCreateRequest request) throws Exception;
	nomin.loyaltyintegration.businessentity.socialpay.payment.check.data.SocialPayPaymentCheckResponseBody SocialPayCheck(nomin.loyaltyintegration.businessentity.socialpay.payment.check.pos.SocialPayPaymentCheckRequest request) throws Exception;
	nomin.loyaltyintegration.businessentity.socialpay.data.SocialPayResponseBody SocialPayCancel(nomin.loyaltyintegration.businessentity.socialpay.payment.cancel.pos.SocialPayPaymentCancelRequest request) throws Exception; 
	
	// Upoint Web Token
//	UpointTokenResponse getUpointToken(UpointTokenCreateRequest upointTokenCreateRequest, Logger log) throws Exception;
//	ProcessTransactionResponseData UpointProcessTransaction(UpointTransactionData UpointTransactionData, String token, Logger log) throws Exception; 
//	UpointConfirmTransactionResponse UpointConfirmTransaction(UpointConfirmTransactionRequest upointConfirmTransactionRequest, Logger log) throws Exception;
//	ProcessTransactionResponseData UpointCancelTransaction(UpointTransactionData UpointTransactionData, Logger log) throws Exception;
	
}
