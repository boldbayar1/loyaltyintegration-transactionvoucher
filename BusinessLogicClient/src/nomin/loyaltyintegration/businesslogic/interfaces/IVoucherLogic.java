package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.entity.Logger;
import nomin.loyaltyintegration.entity.payment.Voucher;
import nomin.loyaltyintegration.entity.payment.VoucherPaymentResponse;

public interface IVoucherLogic {

	// Voucher программ шалгах, бүртгэх, буцаалт хийх
	VoucherPaymentResponse actionVoucher(Voucher voucher, String type, Logger log);
	
}
