package nomin.loyaltyintegration.businesslogic.interfaces;

import nomin.loyaltyintegration.businessentity.qpay.data.PaymentLog;
import nomin.loyaltyintegration.entity.Logger;

public interface ITransactionLog {
	
	void log(Logger log);
	void actionInvoiceBill(String type, String invoiceId, String deviceId, String updateInfo);
	void saveTransaction(PaymentLog paymentLog);
	void actionUpointBillData(String type, Logger logger, String billKey, String updateInfo);
}
